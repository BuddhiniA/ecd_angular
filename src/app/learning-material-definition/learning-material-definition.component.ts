import { Component, OnInit, ViewChild } from '@angular/core';
import { MaterialModel } from "../model/Material";

import { ModalDirective } from 'ngx-bootstrap/modal';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MaterialStock } from '../model/MaterialStock';
import { LoignValidator } from '../login-validator';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
// import { BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-learning-material-definition',
  templateUrl: './learning-material-definition.component.html',
  styleUrls: ['./learning-material-definition.component.css']
})
export class LearningMaterialDefinitionComponent implements OnInit {

  public a: boolean;
  public b: boolean;
  public success: boolean;
  // @ViewChild("makeMtrSetting") public makeMtrSetting: ModalDirective;
  @ViewChild("makeMtrSetting") public makeMtrSetting;
  @ViewChild("makeGroupAdminSetting") public makeGroupAdminSetting;


  public materialModel = Array<MaterialModel>();
  public materialDetailNew = new MaterialModel;
  public materialDetail = new Array<MaterialModel>();
  public mtrForm: FormGroup
  public materialList=new Array<MaterialStock>()
  public selectedMaterial=new MaterialStock();
  constructor(public commonService: CommonWebService, public validator: LoignValidator) {
    this.validation();
  }

  ngOnInit() {
    this.loadToGrid();
  }

  validation() {

    this.mtrForm = new FormGroup({
      materialName: new FormControl(['', Validators.required]),
      status: new FormControl(['', Validators.required]),
      quantity: new FormControl([''])
    })
  }

  // validationEdit(){

  //       this.mtrForm = new FormGroup({
  //         materialName:new FormControl(['',Validators.required]),
  //         status:new FormControl(['',Validators.required]),
  //         quantity:new FormControl([''])
  //       })
  //     }

  openCreateMtr() {
    console.log("create mtr");
    this.makeMtrSetting.show();
  }

  getMtrId(mtr) {
    this.b = true;
    this.a = false;
    console.log("user ", mtr);
    this.materialDetail = mtr;
    this.makeGroupAdminSetting.show()

  }


  getMtrView(mtr) {
    this.b = false;
    this.a = true;
    console.log("user ", mtr);
    this.materialDetail = mtr;
    this.makeGroupAdminSetting.show()
  }

  loadMaterials(){
    this.commonService.processGet(API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/getAllMaterials").subscribe(res=>{
      //this.commonService.processGet("./assets/criteriadetail.json").subscribe(res=>{
     this.materialList=res.responseData
     console.log("ID",this.materialList.length)
    this.selectedMaterial=this.materialList[0];
    
   }),error=>{
       console.log("ERROR")
   }
  }

  saveMtrChanges(evt) {
    console.log("saveMtrChanges");
    let url = API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/updateMaterial";


    this.commonService.processPost(url, this.materialDetail).subscribe(res => {


      let response = res.responseCode
      console.log("response ", response);
      if (response == 1) {
        console.log("success");
        this.success = true;
        console.log("Success")
        this.loadToGrid()

        setTimeout(() => {

          //  this.resetFields()
          this.success = false
        }, 2000)
        // this.successMessage = "User created successfully!"

        // this.isLoading = false;

        // this.resetFields(this.newUserModel);

        // this.successMessage = "User has been created successfully. Password will be sent to the email address";
        // this.success.show();
      } else {
        console.log("NOT success");
      }

      // this.createUserLoader = false;

      this.loadToGrid();
    }, error => {

      console.log("Error");

    })

  }

  loadToGrid() {
    // console.log("User Id ",this.validator.getUserId()); 
    let url = API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/getAllMaterials";
    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.responseCode == 1) {
      
        this.materialModel = res.responseData;
        // this.materialModel. =  this.selectedMaterial.quantity  
        console.log("response data ",this.materialModel);
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.materialModel)
    })
  }

  createMtr() {

    // this.createUserLoader = true;
    console.log("save createMtr")
    let url = API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/saveMaterials";


    this.commonService.processPost(url, this.materialDetailNew).subscribe(res => {


      let response = res.responseData
      console.log("response ", response);
      if (response.responseCode == 1) {

        // this.isSuccess = true;
        // this.successMessage = "User created successfully!"

        // this.isLoading = false;

        // this.resetFields(this.newUserModel);

        // this.successMessage = "User has been created successfully. Password will be sent to the email address";
        // this.success.show();
      } else if (response.flag == 808) {
        // this.failMessage = response.exceptionMessages[0];
        // this.fail.show();
      } else {
        // this.failMessage = "Cannot create the user at this time"
        // this.fail.show();
      }

      // this.createUserLoader = false;

      this.loadToGrid();
    }, error => {

      // this.failMessage = "Error in connection. Try again later!"
      // this.fail.show();

    })
    //
  }

}
