import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LearningMaterialDefinitionComponent } from './learning-material-definition.component';

describe('LearningMaterialDefinitionComponent', () => {
  let component: LearningMaterialDefinitionComponent;
  let fixture: ComponentFixture<LearningMaterialDefinitionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LearningMaterialDefinitionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LearningMaterialDefinitionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
