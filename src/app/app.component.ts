import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  public loginVisible: boolean

  constructor(public _router: Router) {
    console.log(this._router.url)
 

  }

  getCurrentURL():any {


    

    if ((this._router.url == '/login') || (this._router.url == '/')) {
      this.loginVisible = true
    } else {
      this.loginVisible = false
    }
    return this.loginVisible
  }
}