import { Component, OnInit, Input, SimpleChanges, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { ModalDirective } from 'ngx-bootstrap';

export class ActivityDashBoard {

  activityId
  type
  activityDescription
  longitude
  latitude
  status
  learningCenter
  address
  allMilestones
  completedMilestones

}



declare var google;
@Component({
  selector: 'app-map-component',
  templateUrl: './map-component.component.html',
  styleUrls: ['./map-component.component.css'],
  providers: [CommonWebService]
})
export class MapComponentComponent implements OnInit {
  @ViewChild("details") public details: ModalDirective;
  @Input('selectedLocation') selectedLocation: any;
  options: any;
  public activityList = new Array<ActivityDashBoard>()
  public selectedActivity = new ActivityDashBoard()
  public progressVal;
  // map: google.maps.Map;
  // viewTypes: SelectItem[];
  // selectedType: string;
  overlays = new Array<any>();
  constructor(public route: ActivatedRoute, public commonService: CommonWebService,public router:Router) {
    // this.viewTypes = [];
    // this.viewTypes.push({label:'Map View', value:null});
    // this.viewTypes.push({label:'List View', value:{id:1, name: 'New York', code: 'NY'}});


  }

  ngOnChanges(changes: SimpleChanges) {


    console.log("Sss", this.selectedLocation)
    this.getActivityLocations(this.selectedLocation)

  }

  ngOnInit() {
    this.options = {
      center: { lat:  7.8731, lng: 80.7718 },
      zoom: 7
    };

   

  }
  handleOverlayClick(q){
    console.log("Overlay CLicked",q)
    this.selectedActivity = q.overlay.obj
    this.progressVal = q.overlay.obj.completedMilestones/q.overlay.obj.allMilestones
    this.details.show()
  }
  getActivityLocations(a) {
    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getCensusDataForMaps?locCode=" + a.locLevel).subscribe(res => {
    
      if (res.responseCode == 1) {

        this.activityList = res.responseData
        for (let b of this.activityList) {
          this.overlays.push(new google.maps.Marker(
            {
              obj:b,
              position:
              { lat:b.latitude, lng: b.longitude },
              title: b.activityDescription
            }))

        }

      }
      console.log("OVER",this.overlays)
     
    }), error => {
      console.log("ERROR")
    }
  }
  ganttView(){
    
    this.router.navigate(['gantt-view',{activity:this.selectedActivity.activityId}])
  }
}

