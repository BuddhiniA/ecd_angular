import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { TreeNode } from "primeng/primeng";

@Injectable()
export class NodeService {
    
    constructor(private http: Http) {}

    getFiles() {
        return this.http.get('assets/files.json')
                    .toPromise()
                    .then(res => <TreeNode[]> res.json().data);
    }
}