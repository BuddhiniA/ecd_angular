import { Component, OnInit,ViewChild } from '@angular/core';

import { ParentalParticipantModel } from '../model/ParentalParticipant';
import { ActivatedRoute } from '@angular/router';
import { ParticipntModel } from '../model/Participnt';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
@Component({
  selector: 'app-orientation-participant',
  templateUrl: './orientation-participant.component.html',
  styleUrls: ['./orientation-participant.component.css']
})
export class OrientationParticipantComponent implements OnInit {

  public parentalPtList = Array<ParentalParticipantModel>();
  public parentalPtModel = new ParentalParticipantModel;
  public PtList = Array<ParticipntModel>();
  public PtListModel = new ParticipntModel;
  public programId;
  
  @ViewChild("makeParticipant") public makeParticipant;
  @ViewChild("editParticipant") public editParticipant;
  constructor(public commonService: CommonWebService,public route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.programId=params['programId']
      if(this.programId!=null||this.programId!=undefined){
            this.loadToGrid();
           }
      console.log("CID",this.programId)
    })
  }

  loadToGrid(){
    console.log("loadToGrid",this.programId)
        let url = API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getOrientationParticipant?programId="+this.programId;
        
        this.commonService.processGet(url).subscribe(res => {
    
          
          if (res.responseCode == 1) {
            
            this.parentalPtList = res.responseData;
            console.log("response data ", this.parentalPtList)
          }
    
        }, error => {
    
          console.log("error ", error);
          console.log("response dataz ", this.parentalPtList)
        })
      }
  
  getPtId(participant){
    console.log("participant",participant);
    this.parentalPtModel = participant;
    this.editParticipant.show();
  }

  openCreatePt(){
    console.log("openCreatePt");
    this.makeParticipant.show();
  }
  // openEditOrPt(){
  //   console.log("editParticipant");
  //   this.editParticipant.show();
  // }
  createOrPt(){
    let programId = parseInt(this.programId)
    this.parentalPtModel.programId =programId
    // this.parentalPtModel.participantId =100;
    // this.parentalPtModel.learningCenterName = this.selectedLearningCenter.name
    // this.parentalPtModel.learningCenter = this.selectedLearningCenter.sn
    // this.parentalPtModel.district = this.parentalPtModel.address
    // this.parentalPtModel.noOfChildren = 20
    // this.parentalPtModel.status = 2
    // this.parentalPtModel.gender=this.gender==1?this.parentalPtModel.gender="Male":this.parentalPtModel.gender="Female"
    // this.parentalPtModel.designation = "aaa"
    // this.parentalPtModel.designation = "aaa"
    console.log("programId",this.parentalPtModel.programId);
    console.log("learning center ",this.parentalPtModel.learningCenterName);

    console.log("TO BE SAVED",this.parentalPtModel)
    this.commonService.processPost(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/addNewOrientationParticipant",this.parentalPtModel).subscribe(res=>{
      
      
       
        if(res.responseCode==1){
          // this.success=true;
          this.loadToGrid()
        
          setTimeout(()=>{
           
            // this.resetFields()
            // this.success=false
          },2000)
        }
  
      }),error=>{
          console.log("ERROR")
     }
  }

  editOrPt(){
    console.log("editOrPt");
    //NO SERVICE 
    this.commonService.processPost(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/updateHeadOrientParticipant", this.PtListModel).subscribe(res => {
      
            console.log("Sucess?", res.responseCode)
            if (res.responseCode == 1) {
              this.editParticipant.hide();
              this.loadToGrid();
              setTimeout(()=>{
                
                //  this.resetFields()
                //  this.success=false
               },5000)
            }
      
          }), error => {
            console.log("ERROR")
          }
  }
}
