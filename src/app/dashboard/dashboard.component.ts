import { Component, OnInit, SimpleChanges } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { DashboardPlanComponent } from '../dashboard-plan/dashboard-plan.component';

import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { AnnualPlan } from '../model/AnnualPlan';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [CommonWebService]

})
export class DashboardComponent implements OnInit {

  years: SelectItem[];

  data: any
  options: any;
  public detailAray = new AnnualPlan()

  public selectedYear
  public selectedMonth
  constructor(public commonService: CommonWebService) {

  }

  ngOnInit() {

    this.loadDates(2017,2025)
    this.loadDetails(this.selectedYear.id, 1)

  }
  showContent(month) {

  }

  changeAction() {

    this.loadDetails(this.selectedYear.id, this.selectedMonth)
  }
  handleChange(e) {
    var index = e.index;
    this.selectedMonth = index+1
    console.log("MONTH",this.selectedMonth)
    this.loadDetails(this.selectedYear.id, this.selectedMonth)

  }
  loadDetails(year, month) {

    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getAnnualPlan?year=" + year + "&month=" + month).subscribe(res => {

      if (res.responseCode == 1) {
        this.detailAray = res.responseData

        if (this.detailAray != null && this.detailAray != undefined) {
          this.loadTable()
        }

      }

    }), error => {
     
    }
  }
  loadTable() {
   

    this.data = {
      labels: ['Long Term', 'Short Term', 'Head Orientation', 'Admin Staff', 'Parental Awareness', 'Period pre school', 'Staff Capacty'],
      datasets: [
        {
          label: 'Training Program Data based on'+this.selectedYear.id+' '+this.selectedMonth,
          backgroundColor: '#F79423',
          borderColor: '#1E88E5',
          data: [this.detailAray.longTerm, this.detailAray.shortTerm,
          this.detailAray.headOrientation, this.detailAray.adminStaff,
          this.detailAray.parentalAwareness, this.detailAray.periodPreSchool,
          this.detailAray.staffCapacity]
        }
      ]
    }
    this.options = {

      //see http://www.chartjs.org/docs/#bar-chart-barpercentage-vs-categorypercentage
      scales: {


        xAxes: [{
          stacked: false,
          categoryPercentage: 0.5, //宽度设置属性
          barPercentage: 0.5 //宽度设置属性
        }],
        yAxes: [{
          stacked: false
        }]
      },

      legend: {
        position: 'top',
        labels: {
          fontColor: 'rgb(255, 99, 132)'
        }
      }
    };
  }
  loadDates(startYear, endYear) {
    this.years = []
    for (let i = startYear; i < endYear + 1; i++) {
      this.years.push({ label: i, value: { id: i, name: i, code: 'i_' } })
    }
    this.selectedYear = this.years[0].value

  }

}
