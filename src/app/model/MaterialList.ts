
export class MaterialListModel {

    packId: any = null;
    materialId: any = null;
    materialName: any = null;
    quantity: any = null;
    status: any = null;
    availableQuantity: any = null;
}