export class ActivityData{
    activityId
    type
    activityDescription
    learningCenterId
    learningCenter
    categoryId
    category
    status
}