
export class InstitutesModel{

    instituteId: any = null;
    name: any = null;
    address: any = null;
    contactPerson: any = null;
    contactNo: any = null;
    status: any = null;
    userInserted: any = null;
    userModified: any = null;
    dateInserted: any = null;
    dateModified: any = null;
    
}