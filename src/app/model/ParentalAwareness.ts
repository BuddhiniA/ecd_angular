import { MasterTrainerListModel } from "./MasterTrainerList";
import { GuestListModel } from "./GuestList";

export class ParentalAwarenessModel{

    programId:number;
    programName: any = null;    
    description: String = "";
    startDate: any = null;
    endDate: any = null;
    locationId: any = null;
    locationName: any = null;
    budget: any = 0;
    actualBudget: any = null;
    filePath: any = null;
    status: any = null;
    // masterTrainerList = new MasterTrainerListModel()
    // guestList = new GuestListModel()
}