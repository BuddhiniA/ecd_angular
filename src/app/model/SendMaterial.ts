import { PackList } from "./PackList";

export class SendList {
    sendId
    description
    packList = new Array<PackList>()
    criteriaId
    learningCenterId
    learningCenter
    officerName
    officer
    status
    dateInserted
    dateModified
    userInserted
    userModified
}