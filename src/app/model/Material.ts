
export class MaterialModel{

    materialId: any = null;
    materialName: any = null;
    status: any = null;
    quantity: any = null;
    userInserted: any = null;
    userModified: any = null;
    dateInserted: any = null;
    dateModified: any = null;
}