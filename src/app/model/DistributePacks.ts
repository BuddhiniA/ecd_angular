
export class DistributeModel{

    sentId:any = null;
    description:any = null;
    learningCenterId:any = null;
    learningCenter:any = null;
    status:any = null;
    packId:any = null;
    quantity:any = null;
    receivedQuantity:any = null;
    receivedDate:any = null;
}