export class Program {
    public programId = null;
    public programName = null;
    public description = null;
    public programArea = null;
    public startDate = null;
    public endDate = null;
    public locationId = null;
    public locationName = null;
    public venue = null;
    public budget = null;
    public actualBudget = null;
    public filePath = null;
    public status = null;
    public type = null;
    public certificateId = null;
    public certificateName = null;
    public instituteId = null;
    public instituteName = null;
    public awardType = null
    public medium = null

}