export class RegParticipantModel{
    id:any  = null;
    nic:any = null;
    name:any  = null;
    address:any  = null;
    telephone:any = null;
    gender:any  = null;
    childrenCount:any = null;
    district:any  = null;
    designation:any = null;
    workPlace:any  = null;
    participatedStatus:any = null;
    presentedStatus:any = null;
    interviewedStatus:any = null;
    completedStatus:any = null;
    qualifiedStatus:any = null;
    docUpload:any = null;
}