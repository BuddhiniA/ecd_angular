export class OrientationPartModel{

    participantId: any = null;
    programId: any = null;
    nic: any = null;
    name: any = null;
    district: any = null;
    address: any = null;
    telephoneNo: any = null;
    gender: any = null;
    learningCenter: any = null;
    learningCenterName: any = null;
    noOfChildren: any = null;
    status: any = null;
    designation: any = null;
}