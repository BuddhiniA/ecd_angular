export class ECDOfficer {
    userId
    fullName
    userType
    designation
    emailAddress
    telephoneNo
    status
    userImage
}