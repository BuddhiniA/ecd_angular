
export class ActivityCategoryModel{

    categoryId:any = null;
    categoryName:any = null;
    totalBudget:any = null;
    budgetFields:any = null;
    milestoneList:any = null;
    status:any = null;
    userInserted:any = null;
    userModified:any = null;
    dateInserted:any = null;
    dateModified:any = null;    
}