import { FeeWaiver } from "./FeeWaiver";
import { FeeWaiverStudent } from "./FeeWaiverStudent";
import { FeeWaiverPayments } from "./FeeWaiverPayments";

export class FeeWaiverPaymentData{
    feeWaiver = new FeeWaiver()
    student = new FeeWaiverStudent();
    payment = new FeeWaiverPayments();
}