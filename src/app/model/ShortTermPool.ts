
import { MasterTrainerListModel } from "./MasterTrainerList";
import { GuestListModel } from "./GuestList";

export class ShortTermPoolModel{

    personId: any = null;
    personName: any = null;
    remarks: any = null;
    status: any = null;
    coordinator: any = null;
    masterTrainerList = new MasterTrainerListModel();
    guestList = new GuestListModel()
}