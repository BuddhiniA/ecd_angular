
export class GuestListModel{

    personId: any = null;
    personName: any = null;
    remarks: any = null;
    status: any = null;
    coordinator: any = null;
}