import { Criteria } from "./Criteria";
import { SubCriteria } from "./SubCriteria";

export class CriteriaInfo {
    criteria:Criteria=new Criteria();
    subCriteria=new Array<SubCriteria>()
}