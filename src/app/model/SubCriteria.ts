export class SubCriteria{
    seqNo
    censusField
    censusFieldDes
    operator
    value
    points
    function=null
    status
    operatorsList
}