export class Node {
    id
    label
    type
    code
    lifeCode
    districtId
    dsID
    provinceId
    nodeType
    leaf
    children = new Array<Node>()
  }