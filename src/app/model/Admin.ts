export class AdminModel{

    programId: any = null;
    programName: any = null;
    description: any = null;
    programArea: any = null;
    startDate: any = null;
    endDate: any = null;
    locationId: any = null;
    locationName: any = null;
    budget: any = null;
    actualBudget: any = null;
    filePath: any = null;
    status: any = null;
    
}