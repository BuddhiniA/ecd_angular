export class TeacherProgram{
    programId
    programName
    description
    startDate
    endDate
    locationId
    locationName
    budget
    actualBudget
    filePath
    status
}