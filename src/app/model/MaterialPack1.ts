export class MaterialPackFinal{
    packId
    packName
    materialCount
    availablePackCount
    status
    userInserted
    userModified
    dateInserted
    dateModified
    materialData
}