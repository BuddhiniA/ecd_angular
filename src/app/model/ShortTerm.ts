
import { ShortTermPoolModel } from "./ShortTermPool";

export class ShortTermModel{

    programId: any = null;
    description: any = null;
    startDate: any = null;
    endDate: any = null;
    locationId: any = null;
    locationName: any = null;
    budget: any = null;
    status: any = null;
    userInserted: any = null;
    userModified: any = null;
    dateInserted: any = null;
    dateModified: any = null;
    resourcePerson = new ShortTermPoolModel();
    
}