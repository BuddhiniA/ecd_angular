import { MaterialStock } from "./MaterialStock";

export class PackList {
    sendId
    packId
    materialList = new Array<MaterialStock>();
    quantity
    status
}