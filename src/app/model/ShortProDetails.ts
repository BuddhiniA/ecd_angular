
export class ShortProDetailsModel{

    programId: any = null;
    programName: any = null;
    description: any = null;
    startDate: any = null;
    endDate: any = null;
    locationId: any = null;
    locationName: any = null;
    budget: any = null;
    status: any = null;
}