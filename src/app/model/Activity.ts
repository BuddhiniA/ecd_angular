import { ActivityData } from "./ActivityData";
import { Milestone } from "./Milestone";
import { Budget } from "./Budget";

export class Activity {
    activityData = new ActivityData()
    milestoneList= new Array<Milestone>()
    fieldList = new Array<Budget>()
    userModified
    userInserted
    dateInserted
    dateModified
}