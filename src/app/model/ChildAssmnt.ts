export class ChildAssmntModel{
    assessmentId:any = null
    assessDate:any = null
    description:any = null
    nameOfAssessor:any = null
    learningCenter:any = null
    learningCenterName:string=null
    noOfStudents:any = null
    noOfAssessedStudents:any = null
    status:any = null
    userInserted:any = null
    userModified:any = null
    dateInserted:any = null
    dateModified:any = null
}