export class FeeWaiverGrid {

    studentId:String = "";
    criteriaId:number = 0;
    startYear:number = 0;
    startMonth:number = 0;
    learningCenter:number = 0;
    studentName:String = "";
    status:number = 0;
    month1:number = 0;
    month2:number = 0;
    month3:number = 0;
    month4:number = 0;
    month5:number = 0;
    month6:number = 0;
    month7:number = 0;
    month8:number = 0;
    month9:number = 0;
    month10:number = 0;
    month11:number = 0;
    month12:number = 0;
    p1:any = 0.0;
    p2:any = 0.0;
    p3:any = 0.0;
    p4:any = 0.0;
    p5:any = 0.0;
    p6:any = 0.0;
    p7:any = 0.0;
    p8:any = 0.0;
    p9:any = 0.0;
    p10:any = 0.0;
    p11:any = 0.0;
    p12:any = 0.0;

}