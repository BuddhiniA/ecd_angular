export class Comment {

    id;
    activityId;
    milestoneId;
    date;
    comment;
    dateInserted;
    userInserted;
}