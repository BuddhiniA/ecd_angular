export class MaintainDocument{
    activityId;
    milestoneId;
    documentId;
    documentName;
    status;
    availability;
    description;
}