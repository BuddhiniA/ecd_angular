export class GRNDetails{
    grnId
    grnDate
    materialId
    materialName
    quantity
    dateInserted
    dateModified
    userEntered
    userModified
}