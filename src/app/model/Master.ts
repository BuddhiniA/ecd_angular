import { RegParticipantModel } from "./RegParticipant";
import { ResPoolModel } from "./ResPool";
// import { ResourcePoolModel } from "./ResourcePool";

export class MasterModel {

    masterId: any = null;
    masterName: any = null;
    description: any = null;
    startDate: any = null;
    endDate: any = null;
    budgetedValue: any = null;
    status: any = null;
    location: any = null;
    // registerData = new RegParticipantModel();
    // poolData = new ResourcePoolModel();
    // poolData = new ResPoolModel();
    poolId = new Array<number>()
    dateInserted;
    dateModified;
    userInserted;
    userModified
}