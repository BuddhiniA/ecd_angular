export class Claim {
    activityId;
    amount;
    description;
    claimedDate;
    dateInserted;
    userInserted;
    dateModified;
    userModdified;
}