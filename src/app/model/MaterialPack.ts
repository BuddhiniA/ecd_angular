import { MaterialPackDetail } from "./MaterialPackDetail";

export class MaterialPack {
    packId
    packName
    materialCount
    status
    quantity
    userInserted
    userModified    
    dateInserted
    dateModified
    materialData = new Array<MaterialPackDetail>()
    
    
}