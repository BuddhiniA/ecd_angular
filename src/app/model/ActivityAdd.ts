import { BudgetFieldsModel } from "./BudgetFields";
import { MilestoneAll } from "./MilestoneALL";
import { Documents } from "./document";


export class ActivityAddModel{

    categoryId: any = null;
    categoryName: any = null;
    totalBudget: any = null;

    budgetFields = new Array<BudgetFieldsModel>()

    milestoneList = new Array<MilestoneAll>()
    documentList = new Array<Documents>()

    status: any = null;
    userInserted: any = null;
    userModified: any = null;
    dateInserted: any = null;
    dateModified: any = null;
}