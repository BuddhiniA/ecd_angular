
export class AdminPtModel{
    participantId
    programId
    nic
    name
    district
    address
    telephoneNo
    gender
    learningCenter
    learningCenterName
    noOfChildren
    status
    designation
}