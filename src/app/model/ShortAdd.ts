import { ResourcePerson } from "./ResourcePerson";
// import { PrimarySchoolModel } from "./PrimarySchool";
import { ShortProDetailsModel } from "./ShortProDetails";


export class ShortAddModel{

    userInserted: any = " ";
    userModified: any = null;
    dateInserted: any = null;
    dateModified: any = null;
    resourcePool = new Array<ResourcePerson>()
    masterTrainerList = new Array<ResourcePerson>()
    guestList = new Array<ResourcePerson>()
    // primarySchool = new PrimarySchoolModel()
    programDetails = new ShortProDetailsModel()
}