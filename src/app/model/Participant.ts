
export class Participant {

    programId;
    participantId;
    nic;
    name;
    address;
    telephoneNo;
    gender;
    learningCenter;
    learningCenterName;
    noOfChidren;
    organization;
    designation;
    path;
    status;
    userInserted;
    dateInserted;
    userMOdified;
   dateModified;
}