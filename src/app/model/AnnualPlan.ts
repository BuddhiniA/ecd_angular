export class AnnualPlan {
  year
  month
  shortTerm
  longTerm
  periodPreSchool
  staffCapacity
  parentalAwareness
  headOrientation
  pmu
  phdt
  adminStaff
  userInserted
  userModified: string = "";
  dateInserted: any = null;
  dateModified: any = null;
  diplomaPhdt;
  refresherTraining;
  parentalPhdt;
  fig;
  ecdCenter;
  ecdResource;
  feeWaiver;
  learningMaterials;
  childAss;
  census;
  socialAudit;
  mis;
  ecdCenterReg;
   newCdc;
   renCdc;
   newPlay;
   renPlay;
}