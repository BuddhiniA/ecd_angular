export class MaterialPacksModel {

    packId: any = null;
    packName: any = null;
    materialCount: any = null;
    availablePackCount: any = null;
    status: any = null;
    userInserted: any = null;
    userModified: any = null;
    dateInserted: any = null;
    dateModified: any = null;
    materialData: any = null;
}