import { Program } from "./Program";
import { ResourcePerson } from "./ResourcePerson";

export class ProgramData {
    public program = new Program();
    resourcePool = new Array<ResourcePerson>();
    guestList = new Array<ResourcePerson>();
    userInserted;
    userModified;
    dateInserted;
    dateModified;
}