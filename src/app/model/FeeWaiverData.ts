import { FeeWaiver } from "./FeeWaiver";
import { FeeWaiverStudent } from "./FeeWaiverStudent";

export class FeeWaiverData{
    feeWaiver = new FeeWaiver()
    student = new FeeWaiverStudent()
}