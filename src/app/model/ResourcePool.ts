import { MasterTrainerListModel } from "./MasterTrainerList";
import { ResourcePerson } from "./ResourcePerson";
import { ParentalAwarenessModel } from "./ParentalAwareness";

export class ResourcePoolModel{

    
   
    resourcePool = new Array<ResourcePerson>();
    masterTrainerList = new Array<ResourcePerson>();
    guestList = new Array<ResourcePerson>();
    userInserted: any = " ";
    userModified: any = null;
    dateInserted: any = null;
    dateModified: any = null;
    parentalAwareness = new ParentalAwarenessModel()
}

