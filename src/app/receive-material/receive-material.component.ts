import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { CommonWebService } from '../common-web.service';
import { SendList } from '../model/SendMaterial';
import { MaterialPackFinal } from '../model/MaterialPack1';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { API_URL } from '../app_params';
import { LoggedInUserModel } from '../model/loggedInUserModel';


export class Received {

  sendId
  packId
  receivedStatus
  receivedQuantity
  receivedDate

}

export class ReceivedCurrent {

  availablePackCount
  dateInserted
  dateModified
  materialCount
  materialData
  packId
  packName
  quantity
  status
  userInserted
  userModified
  currentQty


}

@Component({
  selector: 'app-receive-material',
  templateUrl: './receive-material.component.html',
  styleUrls: ['./receive-material.component.css'],
  providers: [CommonWebService,LoggedInUserModel]
})
export class ReceiveMaterialComponent implements OnInit {
  @ViewChild("receiveList") public receiveList: ModalDirective;
  public listOfreceievedLists = new Array<SendList>()
  public receievedList = new SendList()

  public fianlPackList = new MaterialPackFinal()
  public pList

  public success: boolean
  public error: boolean;
  public warning: boolean

  public quantity
  public receivedListtoBeSaved = new Array<Received>()
  public fullyReceived: boolean
  public materialListforSHow = new Array<any>()
  // public index = 0
  public receiveForm: FormGroup
  public sendId
  

  constructor(public commonService: CommonWebService,public userData: LoggedInUserModel) { }

  ngOnInit() {

    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.loadAllReceievdList();
    this.validation()
  }
  validation() {
    this.receiveForm = new FormGroup({
      // quantity0: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*[1-9][0-9]*$")]),
      description: new FormControl(''),
      learningcenter: new FormControl('')
    })
  }

  openReceivedList(a) {

    this.pList = new Array<ReceivedCurrent>()
    let index=0
    let pListAli = new Array<any>()
    this.receievedList = a
    this.sendId = a.sendId
    console.log("Sen Id",this.sendId)
    this.commonService.processGet(API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/getAllRecievedMaterialPacks?sendId=" + this.receievedList.sendId).subscribe(res => {
      //this.commonService.processGet("./assets/criterialist.json").subscribe(res=>{
      pListAli = res.responseData
      for (let a of pListAli) {
        let maxQty
        let receiveCurrent = new ReceivedCurrent()
        receiveCurrent.availablePackCount = a.availablePackCount
        receiveCurrent.quantity = a.quantity
        receiveCurrent.packId = a.packId
        receiveCurrent.packName = a.packName
        receiveCurrent.currentQty = a.quantity - a.availablePackCount
        maxQty = a.quantity - a.availablePackCount
        console.log("MAX QTY",maxQty)
        this.pList.push(receiveCurrent)
        // this.receiveForm.addControl('quantity' + index, new FormControl('', [Validators.required, Validators.pattern("^[0-9]*[1-9][0-9]*$"), Validators.max(maxQty)]))
        index++
       
      }
      console.log("receive list",this.pList)
      this.receiveList.show()

    }), error => {
      console.log("ERROR")
    }
  }

  showMaterials(cri) {

    this.commonService.processGet(API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/getMaterialsByPackId?packId=" + cri.packId).subscribe(res => {
      //this.commonService.processGet("./assets/criterialist.json").subscribe(res=>{
      this.materialListforSHow = res.responseData
      console.log("MAterial Pack", this.materialListforSHow)
    }), error => {
      console.log("ERROR")
    }

  }

  abc(e){
    console.log("Input event triggered!")
  }


  addQuantity(a) {
    console.log("INPUT", a)
    let receiveForUpdated = new Received()
    receiveForUpdated.sendId = this.receievedList.sendId
    if (this.quantity != null) {

      var index = this.pList.indexOf(a)

      console.log("INDEZ AND MODEL", this.receivedListtoBeSaved.includes(a))



      if (this.pList[index] != null && !this.checkExists(a.packId)) {


        receiveForUpdated.packId = this.pList[index].packId
        // let date = new Date().getDate()

        // receiveForUpdated.receivedDate = new Date()

        receiveForUpdated.receivedQuantity = parseInt(this.quantity)

        if (this.quantity == this.pList[0].availablePackCount) {
          receiveForUpdated.receivedStatus = 1
          console.log("Equal")
        } else if (this.quantity < this.pList[0].availablePackCount) {
          receiveForUpdated.receivedStatus = 2
          console.log("Less")
        } else if (this.quantity > this.pList[0].availablePackCount) {
          receiveForUpdated.receivedStatus = 0
          console.log("High")
        }

        this.receivedListtoBeSaved.push(receiveForUpdated)

      } else {
        if (this.receivedListtoBeSaved != null || this.receivedListtoBeSaved != undefined) {

          for (let b of this.receivedListtoBeSaved) {
            if (a.packId == b.packId) {
              var indes = this.receivedListtoBeSaved.indexOf(b)
              this.receivedListtoBeSaved[indes].receivedQuantity = parseInt(this.quantity)
            }
          }

        }
      }


    } else {
      this.quantity = 0
    }

    console.log("QUANTITY", this.receivedListtoBeSaved)
  }

  checkExists(id): any {
    if (this.receivedListtoBeSaved != null) {
      for (let a of this.receivedListtoBeSaved) {

        if (a.packId == id) {
          return true
        } else {
          return false
        }
      }
    } else {
      return false
    }
  }

  loadAllReceievdList() {
    this.commonService.processGet(API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/getAllSendMaterials?userId="+ this.userData[0].userId).subscribe(res => {
      //this.commonService.processGet("./assets/criterialist.json").subscribe(res=>{
      this.listOfreceievedLists = res.responseData
      console.log("This time", this.listOfreceievedLists)
    }), error => {
      console.log("ERROR")
    }
  }

  updateReceieved() {

    let receivedUpdateList = new Array<Received>()
    console.log("RECEIEVD LIST FOR SAVED", this.receivedListtoBeSaved)
   

    for (let a of this.pList) {
      let receievdForSaved = new Received()
      receievdForSaved.packId = a.packId
      receievdForSaved.receivedDate = 20171203
      receievdForSaved.receivedQuantity = parseInt(a.currentQty)
      if (a.quantity == a.availablePackCount) {
        console.log("Equal")
        receievdForSaved.receivedStatus = 1
      } else if(a.quantity < a.availablePackCount){
        receievdForSaved.receivedStatus = 2//Partially
        console.log("Les")
      }else {
        receievdForSaved.receivedStatus = 0
        console.log("High")
      }
      receievdForSaved.sendId = this.sendId
      console.log("RECEIEVD FOR SAVED", receievdForSaved)
      receivedUpdateList.push(receievdForSaved)
    }

    console.log("RECEIEVED LIST",receivedUpdateList)
    this.commonService.processPost(API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/saveReceivedStatus", receivedUpdateList).subscribe(res => {
      
      
              if (res.responseCode == 1) {
                this.success=true
                let sentAll:boolean;
                for(let a of receivedUpdateList){
                    if(a.receivedStatus==1){
                      //updateSend
                    }
                }


                setTimeout(() => {
                  this.success = false;
                
      
                }, 2000);
      
      
              }
      
            }), error => {
              console.log("ERROR")
            }

  }

 

}
