import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { MaterialStock } from '../model/MaterialStock';
import { MaterialPack } from '../model/MaterialPack';
import { MaterialPackDetail } from '../model/MaterialPackDetail';
import { API_URL } from '../app_params';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

export class Pack {
  materialType
  quantity
  availableQuantity
  packNumber
}

@Component({
  selector: 'app-material-pack',
  templateUrl: './material-pack.component.html',
  styleUrls: ['./material-pack.component.css'],
  providers: [CommonWebService, MaterialStock, MaterialPack, MaterialPackDetail]
})
export class MaterialPackComponent implements OnInit {
  @ViewChild("addPack") public addPack: ModalDirective;

  public material = new MaterialStock()
  public success: boolean
  public selectedMaterial = new MaterialStock();
  public materialList = new Array<MaterialStock>()
  public materialPack = new MaterialPack()
  public packList = new Array<MaterialPackDetail>()
  public packDetail = new MaterialPackDetail()
  public selectedGlobal = new MaterialStock()
  public randomArr = new Array<Pack>()
  public availableQty = 0;
  materialPackForm: FormGroup;
  public materialPackList = new Array<MaterialPack>()
  public statusList = new Array<any>()

  constructor(public commonService: CommonWebService) {
    this.statusList = [

      { label: 'Active', value: { id: 1, name: 'Istanbul', code: 'IST' } },
      { label: 'Inactive', value: { id: 2, name: 'Paris', code: 'PRS' } }
    ];
    this.validation()
  }

  ngOnInit() {
    this.validation()
    this.loadMaterials()
    this.loadAllMaterialPacks()
  }

  updateStatus(a,b){
    console.log("A",b)
    var index = this.materialPackList.indexOf(a)
    if(a.id==1){
      this.materialPackList[index].status=this.statusList[0]
    }else  if(a.id==1){
      this.materialPackList[index].status=this.statusList[1]
    }
  }
  loadAllMaterialPacks() {

    this.commonService.processGet(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllMaterialPacks").subscribe(res => {

      this.materialPackList = res.responseData
      for(let a of this.materialPackList){
        var index = this.materialPackList.indexOf(a)
        let st = a.status
        if(st==1){
          this.materialPackList[index].status=this.statusList[0]
        }else  if(st==0){
          this.materialPackList[index].status=this.statusList[1]
        }
      }
      console.log("RES", this.materialPackList)

    }), error => {
      console.log("ERROR")
    }
  }
  openAddPack() {
    this.addPack.show()
  }
  validation() {
    this.materialPackForm = new FormGroup({
      packname: new FormControl(['', Validators.required]),
      material: new FormControl(),
      quantity: new FormControl(['', Validators.required, Validators.pattern("^[0-9]*[1-9][0-9]*$")]),
      date: new FormControl()

    })
  }
  loadMaterials() {
    this.commonService.processGet(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllMaterials").subscribe(res => {

      this.materialList = res.responseData
      console.log("ID", this.materialList.length)
      this.selectedMaterial = this.materialList[0];
      this.selectedGlobal = this.materialList[0]

    }), error => {
      console.log("ERROR")
    }
  }
  AddToGrid() {

    let packckDetailtoAr = new MaterialPackDetail
    let arr = new Array<any>();
    let packforTable = new Pack()

    let noOfPacks
    packckDetailtoAr.packId = null
    packckDetailtoAr.materialId = this.selectedGlobal.materialId
    packckDetailtoAr.quantity = this.packDetail.quantity
    packckDetailtoAr.availableQuantity = this.selectedMaterial.quantity
    packckDetailtoAr.status = 1

    packforTable.materialType = this.selectedGlobal.materialName
    packforTable.quantity = this.packDetail.quantity
    packforTable.availableQuantity = this.selectedMaterial.quantity


    arr[0] = this.selectedGlobal.materialName
    arr[1] = this.packDetail.quantity
    arr[2] = this.selectedMaterial.quantity
    // console.log("DAta", packckDetailtoAr)
    noOfPacks = arr[2] / arr[1]
    arr[3] = noOfPacks
    packforTable.packNumber = noOfPacks
    this.packList.push(packckDetailtoAr)
    this.randomArr.push(packforTable)

    console.log("PAcklength", this.randomArr[0].packNumber)
    this.calculateAvailableQuantity()
    console.log("ARRAY", this.randomArr)
  }
  onChange(selected) {

    this.selectedGlobal = selected

  }
  saveMaterialPack() {
    console.log("Inside")
    this.materialPack.materialData = this.packList

    console.log("Material PAck", this.materialPack)
    this.commonService.processPost(API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/addMaterialPack", this.materialPack).subscribe(res => {


      if (res.responseCode == 1) {
        this.success = true;
        setTimeout(() => {
          this.success = false;
          this.materialPack.packName = ""
          this.packDetail.quantity = ""
          this.loadAllMaterialPacks()
        }, 5000);
        console.log("Success")
        



      }

    }), error => {
      console.log("ERROR")
    }
  }
  removeRow(a) {
    var index = this.randomArr.indexOf(a);
    this.randomArr.splice(index, 1);
    this.packList.splice(index, 1);

  }
  calculateAvailableQuantity() {

    this.availableQty = this.randomArr[0].packNumber
    for (let i = 0; i < this.randomArr.length; i++) {

      if (this.randomArr[i].packNumber < this.availableQty) {
        this.availableQty = this.randomArr[i].packNumber
        console.log("Available QTY", this.availableQty)
      }
    }
    console.log("Available QTY", this.availableQty)
  }

}
