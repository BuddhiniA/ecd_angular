import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaterialPackComponent } from './material-pack.component';

describe('MaterialPackComponent', () => {
  let component: MaterialPackComponent;
  let fixture: ComponentFixture<MaterialPackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaterialPackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialPackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
