import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Certificate } from '../model/Certificate';
import { CommonWebService } from '../common-web.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LongTermProgram } from '../model/LongTermProgram';
import { TeacherProgram } from '../model/Teacher';
import { ModalData } from '../model/ModalData';
import { API_URL } from '../app_params';
import { ProgramData } from '../model/ProgramData';
import { ResourcePerson } from '../model/ResourcePerson';
import { DatePipe } from '@angular/common';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { TreeNode } from 'primeng/primeng';


export class Institute {

  instituteId
  name
  address
  contactPerson
  contactNo
  status
  userInserted
  userModified
  dateInserted
  dateModified
}
export class Resource {
  id
  resourceName
  institute
  qualification
  status
  userEnterd
  userModified
  dateInserted
  dateModified
}

export class Other {
  personId
  personName
  remarks;
  status
  coordinator
}



export class  PreshoolTeacherProgram {
  resourcePool = new Array<Resource>()
  guestList = new Array<Resource>()
  userInserted
  userModified
  dateInserted
  dateModified
  program = new TeacherProgram()
}

export class Location {
  locationId
  locationName
}
export class BusinessStructure {
  bisId
  bisName
  parentId
}

export class BizModel{
  parentId
  branches
}
@Component({
  selector: 'app-primary-shool-training-program',
  templateUrl: './primary-shool-training-program.component.html',
  styleUrls: ['./primary-shool-training-program.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class PrimaryShoolTrainingProgramComponent implements OnInit {
  userId: any;
  userData: any;
  selectedNode = new Node();
  files: Node[];
  loadingTree: boolean;
  remark: FormControl;
  name: FormControl;
  programForm: FormGroup;
  programData= new ProgramData();

  @ViewChild("viewProgram") public viewProgram: ModalDirective;
  @ViewChild("addParticipantView") public addParticipantView: ModalDirective;
  

  public longtermProgram;
  public teacher;

  public programList = new Array<TeacherProgram>();
  public selectedprogram
  public certificate
  public certificateList = new Array<Certificate>()
  public selectedCertificate = new Certificate()

  public resourceList = new Array<Resource>()
  public selectedResource = new Resource()
  public resourcePool = new Array<Other>()//this is for adding resources only from resource pool
  public guestList = new Array<Other>()//this is for adding gues list
  public  selectedResourcePersonList = new Array<any>() // this is for store all the resources being added

  public institueList = new Array<Institute>()
  public selectedInstitute

  public modelData = new ModalData()
  public viewParam;

  public programId;

  public success
  public error
  public warn
  public longtermForm: FormGroup
  public status

  public rType
  public remarks
  public selectedOtherResource = new Other()

  public businessList = new Array<any>()

  public location = new Array<Location>()
  public objList = new Array<any>()
  public bizModel = new BizModel();

  public max: number
  public array = new Array<any>()
  public imageSavingURL


  constructor(public commonService: CommonWebService, public route: Router, private datePipe: DatePipe) {
    this.validation()
  }

  ngOnInit() {
    
    this.getAllPrograms()
    this.getAllCertificates()
    this.getAllInstitutes()
    this.getAllResources()
    this.getAllLocations()

    this.getUser()
   
    this.imageSavingURL = API_URL+"ECD-TrainingPrograms-1.0/service/trainings/uploadDocs/?type=primary"

  }

  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.userId = this.userData[0].userId
  }
  validation() {

    this.programForm = new FormGroup({
      programName: new FormControl(['', Validators.required]),
      description: new FormControl(['', Validators.required]),
      startDate: new FormControl(['', Validators.email]),
      endDate: new FormControl('', [Validators.required]),
      location: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      type: new FormControl(),
      resource: new FormControl(),

    })

    this.name = new FormControl('', Validators.required),
      this.remark = new FormControl()
  }

  openView(param1, param2) {
    this.selectedprogram = new PreshoolTeacherProgram()
    this.getAllPrograms()
    if (param1 == 'view') {
      this.viewParam = "VIEW"
      this.getAllProgramsbyId(param2)
      this.modelData.title = "View Primary School Tachers Training Program"
      this.selectedprogram = new PreshoolTeacherProgram()
      this.viewProgram.show()
    } else if (param1 == 'edit') {


      this.viewParam = "EDIT"
      this.rType = 1
      this.modelData.title = "Edit Primary School Tachers Training Program"

      this.viewProgram.show()
      this.getAllProgramsbyId(param2)
     
      this.getAllResources()
      console.log("2 SELECTED CERTIFICATE", this.selectedCertificate)
    } else if (param1 == 'new' && param2 == null) {
      this.selectedprogram = new PreshoolTeacherProgram()
      this.selectedprogram.program.programName=""
      this.selectedprogram.program.description=""
      this.getAllResources();
      this.rType = 1;
      this.programData = new ProgramData()
      this.programData.program.status = 1
      this.programData.program.startDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
      this.programData.program.endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
      this.selectedResourcePersonList = new Array<ResourcePerson>();

      this.status = 1
      this.rType = 1
      this.viewParam = "NEW"
      this.modelData.title = "Add New Period Pre School Teacher Training Program"
      console.log("hjgjh",this.viewParam,this.selectedprogram.program.programName)
      this.viewProgram.show()
    }

  }

  getAllCertificates() {


    // this.selectedCertificate = new Certificate()
    this.commonService.processGet(API_URL+"ECD-CertificateManagement-1.0/service/certificate/getCertificate").subscribe(res => {


      this.certificateList = res.responseData

      if (this.selectedCertificate.certificateId == null) {
        this.selectedCertificate = this.certificateList[0]
      }
      console.log("CERTIFICATE", this.certificateList)



    }), error => {
      console.log("ERROR")
    }
  }

  closeAct(){
    this.viewProgram.hide()
    this.getAllPrograms()
  }

  // getAllLocations() {


   
  //   this.commonService.processGet(API_URL+"ECD-UserManagement-1.0/service/user/getBusinessStructure").subscribe(res => {
  //     if (res.responseCode == 1) {
  //       this.businessList = res.responseData
  //       this.seperateBranches()
  //     }
  //     console.log("CERTIFICATE", this.businessList)
  //   }), error => {
  //     console.log("ERROR")
  //   }
  // }

  seperateBranches() {
    this.max = 0
    for (let i = 1; i < this.businessList.length; i++) {
      if (this.businessList[i] == undefined) {
        this.businessList[i] = new BusinessStructure()
      } else {

        if (this.businessList[i].parentId > this.max) {
          this.max = this.businessList[i].parentId
        }
      }
    }
    console.log("MAX", this.max)
   this.assignbranches()
  }

  assignbranches() {

    let a = new BusinessStructure()

    this.objList = new Array<any>()
    for (let i = 0; i < this.businessList.length-1; i++) {
        a=this.businessList[i]
        let locList = new Array<any>()
        this.bizModel = new BizModel
        this.bizModel.parentId = a.bisId
        
      for (let j = i+1; j < this.businessList.length; j++) {
    
       
        if (this.businessList[i].bisId== this.businessList[j].parentId) {

           locList.push(this.businessList[j])        
           this.bizModel.branches = locList
        }
      }  
    this.objList.push(this.bizModel)
    }
   
    console.log("Branches", this.objList)
   // this.array1 = this.objList[0].branches
  }

  expand(param){
    console.log("EXPANDING...")
    let array1 = new Array<any>()
    let limit = 0

      while(limit<param){
        for(let i=0;i<this.objList.length-1;i++){
          if(this.objList[i].parentId==limit+1){
            array1 = this.objList[i].branches
            this.array.push(array1)
          }
          
        }
        limit++
      }


   
    console.log(this.array)
  }
  getAllInstitutes() {


    this.selectedInstitute = new Institute()
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getAllInstitutes").subscribe(res => {


      this.institueList = res.responseData
      this.selectedInstitute = this.institueList[0]



    }), error => {
      console.log("ERROR")
    }
  }
  getAllResources() {

    this.selectedResource = new Resource()
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getAllResources").subscribe(res => {


      this.resourceList = res.responseData
      this.selectedResource = this.resourceList[0]



    }), error => {
      console.log("ERROR")
    }
  }

  getAllPrograms() {


    
    this.selectedprogram = new TeacherProgram()
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getTeacherInteractions").subscribe(res => {

     
      this.programList = res.responseData
      console.log("LIST", this.programList)


    }), error => {
      console.log("ERROR")
    }
  }

  convert(date:String):any{
      let a = new Array<any>() 
      a=date.split(" ")
     
      return a[0]
  }
  getAllProgramsbyId(param2: TeacherProgram) {
    // this.programList = new Array<TeacherProgram>()
    this.selectedprogram = new PreshoolTeacherProgram()

    this.selectedResourcePersonList = new Array<PreshoolTeacherProgram>()
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getTeacherInteractionsById?programId="+param2.programId).subscribe(res => {


      this.selectedprogram = res.responseData
     
      this.selectedprogram.program.startDate = this.selectedprogram.program.startDate==null?"":this.convert(this.selectedprogram.program.startDate)
      this.selectedprogram.program.endDate = this.selectedprogram.program.endDate==null?"":this.convert(this.selectedprogram.program.endDate)
      this.status = this.selectedprogram.program.status


      this.selectedResource = this.selectedprogram.resourcePool

      console.log("Resource POOL",this.selectedprogram.resourcePool)
      console.log("Resource POOL",this.selectedprogram.guestList)

      for(let a  of this.selectedprogram.resourcePool){
        console.log("A",a)
        this.selectedResourcePersonList.push(a)
      }
      for(let a of this.selectedprogram.guestList){
        console.log("B",a)
        this.selectedResourcePersonList.push(a)
      }
      console.log("Program", this.selectedResourcePersonList)


    }), error => {
      console.log("ERROR")
    }
    console.log("SELECTED CERTIFICATE", this.selectedCertificate)

  }

  updateProgram() {

    console.log("CERTIFICATE", this.selectedCertificate)

    let pro = new LongTermProgram()
    pro.programId = this.selectedprogram.program.programId
    pro.certificateId = this.selectedCertificate.certificateId
    pro.resourceId = this.selectedResource.id
    pro.instituteId = this.selectedInstitute.instituteId
    pro.description = this.selectedprogram.description
    pro.programName = this.selectedprogram.programName
    pro.startDate = this.selectedprogram.startDate
    pro.endDate = this.selectedprogram.endDate
    pro.status = this.status

    console.log("PROGRAM", pro)

    this.commonService.processPost(API_URL+"LongTermTrainigProgram-Management-1.0/service/program/updateProgram", pro).subscribe(res => {

      console.log("Data", res.responseCode)

      // console.log(this.criteriaList.length)
      if (res.responseCode == 1) {
        this.success = true;
        console.log("Success")
        setTimeout(() => {
          this.success = false;
          this.selectedprogram.programName = ""
          this.selectedprogram.description = ""
          this.selectedprogram.status = 1
        }, 2000);


        this.getAllPrograms()



      }

    }), error => {
      console.log("ERROR")
    }

  }

  saveProgram() {
    
    

    this.programData.userInserted = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveTeacherInteractions", this.programData).subscribe(res => {
      if (res.responseCode == 1) {
        this.success = true;
        this.getAllPrograms()
        setTimeout(() => {
          this.success = false
          this.viewProgram.hide()
        }, 2000)
      } else {
        this.error = true
        setTimeout(() => {
          this.error = false
        }, 2000)
      }
    }), error => {
      console.log("ERROR")
      this.error = true
      setTimeout(() => {
        this.error = false
      }, 2000)
    }

  }
  selctedCoordinatorforPool(event,param){
    console.log("Here")
    let isChecked = event.target.checked
    var index 
   
     if(isChecked&&this.resourcePool.length!=0){
      index = this.resourcePool.indexOf(param);
      
      this.resourcePool[index].coordinator=1
      console.log("Ressource Pool",this.resourcePool[index].coordinator)
     }else if(!isChecked&&this.resourcePool.length!=0){
      
     index = this.resourcePool.indexOf(param);
     this.resourcePool[index].coordinator=0
    }
  }

  selctedCoordinator(event,param){

    console.log("sdsdasd")
       let isChecked = event.target.checked
      
    
      var index 
      
       if(isChecked&&this.guestList.length!=0){
        index = this.guestList.indexOf(param);
        this.guestList[index].coordinator=1
       }else if(!isChecked&&this.guestList.length!=0){
        index = this.guestList.indexOf(param);
        this.guestList[index].coordinator=0
       }
  }

  openAddParticipant(a) {
    this.programId = a
    this.modelData.title="Program ID:"+this.programId
    console.log("A", this.programId)
    this.addParticipantView.show()
    // this.route.navigate(['registered-participant', { programId: this.programId, programName: "longTerm" }])

  }


  addResourcePersons() {


    if (this.rType == 1 && this.selectedResource != null) {

      let resourc = new Other()
      resourc.personName = this.selectedResource.resourceName
      resourc.personId = this.selectedResource.id
      this.selectedResourcePersonList.push(resourc)
      this.resourcePool.push(resourc)
    }
    else if (this.rType == 2 && this.selectedOtherResource != null) {
      let other = new Other()
      other.personName = this.selectedOtherResource.personName
      other.remarks = this.selectedOtherResource.remarks
      this.selectedResourcePersonList.push(other)
      this.guestList.push(other)
      setTimeout(() => {
        this.success = false;
        this.selectedOtherResource.personName = ""
        this.selectedOtherResource.remarks = ""
      }, 500);
    }

    console.log("SELECTED RESOURCE", this.selectedResourcePersonList)

  }

  getAllLocations() {

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)
      }


    }), error => {
      console.log("ERROR")
    }
  }


  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()

        node1.children.push(node)






      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }


  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;


    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }

  nodeSelected(event) {
    this.programData.program.locationId = event.node.id
    this.programData.program.locationName = event.node.label
    this.collapseAll()
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  removeRows(person) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool.splice(index2, 1)
    }
    if (index3 > -1) {
      this.programData.guestList.splice(index3, 1)
    }



  }

  removeRowswhenEdit(person: ResourcePerson) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool[index2].status = 2
    }
    if (index3 > -1) {
      this.programData.guestList[index3].status = 2
    }



  }

  checked(e, person) {

    console.log("Per", person)
    console.log("sdsdsd", this.programData.resourcePool)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    console.log("index", index2)
    console.log("in", index3)
    if (index2 > -1) {
      this.programData.resourcePool[index2].coordinator = 1
    }
    if (index3 > -1) {

      this.programData.guestList[index3].coordinator = 1
    }
  }



}
