import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryShoolTrainingProgramComponent } from './primary-shool-training-program.component';

describe('PrimaryShoolTrainingProgramComponent', () => {
  let component: PrimaryShoolTrainingProgramComponent;
  let fixture: ComponentFixture<PrimaryShoolTrainingProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryShoolTrainingProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryShoolTrainingProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
