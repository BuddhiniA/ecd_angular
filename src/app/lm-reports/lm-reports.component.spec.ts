import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LmReportsComponent } from './lm-reports.component';

describe('LmReportsComponent', () => {
  let component: LmReportsComponent;
  let fixture: ComponentFixture<LmReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LmReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LmReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
