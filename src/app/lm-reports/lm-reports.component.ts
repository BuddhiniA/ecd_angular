import { Component, OnInit } from '@angular/core';
import { API_URL } from '../app_params';
import { BusinessLevel } from '../model/BusinessLevel';
import { CommonWebService } from '../common-web.service';
import { Node } from '../model/Node';

@Component({
  selector: 'app-lm-reports',
  templateUrl: './lm-reports.component.html',
  styleUrls: ['./lm-reports.component.css'],
  providers: [CommonWebService]
})
export class LmReportsComponent implements OnInit {
  startDate1: string;
  endDate1: string;
  checked1: number;
  selectedQ: any;
  selectedDs: any;
  selectedYear: any;
  checked: number;
  dsList: any;
  selectedDistrict: any;
  districtList: any;
  userId(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  files: Node[];
  businessList: BusinessLevel[];
  loading: boolean;
  selectedNode: Node;

  constructor(public commonService: CommonWebService) { }

  ngOnInit() {
    this.checked=1
    this.checked1=1
    this.selectedQ=1
    this.selectedYear=2017
    this.getAllDistrict()
  }

  generatePendingReport() {
    window.open(API_URL + "ECD_BIRT/BirtReportController?ReportFormat=html&ReportName=pending_materials.rptdesign", "Pending Materials");
  }
  generateReceivedReport() {
    window.open(API_URL + "ECD_BIRT/BirtReportController?ReportFormat=html&ReportName=received_materials.rptdesign", "Receieved Materials");
  }
  generateDistributeReport() {
    window.open(API_URL + "ECD_BIRT/BirtReportController?ReportFormat=html&ReportName=Distributed_materials.rptdesign", "Distributed Materials");
  }

  getAllDistrict() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getDistrictList?id=0").subscribe(res => {

      if (res.responseCode == 1) {
        this.districtList = res.responseData
        this.selectedDistrict = this.districtList[0]
        if(this.selectedDistrict!=undefined||this.selectedDistrict!=null){
          this.getAllDS()
        }
        
      }


    }), error => {
      console.log("ERROR")
    }
  }
  getAllDS() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getDSList?id=" + this.selectedDistrict.id).subscribe(res => {

      this.dsList = res.responseData


    }), error => {
      console.log("ERROR")
    }
  }
  generateReport(){

    console.log("checked",this.checked)
    if(this.checked1==2){
      if(this.checked==1){
        window.open(API_URL + "ECD_BIRT/BirtReportController?ReportFormat=xlsx&ReportName=fee_waiver_report_district.rptdesign&district="+this.selectedDistrict.id+"&year="+this.selectedYear+"&quarter="+this.selectedQ, "Distributed Materials");
      }else if(this.checked==2){
        window.open(API_URL + "ECD_BIRT/BirtReportController?ReportFormat=xlsx&ReportName=fee_waiver_report_ds.rptdesign&district="+this.selectedDistrict.id+"&ds="+this.selectedDs.id+"&year="+this.selectedYear+"&quarter="+this.selectedQ, "Distributed Materials");
      }
    }
    if(this.checked1==1){
      if(this.checked==1){
        window.open(API_URL + "ECD_BIRT/BirtReportController?ReportFormat=xlsx&ReportName=fee_waiver_report_district_before.rptdesign&district="+this.selectedDistrict.id+"&startDate="+this.startDate1+"&endDate="+this.endDate1, "Distributed Materials");
      }else if(this.checked==2){
        window.open(API_URL + "ECD_BIRT/BirtReportController?ReportFormat=xlsx&ReportName=fee_waiver_report_ds_before.rptdesign&district="+this.selectedDistrict.id+"&ds="+this.selectedDs.id+"&startDate="+this.startDate1+"&endDate="+this.endDate1, "Distributed Materials");
      }
    }
    
    
   
  }
}
