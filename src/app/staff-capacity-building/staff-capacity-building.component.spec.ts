import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StaffCapacityBuildingComponent } from './staff-capacity-building.component';

describe('StaffCapacityBuildingComponent', () => {
  let component: StaffCapacityBuildingComponent;
  let fixture: ComponentFixture<StaffCapacityBuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StaffCapacityBuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StaffCapacityBuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
