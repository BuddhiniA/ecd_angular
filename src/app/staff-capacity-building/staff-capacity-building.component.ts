import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { Certificate } from '../model/Certificate';
import { ModalData } from '../model/ModalData';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonWebService } from '../common-web.service';
import { Router } from '@angular/router';
import { LongTermProgram } from '../model/LongTermProgram';
import { TeacherProgram } from '../model/Teacher';
import { API_URL } from '../app_params';
import { ProgramData } from '../model/ProgramData';
import { DatePipe } from '@angular/common';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { Data } from '../model/Data';
import { TreeNode } from 'primeng/primeng';
import { ResourcePerson } from '../model/ResourcePerson';

export class Building {
  programId
  programName
  description
  programArea
  startDate
  endDate
  locationId
  locationName
  budget
  actualBudget
  filePath
  status
}

export class Institute {

  instituteId
  name
  address
  contactPerson
  contactNo
  status
  userInserted
  userModified
  dateInserted
  dateModified
}
export class Resource {
  id
  resourceName
  institute
  qualification
  status
  userEnterd
  userModified
  dateInserted
  dateModified
}

export class Other {
  personId
  personName
  remarks;
  status
  coordinator
}



export class BuildingProgram {
  resourcePool = new Array<Other>()
  guestList = new Array<Other>()
  userInserted
  userModified
  dateInserted
  dateModified
  program = new Building()
}

export class Location {
  locationId
  locationName
}
export class BusinessStructure {
  bisId
  bisName
  parentId
}

export class BizModel {
  parentId
  branches
}
@Component({
  selector: 'app-staff-capacity-building',
  templateUrl: './staff-capacity-building.component.html',
  styleUrls: ['./staff-capacity-building.component.css'],
  
  providers: [CommonWebService, DatePipe]
})
export class StaffCapacityBuildingComponent implements OnInit {
  selectedProgramArea: number;
  selectedLearningCenter=new Data();
  learningCenterList: any[];
  selectedNode=new Node();
  files: Node[];
  businessList: BusinessLevel[];
  loadingTree: boolean;
  remark: FormControl;
  name: FormControl;
  programForm: FormGroup;
  userData: any;
  programData = new ProgramData();
  staffCapacity: FormGroup;
  public programList;
  public selectedprogram;
  public certificate
  public certificateList = new Array<Certificate>()
  public selectedCertificate = new Certificate()

  public resourceList = new Array<ResourcePerson>()
  public selectedResource
  public resourcePool = new Array<Other>()//this is for adding resources only from resource pool
  public guestList = new Array<Other>()//this is for adding gues list
  public selectedResourcePersonList = new Array<any>() // this is for store all the resources being added

  public institueList = new Array<Institute>()
  public selectedInstitute

  public modelData = new ModalData()
  public viewParam;

  public programId;

  public success
  public error
  public warn
  public longtermForm: FormGroup
  public status

  public rType
  public remarks
  public selectedOtherResource = new Other()

 

  public location = new Array<Location>()
  public objList = new Array<any>()
  public bizModel = new BizModel();

  public selectedArea
  public max: number
  public array = new Array<any>()

  @ViewChild("viewProgram") public viewProgram: ModalDirective;
  @ViewChild("viewParticipant") public viewParticipant: ModalDirective;
  @ViewChild("addParticipantView") public addParticipantView: ModalDirective;



  public buildingList = new Array<Building>()
  public building



  constructor(public commonService: CommonWebService, public route: Router,private datePipe: DatePipe) {
    this.validation()
  }

  ngOnInit() {
    this.getUser()
    this.validation()
    this.getAllLocations()
    this.getAllBuildings()
    this.getAllResources()
    this.getAllInstitutes()
    

  }



  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
  }
  validation() {

    this.programForm = new FormGroup({
      programName: new FormControl(['', Validators.required]),
      description: new FormControl(['', Validators.required]),
      startDate: new FormControl(['', Validators.email]),
      endDate: new FormControl('', [Validators.required]),
      location: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      type: new FormControl(),
      resource: new FormControl(),
      area: new FormControl(),

    })

    this.name = new FormControl('', Validators.required),
      this.remark = new FormControl()

  }
  openView(param1, param2) {
    this.getAllBuildings()
    if (param1 == 2) {
      this.viewParam = 2
      this.getAllProgramsbyId(param2)
      this.modelData.title = "View Staff Capacity Building Program"
      this.viewProgram.show()
    } else if (param1 == 'edit') {

      this.getAllProgramsbyId(param2)
      this.getAllResources()
      this.viewParam =3
      this.rType = 1
      this.modelData.title = "Edit Staff Capacity Building Program"

      this.viewProgram.show()
      console.log("2 SELECTED CERTIFICATE", this.selectedCertificate)
    } else if (param1 ==1 && param2 == null) {
      this.viewParam = 1
      this.getAllResources()
      this.programData = new ProgramData()
      this.rType = 1;
      this.programData.program.status = 1
      this.selectedProgramArea = 1
      this.programData.program.startDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
      this.programData.program.endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd') 
      this.modelData.title = "Add New Staff Capacity Building Program"
      this.viewProgram.show()
    }

  }






 

  

  expand(param) {
    console.log("EXPANDING...")
    let array1 = new Array<any>()
    let limit = 0

    while (limit < param) {
      for (let i = 0; i < this.objList.length - 1; i++) {
        if (this.objList[i].parentId == limit + 1) {
          array1 = this.objList[i].branches
          this.array.push(array1)
        }

      }
      limit++
    }



    console.log(this.array)
  }
  getAllInstitutes() {


    this.selectedInstitute = new Institute()
    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllInstitutes").subscribe(res => {


      this.institueList = res.responseData
      this.selectedInstitute = this.institueList[0]



    }), error => {
      console.log("ERROR")
    }
  }
  getAllResources() {

    this.selectedResource = new ResourcePerson()
    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources").subscribe(res => {


      this.resourceList = res.responseData
      this.selectedResource = this.resourceList[0]



    }), error => {
      console.log("ERROR")
    }
  }

  getAllBuildings() {
    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getStaffCapacity").subscribe(res => {


      this.buildingList = res.responseData


      console.log("CERTIFICATE", this.buildingList)



    }), error => {
      console.log("ERROR")
    }
  }


  updateProgram() {

    console.log("CERTIFICATE", this.selectedCertificate)

    let pro = new BuildingProgram()
    pro.program.programId = this.selectedprogram.program.programId


    pro.program.description = this.selectedprogram.program.description
    pro.program.programName = this.selectedprogram.program.programName
    pro.program.startDate = this.selectedprogram.program.startDate
    pro.program.endDate = this.selectedprogram.program.endDate
    pro.program.status = this.status
    pro.program.programArea = this.getProgramArea(this.selectedArea)
    pro.resourcePool = this.resourcePool
    pro.guestList = this.guestList
    this.selectedprogram.userInserted = "Sandali"
    console.log("PROGRAM", pro)

    // this.commonService.processPost("http://192.0.0.65:8080/LongTermTrainigProgram-Management-1.0/service/program/updateProgram", pro).subscribe(res => {

    //   console.log("Data", res.responseCode)

    //   // console.log(this.criteriaList.length)
    //   if (res.responseCode == 1) {
    //     this.success = true;
    //     console.log("Success")
    //     setTimeout(() => {
    //       this.success = false;
    //       this.selectedprogram.programName = ""
    //       this.selectedprogram.description = ""
    //       this.selectedprogram.status = 1
    //     }, 2000);


    //     this.getAllBuildings()



    //   }

    // }), error => {
    //   console.log("ERROR")
    // }

  }

  saveProgram(e) {

    this.programData.userInserted = this.userData[0].userId
    if(this.selectedProgramArea==1){
      this.programData.program.programArea="M&E"
    }else if(this.selectedProgramArea==2){
      this.programData.program.programArea="Project Management"
    }else if(this.selectedProgramArea==3){
      this.programData.program.programArea="Social"
    }else if(this.selectedProgramArea==4){
      this.programData.program.programArea="Procurement"
    }
   
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveStaffCapacity", this.programData).subscribe(res => {

      if (res.responseCode == 1) {
        this.success = true;
      
        setTimeout(() => {
          this.success = false;
          this.getAllBuildings()
     
          this.viewProgram.hide()
        }, 2000);

      }else{
        this.error = true
        setTimeout(() => {
          this.error = false;
          
        }, 2000);
      }

    }), error => {
      console.log("ERROR")
      this.error = true
      setTimeout(() => {
        this.error = false;
        
      }, 2000);
    }

  }
  selctedCoordinatorforPool(event, param) {
    console.log("Here")
    let isChecked = event.target.checked
    var index

    if (isChecked && this.resourcePool.length != 0) {
      index = this.resourcePool.indexOf(param);

      this.resourcePool[index].coordinator = 1
      console.log("Ressource Pool", this.resourcePool[index].coordinator)
    } else if (!isChecked && this.resourcePool.length != 0) {

      index = this.resourcePool.indexOf(param);
      this.resourcePool[index].coordinator = 0
    }
  }

  selctedCoordinator(event, param) {

    console.log("sdsdasd")
    let isChecked = event.target.checked


    var index

    if (isChecked && this.guestList.length != 0) {
      index = this.guestList.indexOf(param);
      this.guestList[index].coordinator = 1
    } else if (!isChecked && this.guestList.length != 0) {
      index = this.guestList.indexOf(param);
      this.guestList[index].coordinator = 0
    }
  }

  


  getProgramArea(value): any {
    let proArea
    if (value == 1) {
      proArea = "M&E"
    } else if (value == 2) {
      proArea = "Project Management"
    } else if (value == 3) {
      proArea = "Social"
    } else if (value == 4) {
      proArea = "Procurement"
    }
    return proArea
  }
 

  convert(date: String): any {
    let a = new Array<any>()
    a = date.split(" ")

    return a[0]
  }
  getAllProgramsbyId(param2: TeacherProgram) {
    this.programList = new Array<Building>()
    this.selectedprogram = new BuildingProgram()
this.programData=new ProgramData()
    this.selectedResourcePersonList = new Array<any>()
    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getStaffCapacityById?programId=" + param2.programId).subscribe(res => {

      if (res.responseCode == 1) {

        this.programData = res.responseData
        this.selectedResourcePersonList = this.programData.resourcePool
        // this.selectedprogram.program.startDate = this.selectedprogram.program.startDate == null ? "" : this.convert(this.selectedprogram.program.startDate)
        // this.selectedprogram.program.endDate = this.selectedprogram.program.endDate == null ? "" : this.convert(this.selectedprogram.program.endDate)
        // this.status = this.selectedprogram.program.status


        // this.selectedResource = this.selectedprogram.resourcePool

        // console.log("Resource POOL", this.selectedprogram.resourcePool)
        // console.log("Resource POOL", this.selectedprogram.guestList)

        // for (let a of this.selectedprogram.resourcePool) {
        //   console.log("A", a)
        //   this.selectedResourcePersonList.push(a)
        //   this.resourcePool.push(a)
        // }
        // for (let a of this.selectedprogram.guestList) {
        //   console.log("B", a)
        //   this.selectedResourcePersonList.push(a)
        //   this.guestList.push(a)
        // }
        this.selectedResourcePersonList = this.programData.resourcePool
        console.log("Program", this.selectedResourcePersonList)
      }



    }), error => {
      console.log("ERROR")
    }
    console.log("SELECTED CERTIFICATE", this.selectedCertificate)

  }

  getAllLocations() {

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData
        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }

  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node1.children.push(node)
      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node1.children.push(node)
      }

      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }
  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;
    if (event.node.type == "province") {

    }
    else if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.code, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.code, event.node.type)
    }

    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }


  nodeSelected(event) {
    this.programData.program.locationId = event.node.id
    this.programData.program.locationName = event.node.label
    this.collapseAll()
  }



  getAllLearningCentersbyId(id1, id2, type) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id1=" + id1 + "&id2=" + id2 + "&type=" + type).subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;

        this.selectedLearningCenter = this.learningCenterList[0]


      }


    }), error => {
      console.log("ERROR")
    }
  }
  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }
  addResourcePersons() {


    if (this.rType == 1 && this.selectedResource != null) {
      let person = new ResourcePerson()
      person.personId = this.selectedResource.id
      person.personName = this.selectedResource.resourceName
      person.coordinator = 0
      this.programData.resourcePool.push(person)
      this.selectedResourcePersonList.push(person)
    }
    else if (this.rType == 2 && this.selectedOtherResource != null) {

      let person = new ResourcePerson()

      person.personName = this.selectedOtherResource.personName
      person.remarks = this.selectedOtherResource.remarks
      person.coordinator = 0
      this.programData.guestList.push(person)
      this.selectedResourcePersonList.push(person)

      this.selectedResource.personName = ""
      this.selectedResource.remarks = ""

    }
  }

  removeRows(person) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool.splice(index2, 1)
    }
    if (index3 > -1) {
      this.programData.guestList.splice(index3, 1)
    }



  }

  removeRowswhenEdit(person: ResourcePerson) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool[index2].status = 2
    }
    if (index3 > -1) {
      this.programData.guestList[index3].status = 2
    }



  }

  checked(e, person) {

    console.log("Per", person)
    console.log("sdsdsd", this.programData.resourcePool)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    console.log("index", index2)
    console.log("in", index3)
    if (index2 > -1) {
      this.programData.resourcePool[index2].coordinator = 1
    }
    if (index3 > -1) {

      this.programData.guestList[index3].coordinator = 1
    }
  }

  openAddParticipant(a) {
    this.programId = a
    console.log("A", this.programId)
   
    this.addParticipantView.show()

  }
}
