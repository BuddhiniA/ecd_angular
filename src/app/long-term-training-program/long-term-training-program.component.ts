import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonWebService } from '../common-web.service';

import { ModalDirective } from 'ngx-bootstrap';
import { ModalData } from '../model/ModalData';
import { Certificate } from '../model/Certificate';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { API_URL } from '../app_params';

import { DatePipe } from '@angular/common';
import { ResourcePerson } from '../model/ResourcePerson';
import { ProgramData } from '../model/ProgramData';
import { Program } from '../model/Program';
import { InstitutesModel } from '../model/Institutes';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { TreeNode } from 'primeng/primeng';



export class Resource {
  id
  resourceName
  institute
  qualification
  status
  userEnterd
  userModified
  dateInserted
  dateModified
}
export class Guest {
  personId
  programId
  personName
  remarks
  status

}

export class LongTermDetail {
  detailId
  programId
  personId
  resourcePerson
  resourcePersonName
  coordinator
  status
}

// export class LongTermProgram {
//   programId
//   certificateId
//   certificateName
//   instituteId
//   instituteName
//   resourceId
//   resourceName
//   programName
//   description
//   startDate
//   endDate
//   type
//   venue
//   status
//   location
//   longTermDetail=new Array<LongTermDetail>()
//   guestList = new Array<Guest>()
// }



@Component({
  selector: 'app-long-term-training-program',
  templateUrl: './long-term-training-program.component.html',
  styleUrls: ['./long-term-training-program.component.css'],
  providers: [CommonWebService, DatePipe, ProgramData, Program]
})
export class LongTermTrainingProgramComponent implements OnInit {
  viewType: number;
  checke: number;
  type: any;
  selectedNode = new Node();
  files: Node[];
  businessList: any[];
  loadingTree: boolean;
  userData: any;
  programEx: Program;
  @ViewChild("viewProgram") public viewProgram: ModalDirective;
  @ViewChild("addParticipantView") public addParticipantView: ModalDirective;
  remark: FormControl;
  name: FormControl;
  programData;
  title: string;
  bisId: any;





  public programList = new Array<any>();
  public selectedprogram;
  public certificate
  public certificateList = new Array<Certificate>()
  public selectedCertificate = new Certificate()

  // public resourceList = new Array<Resource>()
  // public selectedResource

  public institueList = new Array<InstitutesModel>()
  public selectedInstitute

  public modelData = new ModalData()
  public viewParam;

  public programId;

  public success
  public error
  public warn
  public longtermForm: FormGroup
  public status
  public imageSavingURL

  public remarks
  public selectedOtherResource = new Guest()
  public resourceList = new Array<ResourcePerson>()
  public selectedResource: any
  public resourcePool = new Array<LongTermDetail>()//this is for adding resources only from resource pool
  public guestList = new Array<Guest>()//this is for adding gues list
  public selectedResourcePersonList = new Array<any>() // this is for store all the resources being added

  public rType
  constructor(public commonService: CommonWebService, public router: ActivatedRoute,public route: Router, private datePipe: DatePipe) {
    this.bisId = sessionStorage.getItem("bisId");
   
    this.programData = new ProgramData();




  }

  ngOnInit() {
    this.getAllLocations()
    this.router.queryParams.subscribe(params => {
     
      this.type = params['type']
    },()=>{
      if(this.type=='phdt'){
          this.title="Advance Refresher Training for CDOs in the Plantations"
      }else if(this.type=='pmu'){
        this.title="ECD Certificate/ Diploma Courses for ECD Teachers"
      }
    }
    
    )
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.validation()

    console.log("Program Data", this.programData.program)

    this.getAllPrograms()



    this.imageSavingURL = API_URL + "LongTermTrainigProgram-Management-1.0/service/program/uploadDocs"
  }

  validation() {

    this.longtermForm = new FormGroup({
      programName: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      startdate: new FormControl('', [Validators.required]),
      enddate: new FormControl('', [Validators.required]),
      venue: new FormControl('', [Validators.required]),
      institute: new FormControl(),
      certificate: new FormControl(),
      location: new FormControl(),
      status: new FormControl(),
      type: new FormControl(),

      resource: new FormControl(),
      awardType:new FormControl(),
      awardType1:new FormControl(),

    })

    this.name = new FormControl('', Validators.required),
      this.remark = new FormControl()
  }

  openView(param1,param2) {
    if(param2==1){
      this.viewType=1
      this.programData = new ProgramData()
      this.selectedResourcePersonList = new Array<ResourcePerson>();
      this.getAllCertificates()
      this.getAllInstitutes()
      this.getAllResources()
      this.checke=1
      this.rType = 1
  
      this.programData.program.status = 1
      this.programData.program.startDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
      this.programData.program.endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
  
      this.modelData.title = "Add New Long Term Training Program"
      console.log("checked?",this.checke)
      this.viewProgram.show()
    }else if(param2==2){
      this.viewType=2
      console.log("param",param1)
      this.loadparogrambyId(param1)
    }
   


  }
  loadparogrambyId(id) {
   this.programData = new ProgramData()
   this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllLongProgramsbyId?id="+id).subscribe(res => {
      if(res.responseCode==1){
        this.programData = res.responseData[0]
        this.selectedResourcePersonList = this.programData.resourcePool
        this.viewProgram.show()
      }
     
    


    }), error => {
      console.log("ERROR")
    }
  }

  getAllCertificates() {



    this.commonService.processGet(API_URL + "ECD-CertificateManagement-1.0/service/certificate/getCertificate").subscribe(res => {


      this.certificateList = res.responseData

      if (this.selectedCertificate.certificateId == null) {
        this.selectedCertificate = this.certificateList[0]
      }
      console.log("CERTIFICATE", this.certificateList)



    }), error => {
      console.log("ERROR")
    }
  }

  getAllInstitutes() {


    this.selectedInstitute = new InstitutesModel()
    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllInstitutes").subscribe(res => {


      this.institueList = res.responseData
      this.selectedInstitute = this.institueList[0]



    }), error => {
      console.log("ERROR")
    }
  }
  getAllResources() {

    this.selectedResource = new ResourcePerson()
    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources").subscribe(res => {


      this.resourceList = res.responseData
      this.selectedResource = this.resourceList[0]



    }), error => {
      console.log("ERROR")
    }
  }

  getAllPrograms() {



    this.programList = new Array<any>()

    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllLongPrograms").subscribe(res => {


      this.programList = res.responseData
      console.log("LIST", this.programList)


    }), error => {
      console.log("ERROR")
    }
  }
  // getAllProgramsbyId(param2: LongTermProgram) {
  //   // this.programList = new Array<LongTermProgram>()
  //   this.selectedprogram = new LongTermProgram()
  //   console.log("EDIT",param2)
  //   this.selectedprogram = param2
  //   this.selectedCertificate.certificateId=param2.certificateId
  //   this.selectedCertificate.certificateName = param2.certificateName
  //   this.status = param2.status
  //   console.log("SELECTED CERTIFICATE",this.selectedCertificate)

  // }


  getAllProgramsbyId(param2) {




    this.commonService.processGet(API_URL + "LongTermTrainigProgram-Management-1.0/service/program/getProgrambyId?programId=" + param2.programId).subscribe(res => {


      this.selectedprogram = res.responseData

      // this.selectedprogram.program.startDate = this.selectedprogram.program.startDate==null?"":this.convert(this.selectedprogram.program.startDate)
      // this.selectedprogram.program.endDate = this.selectedprogram.program.endDate==null?"":this.convert(this.selectedprogram.program.endDate)
      this.status = this.selectedprogram.status


      this.selectedResource = this.selectedprogram.longTermDetail

      console.log("Resource POOL", this.selectedprogram.longTermDetail)
      console.log("Resource POOL", this.selectedprogram.guestList)

      for (let a of this.selectedprogram.longTermDetail) {
        console.log("A", a)
        this.selectedResourcePersonList.push(a)
      }
      for (let a of this.selectedprogram.guestList) {
        console.log("B", a)
        this.selectedResourcePersonList.push(a)
      }
      console.log("Program", this.selectedResourcePersonList)


    }), error => {
      console.log("ERROR")
    }
    console.log("SELECTED CERTIFICATE", this.selectedCertificate)

  }

  convert(date: String): any {
    let a = new Array<any>()
    a = date.split(" ")

    return a[0]
  }



  changeType(param){
    if(param==1){
      this.programData.program.certificateId=0
    }
    this.programData.program.awardType = param
  }

  saveProgram() {
    console.log("type",this.programData.program.awardType)
    if (this.bisId == 2) {
      this.programData.program.type = 1
    } else {
      this.programData.program.type = 2
    }
    this.programData.program.certificateId = this.selectedCertificate.certificateId

    this.programData.program.instituteId = this.selectedInstitute.instituteId

    this.programData.userInserted = this.userData[0].userId

    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveLongTermProgram", this.programData).subscribe(res => {

     
      // console.log(this.criteriaList.length)
      if (res.responseCode == 1) {
        this.success = true;
        this.getAllPrograms()
        console.log("Success")
        setTimeout(() => {
          this.success = false;
          this.viewProgram.hide()

        }, 2000);
      }else{
        this.error = true
        setTimeout(() => {
          this.error = false;
          this.viewProgram.hide()

        }, 2000);  
      }

    }), error => {
      console.log("ERROR")
      this.error = true
      setTimeout(() => {
        this.error = false;
        this.viewProgram.hide()

      }, 2000);  
    }
    }

  


  openAddParticipant(a) {
    this.programId = a
    console.log("A", this.programId)

    this.addParticipantView.show()

  }
  selctedCoordinatorforPool(event, param) {
    console.log("Here")
    let isChecked = event.target.checked
    var index

    if (isChecked && this.resourcePool.length != 0) {
      index = this.resourcePool.indexOf(param);

      this.resourcePool[index].coordinator = 1
      console.log("Ressource Pool", this.resourcePool[index].coordinator)
    } else if (!isChecked && this.resourcePool.length != 0) {

      index = this.resourcePool.indexOf(param);
      this.resourcePool[index].coordinator = 0
    }
  }


  addResourcePersons() {


    if (this.rType == 1 && this.selectedResource != null) {
      let person = new ResourcePerson()
      person.personId = this.selectedResource.id
      person.personName = this.selectedResource.resourceName
      person.coordinator = 0
      this.programData.resourcePool.push(person)
      this.selectedResourcePersonList.push(person)
    }
    else if (this.rType == 2 && this.selectedOtherResource != null) {

      let person = new ResourcePerson()

      person.personName = this.selectedOtherResource.personName
      person.remarks = this.selectedOtherResource.remarks
      person.coordinator = 0
      this.programData.guestList.push(person)
      this.selectedResourcePersonList.push(person)

      this.selectedResource.personName = ""
      this.selectedResource.remarks = ""

    }
  }

  removeRows(person) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool.splice(index2, 1)
    }
    if (index3 > -1) {
      this.programData.guestList.splice(index3, 1)
    }



  }

  removeRowswhenEdit(person: ResourcePerson) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool[index2].status = 2
    }
    if (index3 > -1) {
      this.programData.guestList[index3].status = 2
    }



  }

  checked(e, person) {

    console.log("Per", person)
    console.log("sdsdsd", this.programData.resourcePool)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    console.log("index", index2)
    console.log("in", index3)
    if (index2 > -1) {
      this.programData.resourcePool[index2].coordinator = 1
    }
    if (index3 > -1) {

      this.programData.guestList[index3].coordinator = 1
    }
  }

  getAllLocations() {

    
    
    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)
      }


    }), error => {
      console.log("ERROR")
    }
  }


  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()

        node1.children.push(node)






      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }


  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;


    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }

  nodeSelected(event) {
    this.programData.program.locationId = event.node.id
    this.programData.program.locationName = event.node.label
    console.log(this.programData.program.locationName)
    this.collapseAll()
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

}
