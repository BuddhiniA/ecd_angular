import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LongTermTrainingProgramComponent } from './long-term-training-program.component';

describe('LongTermTrainingProgramComponent', () => {
  let component: LongTermTrainingProgramComponent;
  let fixture: ComponentFixture<LongTermTrainingProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LongTermTrainingProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LongTermTrainingProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
