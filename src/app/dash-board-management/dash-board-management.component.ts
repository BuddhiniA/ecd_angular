import { Component, OnInit, Inject, Input, SimpleChanges } from '@angular/core';
import { Node } from '../model/Node';
import { BusinessLevel } from '../model/BusinessLevel';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { MatDialog } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { DateAdapter } from '@angular/material/core';
import { Plan } from '../model/Plan';
import { Promise } from 'q';
import { Activity } from '../model/Activity';
import { ActivityStatus } from '../model/ActivityStatus';
import { ProgramProgress } from '../model/ProgramProgress';
import { CommonDetails } from '../model/CommonDetails';

declare var google;
@Component({
  selector: 'app-dash-board-management',
  templateUrl: './dash-board-management.component.html',
  styleUrls: ['./dash-board-management.component.css'],
  providers: [CommonWebService]
})
export class DashBoardManagementComponent implements OnInit {
  commnList: CommonDetails[];
  programProgress: ProgramProgress[];
  activityStatus2019_newcdc: ActivityStatus;
  activityStatus2021_renplay: ActivityStatus;
  activityStatus2021_newplay: ActivityStatus;
  activityStatus2021_rencdc: ActivityStatus;
  activityStatus2021_newcdc: ActivityStatus;
  activityStatus2020_renplay: ActivityStatus;
  activityStatus2020_newplay: ActivityStatus;
  activityStatus2020_rencdc: ActivityStatus;
  activityStatus2020_newcdc: ActivityStatus;
  activityStatus2019_renplay: ActivityStatus;
  activityStatus2019_newplay:ActivityStatus;
  activityStatus2019_rencdc: ActivityStatus;
  activityStatus2018_renplay: ActivityStatus;
  activityStatus2018_newplay: ActivityStatus;
  activityStatus2018_rencdc: ActivityStatus;
  activityStatus2018_newcdc: ActivityStatus;
  activityStatus2017_renplay: ActivityStatus;
  activityStatus2017_newplay: ActivityStatus;
  activityStatus2017_rencdc: ActivityStatus;
  activityStatus2017_newcdc: ActivityStatus;
 
  maintainActivityModel: any;
  dataBar1:any
  map: any;
  overlays = new Array<any>();

  activityList: any;
  optionsBar: any
  dataBar: any
  selectedYear: any;
  selectedMonth: any;
  selectedActType: any;
  type: any;
  optionsPie: any
  selectedType: string;
  loadingTree: boolean;
  pmuList: Plan[]
  phdtList: Plan[]
  file: Node[];
  businessList: BusinessLevel[];
  userId: any;
  public selectedNode = new Node()
  optionsMap: { center: { lat: number; lng: number; }; zoom: number; };
  options:any

  data: any;
  constructor(public commonService: CommonWebService, public dialog: MatDialog) {
    this.activityStatus2019_newcdc= new ActivityStatus();
    this.activityStatus2021_renplay= new ActivityStatus();
    this.activityStatus2021_newplay= new ActivityStatus();
    this.activityStatus2021_rencdc= new ActivityStatus();
    this.activityStatus2021_newcdc= new ActivityStatus();
    this.activityStatus2020_renplay= new ActivityStatus();
    this.activityStatus2020_newplay= new ActivityStatus();
    this.activityStatus2020_rencdc= new ActivityStatus();
    this.activityStatus2020_newcdc= new ActivityStatus();
    this.activityStatus2019_renplay= new ActivityStatus();
    this.activityStatus2019_newplay= new ActivityStatus();
    this.activityStatus2019_rencdc= new ActivityStatus();
    this.activityStatus2018_renplay= new ActivityStatus();
    this.activityStatus2018_newplay= new ActivityStatus();
    this.activityStatus2018_rencdc= new ActivityStatus();
    this.activityStatus2018_newcdc= new ActivityStatus();
    this.activityStatus2017_renplay= new ActivityStatus();
    this.activityStatus2017_newplay= new ActivityStatus();
    this.activityStatus2017_rencdc= new ActivityStatus();
    this.activityStatus2017_newcdc= new ActivityStatus();
    this.programProgress = new Array<ProgramProgress>()
    this.commnList = new Array<CommonDetails>()
    this.userId = sessionStorage.getItem("bisId");
    this.pmuList = new Array<Plan>()
    this.phdtList = new Array<Plan>()

    this.data = {
      labels: ['A', 'B', 'C'],
      datasets: [
        {
          data: [300, 50],
          backgroundColor: [
            "#34495e",
            "#36A2EB",

          ],
          hoverBackgroundColor: [
            "#34495e",
            "#36A2EB",

          ]
        }]
    };
    this.optionsBar = {
      // title: {
      //     display: true,

      //     fontSize: 16
      // },
      legend: {
        position: 'top'
      },
      groupWidth: '10%',

      scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }
    };

    
    
  }

  ngOnInit() {

    
    if (this.userId == 2) {
      this.selectedActType = '1'
      this.type = 1
    } else if (this.userId == 3) {
      this.selectedActType = '2'
      this.type =2 
    }
    // this.selectedActType = '1'
    // this.selectedMonth = '01'
    // this.selectedYear = '2017'
    // this.loadActivities()
    this.getAllLocations()
    this.optionsMap = {
      center: { lat: 7.8774, lng: 80.7003 },
      zoom: 7
    };


    google.charts.load('current', { packages: ['corechart', 'bar'] });
    google.charts.setOnLoadCallback(drawBasic);


    function drawBasic() {

      var data = google.visualization.arrayToDataTable([
        ['Element', 'Count', { role: 'style' }],
        ['Master Trainer', 8.94, '#1CAF9A'],            // RGB value
        ['Long Term', 10.49, '#1CAF9A'],            // English color name
        ['Short Term', 19.30, '#1CAF9A'],
        ['Parental Awarenes', 19.30, '#1CAF9A'],
        ['Period Pre School', 19.30, '#1CAF9A'],
        ['Admin Staff', 19.30, '#1CAF9A'],
        ['Head Orientation', 21.45, '#1CAF9A'], // CSS-style declaration
      ]);
      var options = {
        title: 'Trainig Programs throug out the year',
        hAxis: {
          title: 'Time of Day',

        },
        legend: { position: 'top' },
        vAxis: {
          title: 'Number of Programs'
        },
        bar: { groupWidth: "60%" },
        width: 400,
        height: 250
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }

    this.optionsPie = {

      legend: {
        position: 'none'
      }
    };
  }

  openView(activityId): void {
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getMaintainActivityById?activityId=" + activityId).subscribe(res => {



      if (res.responseCode == 1) {

        this.maintainActivityModel = res.responseData

        console.log("MAintain activity", this.maintainActivityModel)
        let dialogRef = this.dialog.open(GanttDialog, {
          data: this.maintainActivityModel.milestoneList,
          width: '90%',
          // height:'100%'
    
        });


      }


    }), error => {
      console.log("ERROR")
    }
    
 
  }
  loadActivityCategory(){
    this.commnList = new Array<CommonDetails>()
    let type
    if (this.selectedActType == 1) {
      type = "PMU"
      let plan = new Plan()
      // this.get2017(type)
      // this.get2018(type)
      // this.get2019(type)
      // this.get2020(type)
      // this.get2021(type)
      this.loadActivities()

    } else if (this.selectedActType == 2) {
      type = "PHDT"
      this.get2017(type)
      this.get2018(type)
      this.get2019(type)
      this.get2020(type)
      this.get2021(type)
      this.loadActivities()
    }
  }
  generateReport(type,category,year){
    window.open(API_URL+"ECD_BIRT/BirtReportController?ReportFormat=html&ReportName=ecd_activity.rptdesign&year="+year+"&type="+type+"&category="+category, "ECD ACtivity Details");
    console.log()
   
  }
  loadActivities() {
    this.commnList = new Array<CommonDetails>()
    this.activityList = new Array<any>()
    this.overlays = new Array<any>()
    let date
   
    let type
    if (this.selectedActType == 1) {
      type = "PMU"
      let plan = new Plan()
      // this.get2017(type)
      // this.get2018(type)
      // this.get2019(type)
      // this.get2020(type)
      // this.get2021(type)
      if(this.selectedYear!=undefined){
       
        if(this.selectedMonth==undefined||this.selectedMonth==null){
          this.selectedMonth="00"
          this.getTrainingProgress(type)
          this.getActualPlanned(type)
        }
       
        if(this.selectedMonth!=undefined&&this.selectedMonth!='00'){
          date = this.selectedYear + "-" + this.selectedMonth
          this.getTrainingProgress(type)
          this.getActualPlanned(type)
        }
      }
    

    } else if (this.selectedActType == 2) {
      type = "PHDT"
      this.type=2
      // this.get2017(type)
      // this.get2018(type)
      // this.get2019(type)
      // this.get2020(type)
      // this.get2021(type)
      if(this.selectedYear!=undefined){
       
        if(this.selectedMonth==undefined||this.selectedMonth==null){
          this.selectedMonth="00"
          this.getTrainingProgress(type)
          this.getActualPlanned(type)
          this.getActivityList('PHDT',this.selectedYear)
        }
      
     
        if(this.selectedMonth!=undefined&&this.selectedMonth!='00'){

          console.log("selectedMonth",this.selectedMonth)
          this.getTrainingProgress(type)
          this.getActualPlanned(type)
          this.getActivityList('PHDT',this.selectedYear+"-"+this.selectedMonth)
        }
      }
      
    }
    


  }

getActivityList(type,date){
  this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllActivitiesDashBoard?type=" + type + "&date=" + date).subscribe(res => {
    if (res.responseCode == 1) {
      this.activityList = res.responseData
      for (let b of this.activityList) {

        this.overlays.push(new google.maps.Marker(
          {
            obj: b,
            position:
            { lat: b.latitude, lng: b.longitude },
            title: b.activityDescription
          }

        ))

      }
    }
  })
}

  get2017(type) {
    this.activityStatus2017_newcdc = new ActivityStatus()
    this.activityStatus2017_rencdc=new ActivityStatus()
    this.activityStatus2017_newplay = new ActivityStatus()
    this.activityStatus2017_renplay = new ActivityStatus()
    if (type = 'PHDT') {
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2017&category=2").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2017_newcdc = res.responseData
          console.log("new cdc?",this.activityStatus2017_newcdc)
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2017&category=3").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2017_rencdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2017&category=4").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2017_newplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2017&category=5").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2017_renplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
    }
  }

  get2018(type) {
    this.activityStatus2018_newcdc = new ActivityStatus()
    this.activityStatus2018_rencdc=new ActivityStatus()
    this.activityStatus2018_newplay = new ActivityStatus()
    this.activityStatus2018_renplay = new ActivityStatus()
    if (type = 'PHDT') {
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2018&category=2").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2018_newcdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2018&category=3").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2018_rencdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2018&category=4").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2018_newplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2018&category=5").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2018_renplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
    }
  }

  get2019(type) {
    this.activityStatus2019_newcdc = new ActivityStatus()
    this.activityStatus2019_rencdc=new ActivityStatus()
    this.activityStatus2019_newplay = new ActivityStatus()
    this.activityStatus2019_renplay = new ActivityStatus()
    if (type = 'PHDT') {
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2019&category=2").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2019_newcdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2019&category=3").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2019_rencdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2019&category=4").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2019_newplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2019&category=5").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2019_renplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
    }
  }

  get2020(type) {
    this.activityStatus2020_newcdc = new ActivityStatus()
    this.activityStatus2020_rencdc=new ActivityStatus()
    this.activityStatus2020_newplay = new ActivityStatus()
    this.activityStatus2020_renplay = new ActivityStatus()
    if (type = 'PHDT') {
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2020&category=2").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2020_newcdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2020&category=3").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2020_rencdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2020&category=4").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2020_newplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2020&category=5").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2020_renplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
    }
  }

  get2021(type) {
    this.activityStatus2021_newcdc = new ActivityStatus()
    this.activityStatus2021_rencdc=new ActivityStatus()
    this.activityStatus2021_newplay = new ActivityStatus()
    this.activityStatus2021_renplay = new ActivityStatus()
    if (type = 'PHDT') {
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2021&category=2").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2021_newcdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2021&category=3").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2021_rencdc = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2021&category=4").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2021_newplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityStatus?type=" + type + "&year=2021&category=5").subscribe(res => {
        if (res.responseCode == 1) {

          this.activityStatus2021_renplay = res.responseData
        }


      }), error => {
        console.log("ERROR")
      }
    }
  }

  getTrainingProgress(type){
    
    this.programProgress = new Array<ProgramProgress>()
   
    if(type=='PHDT'){
    
      this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getTrainingProgress?type=2&year="+this.selectedYear+"&month="+this.selectedMonth).subscribe(res => {
        if (res.responseCode == 1) {

          this.programProgress = res.responseData
          for(let pr of this.programProgress){
            
            if(pr.diplomaPhdt!=null){
              let common = new CommonDetails()
              common.name="ECD Diploma Program for CDOs in the Plantations"
              common.value = pr.diplomaPhdt
              this.commnList.push(common)
            }if(pr.refresherTraining!=null){
              let common = new CommonDetails()
              common.name="Advance Refresher Training for CDOs in the Plantations"
              common.value = pr.refresherTraining
              this.commnList.push(common)
            }if(pr.parentalPhdt!=null){
              let common = new CommonDetails()
              common.name="Parental Awareness Sessions in the Plantations"
              common.value = pr.parentalPhdt
              this.commnList.push(common)
            }
            
          }
        }


      }), error => {
        console.log("ERROR")
      }
    }
    if(type=='PMU'){
      
       this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getTrainingProgress?type=2&year="+this.selectedYear+"&month="+this.selectedMonth).subscribe(res => {
         if (res.responseCode == 1) {
 
           this.programProgress = res.responseData
           for(let pr of this.programProgress){
             
             if(pr.masterTrainer!=null){
               let common = new CommonDetails()
               common.name="TOT for Master Trainers on ECD"
               common.value = pr.masterTrainer
               this.commnList.push(common)
             }if(pr.longTerm!=null){
               let common = new CommonDetails()
               common.name="ECD Certificate/ Diploma Courses for ECD Teachers"
               common.value = pr.longTerm
               this.commnList.push(common)
             }if(pr.parentalAwareness!=null){
               let common = new CommonDetails()
               common.name="Parental Awareness Sessions"
               common.value = pr.parentalAwareness
               this.commnList.push(common)
             }
             if(pr.periodPreSchool!=null){
              let common = new CommonDetails()
              common.name="Periodic ECD Teacher Interactions"
              common.value = pr.periodPreSchool
              this.commnList.push(common)
            }
            if(pr.headOrientation!=null){
              let common = new CommonDetails()
              common.name="Periodic ECD Teacher Interactions"
              common.value = pr.headOrientation
              this.commnList.push(common)
            }
            if(pr.staffCapacity!=null){
              let common = new CommonDetails()
              common.name="ECD Training for Administrative Staff"
              common.value = pr.headOrientation
              this.commnList.push(common)
            }
            if(pr.shortTerm!=null){
              let common = new CommonDetails()
              common.name="Short-term Standardized Training for ECD Teachers"
              common.value = pr.shortTerm
              this.commnList.push(common)
            }
             
           }
         }
 
 
       }), error => {
         console.log("ERROR")
       }
     }
  }

  getActualPlanned(type){
    
    this.programProgress = new Array<ProgramProgress>()
    this.commnList = new Array<CommonDetails>()
    if(type=='PHDT'){
     
      this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getTrainingActualPlanned?type=2&year="+this.selectedYear+"&month="+this.selectedMonth).subscribe(res => {
        if (res.responseCode == 1) {
        
          
          this.dataBar1 = {
            labels: ['Diploma Program ', 'Refresher Training ', 'Parental Awareness '],
            datasets: [
              {
                label: 'Actual Count',
                backgroundColor: '#42A5F5',
                borderColor: '#1E88E5',
                data: [res.responseData[0].actualCount.diplomaPhdt, res.responseData[0].actualCount.refresherTraining, res.responseData[0].actualCount.parentalPhdt]
              },
              {
                label: 'Planned Count',
                backgroundColor: '#34495e',
                borderColor: '#34495e',
                data: [res.responseData[0].plannedCount.diplomaPhdt, res.responseData[0].plannedCount.refresherTraining, res.responseData[0].plannedCount.parentalPhdt]
              }
            ]
          }
          this.options = {
            responsive: false,
            maintainAspectRatio: false,
            legend: {
              position: 'bottom'
          }
          };
      
          
        }


      }), error => {
        console.log("ERROR")
      }
    }
   if(type=='PMU'){
      
       this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getTrainingActualPlanned?type=1&year="+this.selectedYear+"&month="+this.selectedMonth).subscribe(res => {
         if (res.responseCode == 1) {
           console.log("actual vs planned",res.responseData[0].actualCount.refresherTraining)
           
           this.dataBar = {
            labels: ['Master', 'Short', 'Parental', 'Long', 'Orientation', 'Periodic', 'Capacity'],
            datasets: [
              {
                label: 'Planned',
                backgroundColor: '#42A5F5',
                borderColor: '#1E88E5',
                data: [res.responseData[0].actualCount.masterTrainer,
                 res.responseData[0].actualCount.shortTerm,
                  res.responseData[0].actualCount.parentalAwareness,
                   res.responseData[0].actualCount.longTerm, 
                   res.responseData[0].actualCount.headOrientation, 
                   res.responseData[0].actualCount.periodPreSchool, 
                   res.responseData[0].actualCount.staffCapacity]
              },
              {
                label: 'Actual',
                backgroundColor: '#34495e',
                borderColor: '#34495e',
                data:[res.responseData[0].actualCount.masterTrainer,
                res.responseData[0].plannedCount.shortTerm,
                 res.responseData[0].plannedCount.parentalAwareness,
                  res.responseData[0].plannedCount.longTerm, 
                  res.responseData[0].plannedCount.headOrientation, 
                  res.responseData[0].plannedCount.periodPreSchool, 
                  res.responseData[0].plannedCount.staffCapacity]
              }
            ]
          }
           this.options = {
             responsive: false,
             maintainAspectRatio: false
           };
       
           
         }
 
 
       }), error => {
         console.log("ERROR")
       }
     }
  }
  setMap(event) {
    this.map = event.map;
  }
  getAllLocations() {

    this.selectedNode = new Node()
    let type
    if (this.userId == 1) {
      type = "root"
    } else if (this.userId == 2 || this.userId == 3) {
      type = "node"
    }



    this.businessList = new Array<BusinessLevel>()
    this.file = new Array<Node>()

    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {

      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {


          if (this.businessList[i].type == type && this.businessList[i].id == parseInt(this.userId)) {
            let node = new Node()

            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.lifeCode = this.businessList[i].lifeCode

            this.selectedNode.leaf = false


          }



        }





        this.addChildren(this.selectedNode)



        this.file.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }


  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {



      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node.lifeCode = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



        // this.getAllLearningCenters()


      }



      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node.lifeCode = this.businessList[i].lifeCode

        node1.children.push(node)
      }






      if (this.businessList[i].type == "region" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node.lifeCode = this.businessList[i].lifeCode


        node1.children.push(node)

      }
      if (node1.type == "region") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "rpc") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node.lifeCode = this.businessList[i].lifeCode
          node1.children.push(node)


        }

      }






      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {
          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node.lifeCode = this.businessList[i].lifeCode
          node1.children.push(node)

          // this.getAllLearningCenters()
        }
      }






    }


    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])

    }

  }

  showView(type) {
    this.type = type
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '300px',
      data: this.file

    });
  }
  

}

@Component({
  selector: 'dialog-overview-example-dialog',
  template: ' <p-tree [value]="data" ></p-tree>',
})
export class DialogOverviewExampleDialog {

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

}


@Component({
  selector: 'dialog-ganttDialog-dialog',
  template: '  <div id="chart_div" ></div>',

})
export class GanttDialog {
  activity: Activity;


  constructor(
    public dialogRef: MatDialogRef<GanttDialog>,
    @Inject(MAT_DIALOG_DATA) public list: any) {

      google.charts.load('current', {'packages':['gantt']});
      google.charts.setOnLoadCallback(drawChart);
      var me=this.list
      function drawChart() {
     
      var newData=[];
        
        
      
       
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Task ID');
        data.addColumn('string', 'Task Name');
        data.addColumn('string', 'Resource');
        data.addColumn('date', 'Start Date');
        data.addColumn('date', 'End Date');
        data.addColumn('number', 'Duration');
        data.addColumn('number', 'Percent Complete');
        data.addColumn('string', 'Dependencies');
  
        for(let a of me){
          
          newData.push([(a.milestoneId+""),a.milestoneName,null,new Date(2017, 1, 1),new Date(2017, 12, 1),null,a.progress,null])
        }
        console.log("DATA",newData)
          data.addRows(newData)
                   
        console.log("chart data",data)
      
        // data.addRows([
        //   ['2014Spring', 'Spring 2014', 'spring',
        //    new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
        //   ['2014Summer', 'Summer 2014', 'summer',
        //    new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
        //   ['2014Autumn', 'Autumn 2014', 'autumn',
        //    new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
        //   ['2014Winter', 'Winter 2014', 'winter',
        //    new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
        //   ['2015Spring', 'Spring 2015', 'spring',
        //    new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
        //   ['2015Summer', 'Summer 2015', 'summer',
        //    new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
        //   ['2015Autumn', 'Autumn 2015', 'autumn',
        //    new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
        //   ['2015Winter', 'Winter 2015', 'winter',
        //    new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
        //   ['Football', 'Football Season', 'sports',
        //    new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
        //   ['Baseball', 'Baseball Season', 'sports',
        //    new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
        //   ['Basketball', 'Basketball Season', 'sports',
        //    new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
        //   ['Hockey', 'Hockey Season', 'sports',
        //    new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
        // ]);
  
        var options = {
          height: 400,
          gantt: {
            trackHeight: 30
          }
        };
  
        var chart = new google.visualization.Gantt(document.getElementById('chart_div'));
  
        chart.draw(data, options);
      }
  
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  // ngOnInit() {


  //   google.charts.load('current', { 'packages': ['gantt'] });
  //   google.charts.setOnLoadCallback(drawChart);



  //   function drawChart() {
  //     var me = this
  //     var data = new google.visualization.DataTable();

  //     data.addColumn('string', 'Task Name');

  //     data.addColumn('date', 'Start Date');
  //     data.addColumn('date', 'End Date');
  //     data.addColumn('number', 'Duration');
  //     data.addColumn('number', 'Percent Complete');
  //     me.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityById?activityId=" + 2).subscribe(res => {


  //       if (res.responseCode == 1) {
  //         this.activity = res.responseData
  //       }

  //       for (let milestone of this.data) {
  //         data.addRows([milestone.milestoneName, milestone.plannedStartDate, milestone.plannedEndDate, milestone.plannedEndDate, milestone.plannedEndDate])

  //       }
  //       data.addRows([
  //         ['2014Spring', 'Spring 2014', 'spring',
  //           new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
  //         ['2014Summer', 'Summer 2014', 'summer',
  //           new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
  //         ['2014Autumn', 'Autumn 2014', 'autumn',
  //           new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
  //         ['2014Winter', 'Winter 2014', 'winter',
  //           new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
  //         ['2015Spring', 'Spring 2015', 'spring',
  //           new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
  //         ['2015Summer', 'Summer 2015', 'summer',
  //           new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
  //         ['2015Autumn', 'Autumn 2015', 'autumn',
  //           new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
  //         ['2015Winter', 'Winter 2015', 'winter',
  //           new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
  //         ['Football', 'Football Season', 'sports',
  //           new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
  //         ['Baseball', 'Baseball Season', 'sports',
  //           new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
  //         ['Basketball', 'Basketball Season', 'sports',
  //           new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
  //         ['Hockey', 'Hockey Season', 'sports',
  //           new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
  //       ]);
  //     })
  //     var options = {
  //       height: 400,
  //       gantt: {
  //         trackHeight: 30
  //       }
  //     };

  //     var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

  //     chart.draw(data, options);
  //   }
 // }


  }





