import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashBoardManagementComponent } from './dash-board-management.component';

describe('DashBoardManagementComponent', () => {
  let component: DashBoardManagementComponent;
  let fixture: ComponentFixture<DashBoardManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashBoardManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashBoardManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
