import { Component, OnInit } from '@angular/core';
import { RequestOptionsArgs, Headers, RequestOptions } from "@angular/http";

import { Router } from "@angular/router";
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { LoggedInUserModel } from '../model/loggedInUserModel';


// import { SharedService } from '../shared-service'

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css'],
  providers: [CommonWebService]
})

export class ForgotpasswordComponent implements OnInit {

  public userId: any;
  public password: any;

  public btnSignin = true;
  public signinMask = false;

  public loginFailMsg: string = "";
  public successMsg: string = "";
  constructor(
    // public userData: LoggedInUserModel,
    //  public sharedService : SharedService, 
    public webservice: CommonWebService, public router: Router) { }
// public userData: LoggedInUserModel,
   logUserData : LoggedInUserModel = new LoggedInUserModel();
   
  ngOnInit() {

  }

  resetlogin() {
    // $event.preventDefault()
   
    console.log("login.....",this.userId);
    // console.log("user ",this.logUserData.userId);
    // this.loginFailMsg = '';
    // this.btnSignin = false;
    // this.signinMask = true;
    // this.userData.userId = this.username;
    // this.userData.password = this.password;
    
    let header: Headers = new Headers();
    header.append('Content-Type', 'application/json');
    let option: RequestOptionsArgs = new RequestOptions();
    
    header.set("room", "DefaultRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "LK");
    header.set("division", "ECDLK");
    header.set("organization", "ECD");
    header.set("system", "ECDCMS");
   
    
    option.headers = header;

    // let url = AppParams.BASE_PATH + "user/validateCmsUser";
    let url = API_URL+"ECD-UserManagement-1.0/service/user/forgetPassword?userId="+this.userId;

    this.webservice.processPostWithHeaders(url, this.userId, option).subscribe(response => {
    

      console.log("response", response.flag);

      let responseData = response.flag;

      if (responseData != null) {

        // let flag = responseData['flag'];
        let flag = responseData;
        console.log("success zzz",flag);
        if (flag == 100) {

          console.log("success zzz",response.data);

          this.loginFailMsg = "sent";

          // let user  :LoggedInUserModel = response.data;

          // this.sharedService.setLoggedInUserst(user);

          // sessionStorage.setItem('id_token', btoa(JSON.stringify(user)) );

          // this.router.navigate(['./login']);
        } else {
          this.loginFailMsg = "Cannot send mail ";
        }
      } else {
        this.loginFailMsg = "Cannot connect with the authentication server";
      }
      this.signinMask = false;
      this.btnSignin = true;

    }, error => {

      this.signinMask = false;
      this.btnSignin = true;

    })
  }

}


