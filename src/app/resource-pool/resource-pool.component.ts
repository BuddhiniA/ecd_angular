import { Component, OnInit, ViewChild } from '@angular/core';
import { ResourcePoolModel } from "../model/ResourcePool";

import { ResourcePerson } from '../model/ResourcePerson';
import { ResourceMainModel } from '../model/ResourceMain';
import { InstitutesModel } from '../model/Institutes';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
@Component({
  selector: 'app-resource-pool',
  templateUrl: './resource-pool.component.html',
  styleUrls: ['./resource-pool.component.css']
})
export class ResourcePoolComponent implements OnInit {
  success: boolean;
  @ViewChild("addResource") public addResource;
  @ViewChild("editResource") public editResource;
  
  
 
  selectedIns: any;




  public resourcepoolModel = Array<ResourcePoolModel>();
  public resourcepoolDetail = new ResourcePoolModel;
  public resourcepoolEdModel = Array<ResourcePoolModel>();
  public resourcepoolEdDetail = new ResourcePoolModel;
  public resourcepersonModel = Array<ResourceMainModel>();
  public resourcepersonDetail = new ResourceMainModel;
  public instituteModel = Array<InstitutesModel>();
  public instituteDetail = new InstitutesModel;
  public ins;
  public resForm: FormGroup
 
 

  constructor(public commonService: CommonWebService) {
    this.validation();
  }

  ngOnInit() {
    this.loadToGrid();
    this.getAllInstitutes();
  }

  validation() {

    this.resForm = new FormGroup({
      resourceName: new FormControl(['', Validators.required]),
      ins: new FormControl(['', Validators.required]),
      qualification: new FormControl(['', Validators.email]),
      status: new FormControl('', [Validators.required]),
      // noOfStudents:new FormControl('',Validators.required),
      // noOfAssessedStudents:new FormControl('',Validators.required)

    })
  }

  openCreateMtr() {
    
    this.resourcepersonDetail.status = 1
    
    this.addResource.show();
  }

  getAllInstitutes() {
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllInstitutes";
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.instituteModel = res.responseData;
        if (this.instituteModel.length != 0) {
          this.selectedIns = this.instituteModel[0]
        }

      }

    }, error => {

      console.log("error ", error);
    })
  }

  getIns(ins) {
    console.log("selectedIns.name", ins.instituteId);
    this.resourcepersonDetail.institute = ins.instituteId
  }

  loadToGrid() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources";
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.resourcepoolModel = res.responseData;
      
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.resourcepoolModel)
    })
  }

  createResource() {


    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveResource";


    this.commonService.processPost(url, this.resourcepersonDetail).subscribe(res => {


     

      if (res.responseCode == 1) {
  
        this.success= true  
              setTimeout(() => {

          this.loadToGrid();
        
          this.success=false

        }, 2000)

      }
    }, error => {

     
      // this.fail.show();

    })
    //
  }

  getResId(r) {
    console.log("r", r);
    this.resourcepersonDetail.id = r.id;
    this.resourcepersonDetail.resourceName = r.resourceName;
    this.resourcepersonDetail.qualification = r.qualification;

    this.resourcepersonDetail.status = r.status;
    this.editResource.show();
  }
  editResourceDtl() {

    console.log("editResourceDtl");
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/updateResource", this.resourcepersonDetail).subscribe(res => {

     
      if (res.responseCode == 1) {
     

        this.success = true
        setTimeout(() => {

        this.success = false
          this.editResource.hide();
          this.loadToGrid();
        }, 1000)
      }

    }), error => {
      console.log(" ")
    }
  }
}
