import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared-service'
import { LoggedInUserModel } from '../model/loggedInUserModel';
import { CommonWebService } from '../common-web.service';
import { LoignValidator } from '../login-validator';
import { API_URL } from '../app_params';




@Component({
  selector: 'app-pagetop',
  templateUrl: './pagetop.component.html',
  styleUrls: ['./pagetop.component.css'],
  providers: [CommonWebService, SharedService, LoggedInUserModel]
})
export class PagetopComponent implements OnInit {
  showImage: any;
  public userId: any;
  public password: any;
  logUserData: LoggedInUserModel = new LoggedInUserModel();
  constructor(public validator: LoignValidator, public commonService: CommonWebService,
    public sharedService: SharedService, public userData: LoggedInUserModel) { }
  //  constructor() {    

  // }

  ngOnInit() {
    // this.userId=this.validator.getUserId();
    // this.sharedService.setLoggedInUserst();
    // 
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    console.log("user zzz", this.userData[0].userId);
    this.getUserImage()
    console.log("user model", this.userData);
  }

  getUserImage() {
    this.commonService.processGet(API_URL + "ECD-UserManagement-1.0/service/user/getUserImage?userId=" + this.userData[0].userId).subscribe(res => {
      if (res.responseCode==1) {
        
        if (res.responseData == null || res.responseData == undefined) {
          this.showImage = "assets/icon/loggeduser.png"
        }else{
          this.showImage =  res.responseData
        }
      }




    }), error => {
      console.log("ERROR")
    }
  }
}
