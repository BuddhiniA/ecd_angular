import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashBoardProgramViewComponent } from './dash-board-program-view.component';

describe('DashBoardProgramViewComponent', () => {
  let component: DashBoardProgramViewComponent;
  let fixture: ComponentFixture<DashBoardProgramViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashBoardProgramViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashBoardProgramViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
