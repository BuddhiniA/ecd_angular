import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommonWebService } from '../common-web.service';
import {API_URL } from '../app_params';
import { ModalData } from '../model/ModalData';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-dash-board-program-view',
  templateUrl: './dash-board-program-view.component.html',
  styleUrls: ['./dash-board-program-view.component.css'],
  providers:[CommonWebService]
})
export class DashBoardProgramViewComponent implements OnInit {
  @ViewChild("details") public details: ModalDirective
  public model = new ModalData()
  public programList = new Array<any>() 
  public selectedProgram:any;
   constructor(public activateRoute: ActivatedRoute,public commonService: CommonWebService) { }

  ngOnInit() {
    this.activateRoute.params.subscribe(params => {
      let program = params['program']
      

      this.loadDetails(program)
    })
  }
  loadDetails(a) {
    switch (a) {
      case 'Long Term': {
        this.model.title="Long Term"
        this.getLongTerm()
        break;
      }
      case 'Short Term': {
        this.model.title="Short Term"
        this.getShortTerm()
        break;
      }
      case 'Head Orientation': {
        this.model.title="Head Orientation"
        this.getHead()
        break;
      }
      case 'Admin Staff': {
        this.model.title="Admn Staff"
        this.getAdmin()
        break;
      }
      case 'Parental Awareness': {
        this.model.title="Parental Awareness"
        this.getParental()
        break;
      }
      case 'Period pre school': {
        this.model.title="Period pre school"
        this.getPeriod()
        break;
      }
      case 'Staff Capacty': {
        this.model.title="Staff Capacty"
        this.getStaff()
        break;
      }

    }
  }
  getLongTerm(){
    
   
    this.commonService.processGet(API_URL+"LongTermTrainigProgram-Management-1.0/service/program/getPrograms").subscribe(res => {
     


      this.programList = res.responseData
      console.log("LIST",this.programList)


    }), error => {
      console.log("ERROR")
    }
  }
  getShortTerm(){
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms").subscribe(res => {
      // this.commonService.processGet("http://192.0.0.65:8080/LongTermTrainigProgram-Management-1.0/service/program/getPrograms").subscribe(res => {


      this.programList = res.responseData
      console.log("LIST",this.programList)


    }), error => {
      console.log("ERROR")
    }
  }
  getHead(){
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getOrientationProgram").subscribe(res => {
      // this.commonService.processGet("http://192.0.0.65:8080/LongTermTrainigProgram-Management-1.0/service/program/getPrograms").subscribe(res => {


      this.programList = res.responseData
      console.log("LIST",this.programList)


    }), error => {
      console.log("ERROR")
    }
  }
  getAdmin(){
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getAdminStaff").subscribe(res => {
      // this.commonService.processGet("http://192.0.0.65:8080/LongTermTrainigProgram-Management-1.0/service/program/getPrograms").subscribe(res => {


      this.programList = res.responseData
      console.log("LIST",this.programList)


    }), error => {
      console.log("ERROR")
    }
  }
  getParental(){
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getParentalAwareness").subscribe(res => {
      // this.commonService.processGet("http://192.0.0.65:8080/LongTermTrainigProgram-Management-1.0/service/program/getPrograms").subscribe(res => {


      this.programList = res.responseData
      console.log("LIST",this.programList)


    }), error => {
      console.log("ERROR")
    }
  }
  getPeriod(){
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getTeacherInteractions").subscribe(res => {
      // this.commonService.processGet("http://192.0.0.65:8080/LongTermTrainigProgram-Management-1.0/service/program/getPrograms").subscribe(res => {


      this.programList = res.responseData
      console.log("LIST",this.programList)


    }), error => {
      console.log("ERROR")
    }
  }
  getStaff(){
    // this.commonService.processGet(GetStaffCapacity).subscribe(res => {
      this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getStaffCapacity").subscribe(res => {


      this.programList = res.responseData
      console.log("LIST",this.programList)


    }), error => {
      console.log("ERROR")
    }
  }
  openView(a){
    this.selectedProgram = a
    this.details.show()
  }
}
