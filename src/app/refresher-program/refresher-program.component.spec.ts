import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RefresherProgramComponent } from './refresher-program.component';



describe('ParentalAwarenessComponent', () => {
  let component: RefresherProgramComponent;
  let fixture: ComponentFixture<RefresherProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RefresherProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RefresherProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
