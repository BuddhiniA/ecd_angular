import { Component, OnInit, ViewChild } from '@angular/core';
import { ParentalAwarenessModel } from '../model/ParentalAwareness';

import { ResourcePoolModel } from '../model/ResourcePool';
import { ResPoolModel } from '../model/ResPool';
import { ResourcePerson } from '../model/ResourcePerson';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { DatePipe } from '@angular/common';
import { ProgramData } from '../model/ProgramData';
import { ModalDirective } from 'ngx-bootstrap';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { TreeNode } from 'primeng/primeng';
import { Data } from '../model/Data';

export class PrimaryProgram {
  userInserted: any = " ";
  userModified: any = null;
  dateInserted: any = null;
  dateModified: any = null;
  resourcePool = new Array<ResourcePerson>()
  // masterTrainerList = new Array<ResourcePersonModel>()
  guestList = new Array<ResourcePerson>()
  program = new ParentalAwarenessModel()
}

@Component({
  selector: 'app-refresher-program',
  templateUrl: './refresher-program.component.html',
  styleUrls: ['./refresher-program.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class RefresherProgramComponent implements OnInit {
  bisId:any;
  preschool: FormControl;
  userData: any;
  selectedLearningCenter = new Data();
  learningCenterList: any[];
  selectedResourcePersonList = new Array<ResourcePerson>();
  selectedNode=new Node();
  files: Node[];
  businessList: any[];
  loadingTree: boolean;
  remark: FormControl;
  name: FormControl;
  programForm: FormGroup;
  programData = new ProgramData();

  public parentalModel = Array<ParentalAwarenessModel>();
  public parentalDetail = new ParentalAwarenessModel;
  public resourceModel = Array<ResourcePoolModel>();
  public resourceDetail = new ResourcePoolModel;
  public resModel = Array<ResPoolModel>();
  public resDetail = new ResPoolModel;
  public personModel = Array<ResourcePerson>();
  public personDetail = new ResourcePerson;
  public success
  public error
  public warn
  public resource = new Array<ResourcePerson>();
  public resourceM = new ResourcePerson;
  public id;
  public d1;
  public d2;
  public resourceN = new ResPoolModel;
  public resourceName;
  public parental = new ParentalAwarenessModel()
  public selectedOtherResource = new ResourcePerson();
  public selectedOtherResourcesList = new Array<ResourcePerson>()
  public selectedResource
  public PrimaryProgram;
  public programId: number;
  public rType;
  public fromPoolList = new Array<ResourcePerson>()
  public parentalForm: FormGroup
  public imageSavingURL

  @ViewChild("makeParentSetting") public makeParentSetting;
  @ViewChild("editParentSetting") public editParentSetting;
  @ViewChild("addParticipantView") public addParticipantView: ModalDirective;
  @ViewChild("showLearningCenters") public showLearningCenters: ModalDirective;

  constructor(public commonService: CommonWebService, public route: Router, private datePipe: DatePipe) {
    // this.validation()
    this.bisId = sessionStorage.getItem("bisId");
  }

  ngOnInit() {
    this.getUser()
    this.validation()
    this.getAllLocations()
   
    this.loadToGrid();
    this.imageSavingURL = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources/?type=parental"


  }
  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
  }
  validation() {

    this.programForm = new FormGroup({
      programName: new FormControl(['', Validators.required]),
      description: new FormControl(['', Validators.required]),
      startDate: new FormControl(['', Validators.required]),
      endDate: new FormControl('', [Validators.required]),
      location: new FormControl(''),
      budget: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      status: new FormControl(''),
      type: new FormControl(),
      resource: new FormControl(),

    })

    this.name = new FormControl('', Validators.required),
     this.remark = new FormControl()
     this.preschool = new FormControl('', Validators.required)

  }

  getPtrId(sh) {
    this.getResources();
    console.log("short", sh);
    this.parentalDetail = sh;
    let date1: String = sh.startDate;
    let date2: String = sh.endDate;
    let dt1 = date1.split(" ");
    let dt2 = date2.split(" ");
    this.d1 = dt1[0];
    this.d2 = dt2[0];
    console.log("d1", this.d1);
    this.editParentSetting.show();
  }

  loadToGrid() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getParentalAwareness";
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.parentalModel = res.responseData;

      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.parentalModel)
    })
  }

  getProgramIdParticipant(programId) {

    this.programId = programId;
    // this.route.navigate(['/registered-participants',{programName:'parentalProgram',programId:this.programId}])
    this.route.navigate(['/parental-participant', { programId: programId }]);
    console.log("this.programId ", this.programId);
    // this.loadParticipants();
    //this.viewParticipantSetting.show();
    console.log("parental", programId);
  }
  openCreatePwr() {

    this.getResources()
    this.programData = new ProgramData()
    this.rType = 1;
    this.programData.program.status = 1
    this.programData.program.startDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.programData.program.endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.selectedResourcePersonList = new Array<ResourcePerson>();
    this.makeParentSetting.show();
  }

  getResources() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources";
    this.commonService.processGet(url).subscribe(res => {
      if (res.responseCode == 1) {

        this.resModel = res.responseData;
      }

    }, error => {

      console.log("error ", error);

    })
  }

  createIns() {
    if(this.bisId==2){
      this.programData.program.type=1
    }else {
      this.programData.program.type=2
    }
    
    this.programData.userInserted = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveParentalPrograms", this.programData).subscribe(res => {

     
      if (res.responseCode == 1) {
        this.success = true;

        this.loadToGrid()

        setTimeout(() => {
         this.makeParentSetting.hide()
          this.success = false
        }, 2000)
      }else{
        this.error = true
        setTimeout(() => {
        this.error = false
         }, 2000)
      }

    }), error => {
      console.log("ERROR")
      this.error = true
        setTimeout(() => {
        this.error = false
         }, 2000)
    }
    // }
  }

 
  // resetFields(){
  //   this.resourceDetail.=""
  //   this.resourceDetail.=""
  //   this.resourceDetail.address=""
  //   this.resourceDetail.address=""
  //   this.resourceDetail.contactPerson=""
  //   this.resourceDetail.contactNo=""
  //   this.resourceDetail.status=""
  // }



  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;
    if (event.node.type == "province") {

    }
    else if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.code, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.code, event.node.type)
    }

    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }


  nodeSelected(event) {
    this.getAllLearningCentersbyId(event.node.districtId, event.node.code, event.node.type)
    this.showLearningCenters.show()
    this.collapseAll()
  }



  getAllLearningCenters() {
    this.selectedLearningCenter = new Data()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCenters").subscribe(res => {

      this.learningCenterList = res.responseData;

      if (this.selectedLearningCenter.sn == null) {
        this.selectedLearningCenter = this.learningCenterList[0]
      }

    }), error => {
      console.log("ERROR")
    }
  }

  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
       node1.children.push(node)
      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
       node1.children.push(node)
      }

      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  getAllLocations() {

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData
         for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }
  getAllLearningCentersbyId(id1, id2, type) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id1=" + id1 + "&id2=" + id2 + "&type=" + type).subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;

        this.selectedLearningCenter = this.learningCenterList[0]


      }


    }), error => {
      console.log("ERROR")
    }
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }
  openAddParticipant(a) {
    this.programId = a
    console.log("A", this.programId)

    this.addParticipantView.show()

  }

  addResourcePersons() {


    if (this.rType == 1 && this.selectedResource != null) {
      let person = new ResourcePerson()
      person.personId = this.selectedResource.id
      person.personName = this.selectedResource.resourceName
      person.coordinator = 0
      this.programData.resourcePool.push(person)
      this.selectedResourcePersonList.push(person)
    }
    else if (this.rType == 2 && this.selectedOtherResource != null) {

      let person = new ResourcePerson()

      person.personName = this.selectedOtherResource.personName
      person.remarks = this.selectedOtherResource.remarks
      person.coordinator = 0
      this.programData.guestList.push(person)
      this.selectedResourcePersonList.push(person)

      this.selectedResource.personName = ""
      this.selectedResource.remarks = ""

    }
  }

  removeRows(person) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool.splice(index2, 1)
    }
    if (index3 > -1) {
      this.programData.guestList.splice(index3, 1)
    }



  }

  removeRowswhenEdit(person: ResourcePerson) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool[index2].status = 2
    }
    if (index3 > -1) {
      this.programData.guestList[index3].status = 2
    }



  }

  checked(e, person) {

    console.log("Per", person)
    console.log("sdsdsd", this.programData.resourcePool)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    console.log("index", index2)
    console.log("in", index3)
    if (index2 > -1) {
      this.programData.resourcePool[index2].coordinator = 1
    }
    if (index3 > -1) {

      this.programData.guestList[index3].coordinator = 1
    }
  }


}
