import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { CommonWebService } from '../common-web.service';
import { Documents } from '../model/document';
import { API_URL } from '../app_params';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

@Component({
  selector: 'app-define-document',
  templateUrl: './define-document.component.html',
  styleUrls: ['./define-document.component.css'],
  providers: [CommonWebService, Documents]
})
export class DefineDocumentComponent implements OnInit {
  @ViewChild("createDocument") public createDocument: ModalDirective;
  @ViewChild("updateDocument") public updateDocument: ModalDirective;

  public document = new Documents();
  public documentsList = new Array<Documents>()
  public success: boolean
  public warning: boolean
  public error: boolean

  public documentForm: FormGroup
  constructor(public commonService: CommonWebService) {
    this.validation()
  }

  ngOnInit() {
    this.loadAllDocuments();
  }

  validation() {

    this.documentForm = new FormGroup({
      docId: new FormControl(),
      docName: new FormControl(['', Validators.required]),
      status: new FormControl()

    })
  }
  loadAllDocuments() {
    this.commonService.processGet(API_URL+"ECD-ActivityMaintenance-1.0/service/Activities/getAllDocuments").subscribe(res => {

      this.documentsList = res.responseData


    }), error => {
      console.log("ERROR")
    }
  }
  openCreateDocument() {
    this.document = new Documents()
    this.document.status = 1
    this.createDocument.show()
  }
  editDocument(a) {
    let doc = new Documents()
    doc.documentId=a.documentId
    doc.documentName=a.documentName
    doc.status=a.status
    this.document = doc
    this.updateDocument.show()
  }

  saveDocument(f: NgForm) {
    if (f.valid) {
      this.commonService.processPost(API_URL+"ECD-ActivityMaintenance-1.0/service/Activities/insertDocument", this.document).subscribe(res => {


        if (res.responseCode == 1) {
          this.success=true
          setTimeout(() => {
            this.success = false;
            this.document.documentName = ""

            this.document.status = 1
            this.loadAllDocuments()
          }, 2000);

        }

      }), error => {
        console.log("ERROR")
      }
    }

  }
  closeUpdateAction() {
    this.updateDocument.hide()
  }

  updateDocumentAction(f: NgForm) {

    let doc = new Documents()
    doc.documentId = this.document.documentId
    doc.documentName = this.document.documentName
    doc.status = this.document.status
    if (f.valid) {
      this.commonService.processPost(API_URL+"ECD-ActivityMaintenance-1.0/service/Activities/updateDocument", doc).subscribe(res => {




        if (res.responseCode == 1) {
          this.success=true
          setTimeout(() => {
            this.success = false;
            this.loadAllDocuments()
          }, 1000);


        }

      }), error => {
        console.log("ERROR")
      }

    }
  }
  closeAction() {
    this.createDocument.hide()
    this.success = false
  }



}

