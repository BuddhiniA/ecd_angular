import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineDocumentComponent } from './define-document.component';

describe('DefineDocumentComponent', () => {
  let component: DefineDocumentComponent;
  let fixture: ComponentFixture<DefineDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
