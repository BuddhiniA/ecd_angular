import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeWaiverPaymentUpdateComponent } from './fee-waiver-payment-update.component';

describe('FeeWaiverPaymentUpdateComponent', () => {
  let component: FeeWaiverPaymentUpdateComponent;
  let fixture: ComponentFixture<FeeWaiverPaymentUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeeWaiverPaymentUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeeWaiverPaymentUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
