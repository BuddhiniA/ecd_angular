import { Component, OnInit } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';

@Component({
  selector: 'app-fee-waiver-payment-update',
  templateUrl: './fee-waiver-payment-update.component.html',
  styleUrls: ['./fee-waiver-payment-update.component.css']
})
export class FeeWaiverPaymentUpdateComponent implements OnInit {

  
  endDate: any;
  startDate: any;
  dsList: any;
  selectedDistrict: any;
  districtList: any;
  selectedDs:any
  constructor(public commonService: CommonWebService) { }

  ngOnInit() {
    this.getAllDistrict()
  }

  getAllDistrict() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getDistrictList?id=0").subscribe(res => {

      if (res.responseCode == 1) {
        this.districtList = res.responseData
        this.selectedDistrict = this.districtList[0]
        if(this.selectedDistrict!=undefined||this.selectedDistrict!=null){
          this.getAllDS()
        }
        
      }


    }), error => {
      console.log("ERROR")
    }
  }
  getAllDS() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getDSList?id=" + this.selectedDistrict.id).subscribe(res => {

      this.dsList = res.responseData


    }), error => {
      console.log("ERROR")
    }
  }
  searchRecords(){
    console.log("Distr",this.selectedDistrict.id,this.selectedDs.id,this.startDate,this.endDate)
  }
}
