import { Component, OnInit } from '@angular/core';
import { RequestOptionsArgs, Headers, RequestOptions } from "@angular/http";

import { LoggedInUserModel } from "../model/loggedInUserModel";

import { SharedService } from '../shared-service'
import { Router, ChildrenOutletContexts } from "@angular/router";
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  // template:'<router-outlet></router-outlet>',
  styleUrls: ['./login.component.css', '../styles/loaders.css'],
  providers: [LoggedInUserModel, ChildrenOutletContexts,CommonWebService,SharedService]
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: any;

  public btnSignin = true;
  public signinMask = false;

  public loginVisible:boolean;
  public loginFailMsg: string = ""; 
  public groupId;

  constructor(public userData: LoggedInUserModel, public sharedService : SharedService, 
    public webservice: CommonWebService, public router: Router) {
     
      //  localStorage.clear();
      //  sessionStorage.clear();
       
  }

  ngOnInit() {
    
    this.loginVisible=true
  }

  login($event) {
    $event.preventDefault()
    console.log("login.....");

    this.loginFailMsg = '';
    this.btnSignin = false;
    this.signinMask = true;
    this.userData.userId = this.username;
    this.userData.password = this.password;
    
    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("system", "ECDCMS");
    header.set("organization", "ECD");
    header.set("division", "ECDLK");
    header.set("countryCode", "LK");
    header.set("branch", "HeadOffice");
    header.set("department", "DefaultDepartment");
    header.set("room", "DefaultRoom");
    
    option.headers = header;

    // let url = AppParams.BASE_PATH + "user/validateCmsUser";
    let url = API_URL+"ECD-UserManagement-1.0/service/user/validateCmsUser";

    this.webservice.processPostWithHeaders(url, this.userData, option).subscribe(response => {
      //  this.webservice.processGetWithHeaders(url, option).subscribe(res => {

      console.log("response", response.flag);

      
      let responseData = response.flag;

      if (responseData != null) {

        // let flag = responseData['flag'];
        let flag = responseData;
        console.log("success zzz",flag);
        if (flag == 1000) {

          
          this.groupId = response.data[0].groups[0].groupId
          let user  :LoggedInUserModel = response.data;
         
          this.sharedService.setLoggedInUserst(user);

          // sessionStorage.setItem("bisId",);

          sessionStorage.setItem('id_token', btoa(JSON.stringify(user)) );

          sessionStorage.setItem("bisId",user[0].extraParams);
          sessionStorage.setItem("userGroup",this.groupId);
          
          if(user[0].extraParams>3){
            console.log("user>>>>>>>>>")
            this.router.navigate(["/define-activities"],{queryParams:{maintain: 'true',type:'maintain'}});
           
          }else if(user[0].extraParams<4){
            this.router.navigate(['./dashboard']);
          }
          
        } else {
           this.loginFailMsg = response.exceptionMessages[0];
          // this.loginFailMsg = "Fail";
        }
      } else {
        this.loginFailMsg = "Cannot connect with the authentication server";
      }
      this.signinMask = false;
      this.btnSignin = true;

    }, error => {

      this.signinMask = false;
      this.btnSignin = true;

    })
  }


}
