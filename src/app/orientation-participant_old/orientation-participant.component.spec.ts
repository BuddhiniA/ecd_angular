import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrientationParticipantComponent } from './orientation-participant.component';

describe('OrientationParticipantComponent', () => {
  let component: OrientationParticipantComponent;
  let fixture: ComponentFixture<OrientationParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrientationParticipantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrientationParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
