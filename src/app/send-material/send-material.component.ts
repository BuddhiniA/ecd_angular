import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { CommonWebService } from '../common-web.service';
import { Router } from '@angular/router';
import { SendList } from '../model/SendMaterial';
import { PackList } from '../model/PackList';
import { MaterialStock } from '../model/MaterialStock';
import { MaterialPack } from '../model/MaterialPack';
import { Criteria } from '../model/Criteria';
import { API_URL } from '../app_params';
import { ResultDetail } from '../model/ResultDetail';
import { Data } from '../model/Data';
import { ECDOfficer } from '../model/ECDOfficer';
import { OverlayPanel } from 'primeng/components/overlaypanel/overlaypanel';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoggedInUserModel } from '../model/loggedInUserModel';
import { TreeNode } from 'primeng/primeng';

export class BusinessLevel {
  id
  lifeCode
  districtId
  dsID
  english
  type
  code
  provinceId
  nodeType


}
export class Node {
  id
  label
  type
  code
  lifeCode
  districtId
  dsID
  provinceId
  nodeType
  leaf
  children = new Array<Node>()
}

export class SendObject {
  sendId
  description
  packList = new Array<PackListObject>()
  criteriaId
  learningCenter
  learningCenterId
  officerName
  status
  dateInserted
  dateModified
  userInserted
  userModified

}

export class PackListObject {
  sendId
  packId
  materialList = new Array<any>()
  quantity
  status
}

export class MaterialObject {
  materialId
  materialName
  status
  quantity
  userInserted
  userModified
  dateInserted
  dateModified
}
@Component({
  selector: 'app-send-material',
  templateUrl: './send-material.component.html',
  styleUrls: ['./send-material.component.css'],
  providers: [CommonWebService, SendList, PackList, LoggedInUserModel]
})

export class SendMaterialComponent implements OnInit {
  selectedPack: MaterialPack;
  @ViewChild("createSendList") public createSendList: ModalDirective;
  @ViewChild("updateSendList") public updateSendList: ModalDirective;
  @ViewChild("showSendList") public showSendList: ModalDirective;
  @ViewChild("viewMaterialList") public viewMaterialList: ModalDirective;
  @ViewChild("showLearningCenters") public showLearningCenters: ModalDirective;

  constructor(public commonService: CommonWebService, public route: Router, public userData: LoggedInUserModel) {
    this.validation()
  }

  public listOfsendLists = new Array<SendList>()
  public sendList = new SendList()

  public packList = new Array<PackList>()
  public loacalpackList = new Array<PackList>()
  public pack = new PackList()
  public materialList = new Array<MaterialStock>()
  public material = new MaterialStock()
  public checked

  public materialPackList = new Array<MaterialPack>()
  public materialPackListlocal = new Array<MaterialPack>()
  public materialPackListlocal1 = new Array<MaterialPack>()

  public selectedMaterialPack
  public criteriaList = new Array<Criteria>()
  public selectedCriteria = new Criteria();
  public previouslySelectedNode = new Node()
  public learningCenterList = new Array<Data>()
  public learningCenterListbyCriteria = new Array<Data>()

  public selectedearningCenterListbyCriteria

  public selectedSendList = new SendList();

  public ecdOfficer = new ECDOfficer()
  public officerList = new Array<ECDOfficer>()
  public selectedOfficer = new ECDOfficer()
  public selectedRow = new SendList();

  public materialStockforaPack = new MaterialStock()
  public quantity
  addSendListForm: FormGroup;

  updateSendListForm: FormGroup;
  public success: boolean
  public error: boolean
  public warning: boolean

  public packListtobesaved = new Array<PackListObject>()
  public materialstobesaved = new Array<MaterialObject>()
  public sendObjecttobesaved = new SendObject()
  public sendObjecttobeUpadted = new SendObject()

  public materialListforSHow = new Array<any>()
  public sendListStatus
  public index = 0
  files = new Array<Node>()
  public businessList = new Array<BusinessLevel>()
  public selectedNode = new Node();
  public selectedLearningCenter = new Data()
  public selectedMedium;
  public mediumList = new Array<any>()
  ngOnInit() {
    this.getAllLocations()
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.sendListStatus = 1
    this.selectedMedium = 1


    this.loadAllsendMaterials();

    // this.getAllLearningCenters()
  }
  cloasAction() {
    // this.createSendList.hide()

  }


  openCreateSendList() {

    this.sendObjecttobesaved = new SendObject()
    // this.route.navigate(["/createnewsendlist"])
    this.materialPackListlocal = new Array<MaterialPack>()
    this.materialListforSHow = new Array<any>()
    this.loadAllMaterialPacks()
    this.getAllOfficers();
    this.checked = 1
    console.log("checked", this.checked)
    this.createSendList.show()
  }
  validation() {
    this.addSendListForm = new FormGroup({
      description: new FormControl(['', Validators.required]),

      // quantity0: new FormControl(['', Validators.required, Validators.pattern("^[0-9]*[1-9][0-9]*$")]),
      packname: new FormControl(),
      learningcenter1: new FormControl(),
      learningcenter2: new FormControl(),
      ecdofficer: new FormControl(),
      status: new FormControl(),
      criteria: new FormControl(),
      radio1: new FormControl(),
      radio2: new FormControl(),
      medium: new FormControl()
    })

    this.updateSendListForm = new FormGroup({
      description: new FormControl(['', Validators.required]),

      // quantity0: new FormControl(['', Validators.required, Validators.pattern("^[0-9]*[1-9][0-9]*$")]),
      packname: new FormControl(),
      learningcenter1: new FormControl(),
      learningcenter2: new FormControl(),
      ecdofficer: new FormControl(),
      status: new FormControl(),
      criteria: new FormControl(),
      radio1: new FormControl(),
      radio2: new FormControl()
    })
  }
  getAllOfficers() {

    this.commonService.processGet(API_URL + "ECD-UserManagement-1.0/service/user/getAllOfficers").subscribe(res => {
      //this.commonService.processGet("./assets/criterialist.json").subscribe(res=>{
      this.officerList = res.responseData

      this.selectedOfficer = this.officerList[0]
    }), error => {
      console.log("ERROR")
    }
  }

  loadAllCriteraList() {

    this.selectedLearningCenter = new Data()
    this.learningCenterList = new Array<Data>()

    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllCriteriabyType?type=3").subscribe(res => {

      if (res.responseCode == 1) {
        this.criteriaList = res.responseData

        this.selectedCriteria = this.criteriaList[0]
        if (this.selectedCriteria != undefined) {
          this.loadPreSchools()
        
        }

      }








    }), error => {
      console.log("ERROR")
    }
  }




  loadPreSchools() {


 
    let url = API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllCriteriaLearningCenters?criteriaId=" + this.selectedCriteria.id;
    this.commonService.processGet(url).subscribe(res => {
  
      if (res.responseCode == 1) {

        this.learningCenterList = res.responseData;
      
        if(this.learningCenterList.length!=0){
          
          this.selectedLearningCenter = this.learningCenterList[0]
          if (this.selectedLearningCenter != undefined) {
    
         
          }else{
            this.selectedLearningCenter != null
          }
        }else{
          this.selectedLearningCenter.name=""
        }
        

      

      }

    }, error => {

      console.log("error ", error);

    })

  }

  getAllLearningCenters() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCenters").subscribe(res => {

      

      // if (this.selectedLearningCenter.sn == null) {
        if(res.responseCode==1){
          this.learningCenterList = res.responseData;
          this.selectedLearningCenter = this.learningCenterList[0]
        }
     
      // }

    }), error => {
      console.log("ERROR")
    }
  }
  getAllLearningCentersbyCriteteria(criteria) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllCriteriaLearningCenters?criteriaId=" + this.selectedCriteria.id).subscribe(res => {

      this.learningCenterListbyCriteria = res.responseData;
      console.log("Leaerning Centers bu Criteria", this.learningCenterListbyCriteria)

    }), error => {
      console.log("ERROR")
    }
  }
  // getAllLearningCentersbyId(id, type) {
  //   this.learningCenterList = new Array<Data>()
  //   this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id=" + id + "&type=" + type).subscribe(res => {

  //     this.learningCenterList = res.responseData;
      
  //     this.selectedLearningCenter = this.learningCenterList[0]

  //   }), error => {
  //     console.log("ERROR")
  //   }
  // }

  loadAllMaterialPacks() {
    this.commonService.processGet(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllMaterialPacks").subscribe(res => {

      this.materialPackList = res.responseData

      this.selectedMaterialPack = this.materialPackList[0]
    }), error => {
      console.log("ERROR")
    }
  }

  loadAllsendMaterials() {

    this.commonService.processGet(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllSendMaterials").subscribe(res => {

      this.listOfsendLists = res.responseData

    }), error => {
      console.log("ERROR")
    }
  }
  showsendList(a) {
    this.selectedRow = a
    console.log("SendList", this.selectedRow)



    this.loadMaterialPAcks()
    this.showSendList.show()

  }
  selectCar(a) {

    console.log("CAR ID", a.packId)
    this.selectedPack = a;
    console.log("CAR ID", this.selectedPack)
    this.showMaterials(a.packId)


  }
  selectPack(event, pack: MaterialPack, overlaypanel: OverlayPanel) {
    this.selectedPack = pack;
    console.log("overlap", overlaypanel)
    overlaypanel.toggle(event);
  }
  updatesendList(a) {
    this.selectedRow = a
    this.getAllOfficers();
    console.log("SendList", this.selectedRow)
    this.sendObjecttobeUpadted.description = a.description

    for (let b of this.learningCenterList) {
      if (b.name == a.learningCenter) {
        this.selectedLearningCenter = b
      }
    }
    for (let c of this.officerList) {
      if (c.fullName == a.officerName) {
        this.selectedOfficer = c
      }
    }


    this.sendListStatus = a.status
    this.selectedOfficer = a.officerName
    this.loadAllMaterialPacks()

    this.loadMaterialPAcksUpdate()
    console.log("SELECTED LEARNNG", this.selectedLearningCenter)
    this.updateSendList.show()
  }
  radioAction() {
    console.log("1", this.checked)
  }

  loadMaterialPAcks() {
    this.commonService.processGet(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllRecievedMaterialPacks?sendId=" + this.selectedRow.sendId).subscribe(res => {
      //this.commonService.processGet("./assets/criterialist.json").subscribe(res=>{
      this.materialPackList = res.responseData
      console.log("Update Material", this.materialPackList)
    }), error => {
      console.log("ERROR")
    }
  }

  loadMaterialPAcksUpdate() {
    this.commonService.processGet(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllRecievedMaterialPacks?sendId=" + this.selectedRow.sendId).subscribe(res => {
      //this.commonService.processGet("./assets/criterialist.json").subscribe(res=>{
      this.materialPackListlocal1 = res.responseData
      console.log("Update Material", this.materialPackList)
    }), error => {
      console.log("ERROR")
    }
  }


  changeAction(selectedCriteria) {
    this.selectedCriteria = selectedCriteria
    this.getAllLearningCentersbyCriteteria(this.selectedCriteria);

  }

  addPack() {


    console.log("PACK ", this.selectedMaterialPack)
    // this.showMaterials(this.selectedMaterialPack.packId)

    //this.quantity=0
    console.log("PACKS", this.pack)
    this.packList.push(this.selectedMaterialPack)

    // console.log("SELECTED MATERIAL PACK",this.selectedMaterialPack.materialData)
    let singlePackLst = new PackListObject();

    for (let a of this.selectedMaterialPack.materialData) {
      let obj = new MaterialObject()
      obj.materialId = a.materialId
      obj.materialName = a.materialName
      obj.quantity = a.quantity
      obj.status = a.status
      singlePackLst.materialList.push(obj)

    }
    console.log("SELECTED MATERIAL PACK", singlePackLst.materialList)
    singlePackLst.packId = this.selectedMaterialPack.packId
    singlePackLst.quantity = this.selectedMaterialPack.quantity
    singlePackLst.status = this.selectedMaterialPack.status

    this.sendObjecttobesaved.packList.push(singlePackLst)
    console.log("SINGLE PACK LIST", this.sendObjecttobesaved.packList)
    // this.packListtobesaved.push(this.selectedMaterialPack)
    this.materialPackListlocal.push(this.selectedMaterialPack)

    // this.addSendListForm.addControl('quantity' + this.index, new FormControl('', [Validators.required, Validators.pattern("^[0-9]*[1-9][0-9]*$")]))
    this.index++

  }

  addPackforUpdate() {


    console.log("PACK ", this.selectedMaterialPack)
    this.showMaterials(this.selectedMaterialPack.packId)

    //this.quantity=0
    console.log("PACKS", this.pack)
    this.packList.push(this.selectedMaterialPack)

    // console.log("SELECTED MATERIAL PACK",this.selectedMaterialPack.materialData)
    let singlePackLst = new PackListObject();

    for (let a of this.selectedMaterialPack.materialData) {
      let obj = new MaterialObject()
      obj.materialId = a.materialId
      obj.materialName = a.materialName
      obj.quantity = a.quantity
      obj.status = a.status
      singlePackLst.materialList.push(obj)

    }
    console.log("SELECTED MATERIAL PACK", singlePackLst.materialList)
    singlePackLst.packId = this.selectedMaterialPack.packId
    singlePackLst.quantity = this.selectedMaterialPack.quantity
    singlePackLst.status = this.selectedMaterialPack.status

    this.sendObjecttobeUpadted.packList.push(singlePackLst)
    console.log("SINGLE PACK LIST", this.sendObjecttobesaved.packList)
    // this.packListtobesaved.push(this.selectedMaterialPack)
    this.materialPackListlocal.push(this.selectedMaterialPack)

    this.updateSendListForm.addControl('quantity' + this.index, new FormControl('', [Validators.required, Validators.pattern("^[0-9]*[1-9][0-9]*$")]))
    this.index++

  }





  inputing() {
    console.log("INPUT")
  }
  addQuantity(a) {
    console.log("INPUT")
    if (this.quantity != null) {
      this.pack.quantity = this.quantity
      var index = this.packListtobesaved.indexOf(a)
      console.log("INDEZ AND MODEL", index, this.packListtobesaved[index])
      if (this.packListtobesaved[index] != null) {

        this.packListtobesaved[index].quantity = this.quantity
      }


    } else {
      this.pack.quantity = 0
    }
    console.log("QUANTITY", this.pack.quantity)
  }
  showMaterials(packId) {

    this.commonService.processGet(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getMaterialsByPackId?packId=" + packId).subscribe(res => {
      //this.commonService.processGet("./assets/criterialist.json").subscribe(res=>{
      this.materialListforSHow = res.responseData
      console.log("MAterial Pack to show", this.materialListforSHow)
    }), error => {
      console.log("ERROR")
    }

  }
  removeRow(a) {
    var index = this.materialPackListlocal.indexOf(a);
    this.materialPackListlocal.splice(index, 1);
    this.materialListforSHow = new Array<any>()

  }

  addSendList() {

    this.sendObjecttobesaved.learningCenterId = this.selectedLearningCenter.sn
    this.sendObjecttobesaved.officerName = this.selectedOfficer.userId
    this.sendObjecttobesaved.userInserted = this.userData[0].userId
    this.sendObjecttobesaved.criteriaId = this.selectedCriteria.criteriaId
    this.sendObjecttobesaved.status = 2

    for (let i = 0; i < this.materialPackListlocal.length; i++) {
      this.sendObjecttobesaved.packList[i].packId = this.materialPackListlocal[i].packId
      this.sendObjecttobesaved.packList[i].quantity = this.materialPackListlocal[i].quantity
      this.sendObjecttobesaved.packList[i].status = 1
      // for(let j=0;j<this.materialPackListlocal[i].materialData.length;j++){
      //   this.sendObjecttobesaved.packList[i].materialList[j]=this.materialPackListlocal[i].materialData
      // }
    }
    console.log("PayLoad", this.sendObjecttobesaved)


    this.commonService.processPost(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/addSendMaterialPack", this.sendObjecttobesaved).subscribe(res => {


      if (res.responseCode == 1) {
        this.success = true;
        console.log("Success")
        this.success = true;
        console.log("Success")
        setTimeout(() => {
          this.success = false;
          this.sendObjecttobesaved.description = ""
          this.materialPackListlocal = null
          this.loadAllsendMaterials()
        }, 2000);

      }

    }), error => {
      console.log("ERROR")
    }
  }

  updateSendListObj() {

    console.log("Updateing")
    this.sendObjecttobeUpadted.sendId =
      this.sendObjecttobeUpadted.learningCenterId = this.selectedLearningCenter.sn
    this.sendObjecttobeUpadted.officerName = this.selectedOfficer.userId
    this.sendObjecttobeUpadted.userInserted = "Danushka"
    this.sendObjecttobeUpadted.criteriaId = this.selectedCriteria.criteriaId
    this.sendObjecttobeUpadted.status = this.sendListStatus
    console.log("PayLoad", this.sendObjecttobesaved)


    this.commonService.processPost(API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/updateSendMaterialPack", this.sendObjecttobeUpadted).subscribe(res => {


      if (res.responseCode == 1) {
        this.success = true;
        console.log("Success")
        this.success = true;
        console.log("Success")
        setTimeout(() => {
          this.success = false;

          this.loadAllsendMaterials()
        }, 2000);

      }

    }), error => {
      console.log("ERROR")
    }
  }



  cloasCreateAction() {
    this.createSendList.hide()
  }
  collapse(evt) {
    console.log("COLLAPSE", evt)
  }
  expand(event) {


    this.previouslySelectedNode.id = this.selectedNode.id
    this.previouslySelectedNode.label = this.selectedNode.label
    this.previouslySelectedNode.type = this.selectedNode.type
    this.previouslySelectedNode.code = this.selectedNode.code
    this.previouslySelectedNode.districtId = this.selectedNode.districtId
    this.previouslySelectedNode.dsID = this.selectedNode.dsID
    this.previouslySelectedNode.provinceId = this.selectedNode.provinceId
  }
  nodeSelect(event) {

    let district = "" + event.node.provinceId + event.node.code;


    if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.code, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.code, event.node.type)
    }


    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }
  }
  addChildren(node1) {

    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)





      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {
          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
      // if (node1.type == "district") {
      //   let district = "" + node1.provinceId + node1.code;
      //   console.log("distrit", district);
      //   if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
      //     let node = new Node()
      //     node.id = this.businessList[i].id
      //     node.label = this.businessList[i].english
      //     node.type = this.businessList[i].type
      //     node.code = this.businessList[i].code
      //     node.districtId = this.businessList[i].districtId
      //     node.dsID = this.businessList[i].dsID
      //     node.provinceId = this.businessList[i].provinceId
      //    node1.children.push(node)
      //     this.getAllLearningCenters()
      //     console.log("SELECTED NODE", this.selectedNode)
      //   }
      // }
      // if (node1.type == "ds") {
      //   if (node1.districtId == this.businessList[i].districtId && node1.code == this.businessList[i].dsID && this.businessList[i].type == "gnd") {
      //     let node = new Node()
      //     node.id = this.businessList[i].id
      //     node.label = this.businessList[i].english
      //     node.type = this.businessList[i].type
      //     node.code = this.businessList[i].code
      //     node.districtId = this.businessList[i].districtId
      //     node.dsID = this.businessList[i].dsID
      //     node.provinceId = this.businessList[i].provinceId
      //    node1.children.push(node)
      //     console.log("SELECTED NODE", this.selectedNode)
      //     this.getAllLearningCenters()
      //   }
      // }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  getAllLocations() {

    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.businessList = res.responseData
        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }

        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }

  onRowSelect(evt) {
   
      this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getMedium?sn="+this.selectedLearningCenter.sn).subscribe(res => {
       if(res.responseCode==1){
          this.mediumList = res.responseData
          for(let a of this.mediumList){
            var inde = this.mediumList.indexOf(a)
            if(a==1){
                this.mediumList[inde] = "Sinhala"
            }else if(a==2){
              this.mediumList[inde] = "Tamil"
            }else if(a==3){
              this.mediumList[inde] = "English"
            }else if(a=='#NULL!'){
              this.mediumList.splice(inde,1)
            }
          }
          this.selectedMedium = this.mediumList[0]
          console.log("Medium List",this.mediumList)
          console.log("Medium",this.selectedMedium)

       }
      }), error => {
        console.log("ERROR")
      }
    
  }
  
  changeFIG(evt){
    this.learningCenterList = new Array<Data>()
    this.selectedLearningCenter = new Data()
    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getFIGSelected").subscribe(res => {
      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData

        this.selectedLearningCenter = this.learningCenterList[0]
      
        this.showLearningCenters.show()
      }

    }), error => {
      console.log("ERROR")
    }
  }

  nodeSelected(event) {
    this.getAllLearningCentersbyId(event.node.districtId, event.node.code, event.node.type)
    this.showLearningCenters.show()
    this.collapseAll()
  }

  getAllLearningCentersbyId(id1, id2, type) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id1=" + id1 + "&id2=" + id2 + "&type=" + type).subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;

        this.selectedLearningCenter = this.learningCenterList[0]

    
      }


    }), error => {
      console.log("ERROR")
    }
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  
}