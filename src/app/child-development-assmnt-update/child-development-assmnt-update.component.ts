import { Component, OnInit, ViewChild } from '@angular/core';
import { ChildAssmntModel } from '../model/ChildAssmnt';

import { AssesmntStudentModel } from '../model/AssesmntStudent';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { Data } from '../model/Data';
import { ModalDirective } from 'ngx-bootstrap';
import { TreeNode } from 'primeng/primeng';
import { DatePipe } from '@angular/common';
import { Student } from '../fee-waiver-payment/fee-waiver-payment.component';

@Component({
  selector: 'app-child-development-assmnt-update',
  templateUrl: './child-development-assmnt-update.component.html',
  styleUrls: ['./child-development-assmnt-update.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class ChildDevelopmentAssmntUpdateComponent implements OnInit {
  assesmentId: any;
  type: number;
  studentForm: FormGroup;
  title: string;
  userId: any;
  userData: any;
  selectedLearningCenter = new Data();
  learningCenterList: any[];
  selectedNode: Node = new Node();
  files: Node[];
  businessList: any[];
  loadingTree: boolean;

  public childassmntList = Array<ChildAssmntModel>();
  public childassmntModel = new ChildAssmntModel;
  public studentList = Array<AssesmntStudentModel>();
  public studentModel = new AssesmntStudentModel;
  public tempstudentModel = new AssesmntStudentModel;
  public childForm: FormGroup
  public success
  public error
  public warn

  @ViewChild("creatChildAssmnt") public creatChildAssmnt;
  @ViewChild("updateChildAssmnt") public updateChildAssmnt;
  @ViewChild("studentModal") public studentModal;
  @ViewChild("addstudentModel") public addstudentModel;
  @ViewChild("showLearningCenters") public showLearningCenters: ModalDirective;
  constructor(public commonService: CommonWebService, private datePipe: DatePipe) {
    this.validation();
  }

  ngOnInit() {
    this.getUser()
    this.getAllLocations()
    this.loadToGrid();
  }

  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.userId = this.userData[0].userId
  }

  validation() {

    this.childForm = new FormGroup({
      assessDate: new FormControl(['', Validators.required]),
      description: new FormControl(['', Validators.required]),
      nameOfAssessor: new FormControl(['', Validators.email]),
      learningCenter: new FormControl('', [Validators.required]),
      noOfStudents: new FormControl('', Validators.required),
      noOfAssessedStudents: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required)

    })
    this.studentForm = new FormGroup({
      id: new FormControl(['', Validators.required]),
      name: new FormControl(['', Validators.required]),
      score: new FormControl(['', Validators.email]),
      status: new FormControl('', Validators.required)
     

    })
  }

  openCreateAssessment() {
    this.childassmntModel = new ChildAssmntModel()
    this.childassmntModel.assessDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.childassmntModel.status = 1
    this.creatChildAssmnt.show();

  }

  getAssmnt(assesment:ChildAssmntModel) {

    this.childassmntModel = new ChildAssmntModel();
    this.childassmntModel.assessDate =  assesment.assessDate
    this.childassmntModel.assessmentId = assesment.assessmentId
    this.childassmntModel.description = assesment.description
    this.childassmntModel.learningCenter = assesment.learningCenter
    this.childassmntModel.learningCenterName = assesment.learningCenterName
    this.childassmntModel.nameOfAssessor = assesment.nameOfAssessor
    this.childassmntModel.noOfAssessedStudents = assesment.noOfAssessedStudents
    this.childassmntModel.noOfStudents =assesment.noOfStudents
    this.childassmntModel.status=assesment.status
    this.updateChildAssmnt.show();
  }
  getIdforStd(aid) {

    this.studentModel.assessemntId = aid;
    this.assesmentId= aid
    this.getAssesmntStudent(aid);
    this.studentModal.show();
   
  }
  getAssesmntStudent(aid) {
    this.studentModal.assessemntId = aid;
   
    let url = API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getAllAssessmentStudents?assessmentId=" + this.studentModal.assessemntId;
    this.commonService.processGet(url).subscribe(res => {

      
      if (res.responseCode == 1) {

        this.studentList = res.responseData;
        console.log("response data ", this.studentList)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.childassmntList)
    })
  }
  getSid(s) {
    console.log("s", s);
    this.studentModel = s;
    console.log("this.tempstudentModel", this.studentModal)
    this.addstudentModel.show();
  }
  updateStudent() {
   
    this.studentModel.userModified = this.userData[0].userId
    this.studentModel.assessemntId = this.assesmentId
    this.commonService.processPost(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updateAssessmentStudents", this.studentModel).subscribe(res => {
     if (res.responseCode == 1) {
         this.success = true;
         setTimeout(() => {
          this.success = false
          this.addstudentModel.hide();
        }, 3000)
      }
   }), error => {
      console.log("ERROR")
    }
  }
  loadToGrid() {
    let url = API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getAllChildAssessments";
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.childassmntList = res.responseData;

      }

    }, error => {

      console.log("error ", error);

    })
  }

  createChildAssessmnt() {

    this.childassmntModel.userInserted = this.userId
    this.childassmntModel.learningCenter = this.selectedLearningCenter.sn
    this.commonService.processPost(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/insertChildAssessments", this.childassmntModel).subscribe(res => {
      if (res.responseCode == 1) {
        this.success = true
        setTimeout(() => {
          this.success = false
          this.loadToGrid()
          this.creatChildAssmnt.hide()
        }, 3000)
      } else {
        this.error = true
        setTimeout(() => {
          this.error = false;

        }, 3000)
      }

    }), error => {
      this.error = true
      setTimeout(() => {
        this.error = false;

      }, 3000)

    }

  }

  updateChildAssessmnt() {
    console.log("UPDATED instituteDetail MODEL", this.childassmntModel)
    this.childassmntModel.userModified = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updateChildAssessments", this.childassmntModel).subscribe(res => {

      console.log("Sucess?", res.responseCode)
      if (res.responseCode == 1) {
        this.success = true

        setTimeout(() => {
          this.loadToGrid();
          this.success = false
        }, 3000)
      } else {
        this.error = true;
        setTimeout(() => {
          this.error = false;
        }, 3000)
      }

    }), error => {
      this.error = true;
      setTimeout(() => {
        this.error = false;
      }, 3000)
    }
  }

  // resetFields() {
  //   this.childassmntModel.assessDate = ""
  //   this.childassmntModel.description = ""
  //   this.childassmntModel.nameOfAssessor = ""
  //   this.childassmntModel.learningCenter = ""
  //   this.childassmntModel.noOfStudents = ""
  //   this.childassmntModel.noOfAssessedStudents = ""
  // }

  getAllLocations() {

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)
      }


    }), error => {
      console.log("ERROR")
    }
  }


  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()

        node1.children.push(node)






      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }


  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;
    if (event.node.type == "province") {

    }
    else if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.code, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.code, event.node.type)
    }

    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }

  getAllLearningCentersbyId(id1, id2, type) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id1=" + id1 + "&id2=" + id2 + "&type=" + type).subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;

        if (this.learningCenterList.length != 0) {
          this.selectedLearningCenter = this.learningCenterList[0]
        }


      }


    }), error => {
      console.log("ERROR")
    }
  }

  nodeSelected(event) {
    this.getAllLearningCentersbyId(event.node.districtId, event.node.code, event.node.type)
    this.showLearningCenters.show()
    this.collapseAll()
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }


  openCreateStudent(type,student){
    if(type==1&&student==null){
      this.type=1
      this.title = "Add New Student"
      this.studentModel = new AssesmntStudentModel()
      this.studentModel.status=1
      this.addstudentModel.show()
    }else if(type==2){
      this.type=2
      this.studentModel = new AssesmntStudentModel()
      this.studentModel.studentId = student.studentId
      this.studentModel.studentName = student.studentName
      this.studentModel.status = student.status
      this.studentModel.score= student.score
     
      this.title= "Update the Student"
      this.addstudentModel.show()
    }
    
  }

  createStudent(e){
    this.studentModel.userInserted = this.userData[0].userId
    this.studentModel.assessemntId = this.assesmentId
    this.commonService.processPost(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/insertAssessmentStudents", this.studentModel).subscribe(res => {
      if (res.responseCode == 1) {
        this.success = true
        setTimeout(() => {
          this.success = false
          this.getAssesmntStudent(this.assesmentId)
          this.addstudentModel.hide()
        }, 3000)
      } else {
        this.error = true
        setTimeout(() => {
          this.error = false;

        }, 3000)
      }

    }), error => {
      this.error = true
      setTimeout(() => {
        this.error = false;

      }, 3000)

    }

  }

  
}
