import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChildDevelopmentAssmntUpdateComponent } from './child-development-assmnt-update.component';

describe('ChildDevelopmentAssmntUpdateComponent', () => {
  let component: ChildDevelopmentAssmntUpdateComponent;
  let fixture: ComponentFixture<ChildDevelopmentAssmntUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChildDevelopmentAssmntUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChildDevelopmentAssmntUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
