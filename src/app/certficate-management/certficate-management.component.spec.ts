import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertficateManagementComponent } from './certficate-management.component';

describe('CertficateManagementComponent', () => {
  let component: CertficateManagementComponent;
  let fixture: ComponentFixture<CertficateManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertficateManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertficateManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
