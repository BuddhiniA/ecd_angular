import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { Certificate } from '../model/Certificate';
import { ModalDirective } from 'ngx-bootstrap';
import { ModalData } from '../model/ModalData';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-certficate-management',
  templateUrl: './certficate-management.component.html',
  styleUrls: ['./certficate-management.component.css'],
  providers: [CommonWebService]
})
export class CertficateManagementComponent implements OnInit {

  @ViewChild("viewCertificate") public viewCertificate: ModalDirective;

  public certificate;
  public certficateList;
  public selectedCertificate

  public modelData = new ModalData();
  public viewParam

  public success;
  public error
  public warn
  public loading=true

  createCertificate: FormGroup;
  constructor(public commonService: CommonWebService) {
    this.validation()
  }

  validation() {
    this.createCertificate = new FormGroup({
      name: new FormControl([Validators.required]),
      status: new FormControl([Validators.required])
    })
  }
  ngOnInit() {

    this.getAllCertificates()
  }
  openView(param1, param2) {
    this.getAllCertificates()
    if (param1 == 'edit') {
      this.viewParam = "EDIT"
      this.modelData.title = "Edit Certificate"

      this.viewCertificate.show()
      this.getAllCertificatesbyId(param2)
    } else if (param1 == 'new' && param2 == null) {
      this.selectedCertificate = new Certificate()
      this.viewParam = "NEW"
      this.modelData.title = "Add New Certificate"
      this.selectedCertificate.status = 1
      this.viewCertificate.show()
    }

  }

  closeView() {
    this.getAllCertificates()
    this.viewCertificate.hide()
  }
  getAllCertificates() {

    this.certficateList = new Array<Certificate>()
    this.selectedCertificate = new Certificate()
    this.commonService.processGet(API_URL+"ECD-CertificateManagement-1.0/service/certificate/getCertificate").subscribe(res => {
      if(res.responseCode==1){
        
        this.certficateList = res.responseData
      }
    

    }), error => {
      console.log("ERROR")
    }
  }

  getAllCertificatesbyId(param2: Certificate) {

    this.certficateList = new Array<Certificate>()
    this.selectedCertificate = new Certificate()
    this.commonService.processGet(API_URL+"ECD-CertificateManagement-1.0/service/certificate/getCertificatebyId?certificateId=" + param2.certificateId).subscribe(res => {
    this.selectedCertificate = res.responseData
    }), error => {
      console.log("ERROR")
    }
  }
  
  saveCertificate() {
    console.log("Certificate", this.selectedCertificate)

    this.commonService.processPost(API_URL+"ECD-CertificateManagement-1.0/service/certificate/saveCertificate", this.selectedCertificate).subscribe(res => {

      console.log("Data", res.responseCode)

      // console.log(this.criteriaList.length)
      if (res.responseCode == 1) {
        this.success = true;
        console.log("Success")
        setTimeout(() => {
          this.success = false;
          this.selectedCertificate.certificateName = ""
          this.selectedCertificate.status = 1
        }, 2000);


        this.getAllCertificates()



      }

    }), error => {
      console.log("ERROR")
    }

  }

  updateCertificate() {
    this.commonService.processPost(API_URL+"ECD-CertificateManagement-1.0/service/certificate/updateCertificate", this.selectedCertificate).subscribe(res => {

      console.log("Data", res.responseCode)

      // console.log(this.criteriaList.length)
      if (res.responseCode == 1) {
        this.success = true;
        console.log("Success")
        setTimeout(() => {
          this.success = false;

        }, 2000);


        this.getAllCertificates()



      }

    }), error => {
      console.log("ERROR")
    }
  }
}
