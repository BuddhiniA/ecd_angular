import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashBoardActivityDetailsComponent } from './dash-board-activity-details.component';

describe('DashBoardActivityDetailsComponent', () => {
  let component: DashBoardActivityDetailsComponent;
  let fixture: ComponentFixture<DashBoardActivityDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashBoardActivityDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashBoardActivityDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
