import { Component, OnInit, Input } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonWebService } from '../common-web.service';
import {  API_URL } from '../app_params';
import { AnnualPlan } from '../dashboard-plan/dashboard-plan.component';

export class BusinessLevel {
  bisId
  bisName
  bisLevel
  parentId
  locLevel
  status
  userInserted
  userModified
  dateInserted
  daetModified
}
export class Month{
  month
  number
}
export class Year{
  year
  
}

@Component({
  selector: 'app-dash-board-activity-details',
  templateUrl: './dash-board-activity-details.component.html',
  styleUrls: ['./dash-board-activity-details.component.css'],
  providers: [CommonWebService]
})
export class DashBoardActivityDetailsComponent implements OnInit {


  public businessList = new Array<BusinessLevel>()
  public dsDivisionList
  public gnDivisionList
  viewTypes: SelectItem[];
  public selectedType: any;
  public selectedDistric
  public selectedDsDivision;
  public selectedGnDivision

  data: any
  public userSelectedType = "DISTRICT"
  public detailAray = new AnnualPlan()
  public selectedLoc

  constructor(public activateRoute: ActivatedRoute, public commonService: CommonWebService, public route: Router) {
    this.viewTypes = [];
    this.viewTypes.push({ label: 'Map View', value: { id: 1, name: 'Map View', code: 'NY' } });
    this.viewTypes.push({ label: 'List View', value: { id: 2, name: 'List View', code: 'NY' } });

    this.selectedType = this.viewTypes[0]
    // this.data = {
    //   labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    //   datasets: [
    //     {
    //       label: 'My First dataset',
    //       backgroundColor: '#42A5F5',
    //       borderColor: '#1E88E5',
    //       data: [65, 59, 80, 81, 56, 55, 40]
    //     },
    //     {
    //       label: 'My Second dataset',
    //       backgroundColor: '#9CCC65',
    //       borderColor: '#7CB342',
    //       data: [28, 48, 40, 19, 86, 27, 90]
    //     }
    //   ]
    // }

  }

  ngOnInit() {
    console.log("TYPE", this.selectedType.name)
    this.viewTypes = [];
    this.viewTypes.push({ label: 'Map View', value: { id: 1, name: 'Map View', code: 'NY' } });
    this.viewTypes.push({ label: 'List View', value: { id: 2, name: 'List View', code: 'NY' } });

    this.selectedType.name = 'Map View'
    this.loadParams()
    this.loadData()
    
   
  }
  loadParams() {
    this.activateRoute.params.subscribe(params => {
      let month = params['month']
      let year = params['year']
      console.log("CID", month, year)
      this.loadDetails(month, year)
    })

  }
  loadData() {
    this.getAllDistrictLevel(2)


  }
  getAllDistrictLevel(bisId) {

    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getBisValues?bisId=" + bisId).subscribe(res => {
    // this.commonService.processGet(GetBisValues).subscribe(res => {
      if (res.responseCode == 1) {

        this.businessList = res.responseData
        this.selectedDistric = this.businessList[0]
        this.getAllDSDivisionLevel(this.selectedDistric.bisId)

        this.markLocations()
      }


    }), error => {
      console.log("ERROR")
    }
  }
  getAllDSDivisionLevel(bisId) {

    this.dsDivisionList = new Array<BusinessLevel>()
    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getBisValues?bisId=" + bisId).subscribe(res => {
    // this.commonService.processGet(GetBisValues).subscribe(res => {
      if (res.responseCode == 1) {

        this.dsDivisionList = res.responseData
        this.selectedDsDivision = this.dsDivisionList[0]
        this.markLocations()
        this.getAllGNDivisionLevel(this.selectedDsDivision.bisId)
        

      }


    }), error => {
      console.log("ERROR")
    }
  }

  getAllGNDivisionLevel(bisId) {

    this.gnDivisionList = new Array<BusinessLevel>()
    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getBisValues?bisId=" + bisId).subscribe(res => {
    // this.commonService.processGet(GetBisValues).subscribe(res => {
      if (res.responseCode == 1) {

        this.gnDivisionList = res.responseData
        this.selectedGnDivision = this.gnDivisionList[0]
        this.markLocations()

      }


    }), error => {
      console.log("ERROR")
    }
  }
  changeSelection(type) {

    switch (type) {
      case "dis": {


        this.userSelectedType = "DSDIVISION"
        let bisId = this.selectedDistric.bisId;
        this.getAllDSDivisionLevel(bisId)
        break;
      }
      case "dn": {

        this.userSelectedType = "GNDIVISION"
        let bisId = this.selectedDsDivision.bisId;
        this.getAllGNDivisionLevel(bisId)
        break;
      }
      default: {
        console.log("VISION")
      }
    }

  }
  reset() {


    // this.selectedDistric=null

    this.selectedDsDivision = null
    this.selectedGnDivision = null
  }

  markLocations() {


    if (this.selectedGnDivision != null) {
      this.selectedLoc = this.selectedGnDivision.locLevel
      console.log("SELECTED LOCATION1",this.selectedGnDivision)
      console.log("SELECTED LOCATION1",this.selectedLoc)
      
    } else {
      if (this.selectedDsDivision != null) {
        this.selectedLoc = this.selectedDsDivision.locLevel
        console.log("SELECTED LOCATION2",this.selectedLoc)
        
      } else {
        this.selectedLoc = this.selectedDistric.locLevel
        console.log("SELECTED LOCATION3",this.selectedLoc)
        
      }
    }

   

  }

  loadDetails(month, year) {

    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getAnnualPlan?year="+year+"&month="+month).subscribe(res => {
    // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      if (res.responseCode == 1) {
        this.detailAray = res.responseData
        console.log("RES", this.detailAray)
        if (this.detailAray != null&&this.detailAray!=undefined) {
          this.loadTable()
        }


      }

    }), error => {
      console.log("ERROR")
    }
  }
  loadTable() {
    
    this.data = {
      labels: ['Long Term', 'Short Term', 'Head Orientation', 'Admin Staff', 'Parental Awareness', 'Period pre school', 'Staff Capacty'],
      datasets: [
        {
          label: 'Training Data',
          backgroundColor: '#F79423',
          borderColor: '#1E88E5',
          data: [this.detailAray.longTerm, this.detailAray.shortTerm,
          this.detailAray.headOrientation, this.detailAray.adminStaff,
          this.detailAray.parentalAwareness, this.detailAray.periodPreSchool,
          this.detailAray.staffCapacity]
        }
      ]
    }
  }
  selectData(a){

    console.log("A",a.element._view.label)
    this.route.navigate(['program_view',{program:a.element._view.label}])
  }
}
