import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributedMaterialsComponent } from './distributed-materials.component';

describe('DistributedMaterialsComponent', () => {
  let component: DistributedMaterialsComponent;
  let fixture: ComponentFixture<DistributedMaterialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributedMaterialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributedMaterialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
