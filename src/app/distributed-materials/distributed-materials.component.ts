import { Component, OnInit, ViewChild } from '@angular/core';
import { DistributeModel } from "../model/DistributedMaterials";
import { Preschool } from "../model/Preschool";
import { PacksModel } from "../model/Packs";
import { MaterialPacksModel } from "../model/MaterialPacks";
import { MaterialListModel } from "../model/MaterialList";
import { DistributeInfoModel } from "../model/DistributeInfo";

import { ModalDirective } from 'ngx-bootstrap/modal';
import { LoggedInUserModel } from "../model/loggedInUserModel";
import { SharedService } from '../shared-service'
// import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { CommonWebService } from '../common-web.service';
import { LoignValidator } from '../login-validator';
import { API_URL } from '../app_params';

export class ImageResponse{
  responseCode
  responseData = new Array<any>()
}


@Component({
  selector: 'app-distributed-materials',
  templateUrl: './distributed-materials.component.html',
  styleUrls: ['./distributed-materials.component.css'],
  providers: [LoggedInUserModel, CommonWebService, SharedService]
})
export class DistributedMaterialsComponent implements OnInit {
  distributed: boolean = false;
  distributedPending: boolean = false;
  hideDiv: boolean;

  public Id: any;
  public Name: any;
  public centerId: any;
  public sendId: any;
  public packId: any;
  public status: any;
  public d1 = 1;

  public distributeModel = Array<DistributeModel>();
  public distributeList = new DistributeModel()
  public distributedModel = Array<DistributeModel>();
  public preschoolModel = Array<Preschool>();
  public preshoolList = new Preschool()
  public packsModel = Array<PacksModel>();
  public packsList = new PacksModel()
  public mtrpacksModel = Array<MaterialPacksModel>();
  public mtrpacksList = new MaterialPacksModel()
  public mtrlistsModel = Array<MaterialListModel>();
  public mtrlistList = new MaterialListModel();
  public distributeinfoModel = Array<DistributeInfoModel>();
  public distributeinfoList = new DistributeInfoModel();
  public selecteLearningCenter = new Preschool()

  public image;
  public sendIdArr = new Array<Number>();

  public imagesList = new Array<any>()

  
  // public uploader: FileUploader = new FileUploader({ url: API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/saveDistributeImages", queueLimit: 1 });

  public imageUrl =  API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/uploadImage"
  constructor(public commonService: CommonWebService, public validator: LoignValidator,
    public userData: LoggedInUserModel, public sharedService: SharedService) {
    // this.validator.validateUser("")
  }

  @ViewChild('childModal') public childModal: ModalDirective;
  @ViewChild("viewDistribute") viewDistribute;
  @ViewChild("viewPacks") viewPacks;
  @ViewChild("viewMtr") viewMtr;

  ngOnInit() {
    this.distributeinfoList.dateDistribute = new Date()
    this.distributed = false;
    this.distributedPending = true;
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));

    console.log("user model", this.userData);
    // console.log("token", getUser);
    this.loadToGrid();
    this.loadLearningCenter();
    this.loadDistributedMtrList();
    this.loadDistributedList();
  }

  getDbtId(a) {
    console.log("sendId ", a);
    this.viewPacks.show();
    this.sendId = a
    this.loadReceivedMtrPacks();
    console.log("status", this.status);
  }

  getPackId(p) {
    console.log("packId ", p);
    this.viewMtr.show();
    this.packId = p;
    this.loadMaterialList();
    console.log("status", this.status);
  }

  onChange(value) {
    console.log("selected Learning center");
    this.centerId = value;

    this.loadPacks();

  }

  checksendId(sid) {
    console.log("SID", sid);

  }

  loadToGrid() {
    console.log("User Id ", this.userData[0].userId);
    let url = API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllDistributeMaterialPacks?userId=" + this.userData[0].userId;
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.distributeModel = res.responseData;
        console.log("response data ", this.distributeModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.distributeModel)
    })
  }

  loadDistributedMtrList() {

    this.packId;
    let url = API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllDistributeMaterialPacksComplete?userId=" + this.userData[0].userId;

    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.distributedModel = res.responseData;
        console.log("loadDistributedMtrList", this.distributedModel)
        console.log("response loadDistributedMtrList ", this.distributeModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response mtrpacksModelz ", this.mtrlistsModel)
    })
  }
  loadDistributedList() {


    this.distributed = true;

    this.distributedPending = false;

  }
  loadDistributedListPending() {
    this.distributed = false;

    this.distributedPending = true;
  }
  loadLearningCenter() {

    let url = API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getDistributeLearningCenters?userId=" + this.userData[0].userId;
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.preschoolModel = res.responseData;
        this.selecteLearningCenter=this.preschoolModel[0]
        this.loadPacks()
        console.log("response data ", this.preschoolModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.preschoolModel)
    })
  }

  loadPacks() {
    this.centerId;
    console.log("lea",this.centerId)
    let url = API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllPackForLearningCenter?learningCenter=" + parseInt(this.selecteLearningCenter.id) + "&userId=" + this.userData[0].userId;
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.packsModel = res.responseData;
        console.log("response data ", this.packsModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.packsModel)
    })
  }

  openDistribute() {
    console.log("Open addCategory");
    this.viewDistribute.show();
    // this.loadPacks()
  }

  loadReceivedMtrPacks() {
    this.sendId;
    let url = API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getAllRecievedMaterialPacks?status=1&sendId=" + this.sendId;

    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.mtrpacksModel = res.responseData;
        console.log("response mtrpacksModel ", this.packsModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response mtrpacksModelz ", this.mtrpacksModel)
    })
  }

  loadMaterialList() {
    this.packId;
    let url = API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/getMaterialsByPackId?packId=" + this.packId;;

    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.mtrlistsModel = res.responseData;
        console.log("response mtrpacksModel ", this.mtrlistsModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response mtrpacksModelz ", this.mtrlistsModel)
    })
  }



  saveDtrChanges(evt) {
    
    console.log("saveDtrChanges");

    let url = API_URL + "ECD-MaterialMaintanance-1.0/service/Materials/saveDistribute";

    for(let a of  this.packsModel){
      this.distributeinfoList.sendId.push(a.sendId)
    }
   
    this.distributeinfoList.learningCenter = this.selecteLearningCenter.id;
 
    console.log("TO be saved distribute",this.distributeinfoList)
    this.commonService.processPost(url, this.distributeinfoList).subscribe(res => {


      let response = res.responseCode

      console.log("response ", response);
      if (response == 1) {
        console.log("success");
       
      } else {
        console.log("NOT success");
      }

   

      this.loadToGrid();
    }, error => {

      console.log("Error");

    })

  }

  onChangeCheckBox(sendId: number, isChecked: boolean) {
    console.log("onChangeCheckBox");

    if (isChecked) {
      this.sendIdArr.push(sendId);
      console.log("senz", this.sendIdArr)
    } else {

      let index = this.sendIdArr.splice(this.sendIdArr.findIndex(x => x == sendId));
      console.log("index", index)
      console.log("senzremove", this.sendIdArr)

    }
    return this.sendIdArr;

  }
  onBasicUpload(e){
    let imageR = new ImageResponse()
    imageR = JSON.parse(e.xhr.response)
    this.distributeinfoList.image = imageR.responseData[0]

  }

  
}
