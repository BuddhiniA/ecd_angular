import { Component, OnInit, ViewChild, Input, SimpleChanges } from '@angular/core';
// import { CommonWebService } from '../service/common-web.service';
// import { ShortTermModel } from '../model/ShortTerm';
// import { ShortTermParticipantModel } from '../model/ShortTermParticipant';
import { ActivatedRoute } from '@angular/router';
import { Data } from '../model/Data';
import { FormGroup, FormControl, Validator, Validators } from '@angular/forms';
import { CommonWebService } from '../common-web.service';
import { LongTermParticipant } from '../model/LongTermParticipant';
import { ModalData } from '../model/ModalData';
import { Participant } from '../model/Participant';
import { LongTermDetail } from '../long-term-training-program/long-term-training-program.component';
import { API_URL } from '../app_params';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { TreeNode } from 'primeng/primeng';
import { ModalDirective } from 'ngx-bootstrap';


export class StaffCapacityParticipant {

  participantId
  programId
  nic
  name
  district
  address
  telephoneNo
  gender
  learningCenter
  learningCenterName
  noOfChildren
  status
  designation
}

export class PeriodPre {
  participantId
  programId
  nic
  name
  district
  address
  telephoneNo
  gender
  learningCenter
  learningCenterName
  noOfChildren
  status

}

export class LongTermParticipantModel {

  participantId;
  programId;
  nic;
  name;
  district;
  address;
  telephoneNo;
  gender;
  learningCenter;
  learningCenterName;
  noOfChildren;
  status;
}
export class Program {
  programId
  programType
}


@Component({
  selector: 'app-registered-participant',
  templateUrl: './registered-participant.component.html',
  styleUrls: ['./registered-participant.component.css'],
  providers: [CommonWebService]
})
export class RegisteredParticipantComponent implements OnInit {
  organizationStaff: FormControl;
  designation: any;
  organization: FormControl;
  preschool: FormControl;
  noofchildren: FormControl;
  selectedNode = new Node();
  files: Node[];
  loadingTree: boolean;
  businessList: any;
  selectedOrganization: number;
  userData: any;


  @Input('program') program: Program;

  @ViewChild("viewParticipantSetting") public viewParticipantSetting;
  @ViewChild("updateParticipantSetting") public updateParticipantSetting;
  @ViewChild("addParticipantSetting") public addParticipantSetting;
  @ViewChild("showLearningCenters") public showLearningCenters: ModalDirective;
  public programId;
  // public shortModel = Array<ShortTermModel>();
  // public shortDetail = new ShortTermModel();
  public participantModel
  public participantDetail
  public d1;
  public d2;
  public learningCenterList = new Array<Data>()
  public selectedLearningCenter = new Data();
  public programType
  public participant = new Participant();

  public longTermProgram;

  public success
  public error
  public warn

  public type;
  public createParticipant: FormGroup


  public staffCapacityList = new Array<StaffCapacityParticipant>()

  public modalData = new ModalData()
  public longTermParticipantList = new Array<LongTermParticipant>();
  public longTermParticipant = new LongTermParticipant();

  public selectedGender = 1;
  public imageUrl =  API_URL + "ECD-TrainingPrograms-1.0/service/trainings/uploadDocs?type=master"
  constructor(public commonService: CommonWebService, public route: ActivatedRoute) {
    this.validation()
    this.getUser()
    this.getAllLocations()
  }

  validation() {
    this.createParticipant = new FormGroup({
      nic: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      district: new FormControl(''),
      address: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),

      gender: new FormControl('', Validators.required),

    })

    this.noofchildren = new FormControl('', Validators.required)
    this.preschool = new FormControl('', Validators.required)
    this.organization = new FormControl('', Validators.required)
 
    this.designation = new FormControl('', Validators.required)
  }
  ngOnChanges(changes: SimpleChanges) {

    this.loadProgramDetails()
    this.participant = new Participant()

    if (this.programType == "short") {

      this.modalData.title = "Short Term Training Program Definition"
      this.loadParticipants_ShortTerm()

    } else if (this.programType == "longTerm") {

      this.modalData.title = "Long Term Training Program Definition"
      this.loadParticipants_LongTerm()
    } else if (this.programType == "staff") {
      this.modalData.title = "Staff Capacity Building Participants"
      this.loadParticipants_StaffCapcity()
    } else if (this.programType == "periodPre") {

      this.loadPeriodPreSchoolTeachers()
    } else if (this.programType == "mastertrainer") {
      console.log("this.pr", this.programId)
      this.loaMasterTrainers()
    } else if (this.programType == "adminstaff") {

      this.modalData.title = "Admin Staff Participants"
      this.selectedOrganization = 1
      this.participant.gender = 1
      this.loadAdminStaff()
    } else if (this.programType == "parental") {

      this.modalData.title = "Parental Awareness Program Participants"

      this.participant.gender = 1
      this.loadParentalList()
    }else if (this.programType == "primary") {

      this.modalData.title = "Primary School Teacher Trainig and Head Orientation Participants"

      this.participant.gender = 1
      this.loadheadOrientation()
    }

  }

  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
  }

  loadheadOrientation() {
    this.participantModel = new Array<StaffCapacityParticipant>();
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getOrientationParticipant?programId=" + this.programId
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.participantModel = res.responseData;

      }

    }, error => {

      console.log("error ", error);

    })
  }

  loadAdminStaff() {
    this.participantModel = new Array<StaffCapacityParticipant>();
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAdminStaffParticipant?programId=" + this.programId
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.participantModel = res.responseData;

      }

    }, error => {

      console.log("error ", error);

    })
  }

  loadParentalList() {
    this.participantModel = new Array<StaffCapacityParticipant>();
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllParentalParticipants?programId=" + this.programId
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.participantModel = res.responseData;

      }

    }, error => {

      console.log("error ", error);

    })
  }


  loaMasterTrainers() {
    this.participantModel = new Array<StaffCapacityParticipant>();
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getMasterTrainerParticipant?programId=" + this.programId
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.participantModel = res.responseData;

      }

    }, error => {

      console.log("error ", error);

    })
  }
  ngOnInit() {

    // this.loadProgramDetails()
    // this.getAllLeaningCenters();

    // this.route.params.subscribe(params => {
    //   this.programType = params['programName']
    //   this.programId = params['programId']
    //   console.log("CID", this.programId, this.programType)
    // // })
    // if (this.programType == "shortTerm") {
    //   //this.loadParticipants_ShortTerm();
    //   this.modalData.title = "Short Term Training Program Definition"
    //   // this.participantModel = Array<ShortTermParticipantModel>();
    //   // this.participantDetail = new ShortTermParticipantModel();

    // } else if (this.programType == "longTerm") {

    //   this.modalData.title = "Long Term Training Program Definition"
    //   this.loadParticipants_LongTerm()
    // } else if (this.programType == "staffcapacity") {
    //   this.modalData.title = "Staff Capacity Building Participants"
    //   this.loadParticipants_StaffCapcity()
    // }else if(this.programType == "periodPre"){

    //   this.loadPeriodPreSchoolTeachers()
    // }

  }

  loadPeriodPreSchoolTeachers() {
    console.log("PERIOD PRE")
    this.participantModel = new Array<StaffCapacityParticipant>();
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getTeacherParticipants?programId=" + this.programId
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.participantModel = res.responseData;
        console.log("longterm", this.participantModel)
      }

    }, error => {

      console.log("error ", error);

    })
  }
  loadProgramDetails() {
    this.programId = this.program.programId
    this.programType = this.program.programType
    console.log("Program ID", this.programId, this.programType)
  }
  loadParticipants_StaffCapcity() {
    this.participantModel = new Array<StaffCapacityParticipant>();
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getStaffCapacityParticipant?programId=" + this.programId
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.participantModel = res.responseData;
        console.log("longterm", this.participantModel)
      }

    }, error => {

      console.log("error ", error);

    })
  }

  getAllLeaningCenters() {

    // this.selectedLearningCenter = new Data()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCenters").subscribe(res => {

      this.learningCenterList = res.responseData;

      if (this.selectedLearningCenter.sn == null || this.selectedLearningCenter.sn == undefined) {
        this.selectedLearningCenter = this.learningCenterList[0]
      }

    }), error => {
      console.log("ERROR")
    }
  }
  loadParticipants_LongTerm() {

    console.log("LONG TERM")
    this.participantModel = new Array<LongTermParticipantModel>();
    let url = API_URL + "LongTermTrainigProgram-Management-1.0/service/participant/getParticipants?programId=" + this.programId
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        this.participantModel = res.responseData;
        console.log("longterm here  ", this.participantModel)
      }

    }, error => {

      console.log("error ", error);

    })

  }

  loadParticipants_ShortTerm() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllShortTermParticipants?programId=" + this.programId;
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.participantModel = res.responseData;
     
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.participantDetail)
    })

  }
  openaddParticipant() {


    this.participant = new Participant();

  

    this.participant.gender = 1
  
    this.addParticipantSetting.show();
  }
  editParticipant(a) {

    this.participantDetail = new Participant()

    if (this.programType == "shortTerm") {


    } else if (this.programType == "longTerm") {


      let p = new LongTermParticipantModel();
      p.programId = a.programId
      p.participantId = a.participantId
      p.nic = a.nic
      p.name = a.name
      p.address = a.address
      p.district = a.district
      p.telephoneNo = a.telephoneNo
      p.learningCenterName = a.learningCenterName
      p.noOfChildren = a.noOfChildren
      p.status = a.status
      this.selectedLearningCenter.name = a.learningCenterName
      this.selectedLearningCenter.sn = a.learningCenter

      if (a.gender == "Female") {
        this.selectedGender = 1
      } else {
        this.selectedGender = 2
      }
      this.participantDetail = p


    } else if (this.programType = "staffcapacity") {
      this.participantDetail.nic = a.nic
      this.participantDetail.name = a.name
      this.participantDetail.telephoneNo = a.telephoneNo
      this.participantDetail.address = a.address
      this.selectedLearningCenter.name = a.learningCenterName
      this.selectedLearningCenter.sn = a.learningCenter
      this.selectedGender = a.gender == "Female" ? 1 : 2
    }


    this.updateParticipantSetting.show();

  }
  updateStaffCapacity() {
    let programId = parseInt(this.programId)
    this.participantDetail.programId = programId
    this.participantDetail.learningCenter = this.selectedLearningCenter.sn
    this.participantDetail.learningCenterName = this.selectedLearningCenter.name
    this.participantDetail.gender = this.selectedGender == 1 ? "Female" : "Male"
    console.log("TO BE SAVED", this.participantDetail)

    this.commonService.processPost(API_URL + "LongTermTrainigProgram-Management-1.0/service/participant/updateParticipant", this.participantDetail).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadParticipants_LongTerm()

        setTimeout(() => {

          this.resetFields()
          this.success = false
        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }
  }



  updateLongTerm() {

    let programId = parseInt(this.programId)
    this.participantDetail.programId = programId
    this.participantDetail.learningCenter = this.selectedLearningCenter.sn
    this.participantDetail.learningCenterName = this.selectedLearningCenter.name
    this.participantDetail.gender = this.selectedGender

    console.log("TO BE SAVED", this.participantDetail)

    this.commonService.processPost(API_URL + "LongTermTrainigProgram-Management-1.0/service/participant/updateParticipant", this.participantDetail).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadParticipants_LongTerm()

        setTimeout(() => {
          this.success = false
          this.resetFields()

        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }
  }

  updateParticipant() {
    if (this.programType == "shortTerm") {
      // this.participantDetail.gender="Female"
      //  this.participantDetail = a
      //this.saveParticipantShortTerm()

    } else if (this.programType == "longTerm") {

      this.updateLongTerm()
    } else if (this.programType == "staff") {
      this.updateStaffCapacity()
    } else if (this.programType == "periodPre") {
      this.updatePeriodPre()
    }
  }

  updatePeriodPre() {

    let programId = parseInt(this.programId)
    this.participantDetail.programId = programId
    this.participantDetail.learningCenter = this.selectedLearningCenter.sn
    this.participantDetail.learningCenterName = this.selectedLearningCenter.name
    this.participantDetail.gender = this.selectedGender == 1 ? "Female" : "Male"
    console.log("TO BE SAVED", this.participantDetail)

    this.commonService.processPost(API_URL + "LongTermTrainigProgram-Management-1.0/service/participant/updateParticipant", this.participantDetail).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadParticipants_LongTerm()

        setTimeout(() => {

          this.resetFields()
          this.success = false
        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }
  }
  saveParticipant() {


    if (this.programType == "shortTerm") {
     
    } else if (this.programType == "longTerm") {

      this.saveLongTermParticipants();
    } else if (this.programType == "staff") {
      this.saveStaffCapacity()
    } else if (this.programType == "periodPre") {
      this.savePeriodPre()
    } else if (this.programType == "mastertrainer") {
      this.saveMasterTrainer()
    } else if (this.programType == "adminstaff") {
      this.saveAdminStaff()
    }else if (this.programType == "parental") {
      this.saveParental()
    }else if (  this.programType == "primary") {
      this.savePrimary()
    }else if (  this.programType == "short") {
      this.saveShort()
    }
  
  }

  saveShort() {

    let programId = parseInt(this.programId)
    this.participant.programId = programId
    this.participant.learningCenter = this.selectedLearningCenter.sn
  
    this.participant.userInserted = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/addNewShortTermParticipant", this.participant).subscribe(res => {
      if (res.responseCode == 1) {
        this.success = true;
        setTimeout(() => {
          this.success = false
          this.addParticipantSetting.hide()
          this.loadParticipants_ShortTerm()
        }, 2000)
      }
    }), error => {
      console.log("ERROR")
    }
  }
  savePrimary() {

    let programId = parseInt(this.programId)
    this.participant.programId = programId
    this.participant.learningCenter = this.selectedLearningCenter.sn
    this.participant.userInserted = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/addNewOrientationParticipant", this.participant).subscribe(res => {
      if (res.responseCode == 1) {
        this.success = true;
        setTimeout(() => {
          this.success = false
          this.addParticipantSetting.hide()
          this.loadheadOrientation()
        }, 2000)
      }
    }), error => {
      console.log("ERROR")
    }
  }
  saveParental() {

    let programId = parseInt(this.programId)
    this.participant.programId = programId
    
    this.participant.userInserted = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/addNewParentalParticipant", this.participant).subscribe(res => {
      if (res.responseCode == 1) {
        this.success = true;
        setTimeout(() => {
          this.success = false
          this.addParticipantSetting.hide()
          this.loadParentalList()
        }, 2000)
      }
    }), error => {
      console.log("ERROR")
    }
  }
  saveAdminStaff() {

    let programId = parseInt(this.programId)
    this.participant.programId = programId
    if (this.selectedOrganization == 1) {
      this.participant.organization = "MWCA"
    } else if (this.selectedOrganization == 2) {
      this.participant.organization = "PCs"
    } else if (this.selectedOrganization == 3) {
      this.participant.organization = "PHDT"
    }
    this.participant.userInserted = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/addNewAdminStaff", this.participant).subscribe(res => {
      if (res.responseCode == 1) {
        this.success = true;
        setTimeout(() => {
          this.success = false
          this.addParticipantSetting.hide()
          this.loadAdminStaff()
        }, 2000)
      }
    }), error => {
      console.log("ERROR")
    }
  }


  updateStatus(evt, param, training) {
    console.log("ssss", training)
    if (param == 1 && evt.target.checked) {

      let location = training.district
      this.participantDetail = new LongTermParticipantModel()
      this.participantDetail.programId = training.programId
      this.participantDetail.nic = training.nic
      this.participantDetail.name = training.name
      this.participantDetail.district = training.district
      this.participantDetail.address = training.address
      this.participantDetail.learningCenter = training.learningCenter
      this.participantDetail.telephoneNumber = training.telephoneNumber
      this.participantDetail.gender = training.gender
      this.participantDetail.noOfChildren = training.noOfChildren
      this.participantDetail.participantId = training.participantId
      this.participantDetail.status = 1
      console.log("TO BE UPDATED LONG TERM PARTICIPANT", this.participantDetail)
      this.updateStatusOfParticipant(this.participantDetail)
    } else if (param == 1 && !evt.target.checked) {
      let trainingProgram = new LongTermDetail()
      trainingProgram = training
      training.status = 0
      this.updateStatusOfParticipant(trainingProgram)
    } else if (param == 2 && evt.target.checked) {
      let trainingProgram = new LongTermDetail()
      trainingProgram = training
      training.status = 2
      this.updateStatusOfParticipant(trainingProgram)
    } else if (param == 2 && !evt.target.checked) {
      let trainingProgram = new LongTermDetail()
      trainingProgram = training
      training.status = 1
      this.updateStatusOfParticipant(trainingProgram)
    } else if (param == 3 && evt.target.checked) {
      let trainingProgram = new LongTermDetail()
      trainingProgram = training
      training.status = 3
      this.updateStatusOfParticipant(trainingProgram)
    } else if (param == 3 && !evt.target.checked) {
      let trainingProgram = new LongTermDetail()
      trainingProgram = training
      training.status = 2
      this.updateStatusOfParticipant(trainingProgram)
    }


  }
  updateStatusOfParticipant(trainingProgram) {

    console.log("EVENT STATUS", trainingProgram)
    this.commonService.processPost(API_URL + "LongTermTrainigProgram-Management-1.0/service/participant/updateParticipant", trainingProgram).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadParticipants_LongTerm()

        setTimeout(() => {

          // this.resetFields()
          this.success = false
        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }
  }
  savePeriodPre() {

    let programId = parseInt(this.programId)
    this.participantDetail.programId = programId
    this.participantDetail.gender = this.selectedGender == 1 ? "Female" : "Male"
    this.participantDetail.learningCenter = this.selectedLearningCenter.sn
    this.participantDetail.learningCenterName = this.selectedLearningCenter.name
    console.log("TO BE SAVED", this.participantDetail)
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/addNewTeacherParticipant", this.participantDetail).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadPeriodPreSchoolTeachers()

        setTimeout(() => {

          this.resetFields()
          this.success = false
        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }
  }

  saveMasterTrainer() {

    let programId = parseInt(this.programId)
    this.participant.programId = programId

    this.participant.learningCenter = this.selectedLearningCenter.sn
    this.participant.userInserted = this.userData[0].userId
    console.log("TO BE SAVED", this.participant)
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/addNewMasterTrainerParticipant", this.participant).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadPeriodPreSchoolTeachers()

        setTimeout(() => {


          this.success = false
        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }
  }
  saveLongTermParticipants() {
    this.type = 'NEW'
    let programId = parseInt(this.programId)
    this.participantDetail.programId = programId
    this.participantDetail.learningCenter = this.selectedLearningCenter.sn
    this.participantDetail.learningCenterName = this.selectedLearningCenter.name
    this.participantDetail.gender = this.selectedGender
    console.log("TO BE SAVED", this.participantDetail)

    this.commonService.processPost(API_URL + "LongTermTrainigProgram-Management-1.0/service/participant/saveParticipant", this.participantDetail).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadParticipants_LongTerm()

        setTimeout(() => {

          this.resetFields()
          this.success = false
        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }


  }

  saveParticipantShortTerm() {


    let programId = parseInt(this.programId)
    this.participantDetail.programId = programId
    this.participantDetail.learningCenter = this.selectedLearningCenter.sn
    this.participantDetail.learningCenterName = this.selectedLearningCenter.name
    console.log("TO BE SAVED", this.participantDetail)
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/addNewParticipant", this.participantDetail).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadParticipants_ShortTerm()

        setTimeout(() => {

          this.resetFields()
          this.success = false
        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }


  }

  saveStaffCapacity() {
    let programId = parseInt(this.programId)
    this.participant.programId = programId
 
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/addNewStaffCapacityParticipant", this.participant).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadParticipants_StaffCapcity()

        setTimeout(() => {

        
          this.success = false
          this.addParticipantSetting.hide()
        }, 2000)
      }

    }), error => {
      console.log("ERROR")
    }

  }
  resetFields() {
    this.participantDetail.nic = ""
    this.participantDetail.name = ""
    this.participantDetail.district = ""
    this.participantDetail.address = ""
    this.participantDetail.telephoneNo = ""
    this.participantDetail.noOfChildren = ""
  }

  collapse(evt) {
    console.log("COLLAPSE", evt)
  }

  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;
    if (event.node.type == "province") {

    }
    else if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.code, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.code, event.node.type)
    }

    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }


  nodeSelected(event) {
    this.getAllLearningCentersbyId(event.node.districtId, event.node.code, event.node.type)
    this.showLearningCenters.show()
    this.collapseAll()
  }



  getAllLearningCenters() {
    this.selectedLearningCenter = new Data()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCenters").subscribe(res => {

      this.learningCenterList = res.responseData;

      if (this.selectedLearningCenter.sn == null) {
        this.selectedLearningCenter = this.learningCenterList[0]
      }

    }), error => {
      console.log("ERROR")
    }
  }

  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()

        node1.children.push(node)






      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  getAllLocations() {

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }
  getAllLearningCentersbyId(id1, id2, type) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id1=" + id1 + "&id2=" + id2 + "&type=" + type).subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;

        this.selectedLearningCenter = this.learningCenterList[0]


      }


    }), error => {
      console.log("ERROR")
    }
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  

}
