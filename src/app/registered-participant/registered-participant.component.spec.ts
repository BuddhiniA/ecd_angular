import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisteredParticipantComponent } from './registered-participant.component';

describe('RegisteredParticipantComponent', () => {
  let component: RegisteredParticipantComponent;
  let fixture: ComponentFixture<RegisteredParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisteredParticipantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisteredParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
