import { Component, OnInit, Input, ViewChild, SimpleChanges } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ModalData } from '../model/ModalData';
import { Router } from '@angular/router';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';

export class AnnualPlan {
  year
  month
  shortTerm
  longTerm
  periodPreSchool
  staffCapacity
  parentalAwareness
  headOrientation
  pmu
  phdt
  adminStaff
  userInserted
  userModified
  dateInserted
  dateModified
}



export class ActivityCompete {
  training
  actual
  planned
  precentage
}

export class Month {
  month
  number
}
export class Year {
  year

}
@Component({
  selector: 'app-dashboard-plan',
  templateUrl: './dashboard-plan.component.html',
  styleUrls: ['./dashboard-plan.component.css'],
  providers: [CommonWebService]
})
export class DashboardPlanComponent implements OnInit {

  public activityCompletionList = new Array<ActivityCompete>()
  public activityComplete = new ActivityCompete()
  // public month = new Month()
  // public year = new Year()
  public selectedYear
  public selectedMonth
  public shortTermList = new Array<any>()
  public mastrList = new Array<any>()
  public longList = new Array<any>()
  public headList = new Array<any>()
  public periodList = new Array<any>()
  public adminList = new Array<any>()
  public staffist = new Array<any>()
  public parentalList = new Array<any>()


  public shortTermVal
  public longTermVAl
  public masterVal
  public headVAl
  public parentalVal
  public staffVal
  public adminVal
  public periodVal
  public pmuVal
  public phdtVal

  public fromDate
  public toDate
  public minDate = new Date();

  ngOnInit(): void {
    let date = new Date()
    this.fromDate = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()
    this.toDate = date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate()
     console.log("DATE",date.getFullYear()+"-"+date.getMonth()+"-"+date.getDate())

    if (this.year != null && this.month != null) {
      let year = this.year
      let month = this.month
      this.selectedMonth=1
      console.log("YEAR AND MONTH", this.year, this.month)


      // this.loadDetails(year,month);
    }
  }

  ngOnChanges(changes: SimpleChanges) {

   
    this.selectedYear = this.year.year.id
    this.selectedMonth = this.month.number
    console.log("CLICKED", this.selectedYear, this.selectedMonth)
    this.loadDetails(this.selectedYear, this.selectedMonth);
    this.loadAnnualValuesforProgress(this.selectedYear,this.selectedMonth)


  }
  @Input('month') month: Month;
  @Input('year') year: Year;

  @ViewChild("viewDetails") public viewDetails: ModalDirective;

  public model = new ModalData()
  value: number = 20;
  public detailAray = new Array<any>()
  constructor(public route: Router, public commonService: CommonWebService) {

    


  }

  Open(type) {



    switch (type) {
      case "mastertrainer": {
        let typeLoc = "MASTER_TRAINER"
        console.log("INSIDE", this.selectedMonth, this.selectedYear)
        this.loadAnnualValues(this.selectedYear, this.selectedMonth, typeLoc)
        this.loadMasterList()
        this.model.title = "Master Trainer Program"

        break;
      }
      case "longterm": {
        let typeLoc = "LONG_TERM"
        console.log("INSIDE", this.selectedMonth, this.selectedYear)
        this.loadAnnualValues(this.selectedYear, this.selectedMonth, typeLoc)
        this.model.title = "Long Term Training Program"
        break;
      }
      case 'shortterm': {
        let typeLoc = "SHORT_TERM"
        this.model.title = "Short Term Training Program"
        console.log("INSIDE", this.selectedMonth, this.selectedYear)
        this.loadAnnualValues(this.selectedYear, this.selectedMonth, typeLoc)
        this.model.title = "Long Term Training Program"
        break;
      }
      case 'parental': {
        let typeLoc = "PARENTAL_AWARENESS"
        this.model.title = "Parental Awareness Training Program"
        console.log("INSIDE", this.selectedMonth, this.selectedYear)
        this.loadAnnualValues(this.selectedYear, this.selectedMonth, typeLoc)
       
        break;
      }
      case 'period': {
        let typeLoc = "PRERIOD_PRE_SCHOOL_TEACHER"
        this.model.title = "Period Pre School Teacher Training Program"
        console.log("INSIDE", this.selectedMonth, this.selectedYear)
        this.loadAnnualValues(this.selectedYear, this.selectedMonth, typeLoc)
       
        break;
      }
      case 'staff': {
        let typeLoc = "STAFF_CAPACITY_BUILDING"
        this.model.title = "Staff Capacity Building Training Program"
        console.log("INSIDE", this.selectedMonth, this.selectedYear)
        this.loadAnnualValues(this.selectedYear, this.selectedMonth, typeLoc)
        
        break;
      }
      case 'admin': {
        let typeLoc = "ADMINISTRATIVE_STAFF_TRAINING"
        this.model.title = "Administrative Staff Service Training Program"
        console.log("INSIDE", this.selectedMonth, this.selectedYear)
        this.loadAnnualValues(this.selectedYear, this.selectedMonth, typeLoc)
        
        break;
      }
      case 'head': {
        let typeLoc = "HEAD_ORIENTATION"
        this.model.title = "Head Orientation Training Program"
        console.log("INSIDE", this.selectedMonth, this.selectedYear)
        this.loadAnnualValues(this.selectedYear, this.selectedMonth, typeLoc)

        break;
      }
    }

    this.viewDetails.show()
  }
  ngAfterViewInit() {
    console.log("LOADONG>>")
  }
  openMap() {
    this.route.navigate(['dashboard_details', { month: this.month.number, year: this.year.year.id }])
  }
  loadDetails(year, month) {

    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getAnnualPlanValues?year=" + year + "&month=" + month).subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      this.detailAray = res.responseData
      console.log("RESPONSE OF DETAIL ARRAY", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }


  loadAnnualValues(year, month, type) {
    this.activityComplete = new ActivityCompete()
    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getAnnualPlanValues?year=" + year + "&month=" + month).subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      if (res.responseCode == 1) {
        this.activityCompletionList = res.responseData
        for (let activityCompete of this.activityCompletionList) {
          if (activityCompete.training == type) {
            this.activityComplete = activityCompete
            console.log("LIST ACTIVITY COMPLETED", this.activityComplete)
          }
        }
      }


    }), error => {
      console.log("ERROR")
    }
  }

  loadAnnualValuesforProgress(year, month) {
    this.activityComplete = new ActivityCompete()
    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getAnnualPlanValues?year=" + year + "&month=" + month).subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      if (res.responseCode == 1) {
        this.activityCompletionList = res.responseData
        for (let activityCompete of this.activityCompletionList) {
          if (activityCompete.training == "MASTER_TRAINER") {
            this.masterVal = activityCompete.precentage

          } else if (activityCompete.training == "LONG_TERM") {
            this.longTermVAl = activityCompete.precentage
          } else if (activityCompete.training == "HEAD_ORIENTATION") {
            this.headVAl = activityCompete.precentage
          } else if (activityCompete.training == "PARENTAL_AWARENESS") {
            this.parentalVal = activityCompete.precentage
          } else if (activityCompete.training == "PRERIOD_PRE_SCHOOL_TEACHER") {
            this.periodVal = activityCompete.precentage
          } else if (activityCompete.training == "SHORT_TERM") {
            this.shortTermVal = activityCompete.precentage
          } else if (activityCompete.training == "STAFF_CAPACITY_BUILDING") {
            this.staffVal = activityCompete.precentage
          } else if (activityCompete.training == "ADMINISTRATIVE_STAFF_TRAINING") {
            this.adminVal = activityCompete.precentage
          }else if (activityCompete.training == "ACTIVITIES_PHDT") {
            this.phdtVal = activityCompete.precentage
          }else if (activityCompete.training == "ACTIVITIES_PMU") {
            this.pmuVal = activityCompete.precentage
          }

        }
      }

      console.log("percentages",this.phdtVal,this.pmuVal)

    }), error => {
      console.log("ERROR")
    }
  }

  loadMasterList() {
   
    this.dateValidation()
    
  
    console.log("FROM TO DATE",this.fromDate,this.toDate)
    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms?fromYear="+this.fromDate+"&toYear="+this.toDate).subscribe(res => {
      this.mastrList = res.responseData
      console.log("MasterList", this.mastrList)

    }), error => {
      console.log("ERROR")
    }
  }
  dateValidation(){

    
    let date:string = this.fromDate
    let ar:any[] = date.split('-');
    
    this.minDate.setFullYear(ar[0])
    this.minDate.setMonth(ar[1])
    this.minDate.setDate(ar[2])
    console.log("CHANGING",this.minDate)
    // this.minDate = this.minDate.getFullYear()+"-"+(this.minDate.getMonth()+1)+"-"+this.minDate.getDate()
  }
  loadLongTermList() {

    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms").subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      this.mastrList = res.responseData
      console.log("RES", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }

  loadShortTermList() {

    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms").subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      this.mastrList = res.responseData
      console.log("RES", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }

  loadParentalList() {

    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms").subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      this.mastrList = res.responseData
      console.log("RES", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }

  loadAdminList() {

    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms").subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      this.mastrList = res.responseData
      console.log("RES", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }

  loadStaffList() {

    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms").subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      this.mastrList = res.responseData
      console.log("RES", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }

  loadHeadList() {

    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms").subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      this.mastrList = res.responseData
      console.log("RES", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }

  loadPeriodList() {

    this.commonService.processGet(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms").subscribe(res => {
      // this.commonService.processGet(GetAnnualPlan).subscribe(res => {
      this.mastrList = res.responseData
      console.log("RES", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }




}
