import { Component, OnInit, Injectable, ViewChild } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { LongTermProgram } from '../model/LongTermProgram';
import { ModalDirective } from 'ngx-bootstrap';
import { API_URL } from '../app_params';



@Component({
  selector: 'app-training-program',
  templateUrl: './training-program.component.html',
  styleUrls: ['./training-program.component.css'],
  providers: [CommonWebService]
})
@Injectable()
export class TrainingProgramComponent implements OnInit {
  @ViewChild("viewDetails") public viewDetails: ModalDirective;
  loading: boolean;

  public programList= new Array<LongTermProgram>();
  public selectedprogram;
  
 
  display: boolean = false;
  
      showDialog() {
          this.display = true;
      }

  constructor(public commonService: CommonWebService) {
    this.getAllPrograms()
   }

  ngOnInit() {
    this.getAllPrograms()
  }
  getAllPrograms() {
      
        this.selectedprogram = new LongTermProgram()
        this.commonService.processGet(API_URL+"LongTermTrainigProgram-Management-1.0/service/program/getPrograms").subscribe(res => {
    

          this.programList = res.responseData
          console.log("LIST",this.programList)

          this.loading = false;
    
    
        }), error => {
          console.log("ERROR")
        }
      }
      selectCar(car:LongTermProgram){
          console.log("car",car)
          this.viewDetails.show()
      }
}
