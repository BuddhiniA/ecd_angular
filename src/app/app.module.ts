import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BlockUIModule} from 'primeng/blockui';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { CensusDataManagementComponent } from './census-data-management/census-data-management.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { LoginComponent } from './login/login.component';
import { GalleryComponent } from './gallery/gallery.component';
import { FeewaiverRegistrationComponent } from './feewaiver-registration/feewaiver-registration.component';
import { ParentalParticipantComponent } from './parental-participant/parental-participant.component';
import { ActivityCategoriesComponent } from './activity-categories/activity-categories.component';
import { FeeWaiverPaymentComponent } from './fee-waiver-payment/fee-waiver-payment.component';
import { OrientationParticipantComponent } from './orientation-participant/orientation-participant.component';
import { ResourcePoolComponent } from './resource-pool/resource-pool.component';
import { AdministrativeStaffComponent } from './administrative-staff/administrative-staff.component';
import { TrainingInstituteComponent } from './training-institute/training-institute.component';
import { MasterTrainerProgramsComponent } from './master-trainer-programs/master-trainer-programs.component';
import { LmReportsComponent } from './lm-reports/lm-reports.component';
import { LearningMaterialDefinitionComponent } from './learning-material-definition/learning-material-definition.component';
import { DistributedMaterialsComponent } from './distributed-materials/distributed-materials.component';
import { ChildDevelopmentAssmntUpdateComponent } from './child-development-assmnt-update/child-development-assmnt-update.component';
import { BusinessStructureComponent } from './business-structure/business-structure.component';
import { ShorttermTrainerProgramComponent } from './shortterm-trainer-program/shortterm-trainer-program.component';
import { PrimaryschoolHeadorientationProgramComponent } from './primaryschool-headorientation-program/primaryschool-headorientation-program.component';
import { ParentalAwarenessComponent } from './parental-awareness/parental-awareness.component';
import { GanttChart } from './gantt-utils/gantt.component';
import { AnnualPlanDetailComponent } from './annual-plan-detail/annual-plan-detail.component';
import { AnnualPlanComponent } from './annual-plan/annual-plan.component';
import { MaintainActivityComponent } from './maintain-activity/maintain-activity.component';
import { SocialAuditsComponent } from './social-audits/social-audits.component';
import { CustomReportsComponent } from './custom-reports/custom-reports.component';
import { DashBoardProgramViewComponent } from './dash-board-program-view/dash-board-program-view.component';
import { TrainingProgramComponent } from './training-program/training-program.component';
import { GanttComponent } from './gantt/gantt.component';
import { ListComponent } from './list/list.component';
import { DashBoardActivityDetailsComponent } from './dash-board-activity-details/dash-board-activity-details.component';
import { MapComponentComponent } from './map-component/map-component.component';
import { DashboardPlanComponent } from './dashboard-plan/dashboard-plan.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PrimaryShoolTrainingProgramComponent } from './primary-shool-training-program/primary-shool-training-program.component';
import { StaffCapacityBuildingComponent } from './staff-capacity-building/staff-capacity-building.component';
import { RegisteredParticipantComponent } from './registered-participant/registered-participant.component';
import { LongTermTrainingProgramComponent } from './long-term-training-program/long-term-training-program.component';
import { CertficateManagementComponent } from './certficate-management/certficate-management.component';
import { DefineActivityComponent } from './define-activity/define-activity.component';
import { DefineDocumentComponent } from './define-document/define-document.component';
import { MaterialPackComponent } from './material-pack/material-pack.component';
import { ReceiveMaterialComponent } from './receive-material/receive-material.component';
import { SendMaterialComponent } from './send-material/send-material.component';
import { MaterialStockComponent } from './material-stock/material-stock.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { PagetopComponent } from './pagetop/pagetop.component';
import { CriteriaResultManagementComponent } from './criteria-result-management/criteria-result-management.component';
import { CriteriaManagementComponent } from './criteria-management/criteria-management.component';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { TabViewModule } from 'primeng/components/tabview/tabview';
import { ButtonModule } from 'primeng/components/button/button';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ProgressBarModule } from 'primeng/components/progressbar/progressbar';
import { ChartModule } from 'primeng/components/chart/chart';
import { PickListModule } from 'primeng/components/picklist/picklist';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { HttpModule } from '@angular/http';
import { GMapModule } from 'primeng/components/gmap/gmap';
import { routing } from './app.routing';
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { FileUploadModule } from 'primeng/components/fileupload/fileupload';
import { SharedModule } from 'primeng/components/common/shared';
import { TreeModule } from 'primeng/components/tree/tree';
import { PanelModule } from 'primeng/components/panel/panel';
import { GalleriaModule } from 'primeng/components/galleria/galleria';
import { OverlayPanelModule } from 'primeng/components/overlaypanel/overlaypanel';
import { ContextMenuModule } from 'primeng/components/contextmenu/contextmenu';
import { MenubarModule } from 'primeng/components/menubar/menubar';
import { RouterModule } from '@angular/router';
import { ModalModule } from 'ngx-bootstrap';
import { CommonWebService } from './common-web.service';
import { DatePipe } from '@angular/common';
import { LoignValidator } from './login-validator';
import { ProgressSpinnerModule } from 'primeng/components/progressspinner/progressspinner';
import { MiniCensusDataManagementComponent } from './mini-census-data-management/mini-census-data-management.component';

import {MatCardModule} from '@angular/material/card'
import {MatButtonModule} from '@angular/material/button'

import {CardModule} from 'primeng/card';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatSelectModule} from '@angular/material/select'
import { DialogOverviewExampleDialog, DashBoardManagementComponent, GanttDialog } from './dash-board-management/dash-board-management.component';
import { RefresherProgramComponent } from './refresher-program/refresher-program.component';
import { CensusViewComponent } from './census-view/census-view.component';
import { FeeWaiverPaymentUpdateComponent } from './fee-waiver-payment-update/fee-waiver-payment-update.component';



@NgModule({
  declarations: [
    AppComponent,
    CriteriaManagementComponent,
    CriteriaResultManagementComponent,
    PagetopComponent,
    SideBarComponent,
    MaterialStockComponent,
    SendMaterialComponent,
    ReceiveMaterialComponent,
    MaterialPackComponent,
    DefineDocumentComponent,
    DefineActivityComponent,
    CertficateManagementComponent,
    LongTermTrainingProgramComponent,
    RegisteredParticipantComponent,
    StaffCapacityBuildingComponent,
    PrimaryShoolTrainingProgramComponent,
    DashboardComponent,
    DashboardPlanComponent,
    MapComponentComponent,
    DashBoardActivityDetailsComponent,
    ListComponent,
    GanttComponent,
    TrainingProgramComponent,
    DashBoardProgramViewComponent,
    CustomReportsComponent,
    FeeWaiverPaymentComponent,
    SocialAuditsComponent,
    MaintainActivityComponent,
    AnnualPlanComponent,
    AnnualPlanDetailComponent,
    GanttChart,
    ParentalAwarenessComponent,
    PrimaryschoolHeadorientationProgramComponent,
    ShorttermTrainerProgramComponent,
    BusinessStructureComponent,
    ChildDevelopmentAssmntUpdateComponent,
    DistributedMaterialsComponent,
    LearningMaterialDefinitionComponent,
    LmReportsComponent,
    MasterTrainerProgramsComponent,
    TrainingInstituteComponent,
    AdministrativeStaffComponent,
    ResourcePoolComponent,
    OrientationParticipantComponent,
    FeeWaiverPaymentComponent,
    ActivityCategoriesComponent,
    ParentalParticipantComponent,
    FeewaiverRegistrationComponent,
    GalleryComponent,
    LoginComponent,
    UsermanagementComponent,
    ForgotpasswordComponent,
    CensusDataManagementComponent,
    MiniCensusDataManagementComponent,
    DashBoardManagementComponent,
    DialogOverviewExampleDialog,
    RefresherProgramComponent,
    GanttDialog,
    CensusViewComponent,
    FeeWaiverPaymentUpdateComponent,
    
  ],
  entryComponents:[GanttDialog,DialogOverviewExampleDialog,LongTermTrainingProgramComponent,RegisteredParticipantComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    DropdownModule,
    TabViewModule,
    ModalModule.forRoot(),
    // MdDialogModule,
    // MdButtonModule,MatButtonModule
   
    ProgressSpinnerModule,
    ButtonModule,
    InputTextModule,
    ProgressBarModule,
    ChartModule,
    PickListModule,
    BrowserAnimationsModule,
    FormsModule,
    DialogModule,
    CalendarModule,
    HttpModule,
    GMapModule,
    routing,
    DataTableModule,
    FileUploadModule,
    SharedModule,
    TreeModule,
    PanelModule,
    ReactiveFormsModule,
    GalleriaModule,
    OverlayPanelModule,
    ContextMenuModule,
    MenubarModule,
    CardModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,
    MatDividerModule,
    MatSelectModule,
    BlockUIModule,
    // NgBootstrapFormValidationModule.forRoot(),
    RouterModule.forRoot([
      {
        path: 'criteria-management',
        component: CriteriaManagementComponent
      },

      {
        path: 'criteriaresult-management',
        component: CriteriaResultManagementComponent
      }
    ])
  ],
  providers: [CommonWebService,DatePipe,LoignValidator],
  bootstrap: [AppComponent]
})
export class AppModule { }
