import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CensusDataManagementComponent } from './census-data-management.component';

describe('CensusDataManagementComponent', () => {
  let component: CensusDataManagementComponent;
  let fixture: ComponentFixture<CensusDataManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CensusDataManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CensusDataManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
