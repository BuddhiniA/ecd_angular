import { Component, OnInit, ViewChild } from '@angular/core';
import { InstitutesModel } from '../model/Institutes';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';

@Component({
  selector: 'app-training-institute',
  templateUrl: './training-institute.component.html',
  styleUrls: ['./training-institute.component.css']
})
export class TrainingInstituteComponent implements OnInit {

  public instituteModel = Array<InstitutesModel>();
  public instituteDetail = new  InstitutesModel;
  public insForm :FormGroup
  public updateInstituteForm :FormGroup
  public insId
  public success
  public error
  public warn
  
  @ViewChild("makeInsSetting") public makeInsSetting;
  @ViewChild("updateInsSetting") public updateInsSetting;

  constructor(public commonService: CommonWebService) { 
    this.validation();
    this.validationUpdate();
  }

  ngOnInit() {

     this.loadToGrid();
  }

  validationUpdate(){
    
        this.updateInstituteForm = new FormGroup({
          name:new FormControl(['',Validators.required]),
          address:new FormControl(['',Validators.required]),
          contactPerson:new FormControl(['',Validators.email]),
          contactNo:new FormControl('',[Validators.required]),
          // status:new FormControl('',[Validators.required,Validators.maxLength(12)]),
          // email:new FormControl('',[Validators.email]),
          // utype:new FormControl('',[Validators.required]),
          status:new FormControl('',Validators.required)
         
        })
      }

      validation(){
        
            this.insForm = new FormGroup({
              name:new FormControl(['',Validators.required]),
              address:new FormControl(['',Validators.required]),
              contactPerson:new FormControl(['',Validators.email]),
              no:new FormControl('',[Validators.required]),
              // status:new FormControl('',[Validators.required,Validators.maxLength(12)]),
              // email:new FormControl('',[Validators.email]),
              // utype:new FormControl('',[Validators.required]),
              status:new FormControl('',Validators.required)
             
            })
          }   

  openCreateIns(){
    this.makeInsSetting.show();
  }

  getInsId(ins){
    console.log("updateInsSetting ",ins);
    this.instituteDetail = ins;
    this.updateInsSetting.show();
  }

  loadToGrid(){
    let url = API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getAllInstitutes";
    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.responseCode == 1) {
        
        this.instituteModel = res.responseData;
        console.log("response data ", this.instituteModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.instituteModel)
    })
  }

  createIns(){
      
    this.commonService.processPost(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/saveInstitutes",this.instituteDetail).subscribe(res=>{
      
      
       
        if(res.responseCode==1){
          this.success=true;
          this.loadToGrid()
        
          setTimeout(()=>{
           
            this.resetFields()
            this.success=false
          },2000)
        }
  
      }),error=>{
          console.log("ERROR")
      }

  }
  updateIns(){
    console.log("UPDATED instituteDetail MODEL", this.instituteDetail)
    this.commonService.processPost(API_URL+"ECD-TrainingPrograms-1.0/service/trainings/updateInstitute", this.instituteDetail).subscribe(res => {

      console.log("Sucess?", res.responseCode)
      if (res.responseCode == 1) {
        this.updateInsSetting.hide();
        this.loadToGrid();
        setTimeout(()=>{
          
           this.resetFields()
           this.success=false
         },5000)
      }

    }), error => {
      console.log("ERROR")
    }
  }


  resetFields(){
    this.instituteDetail.instituteId=""
    this.instituteDetail.name=""
    this.instituteDetail.address=""
    this.instituteDetail.address=""
    this.instituteDetail.contactPerson=""
    this.instituteDetail.contactNo=""
    this.instituteDetail.status=""
  }
}
