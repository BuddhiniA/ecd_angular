import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefineActivityComponent } from './define-activity.component';

describe('DefineActivityComponent', () => {
  let component: DefineActivityComponent;
  let fixture: ComponentFixture<DefineActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefineActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefineActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
