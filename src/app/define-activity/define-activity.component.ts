import { Component, OnInit, ViewChild, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { ModalDirective } from 'ngx-bootstrap';
import { ModalData } from '../model/ModalData';
import { Documents } from '../model/document';
import { FormControl, FormGroup, Validators, NgForm, FormBuilder } from "@angular/forms";
import { Data } from '../model/Data';
import { ActivityCategoryModel } from '../model/ActivityCategory';
import { Comment } from '../model/Comment';
import { Observable } from "rxjs/Observable";
import { MilestoneAll } from '../model/MilestoneALL';

import { ActivatedRoute, Route, Router } from '@angular/router';
import { API_URL } from '../app_params';
// import { UploaderOptions, UploadFile, UploadInput, UploadOutput, UploadStatus } from 'ngx-uploader';
import { Photo } from '../model/Photo';
import { EXIF } from 'exif-js';
import { DatePipe } from '@angular/common';
import { Http, RequestOptions, Headers, Response, RequestOptionsArgs, } from '@angular/http';
import { LoggedInUserModel } from '../model/loggedInUserModel';
import { CategoryDoc } from '../activity-categories/activity-categories.component';
import { TreeNode } from 'primeng/components/common/api';
import { Claim } from '../model/claim';
import { Center } from '../model/ECD_Center';
import { NumberFormatPipe } from '../numberformat';
import { NullAstVisitor } from '@angular/compiler';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { MaintainDocument } from '../model/MaintainDocument';
import { error } from 'util';
import { Address } from '../model/Address';



export class Category {
  budgetFields
  categoryId
  categoryName
  dateInserted
  dateModified
  milestoneList = new Array<Milestone>()
  status
  totalBudget
  userInserted
  userModified
}


export class BusinessLevel {
  id
  lifeCode
  districtId
  dsID
  english
  type
  code
  provinceId
  nodeType



}
export class Node {
  id
  label
  type
  code
  lifeCode
  districtId
  dsID
  provinceId
  nodeType
  leaf
  children = new Array<Node>()
}

export class Status {
  id
  name
}

export class CategoryMilestone {

  milestoneId
  milestoneName
  status

}
export class Activity {
  activityData = new ActivityData()
  milestoneList = new Array<Milestone>()
  fieldList = new Array<Budget>()
  female
  male
  userModified
  userInserted
  dateInserted
  dateModified
}

export class Milestone {
  milestoneId
  milestoneName;
  plannedStartDate
  plannedEndDate
  actualStartDate
  actualEndDate
  documentList = new Array<Documents>()
}

export class Budget {
  fieldId;
  fieldName;
  status;
  budgetValue
}

export class ActivityData {
  activityId
  type
  selectedPMUType
  activityDescription
  learningCenterId
  learningCenter
  longitude
  latitude
  address
  locationCode
  categoryId
  category
  status
  male
  femalel
  location = new Address()
  plannedStartDate;
  plannedEndDate;
  year
}

export class UpdateDocModel {

  activityId
  milestoneId
  documentId
  documentName
  status
  availability
  description
}

export class UpdateMilestone {

  milestoneId
  milestoneName;
  plannedStartDate
  plannedEndDate
  actualStartDate
  actualEndDate
  documentList = new Array<UpdateDocModel>()
  docDescription
  status
  matchedPhotosList = new Array<Photo>()
  grants: any
  grantDate
  nonMatchedPhotosList = new Array<Photo>()
  progress
}

export class Field {
  fieldId;
  fieldName;
  status;
  budgetValue
  allocatedBudget

}
export class ActivityDetail {
  activityId
  type
  activityDescription
  learningCenterId
  learningCenter
  categoryId
  category
  status
}
export class MaintainActivity {

  activityData = new ActivityData()
  milestoneList = new Array<UpdateMilestone>()
  fieldList = new Array<Field>()
  claimList = new Array<Claim>()
  startDate
  endDate
  remark
  userModified
  userInserted
  dateInserted
  dateModified
}

export interface UploadProgress {
  // status: UploadStatus; // current status of upload for specific file (Queue | Uploading | Done | Canceled)
  data?: {
    percentage: number; // percentage of upload already completed
    speed: number; // current upload speed per second in bytes
    speedHuman: string; // current upload speed per second in human readable form,
    eta: number; // estimated time remaining in seconds
    etaHuman: string; // estimated time remaining in human readable format
  };
}

export class ResponseData {
  responseCode
  responseData
}
export interface UploadFile {
  id: string; // unique id of uploaded file instance
  fileIndex: number; // fileIndex in internal ngx-uploader array of files
  lastModifiedDate: Date; // last modify date of the file (Date object)
  name: string; // original name of the file
  size: number; // size of the file in bytes
  type: string; // mime type of the file
  form: FormData; // FormData object (you can append extra data per file, to this object)
  progress: UploadProgress;
  response: any// response when upload is done (parsed JSON or string)
  responseStatus?: number; // response status code when upload is done
}

// output events emitted by ngx-uploader
export interface UploadOutput {
  type: 'addedToQueue' | 'allAddedToQueue' | 'uploading' | 'done' | 'removed' | 'start' | 'cancelled' | 'dragOver' | 'dragOut' | 'drop';
  file?: UploadFile;
  nativeFile?: File; // native javascript File object, can be used to process uploaded files in other libraries
}

// input events that user can emit to ngx-uploader
export interface UploadInput {
  type: 'uploadAll' | 'uploadFile' | 'cancel' | 'cancelAll' | 'remove' | 'removeAll';
  url?: string; // URL to upload file to
  method?: string; // method (POST | PUT)
  id?: string; // unique id of uploaded file
  fieldName?: string; // field name (default 'file')
  fileIndex?: number; // fileIndex in internal ngx-uploader array of files
  file?: UploadFile; // uploading file
  data?: { [key: string]: string | Blob }; // custom data sent with the file
  headers?: { [key: string]: string }; // custom headers
  concurrency?: number; // concurrency of how many files can be uploaded in parallel (default is 0 which means unlimited)
  withCredentials?: boolean; // apply withCredentials option
}






@Component({
  // changeDetection:ChangeDetectionStrategy.OnPush,
  selector: 'app-define-activity',
  templateUrl: './define-activity.component.html',
  styleUrls: ['./define-activity.component.css'],
  providers: [CommonWebService, DatePipe, LoggedInUserModel, NumberFormatPipe]

})


export class DefineActivityComponent implements OnInit {
  year: any;
  selectedDescription: any;
  selectedBudgetListExtra = new Array<Budget>()
  addingNewMilestone: boolean;
  addingNewBudget: boolean;
  plannedActivityEndDate: any;
  plannedActivityStartDate: any;
  loactionForMaintain: string
  nodeBelongs: Node;
  bisId: any;
  commentList = new Array<Comment>();
  dateCommented: any;
  postedComment: Comment = new Comment();
  selectedMilestoneId: any;
  docList = new Array<MaintainDocument>();
  firstPhoto: boolean;
  currentLocation: any;
  milestoneNameControl: FormControl;
  budgetValControl: FormControl;
  budgetNameControl: FormControl;
  currentlySelectedMilestone: any;
  totalGrant: number;
  totalClaim: any;
  otherBudgetVal: any;
  otherBudget: any;
  number: any;
  selectedAreaPMU: any;
  loadingTree: boolean;
  claimForm: FormGroup;
  maleCount: number;
  femaleCount: number;
  initialLong: number;
  initialLang: number;
  selectedBudgetForShow: any;
  divisionControl: FormControl;
  learningCenterControl: FormControl;
  commentControl: FormControl

  userGroup: string;
  @ViewChild("viewActivity") public viewActivity: ModalDirective;

  @ViewChild("createDocument") public createDocument: ModalDirective;
  @ViewChild("addMilestone") public addMilestone: ModalDirective;
  @ViewChild("addBudget") public addBudget: ModalDirective;
  @ViewChild("showDocuments") public showDocuments: ModalDirective;
  @ViewChild("addPictures") public addPictures: ModalDirective;
  @ViewChild("showLearningCenters") public showLearningCenters: ModalDirective;
  @ViewChild("addCalims") public addCalims: ModalDirective;

  public milestoneStatus
  public success: boolean;
  public error: boolean;
  public warnning: boolean;
  public msg

  public param;

  public evt3;

  public status
  public modalData: ModalData = new ModalData()
  public document: Documents = new Documents()
  public documentList = new Array<Documents>()

  public selecteddocumentList
  public selectedDocument = new Documents()


  public learningCenterList = new Array<Data>()
  public selectedLearningCenter = new Data()

  public categoryList = new Array<ActivityCategoryModel>()
  public selectedCategory = new ActivityCategoryModel()

  public budgetList = new Array<Budget>()
  public selectedBudget = new Budget()
  public selectedBudgetList
  public actualBudget = 0
  public comment

  public milestonesAll = new Array<MilestoneAll>()
  public milestonesList
  public selectedMilestonesList = new Array<MilestoneAll>()
  public selectedMilestone = new MilestoneAll()
  public selectedMilestoneSub = new Milestone()
  public plannedStartDate
  public plannedEndDate

  public selectedType;
  public description

  public activity = new Activity()
  public activityList

  public initialDocList: boolean = true
  public maintainActivity

  public userId;
  public userType = "ECDOFFICER";
  public viewChangeParam;
  public viewChangeParamSub

  public activityId
  public activityStatus
  public addedDocs = new Array<UpdateDocModel>();
  public addPhotoBtnCount = 0
  public response: Response

  public selectedMilestoneforPhoto = new UpdateMilestone()
  public disable = false

  public photoRow1 = new Array<Photo>()
  public photoRow2 = new Array<Photo>()

  public photo1 = new Photo()
  //this.photo1.path = n
  public photo2 = new Photo();
  public photo3 = new Photo();
  public photo4 = new Photo();

  public photo = new Photo()
  public photoList = new Array<Photo>()

  public updateDocModel
  public updateDocModelList = new Array<UpdateDocModel>()

  public selectedUpdatedMilestoneList = new Array<any>()
  public selectedUpdatedMilestone = new UpdateMilestone()
  public maintainActivityList = new Array<MaintainActivity>()
  public maintainActivityModel = new MaintainActivity()
  public selectedMaintainActivityFieldList = new Array<Field>()

  public statusList = new Array<any>()

  public allocatedBudget: any
  public matched1: boolean = false
  public matched2: boolean = false
  public matched3: boolean = false
  public matched4: boolean = false

  public matchedPhotoCount;
  public nonMatchedPhotoCount;


  public imageSavingURL
  public images = new Array<any>();

  public imageCount = new Array<any>()
  public milestoneId
  formGroup: FormGroup;

  public otherMilestoneName
  public categoryMilestoneList = new Array<CategoryMilestone>()
  ////////////////////////////
  // options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  // public userData
  public memberDetails
  file = new Array<Node>()
  public businessList = new Array<BusinessLevel>()
  public selectedNode = new Node()
  public previouslySelectedNode = new Node()
  public iterater = 0

  public baseUrl = API_URL

  public longitude;
  public latitude
  public headers: Headers = new Headers;
  public type;
  public category = new Category()
  public startDate
  public endDate
  public descControl;
  public catControl;
  public selectedDivision = new Node();

  public claimGrid = new Array<Claim>()
  public claim = new Claim()

  public selectedActivityType: number;
  public loading: boolean
  public statusofaActivity;

  // public mapUrl: SafeResourceUrl

  ////////////////////////////

  constructor(public commonService: CommonWebService, public userData: LoggedInUserModel, public router: Router, public route: ActivatedRoute, private datePipe: DatePipe, private formBuilder: FormBuilder, private http: Http, private numberPipe: NumberFormatPipe, private sanitizer: DomSanitizer) {
    this.userId = sessionStorage.getItem("bisId");
    if (this.userId == 3) {
      this.selectedType = "PHDT"
    } else if (this.userId == 2 || this.userId == 1) {
      this.selectedType = "PMU"
    }

    this.files = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = this.humanizeBytes;
    this.statusList = [


      { label: 'Status', value: { id: 0, name: 'Pending' } },
      // { label: 'Pending', value: { id: 6, name: 'Pending' } },
      { label: 'In Progress', value: { id: 2, name: 'In Progress' } },
      { label: 'Completed', value: { id: 3, name: 'Completed' } },
      { label: 'Cancel', value: { id: 1, name: 'Pending' } },
      { label: 'Grant Approved', value: { id: 4, name: 'Completed' } },
      { label: 'Grant Released', value: { id: 5, name: 'Completed' } }

    ]


  }
  ngOnInit() {
    this.descControl = new FormControl('', [Validators.required]);
    this.learningCenterControl = new FormControl('', [Validators.required]);
    this.divisionControl = new FormControl('', [Validators.required]);
    this.catControl = new FormControl('', [Validators.required]);
    this.budgetNameControl = new FormControl('', [Validators.required]);
    this.budgetValControl = new FormControl('', [Validators.required]);
    this.milestoneNameControl = new FormControl('', [Validators.required]);
    this.commentControl = new FormControl('', [Validators.required]);
    this.loading = true
    this.loadingTree = true

    this.userGroup = sessionStorage.getItem('userGroup');
    // this.userData = JSON.parse(atob(getUser));

    // this.activityList = null



    this.getUser()

    // this.getAllLocations()

    // this.getAllLearningCenters()

    this.validation()



    this.plannedStartDate = new Date()
    this.plannedEndDate = new Date()
    this.startDate = new Date()
    this.endDate = new Date()




  }

  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));

    this.bisId = this.userData[0].extraParams

    if (this.userGroup == 'AdminECDGrp' || this.userGroup == 'AuthECDGrp') {
      this.userType = "ADMIN"
      this.getActivitySeperately()

    } else if (this.userGroup == "ECDOfficerGrp" || this.userGroup == "ECDPIOOfficerGrp" || this.userGroup == "ECDPHDTGrp") {
      this.userType = "ECDOFFICER"
      this.getActivitySeperately()
    }

    if (this.userGroup == "ECDMasterGrp") {
      if (this.bisId != undefined || this.bisId != null) {
        this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getLocation").subscribe(res => {
          if (res.responseCode == 1) {
            this.nodeBelongs = res.responseData
            if(this.bisId<38){
              this.loactionForMaintain = this.nodeBelongs[0].provinceId + "-" + this.nodeBelongs[0].provinceId + this.nodeBelongs[0].code
            }else{
              this.loactionForMaintain = this.nodeBelongs[0].provinceId + "-" + this.nodeBelongs[0].districtId
            }
            


            this.getActivitySeperately()

          }


        }), error => {
          console.log("ERROR")
        }
      }
    }



  }

  getActivitySeperately() {
    this.activityList = new Array<ActivityData>()
    this.claimGrid = new Array<Claim>()

    // MAINTAIN
    this.route.queryParams.subscribe(params => {
      this.maintainActivity = params['maintain']
      this.type = params['type']


      if (this.maintainActivity == 'true') {

        if (this.userType == "ECDOFFICER") {
          this.viewChangeParam = "ECDOFFICER_MAINTAIN"

          if (this.userGroup == "ECDMasterGrp") {

            this.getAllActivitiesforMainTain(this.loactionForMaintain)
          } else {

            this.getAllActivitiesbyStatus(4)
          }


        } else if (this.userType == "ADMIN") {

          if (this.type == "maintain") {
            this.viewChangeParam = "ECDOFFICER_MAINTAIN"

            this.getAllActivitiesbyStatus(4)
          } else if (this.type == "auth") {

            this.viewChangeParam = "ADMIN_MAINTAIN"
            this.getAllActivitiesAuth()
          }
        }
        this.modalData.mainTitle = "Maintain Activity"
      }




      // DEFINE
      if (this.maintainActivity == 'false') {

        if (this.userType == "ECDOFFICER" || this.userType == "ADMIN") {

          this.getAllActivities()
          this.viewChangeParam = "ECDOFFICER_DEFINE"
        }


        this.modalData.mainTitle = "Define Activity"

      }

    })

  }


  getAllActivitiesforMainTain(location) {

    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllActivitiesMaintain?location=" + location).subscribe(res => {

      if (res.responseCode == 1) {
        this.loading = false;
        this.activityList = res.responseData
      }

    }), error => {
      console.log("ERROR")
    }
  }
  openActivity(param1, param2: String) {




    this.otherMilestoneName = null
    this.otherBudget = null
    this.otherBudgetVal = 0
    this.totalGrant = 0
    this.totalClaim = 0
    this.selectedDivision = new Node()

    if (param1 != null) {
      this.status = param1.status
    }


    if (param1 == null && param2 == 'new') {
      this.plannedActivityStartDate = null
      this.plannedActivityEndDate = null
      this.loadingTree = true
      this.getAllMilestoneList()
      this.getAllDocuments()
      this.getAllCategories()
      this.getAllBudgetList()
      this.allocatedBudget = 0
      this.selectedActivityType = 1
      this.selectedLearningCenter = new Data()
      this.collapseAll()
      this.getAllLocations()
      this.resetFields()

      this.modalData.title = "Create Activity"
      this.resetFields()
      // this.getAllLearningCenters()


      this.param = 'new'
      this.actualBudget = 0
      // this.selectedType = "PMU"
      this.viewChangeParamSub = "ECDOFFICER_DEFINE_NEW"
      console.log()

      this.viewActivity.show()

    } else if (param2 == 'view') {
      this.modalData.title = "View Activity"
      this.param = 'view'


      if (this.viewChangeParam == "ECDOFFICER_DEFINE") {
        this.viewChangeParamSub = "ECDOFFICER_DEFINE_VIEW"

      }
      this.getActivitybyId(param1)



      this.viewActivity.show()

    } else if (param2 == 'edit') {


      if (this.viewChangeParam == "ECDOFFICER_DEFINE") {
        // this.getAllCategories()
        this.statusofaActivity = 0
        this.selectedBudgetListExtra = new Array<Budget>()

        this.actualBudget = 0
        this.allocatedBudget = 0
        this.addingNewBudget = false
        this.addingNewMilestone = false
        this.getAllMilestoneList()
        this.getAllBudgetList()
        this.getAllDocuments()
        this.param = 'edit'
        this.viewChangeParamSub = "ECDOFFICER_DEFINE_EDIT"
        this.getActivitybyId(param1)
        this.modalData.title = "Update Activity"

      } else if (this.viewChangeParam == "ADMIN_MAINTAIN") {
        this.param = 'edit'
        this.viewChangeParamSub = "ADMIN_MAINTAIN_EDIT"

        this.getAllMAintainActivities(param1)
        this.modalData.title = "Authorize Activity"

      } else if (this.viewChangeParam == "ECDOFFICER_MAINTAIN") {

        this.param = 'edit'

        this.viewChangeParamSub = "ECDOFFICER_MAINTAIN_EDIT"


        this.getAllMAintainActivities(param1)
        this.modalData.title = "Maintain Activity"



      }
      //  this.getActivitybyId(param1)


      console.log("param", this.param)



      this.viewActivity.show()
    }





  }


  getAllBudgetList() {
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllBudgetFields").subscribe(res => {

      this.budgetList = res.responseData;
      this.selectedBudget = this.budgetList[0]

    }), error => {
      console.log("ERROR")
    }
  }
  getActivitybyId(param1: ActivityData) {
    this.milestonesList = new Array<Milestone>()
    this.selectedBudgetList = new Array<Budget>()

    this.activityId = param1.activityId
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityById?activityId=" + this.activityId).subscribe(res => {
      this.activity = res.responseData
      // this.year= this.activity.activityData.year
      this.description = this.activity.activityData.activityDescription
      this.selectedType = this.activity.activityData.type
      this.selectedCategory.categoryName = this.activity.activityData.category
      this.selectedCategory.categoryId = this.activity.activityData.categoryId
      this.statusofaActivity = this.activity.activityData.status
      this.plannedActivityStartDate = this.activity.activityData.plannedStartDate
      this.plannedActivityEndDate = this.activity.activityData.plannedEndDate
      if (this.selectedType == "PMU") {
        this.selectedLearningCenter.name = this.activity.activityData.learningCenter
        this.selectedLearningCenter.sn = this.activity.activityData.learningCenterId
        this.selectedActivityType = this.activity.activityData.selectedPMUType
      } else if (this.selectedType == "PHDT") {
        this.selectedDivision.label = this.activity.activityData.learningCenter
        this.selectedDivision.id = this.activity.activityData.learningCenterId
        this.femaleCount = this.activity.activityData.femalel
        this.maleCount = this.activity.activityData.male

      }

      for (let a of this.activity.milestoneList) {
        let m = new Milestone()

        m.milestoneId = a.milestoneId
        m.milestoneName = a.milestoneName


        m.plannedStartDate = m.plannedStartDate == null ? "" : new Date(a.plannedStartDate)
        m.plannedEndDate = m.plannedEndDate == null ? "" : new Date(a.plannedEndDate)
        m.documentList = a.documentList

        this.milestonesList.push(m)

      }
      this.selectedBudgetList = this.activity.fieldList
      for (let a of this.selectedBudgetList) {
        this.actualBudget = this.actualBudget + a.budgetValue
        this.allocatedBudget = this.allocatedBudget + a.allocatedBudget
      }


    }), error => {

    }
  }


  getAllMAintainActivities(param1: ActivityData) {

    this.milestoneStatus = 0;
    this.claimGrid = new Array<Claim>()
    this.activityId = param1.activityId
    if (this.selectedMilestonesList.length != 0) {

      this.selectedMilestonesList.splice(0, this.selectedMilestonesList.length - 1)

    }
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getMaintainActivityById?activityId=" + this.activityId).subscribe(res => {



      if (res.responseCode == 1) {

        this.maintainActivityModel = res.responseData



        this.setMaintenanceValues()

      }


    }), error => {
      console.log("ERROR")
    }

  }
  setMaintenanceValues() {

   
    this.allocatedBudget = 0
    this.actualBudget = 0
    this.totalGrant = 0
    this.totalClaim = 0
    this.selectedUpdatedMilestoneList = new Array<UpdateMilestone>()
    this.selectedMaintainActivityFieldList = new Array<Field>()
    this.selectedType = this.maintainActivityModel.activityData.type
    this.selectedMaintainActivityFieldList = this.maintainActivityModel.fieldList
    for (let a of this.selectedMaintainActivityFieldList) {

      this.allocatedBudget = this.allocatedBudget + a.allocatedBudget
      this.actualBudget = this.actualBudget + a.budgetValue

    }
    this.description = this.maintainActivityModel.activityData.activityDescription

    if (this.selectedType == "PMU") {


      this.selectedActivityType = this.maintainActivityModel.activityData.selectedPMUType
      this.selectedLearningCenter.sn = this.maintainActivityModel.activityData.learningCenterId
      this.selectedLearningCenter.name = this.maintainActivityModel.activityData.learningCenter
    } else if (this.selectedType == "PHDT") {

      this.femaleCount = this.maintainActivityModel.activityData.femalel
      this.maleCount = this.maintainActivityModel.activityData.male
      this.selectedDivision.label = this.maintainActivityModel.activityData.learningCenter
      this.selectedDivision.id = this.maintainActivityModel.activityData.learningCenterId
    }



    this.selectedCategory.categoryId = this.maintainActivityModel.activityData.categoryId
    this.selectedCategory.categoryName = this.maintainActivityModel.activityData.category
    // this.selectedCategory.totalBudget = this.maintainActivityModel.activity\
    this.year = this.maintainActivityModel.activityData.year


    this.activityStatus = 4

    for (let i = 0; i < this.maintainActivityModel.milestoneList.length; i++) {



      let milestone = new UpdateMilestone()
      this.selectedUpdatedMilestone = this.maintainActivityModel.milestoneList[i]
      this.totalGrant = this.totalGrant + this.maintainActivityModel.milestoneList[i].grants
      this.selectedUpdatedMilestone.plannedEndDate = this.selectedUpdatedMilestone.plannedEndDate == null ? "" : this.convertDatetoString(this.selectedUpdatedMilestone.plannedEndDate)
      this.selectedUpdatedMilestone.plannedStartDate = this.selectedUpdatedMilestone.plannedStartDate == null ? "" : this.convertDatetoString(this.selectedUpdatedMilestone.plannedStartDate)
      this.selectedUpdatedMilestone.actualStartDate = this.selectedUpdatedMilestone.actualStartDate == null ? "" : new Date(this.selectedUpdatedMilestone.actualStartDate)
      this.selectedUpdatedMilestone.actualEndDate = this.selectedUpdatedMilestone.actualEndDate == null ? "" : new Date(this.selectedUpdatedMilestone.actualEndDate)
      this.selectedUpdatedMilestone.grantDate = this.selectedUpdatedMilestone.grantDate == null ? "" : new Date(this.selectedUpdatedMilestone.grantDate)
      this.selectedUpdatedMilestone.matchedPhotosList = this.maintainActivityModel.milestoneList[i].matchedPhotosList
      this.selectedUpdatedMilestone.nonMatchedPhotosList = this.maintainActivityModel.milestoneList[i].nonMatchedPhotosList
      this.selectedUpdatedMilestone.documentList = this.maintainActivityModel.milestoneList[i].documentList
      this.selectedUpdatedMilestone.status = this.maintainActivityModel.milestoneList[i].status
      this.selectedUpdatedMilestone.progress = this.maintainActivityModel.milestoneList[i].progress

      let st = this.maintainActivityModel.milestoneList[i].status
      if (st == 1) {
        this.selectedUpdatedMilestone.status = this.statusList[3].label
        // this.selectedUpdatedMilestone.progress = 0
      } else if (st == 2) {
        this.selectedUpdatedMilestone.status = this.statusList[1].label
        // this.selectedUpdatedMilestone.progress = 20
      } else if (st == 3) {
        this.selectedUpdatedMilestone.status = this.statusList[2].label
        // this.selectedUpdatedMilestone.progress = 100
      } else if (st == 4) {
        this.selectedUpdatedMilestone.status = this.statusList[4].label
        // this.selectedUpdatedMilestone.progress = 100
      } else if (st == 4) {
        this.selectedUpdatedMilestone.status = this.statusList[5].label
        // this.selectedUpdatedMilestone.progress = 100
      }
      // else if (st == 5) {
      //   this.selectedUpdatedMilestone.status = this.statusList[6].label

      // }

      for (let b of this.selectedUpdatedMilestone.matchedPhotosList) {
        let photo = new Photo()
        photo.photoId = b.photoId
        photo.description = b.description
        photo.latitude = b.latitude
        photo.longitude = b.longitude
        photo.path = API_URL + "ECD_images/activity" + b.path

        this.photoList.push(photo)
      }

      for (let b of this.selectedUpdatedMilestone.nonMatchedPhotosList) {

        let photo = new Photo()
        photo.photoId = b.photoId
        photo.description = b.description
        photo.latitude = b.latitude
        photo.longitude = b.longitude
        photo.path = API_URL + "ECD_images/activity" + b.path


        this.photoList.push(photo)
      }


      this.selectedUpdatedMilestoneList.push(this.selectedUpdatedMilestone)


    }
    this.claimGrid = this.maintainActivityModel.claimList
    for (let claim of this.maintainActivityModel.claimList) {
      this.totalClaim = this.totalClaim + claim.amount
    }
    this.totalClaim = this.totalClaim - this.totalGrant
  }

  onSelect(ev) {
    this.evt3 = ev;
  }

  setOutput(milestone, evt) {

    var index = this.selectedUpdatedMilestoneList.indexOf(milestone)
    let st = evt.value.id
    console.log("ST", st)
    if (st == 1) {
      this.selectedUpdatedMilestoneList[index].status = this.statusList[3].label
      this.selectedUpdatedMilestoneList[index].progress = 0

    } else if (st == 2) {//in progress
      this.selectedUpdatedMilestoneList[index].status = this.statusList[1].label
      this.selectedUpdatedMilestoneList[index].progress = 20
    } else if (st == 3) {
      this.selectedUpdatedMilestoneList[index].status = this.statusList[2].label
      this.selectedUpdatedMilestoneList[index].progress = 100
    } else if (st == 4) {
      this.selectedUpdatedMilestoneList[index].status = this.statusList[4].label
      this.selectedUpdatedMilestoneList[index].progress = 100
    } else if (st == 5) {
      this.selectedUpdatedMilestoneList[index].status = this.statusList[5].label
      this.selectedUpdatedMilestoneList[index].progress = 100
    }
    // else if (st == 6) {
    //   this.selectedUpdatedMilestoneList[index].status = this.statusList[1].label
    //   this.selectedUpdatedMilestoneList[index].progress = 100
    // }


  }
  convertDatetoString(date): any {


    let a = new Array<any>()
    a = date.split(" ")

    return a[0]
  }

  getAllActivities() {

    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllActivities").subscribe(res => {

      if (res.responseCode == 1) {
        this.loading = false;
        this.activityList = res.responseData
      }




    }), error => {
      console.log("ERROR")
    }
  }
  getAllActivitiesbyStatus(status) {

    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllActivities?status=" + status).subscribe(res => {
      if (res.responseCode == 1) {
        this.loading = false;
        this.activityList = res.responseData
      }

    }), error => {
      console.log("ERROR")
    }
  }

  getAllActivitiesAuth() {

    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllActivitiesAuth?type=" + this.bisId).subscribe(res => {
      if (res.responseCode == 1) {
        this.loading = false;
        this.activityList = res.responseData
      }

    }), error => {
      console.log("ERROR")
    }
  }

  loadCatDetails(cat) {

    this.actualBudget = 0
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategoryDetails?catId=" + cat.categoryId).subscribe(res => {



      this.milestonesList = new Array<any>()
      this.category = res.responseData
      this.actualBudget = this.selectedCategory.totalBudget
      for (let a of this.category.milestoneList) {
        let m = new Milestone()

        m.milestoneName = a.milestoneName
        m.milestoneId = a.milestoneId
        // m.plannedStartDate = new Date()//Set todays date for milstone as planned start date
        // m.plannedEndDate = new Date()//Set todays date for milstone as planned end date
        m.documentList = a.documentList
        this.milestonesList.push(m)
      }


    }), error => {
      console.log("ERROR")
    }
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategoryDetailsBudget?catId=" + this.selectedCategory.categoryId).subscribe(res => {



      this.selectedBudgetList = new Array<Budget>()


      this.selectedBudgetForShow
      if (res.responseCode == 1) {
        this.selectedBudgetList = res.responseData
      }




    }), error => {
      console.log("ERROR")
    }
  }


  getAllCategories() {

    this.category = new Category()
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategories").subscribe(res => {


      if (res.responseCode == 1) {
        this.categoryList = res.responseData
        if (this.categoryList.length != 0) {
          this.selectedCategory = this.categoryList[0]
          if (this.selectedCategory.categoryId != null) {

            this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategoryDetails?catId=" + this.selectedCategory.categoryId).subscribe(res => {



              this.category = res.responseData
              this.actualBudget = this.selectedCategory.totalBudget
              this.milestonesList = new Array<Milestone>()

              for (let a of this.category.milestoneList) {
                let m = new Milestone()
                m.milestoneName = a.milestoneName
                m.milestoneId = a.milestoneId
                m.documentList = a.documentList
                // m.plannedStartDate = new Date()//todays date as
                // m.plannedEndDate = new Date()
                this.milestonesList.push(m)
              }



            }), error => {
              console.log("ERROR")
            }

            this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategoryDetailsBudget?catId=" + this.selectedCategory.categoryId).subscribe(res => {



              this.selectedBudgetList = new Array<Budget>()
              this.selectedBudgetList = res.responseData



            }), error => {
              console.log("ERROR")
            }
          }
        }

      }


    }), error => {
      console.log("ERROR")
    }
  }

  getAllDocuments() {
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllDocuments").subscribe(res => {
      this.documentList = res.responseData
      this.selectedDocument = this.documentList[0]

    }), error => {
      console.log("ERROR")
    }
  }

  getAllLearningCenters() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCenters").subscribe(res => {

      this.learningCenterList = res.responseData;

      if (this.selectedLearningCenter.sn == null) {
        this.selectedLearningCenter = this.learningCenterList[0]
      }

    }), error => {
      console.log("ERROR")
    }
  }

  getAllMilestoneList() {
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllMilestones").subscribe(res => {

      this.milestonesAll = res.responseData;
      this.selectedMilestone = this.milestonesAll[0]
      this.setMilestoneValues()



    }), error => {
      console.log("ERROR")
    }

  }

  ///////////////////////////DOCUMENTS//////////////////////////////////////////////////////////////////////

  opencreateDocument(a: Milestone) {
    this.selectedMilestoneSub = a
    if (this.selecteddocumentList == null) {
      this.selecteddocumentList = new Array<Documents>()
      this.getAllDocuments()
    }

    if (a.documentList.length == 0) {
      this.getAllDocuments()
    }
    this.selecteddocumentList = a.documentList
    this.createDocument.show()
  }

  addDocumentstoGrid() {

    this.selecteddocumentList.push(this.selectedDocument)

    this.removeFromList()
    this.selectedDocument = this.documentList[0]
  }

  removeRow(a: Documents) {

    var index = this.selecteddocumentList.indexOf(a);

    this.selecteddocumentList.splice(index, 1);

    this.addToList(a);

  }
  addToList(a: Documents) {
    this.documentList.push(a)

  }

  saveDocList() {
    let selectedDocumentList: Documents[] = this.selecteddocumentList
    this.selectedMilestoneSub.documentList = selectedDocumentList

    this.createDocument.hide()

  }
  ////////////////////////////MILESTONES////////////////////////////////////////////////

  openAddMilestones() {
    this.getAllMilestoneList()
    this.plannedStartDate = new Date()
    this.plannedEndDate = new Date()
    if (this.milestonesList == null) {
      this.milestonesList = new Array<Milestone>()
      this.getAllMilestoneList()

    }
    // this.reoveFromMilestonesPickList()
    this.addMilestone.show()


  }

  addMilestoneExtra() {
    this.addingNewMilestone = true;
    let m = new Milestone()
    if (this.otherMilestoneName != undefined) {

      m.milestoneId = 0
      m.milestoneName = this.otherMilestoneName
    } else {
      m.milestoneId = this.selectedMilestone.milestoneId
      m.milestoneName = this.selectedMilestone.milestoneName
    }


    m.plannedStartDate = new Date()
    m.plannedEndDate = new Date()

    this.milestonesList.push(m)
    this.otherMilestoneName = ""
  }



  setMilestoneValues() {

    this.selectedMilestoneSub.milestoneId = this.selectedMilestone.milestoneId
    this.selectedMilestoneSub.milestoneName = this.selectedMilestone.milestoneName

    if (this.selectedMilestoneSub.plannedStartDate == null && this.selectedMilestoneSub.plannedEndDate == null) {
      this.selectedMilestoneSub.plannedStartDate = new Date().getDate
      this.selectedMilestoneSub.plannedEndDate = new Date().getDate
    }

  }

  addMilestonesC() {

  }
  addMilestonesToMainGrid() {


    let d1 = this.datePipe.transform(this.plannedEndDate, 'yyyy-MM-dd');
    this.selectedMilestoneSub.plannedStartDate = this.datePipe.transform(this.plannedStartDate, 'yyyy-MM-dd');
    this.selectedMilestoneSub.plannedEndDate = this.datePipe.transform(this.plannedEndDate, 'yyyy-MM-dd');

    if (this.selectedMilestoneSub.documentList == null || this.selectedMilestoneSub.documentList == undefined) {
      this.selectedMilestoneSub.documentList = new Array<Documents>()
    }
    let milestoneLocal: Milestone = this.selectedMilestoneSub

    this.milestonesList.push(milestoneLocal)
    this.addMilestone.hide()
  }


  resetFields() {



    this.description = null
    this.selecteddocumentList = null

  }




  addBudgettoMainGrid() {
    this.selectedBudgetList.push(this.selectedBudget)

    this.addBudget.hide()
  }
  removeFromList() {
    for (let i = 0; i <= this.selecteddocumentList.length; i++) {
      for (let j = 0; j <= this.documentList.length; j++) {
        if (this.selecteddocumentList[i] == this.documentList[j]) {
          var index = this.documentList.indexOf(this.documentList[j]);
          this.documentList.splice(index, 1);
        }
      }
    }
  }

  reoveFromMilestonesPickList() {
    for (let i = 0; i <= this.milestonesAll.length; i++) {
      for (let j = 0; j <= this.milestonesList.length; j++) {
        if (this.milestonesAll[i].milestoneId == this.milestonesList[j].milestoneId) {
          var index = this.milestonesAll.indexOf(this.milestonesAll[j]);
          this.milestonesAll.splice(index, 1);
        }
      }
    }
  }




  removeMilestoneRows(a) {
    var index = this.milestonesList.indexOf(a);
    this.milestonesList.splice(index, 1);
  }

  removeBudgetRows(a) {


    this.actualBudget = this.actualBudget - a.budgetValue
    this.allocatedBudget = this.allocatedBudget - a.allocatedBudget
    if (this.statusofaActivity == 4) {
      var index1 = this.selectedBudgetListExtra.indexOf(a);
      this.selectedBudgetListExtra.splice(index1, 1);
      var index = this.selectedBudgetList.indexOf(a);
      this.selectedBudgetList.splice(index, 1);
    } else {
      var index = this.selectedBudgetList.indexOf(a);
      this.selectedBudgetList.splice(index, 1);
    }

  }
  openAddBudget() {
    if (this.selectedBudgetList == null) {
      this.selectedBudgetList = new Array<Budget>()
      this.getAllBudgetList()
    }
    this.addBudget.show()

  }



  cloasCreateActivity() {
    this.resetFields()
    // this.viewChangeParam=""
    this.viewActivity.hide()
  }

  clickOther() {

  }

  selectMilestone() {

  }
  updateActivity(status) {


    // this.activity.milestoneList = this.selectedUpdatedMilestoneList
    this.headers.set("status", status)
    this.setValues(status)


    let options = new RequestOptions({ headers: this.headers });
    this.activity.activityData.activityId = this.activityId
    this.commonService.processPostWithHeaders(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/updateActivity", this.activity, options).subscribe(res => {




      if (res.responseCode == 1) {
        this.success = true;

        setTimeout(() => {
          this.success = false;
          this.viewActivity.hide()
        }, 1000);


        this.getAllActivities()

        // this.viewActivity.hide()
      }

    }), error => {
      console.log("ERROR")
    }

  }

  testClick() {

  }

  saveActivity(status) {




    if (this.milestonesList != undefined) {
      if (this.selectedBudgetList != undefined) {



        if (this.selectedActivityType != 1 && this.selectedType == 'PMU') {

          let center = new Center()
          center.location_id = this.selectedAreaPMU
          center.name = this.selectedLearningCenter.name
          if (this.selectedActivityType == 2) {


            this.commonService.processPost(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveResourceCenter", center).subscribe(res => {


              if (res.responseCode == 1) {

                this.selectedLearningCenter.sn = res.responseData
                this.saveAction(status)
              } else {
                this.warnning = true
                this.msg = "Resource Center Cannot Create!"
              }

            }), error => {
              this.warnning = true
              this.msg = "Server Error!"
            }


          } else if (this.selectedActivityType == 3) {


            this.commonService.processPost(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveCenter", center).subscribe(res => {


              if (res.responseCode == 1) {
                this.selectedLearningCenter.sn = res.responseData
                this.saveAction(status)
              } else {
                this.warnning = true
                this.msg = "Resource Center Cannot Create!"
              }

            }), error => {
              this.warnning = true
              this.msg = "Server Error!"
            }


          }
        } else if (this.selectedActivityType == 1 && this.selectedType == 'PMU') {

          this.saveAction(status)

        } else if (this.selectedType == 'PHDT') {
          this.saveAction(status)
        }

      } else {
        this.warnning = true
        this.msg = "Budget List cannot be empty"
        setTimeout(() => {
          this.warnning = false;
          this.msg = null
          this.description = null
        }, 1000);
      }

    } else {
      this.warnning = true
      this.msg = "Milestone List cannot be empty"
      setTimeout(() => {
        this.warnning = false;
        this.msg = null
        this.description = null
      }, 1000);
    }


    console.log("SELECTED NODE", this.currentLocation)
  }



  saveAction(status) {
    // this.mapUrl=null

    if (this.selectedType == "PMU") {

      if (this.selectedLearningCenter.address != null || this.selectedLearningCenter.address != undefined) {

        /////////////////////////////////SEt Values////////////////////////////////////// https://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&key=YOUR_API_KEY

        this.http.get("https://maps.google.com/maps/api/geocode/json?address=" + this.selectedLearningCenter.address + "&key=AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc").map(res => res.json()).subscribe(res => {

          if (res.results.length != 0) {

            this.activity.activityData.longitude = res.results[0].geometry.location.lng
            this.activity.activityData.latitude = res.results[0].geometry.location.lat
            this.activity.milestoneList = new Array<Milestone>()
            this.activity.fieldList = new Array<Field>()
            this.activityStatus = status
            if (this.activity.activityData.activityId != null) {
              this.activity.activityData.activityId = this.activityId
            }
            this.activity.activityData.status = status
            this.activity.activityData.activityDescription = this.description
            if (this.selectedType == "PMU") {
              this.activity.activityData.learningCenterId = this.selectedLearningCenter.sn
              this.activity.activityData.learningCenter = this.selectedLearningCenter.name
              this.activity.activityData.address = this.selectedLearningCenter.address
              this.activity.activityData.category = this.selectedCategory.categoryName
              this.activity.activityData.categoryId = this.selectedCategory.categoryId
              let ar = new Array()
              ar = this.currentLocation.lifeCode.split("-")
              ar.splice(2, 1)
              let currentLocation = ar[0] + "-" + ar[1] + "-" + this.currentLocation.code
              console.log("Current Lo", currentLocation)
              if (this.selectedActivityType == 1) {

                this.activity.activityData.locationCode = currentLocation
              } else {
                this.activity.activityData.locationCode = currentLocation
              }
            } else if (this.selectedType == "PHDT") {
              this.activity.activityData.learningCenterId = this.selectedDivision.id
              this.activity.activityData.learningCenter = this.selectedDivision.label
              this.activity.activityData.address = this.selectedDivision.label
              this.activity.activityData.category = this.selectedCategory.categoryName
              this.activity.activityData.categoryId = this.selectedCategory.categoryId
              this.activity.activityData.locationCode = this.currentLocation.lifeCode
              this.activity.female = this.femaleCount
              this.activity.male = this.maleCount
            }
            this.activity.activityData.selectedPMUType = this.selectedActivityType
            this.activity.activityData.type = this.selectedType
            for (let b of this.milestonesList) {
              let m = new Milestone()
              if (this.activity.activityData.activityId * 10 < b.milestoneId) {
                m.milestoneId = 0
              } else {
                m.milestoneId = b.milestoneId
              }
              m.milestoneName = b.milestoneName
              m.plannedStartDate = this.datePipe.transform(b.plannedStartDate, 'yyyy-MM-dd')
              m.plannedEndDate = this.datePipe.transform(b.plannedEndDate, 'yyyy-MM-dd')
              m.documentList = b.documentList
              m.actualEndDate = this.datePipe.transform(b.actualEndDate, 'yyyy-MM-dd')
              m.actualStartDate = this.datePipe.transform(b.actualStartDate, 'yyyy-MM-dd')
              this.activity.milestoneList.push(m)
            }
            for (let a of this.selectedBudgetList) {
              let f = new Field()
              if (this.activity.activityData.activityId * 10 < a.fieldId) {
                f.fieldId = 0
              } else {
                f.fieldId = a.fieldId
              }
              f.fieldId = a.fieldId
              f.fieldName = a.fieldName
              f.budgetValue = a.budgetValue
              f.allocatedBudget = parseInt(a.allocatedBudget)
              this.activity.fieldList.push(f)
            }
            this.activity.userInserted = this.userData[0].userId

            //////////////////////////////////////////////////////////////////////////////////

            ///////////Saving Activity///////////////////////////////////

            this.activity.activityData.plannedStartDate = this.plannedActivityStartDate
            this.activity.activityData.plannedEndDate = this.plannedActivityEndDate

            let options: RequestOptionsArgs = new RequestOptions();
            let header: Headers = new Headers();
            header.set('bisId', this.currentLocation.parent.parent.parent.id);
            options.headers = header;

            this.http.post(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveActivity", this.activity, options).map(res => res.json()).subscribe(res => {


              if (res.responseCode == 1) {
                this.success = true;
                this.getAllActivities()
                this.msg = "Yo have successfully added an activity"

                setTimeout(() => {
                  this.success = false;
                  this.msg = null
                  this.viewActivity.hide()
                }, 1000);




                this.resetFields()
              } else {
                this.error = true
                this.msg = "Error occurs"
                setTimeout(() => {
                  this.error = false;
                  this.msg = null
                  this.description = null
                }, 1000);

              }

            }), error => {
              console.log("ERROR")
              this.warnning = true
              this.msg = "Internal Server Error"
              setTimeout(() => {
                this.warnning = false;
                this.msg = null
              }, 1000);
            }




            //////////////////////////////////////////////////////////////


          } else {

            ///////////////////////////Set Values/////////////////////////////////////

            this.http.get("https://maps.google.com/maps/api/geocode/json?address=" + this.currentLocation.label + "&key=AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc").map(res => res.json()).subscribe(res => {

              if (res.results.length != 0) {
                this.activity.activityData.longitude = res.results[0].geometry.location.lng
                this.activity.activityData.latitude = res.results[0].geometry.location.lat
                this.activity.milestoneList = new Array<Milestone>()
                this.activity.fieldList = new Array<Field>()
                this.activityStatus = status
                if (this.activity.activityData.activityId != null) {
                  this.activity.activityData.activityId = this.activityId
                }
                this.activity.activityData.status = status
                this.activity.activityData.activityDescription = this.description
                if (this.selectedType == "PMU") {
                  this.activity.activityData.learningCenterId = this.selectedLearningCenter.sn
                  this.activity.activityData.learningCenter = this.selectedLearningCenter.name
                  this.activity.activityData.address = this.selectedLearningCenter.address
                  this.activity.activityData.category = this.selectedCategory.categoryName
                  this.activity.activityData.categoryId = this.selectedCategory.categoryId
                  let ar = new Array()
                  ar = this.currentLocation.lifeCode.split("-")
                  ar.splice(2, 1)
                  let currentLocation = ar[0] + "-" + ar[1] + "-" + this.currentLocation.code
                  console.log("Current Lo", currentLocation)
                  if (this.selectedActivityType == 1) {

                    this.activity.activityData.locationCode = currentLocation
                  } else {
                    this.activity.activityData.locationCode = currentLocation
                  }
                } else if (this.selectedType == "PHDT") {
                  this.activity.activityData.learningCenterId = this.selectedDivision.id
                  this.activity.activityData.learningCenter = this.selectedDivision.label
                  this.activity.activityData.address = this.selectedDivision.label
                  this.activity.activityData.category = this.selectedCategory.categoryName
                  this.activity.activityData.categoryId = this.selectedCategory.categoryId
                  this.activity.activityData.locationCode = this.currentLocation.lifeCode
                  this.activity.female = this.femaleCount
                  this.activity.male = this.maleCount
                }
                this.activity.activityData.selectedPMUType = this.selectedActivityType
                this.activity.activityData.type = this.selectedType
                for (let b of this.milestonesList) {
                  let m = new Milestone()
                  if (this.activity.activityData.activityId * 10 < b.milestoneId) {
                    m.milestoneId = 0
                  } else {
                    m.milestoneId = b.milestoneId
                  }
                  m.milestoneName = b.milestoneName
                  m.plannedStartDate = this.datePipe.transform(b.plannedStartDate, 'yyyy-MM-dd')
                  m.plannedEndDate = this.datePipe.transform(b.plannedEndDate, 'yyyy-MM-dd')
                  m.documentList = b.documentList
                  m.actualEndDate = this.datePipe.transform(b.actualEndDate, 'yyyy-MM-dd')
                  m.actualStartDate = this.datePipe.transform(b.actualStartDate, 'yyyy-MM-dd')
                  this.activity.milestoneList.push(m)
                }
                for (let a of this.selectedBudgetList) {
                  let f = new Field()
                  if (this.activity.activityData.activityId * 10 < a.fieldId) {
                    f.fieldId = 0
                  } else {
                    f.fieldId = a.fieldId
                  }
                  f.fieldId = a.fieldId
                  f.fieldName = a.fieldName
                  f.budgetValue = a.budgetValue
                  f.allocatedBudget = parseInt(a.allocatedBudget)
                  this.activity.fieldList.push(f)
                }
                this.activity.userInserted = this.userData[0].userId

                ///////////////////////////////////////////////////////////////////////////////////////

                ///////////Saving Activity///////////////////////////////////

                this.activity.activityData.plannedStartDate = this.plannedActivityStartDate
                this.activity.activityData.plannedEndDate = this.plannedActivityEndDate

                let options: RequestOptionsArgs = new RequestOptions();
                let header: Headers = new Headers();
                header.set('bisId', this.currentLocation.parent.parent.parent.id);
                options.headers = header;

                this.http.post(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveActivity", this.activity, options).map(res => res.json()).subscribe(res => {


                  if (res.responseCode == 1) {
                    this.success = true;
                    this.msg = "Yo have successfully added an activity"

                    this.getAllActivities()
                    setTimeout(() => {
                      this.success = false;
                      this.msg = null
                      this.viewActivity.hide()
                    }, 1000);




                    this.resetFields()
                  } else {
                    this.error = true
                    this.msg = "Error occurs"
                    setTimeout(() => {
                      this.error = false;
                      this.msg = null
                      this.description = null
                    }, 1000);

                  }

                }), error => {
                  console.log("ERROR")
                  this.warnning = true
                  this.msg = "Internal Server Error"
                  setTimeout(() => {
                    this.warnning = false;
                    this.msg = null
                  }, 1000);
                }




                //////////////////////////////////////////////////////////////


              } else {
                /////////////////////////Set Values/////////////////////////////////////////////////

                this.http.get("https://maps.google.com/maps/api/geocode/json?address=" + this.currentLocation.parent.label + "&key=AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc").map(res => res.json()).subscribe(res => {

                  if (res.results.length != 0) {
                    this.activity.activityData.longitude = res.results[0].geometry.location.lng
                    this.activity.activityData.latitude = res.results[0].geometry.location.lat
                    this.activity.milestoneList = new Array<Milestone>()
                    this.activity.fieldList = new Array<Field>()
                    this.activityStatus = status
                    if (this.activity.activityData.activityId != null) {
                      this.activity.activityData.activityId = this.activityId
                    }
                    this.activity.activityData.status = status
                    this.activity.activityData.activityDescription = this.description
                    if (this.selectedType == "PMU") {
                      this.activity.activityData.learningCenterId = this.selectedLearningCenter.sn
                      this.activity.activityData.learningCenter = this.selectedLearningCenter.name
                      this.activity.activityData.address = this.selectedLearningCenter.address
                      this.activity.activityData.category = this.selectedCategory.categoryName
                      this.activity.activityData.categoryId = this.selectedCategory.categoryId
                      let ar = new Array()
                      ar = this.currentLocation.lifeCode.split("-")
                      ar.splice(2, 1)
                      let currentLocation = ar[0] + "-" + ar[1] + "-" + this.currentLocation.code

                      if (this.selectedActivityType == 1) {

                        this.activity.activityData.locationCode = currentLocation
                      } else {
                        this.activity.activityData.locationCode = currentLocation
                      }
                    } else if (this.selectedType == "PHDT") {
                      this.activity.activityData.learningCenterId = this.selectedDivision.id
                      this.activity.activityData.learningCenter = this.selectedDivision.label
                      this.activity.activityData.address = this.selectedDivision.label
                      this.activity.activityData.category = this.selectedCategory.categoryName
                      this.activity.activityData.categoryId = this.selectedCategory.categoryId
                      this.activity.activityData.locationCode = this.currentLocation.lifeCode
                      this.activity.female = this.femaleCount
                      this.activity.male = this.maleCount
                    }
                    this.activity.activityData.selectedPMUType = this.selectedActivityType
                    this.activity.activityData.type = this.selectedType
                    for (let b of this.milestonesList) {
                      let m = new Milestone()
                      if (this.activity.activityData.activityId * 10 < b.milestoneId) {
                        m.milestoneId = 0
                      } else {
                        m.milestoneId = b.milestoneId
                      }
                      m.milestoneName = b.milestoneName
                      m.plannedStartDate = this.datePipe.transform(b.plannedStartDate, 'yyyy-MM-dd')
                      m.plannedEndDate = this.datePipe.transform(b.plannedEndDate, 'yyyy-MM-dd')
                      m.documentList = b.documentList
                      m.actualEndDate = this.datePipe.transform(b.actualEndDate, 'yyyy-MM-dd')
                      m.actualStartDate = this.datePipe.transform(b.actualStartDate, 'yyyy-MM-dd')
                      this.activity.milestoneList.push(m)
                    }
                    for (let a of this.selectedBudgetList) {
                      let f = new Field()
                      if (this.activity.activityData.activityId * 10 < a.fieldId) {
                        f.fieldId = 0
                      } else {
                        f.fieldId = a.fieldId
                      }
                      f.fieldId = a.fieldId
                      f.fieldName = a.fieldName
                      f.budgetValue = a.budgetValue
                      f.allocatedBudget = parseInt(a.allocatedBudget)
                      this.activity.fieldList.push(f)
                    }
                    this.activity.userInserted = this.userData[0].userId

                    /////////////////////////////////////////////////////////////////////////////////////////////////

                    ///////////Saving Activity///////////////////////////////////


                    this.activity.activityData.plannedStartDate = this.plannedActivityStartDate
                    this.activity.activityData.plannedEndDate = this.plannedActivityEndDate


                    let options: RequestOptionsArgs = new RequestOptions();
                    let header: Headers = new Headers();
                    header.set('bisId', this.currentLocation.parent.parent.parent.id);
                    options.headers = header;

                    this.http.post(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveActivity", this.activity, options).map(res => res.json()).subscribe(res => {


                      if (res.responseCode == 1) {
                        this.success = true;
                        this.msg = "Yo have successfully added an activity"
                        this.getAllActivities()

                        setTimeout(() => {
                          this.success = false;
                          this.msg = null
                          this.viewActivity.hide()
                        }, 1000);



                        this.resetFields()
                      } else {
                        this.error = true
                        this.msg = "Error occurs"
                        setTimeout(() => {
                          this.error = false;
                          this.msg = null
                          this.description = null
                        }, 1000);

                      }

                    }), error => {
                      console.log("ERROR")
                      this.warnning = true
                      this.msg = "Internal Server Error"
                      setTimeout(() => {
                        this.warnning = false;
                        this.msg = null
                      }, 1000);
                    }




                    //////////////////////////////////////////////////////////////



                  } else {
                    // this.mapUrl = this.sanitizer.bypassSecurityTrustResourceUrl("http://maps.google.com/maps/api/geocode/json?address=" + this.currentLocation.parent.parent.label)

                    this.http.get("https://maps.google.com/maps/api/geocode/json?address=" + this.currentLocation.parent.parent.label + "&key=AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc").map(res => res.json()).subscribe(res => {

                      if (res.results.length != 0) {


                        ////////////Set Values//////////////////
                        this.activity.activityData.longitude = res.results[0].geometry.location.lng
                        this.activity.activityData.latitude = res.results[0].geometry.location.lat
                        this.activity.milestoneList = new Array<Milestone>()
                        this.activity.fieldList = new Array<Field>()
                        this.activityStatus = status
                        if (this.activity.activityData.activityId != null) {
                          this.activity.activityData.activityId = this.activityId
                        }
                        this.activity.activityData.status = status
                        this.activity.activityData.activityDescription = this.description
                        if (this.selectedType == "PMU") {
                          this.activity.activityData.learningCenterId = this.selectedLearningCenter.sn
                          this.activity.activityData.learningCenter = this.selectedLearningCenter.name
                          this.activity.activityData.address = this.selectedLearningCenter.address
                          this.activity.activityData.category = this.selectedCategory.categoryName
                          this.activity.activityData.categoryId = this.selectedCategory.categoryId
                          let ar = new Array()
                          ar = this.currentLocation.lifeCode.split("-")
                          ar.splice(2, )
                          let currentLocation = ar[0] + "-" + ar[1] + "-" + this.currentLocation.code
                          console.log("Current Lo", currentLocation)
                          if (this.selectedActivityType == 1) {

                            this.activity.activityData.locationCode = currentLocation
                          } else {
                            this.activity.activityData.locationCode = currentLocation
                          }
                        } else if (this.selectedType == "PHDT") {
                          this.activity.activityData.learningCenterId = this.selectedDivision.id
                          this.activity.activityData.learningCenter = this.selectedDivision.label
                          this.activity.activityData.address = this.selectedDivision.label
                          this.activity.activityData.category = this.selectedCategory.categoryName
                          this.activity.activityData.categoryId = this.selectedCategory.categoryId
                          this.activity.activityData.locationCode = this.currentLocation.lifeCode
                          this.activity.female = this.femaleCount
                          this.activity.male = this.maleCount
                        }
                        this.activity.activityData.selectedPMUType = this.selectedActivityType
                        this.activity.activityData.type = this.selectedType
                        for (let b of this.milestonesList) {
                          let m = new Milestone()
                          if (this.activity.activityData.activityId * 10 < b.milestoneId) {
                            m.milestoneId = 0
                          } else {
                            m.milestoneId = b.milestoneId
                          }
                          m.milestoneName = b.milestoneName
                          m.plannedStartDate = this.datePipe.transform(b.plannedStartDate, 'yyyy-MM-dd')
                          m.plannedEndDate = this.datePipe.transform(b.plannedEndDate, 'yyyy-MM-dd')
                          m.documentList = b.documentList
                          m.actualEndDate = this.datePipe.transform(b.actualEndDate, 'yyyy-MM-dd')
                          m.actualStartDate = this.datePipe.transform(b.actualStartDate, 'yyyy-MM-dd')
                          this.activity.milestoneList.push(m)
                        }
                        for (let a of this.selectedBudgetList) {
                          let f = new Field()
                          if (this.activity.activityData.activityId * 10 < a.fieldId) {
                            f.fieldId = 0
                          } else {
                            f.fieldId = a.fieldId
                          }
                          f.fieldId = a.fieldId
                          f.fieldName = a.fieldName
                          f.budgetValue = a.budgetValue
                          f.allocatedBudget = parseInt(a.allocatedBudget)
                          this.activity.fieldList.push(f)
                        }
                        this.activity.userInserted = this.userData[0].userId
                        ///////////////////////////////////////////////////////

                        //////////////////////Save Activity///////////////////////

                        this.activity.activityData.plannedStartDate = this.plannedActivityStartDate
                        this.activity.activityData.plannedEndDate = this.plannedActivityEndDate


                        let options: RequestOptionsArgs = new RequestOptions();
                        let header: Headers = new Headers();
                        header.set('bisId', this.currentLocation.parent.parent.parent.id);
                        options.headers = header;

                        this.http.post(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveActivity", this.activity, options).map(res => res.json()).subscribe(res => {


                          if (res.responseCode == 1) {
                            this.success = true;
                            this.msg = "Yo have successfully added an activity"
                            this.getAllActivities()
                            setTimeout(() => {
                              this.success = false;
                              this.msg = null
                              this.viewActivity.hide()
                            }, 1000);




                            this.resetFields()
                          } else {
                            this.error = true
                            this.msg = "Error occurs"
                            setTimeout(() => {
                              this.error = false;
                              this.msg = null
                              this.description = null
                            }, 1000);

                          }

                        }), error => {
                          console.log("ERROR")
                          this.warnning = true
                          this.msg = "Internal Server Error"
                          setTimeout(() => {
                            this.warnning = false;
                            this.msg = null
                          }, 1000);
                        }
                        ///////////////////////////////////////////////  

                      } else {
                        ////////////Set Values//////////////////

                        this.activity.activityData.longitude = 0.0
                        this.activity.activityData.latitude = 0.0
                        this.activity.milestoneList = new Array<Milestone>()
                        this.activity.fieldList = new Array<Field>()
                        this.activityStatus = status
                        if (this.activity.activityData.activityId != null) {
                          this.activity.activityData.activityId = this.activityId
                        }
                        this.activity.activityData.status = status
                        this.activity.activityData.activityDescription = this.description
                        if (this.selectedType == "PMU") {
                          this.activity.activityData.learningCenterId = this.selectedLearningCenter.sn
                          this.activity.activityData.learningCenter = this.selectedLearningCenter.name
                          this.activity.activityData.address = this.selectedLearningCenter.address
                          this.activity.activityData.category = this.selectedCategory.categoryName
                          this.activity.activityData.categoryId = this.selectedCategory.categoryId
                          let ar = new Array()
                          ar = this.currentLocation.lifeCode.split("-")
                          ar.splice(2, 1)
                          let currentLocation = ar[0] + "-" + ar[1] + "-" + this.currentLocation.code
                          console.log("Current Lo", currentLocation)
                          if (this.selectedActivityType == 1) {

                            this.activity.activityData.locationCode = currentLocation
                          } else {
                            this.activity.activityData.locationCode = currentLocation
                          }
                        } else if (this.selectedType == "PHDT") {
                          this.activity.activityData.learningCenterId = this.selectedDivision.id
                          this.activity.activityData.learningCenter = this.selectedDivision.label
                          this.activity.activityData.address = this.selectedDivision.label
                          this.activity.activityData.category = this.selectedCategory.categoryName
                          this.activity.activityData.categoryId = this.selectedCategory.categoryId
                          this.activity.activityData.locationCode = this.currentLocation.lifeCode
                          this.activity.female = this.femaleCount
                          this.activity.male = this.maleCount
                        }
                        this.activity.activityData.selectedPMUType = this.selectedActivityType
                        this.activity.activityData.type = this.selectedType
                        for (let b of this.milestonesList) {
                          let m = new Milestone()
                          if (this.activity.activityData.activityId * 10 < b.milestoneId) {
                            m.milestoneId = 0
                          } else {
                            m.milestoneId = b.milestoneId
                          }
                          m.milestoneName = b.milestoneName
                          m.plannedStartDate = this.datePipe.transform(b.plannedStartDate, 'yyyy-MM-dd')
                          m.plannedEndDate = this.datePipe.transform(b.plannedEndDate, 'yyyy-MM-dd')
                          m.documentList = b.documentList
                          m.actualEndDate = this.datePipe.transform(b.actualEndDate, 'yyyy-MM-dd')
                          m.actualStartDate = this.datePipe.transform(b.actualStartDate, 'yyyy-MM-dd')
                          this.activity.milestoneList.push(m)
                        }
                        for (let a of this.selectedBudgetList) {
                          let f = new Field()
                          if (this.activity.activityData.activityId * 10 < a.fieldId) {
                            f.fieldId = 0
                          } else {
                            f.fieldId = a.fieldId
                          }
                          f.fieldId = a.fieldId
                          f.fieldName = a.fieldName
                          f.budgetValue = a.budgetValue
                          f.allocatedBudget = parseInt(a.allocatedBudget)
                          this.activity.fieldList.push(f)
                        }
                        this.activity.userInserted = this.userData[0].userId
                      }
                      /////////////////////////////////////////////////////////

                      ///////////Saving Activity///////////////////////////////////

                      this.activity.activityData.plannedStartDate = this.plannedActivityStartDate
                      this.activity.activityData.plannedEndDate = this.plannedActivityEndDate

                      let options: RequestOptionsArgs = new RequestOptions();
                      let header: Headers = new Headers();
                      header.set('bisId', this.currentLocation.parent.parent.parent.id);
                      options.headers = header;

                      this.http.post(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveActivity", this.activity, options).map(res => res.json()).subscribe(res => {


                        if (res.responseCode == 1) {
                          this.success = true;
                          this.msg = "Yo have successfully added an activity"
                          this.getAllActivities()
                          setTimeout(() => {
                            this.success = false;
                            this.msg = null
                            this.viewActivity.hide()
                          }, 1000);




                          this.resetFields()
                        } else {
                          this.error = true
                          this.msg = "Error occurs"
                          setTimeout(() => {
                            this.error = false;
                            this.msg = null
                            this.description = null
                          }, 1000);

                        }

                      }), error => {
                        console.log("ERROR")
                        this.warnning = true
                        this.msg = "Internal Server Error"
                        setTimeout(() => {
                          this.warnning = false;
                          this.msg = null
                        }, 1000);
                      }




                      //////////////////////////////////////////////////////////////



                    }), error => {
                      console.log("ERROR")
                    }
                  }

                }), error => {
                  console.log("ERROR")
                }
              }

            }), error => {
              console.log("ERROR")
            }


          }




          // this.commonService.processPost(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveActivity", this.activity).subscribe(res => {


          //   if (res.responseCode == 1) {
          //     this.success = true;
          //     this.msg = "Yo have successfully added an activity"

          //     setTimeout(() => {
          //       this.success = false;
          //       this.msg = null

          //     }, 1000);


          //     this.getAllActivities()

          //     this.resetFields()
          //   } else {
          //     this.error = true
          //     this.msg = "Error occurs"
          //     setTimeout(() => {
          //       this.error = false;
          //       this.msg = null
          //       this.description = null
          //     }, 1000);

          //   }

          // }), error => {
          //   console.log("ERROR")
          //   this.warnning = true
          //   this.msg = "Internal Server Error"
          //   setTimeout(() => {
          //     this.warnning = false;
          //     this.msg = null
          //   }, 1000);
          // }
        }), error => {
          console.log("ERROR")
        }
      } else {

        this.warnning = true
        this.msg = "Choose a area"
        console.log(this.warnning)
        setTimeout(() => {
          this.warnning = false;
          this.msg = null
        }, 1000);
      }
    } else if (this.selectedType == "PHDT") {


      this.http.get("https://maps.google.com/maps/api/geocode/json?address=" + this.selectedDivision.label + "&key=AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc").map(res => res.json()).subscribe(res => {

        if (res.results.length != 0) {
          this.activity.activityData.longitude = res.results[0].geometry.location.lng
          this.activity.activityData.latitude = res.results[0].geometry.location.lat
          this.activity.milestoneList = new Array<Milestone>()
          this.activity.fieldList = new Array<Field>()
          this.activityStatus = status
          if (this.activity.activityData.activityId != null) {
            this.activity.activityData.activityId = this.activityId
          }
          this.activity.activityData.status = status
          this.activity.activityData.activityDescription = this.description
          if (this.selectedType == "PMU") {
            this.activity.activityData.learningCenterId = this.selectedLearningCenter.sn
            this.activity.activityData.learningCenter = this.selectedLearningCenter.name
            this.activity.activityData.address = this.selectedLearningCenter.address
            this.activity.activityData.category = this.selectedCategory.categoryName
            this.activity.activityData.categoryId = this.selectedCategory.categoryId
            if (this.selectedActivityType == 1) {
              this.activity.activityData.locationCode = this.currentLocation.lifeCode
            } else {
              this.activity.activityData.locationCode = this.currentLocation.lifeCode
            }
          } else if (this.selectedType == "PHDT") {
            this.activity.activityData.learningCenterId = this.selectedDivision.id
            this.activity.activityData.learningCenter = this.selectedDivision.label
            this.activity.activityData.address = this.selectedDivision.label
            this.activity.activityData.category = this.selectedCategory.categoryName
            this.activity.activityData.categoryId = this.selectedCategory.categoryId
            this.activity.activityData.locationCode = this.currentLocation.lifeCode
            this.activity.female = this.femaleCount
            this.activity.male = this.maleCount
          }
          this.activity.activityData.selectedPMUType = this.selectedActivityType
          this.activity.activityData.type = this.selectedType
          for (let b of this.milestonesList) {
            let m = new Milestone()
            if (this.activity.activityData.activityId * 10 < b.milestoneId) {
              m.milestoneId = 0
            } else {
              m.milestoneId = b.milestoneId
            }
            m.milestoneName = b.milestoneName
            m.plannedStartDate = this.datePipe.transform(b.plannedStartDate, 'yyyy-MM-dd')
            m.plannedEndDate = this.datePipe.transform(b.plannedEndDate, 'yyyy-MM-dd')
            m.documentList = b.documentList
            m.actualEndDate = this.datePipe.transform(b.actualEndDate, 'yyyy-MM-dd')
            m.actualStartDate = this.datePipe.transform(b.actualStartDate, 'yyyy-MM-dd')
            this.activity.milestoneList.push(m)
          }
          for (let a of this.selectedBudgetList) {
            let f = new Field()
            if (this.activity.activityData.activityId * 10 < a.fieldId) {
              f.fieldId = 0
            } else {
              f.fieldId = a.fieldId
            }
            f.fieldId = a.fieldId
            f.fieldName = a.fieldName
            f.budgetValue = a.budgetValue
            f.allocatedBudget = parseInt(a.allocatedBudget)
            this.activity.fieldList.push(f)
          }
          this.activity.userInserted = this.userData[0].userId

        } else {

          this.activity.activityData.longitude = 0.0
          this.activity.activityData.latitude = 0.0
          this.activity.milestoneList = new Array<Milestone>()
          this.activity.fieldList = new Array<Field>()
          this.activityStatus = status
          if (this.activity.activityData.activityId != null) {
            this.activity.activityData.activityId = this.activityId
          }
          this.activity.activityData.status = status
          this.activity.activityData.activityDescription = this.description
          if (this.selectedType == "PMU") {
            this.activity.activityData.learningCenterId = this.selectedLearningCenter.sn
            this.activity.activityData.learningCenter = this.selectedLearningCenter.name
            this.activity.activityData.address = this.selectedLearningCenter.address
            this.activity.activityData.category = this.selectedCategory.categoryName
            this.activity.activityData.categoryId = this.selectedCategory.categoryId
            if (this.selectedActivityType == 1) {
              this.activity.activityData.locationCode = this.currentLocation.lifeCode
            } else {
              this.activity.activityData.locationCode = this.currentLocation.lifeCode
            }
          } else if (this.selectedType == "PHDT") {
            this.activity.activityData.learningCenterId = this.selectedDivision.id
            this.activity.activityData.learningCenter = this.selectedDivision.label
            this.activity.activityData.address = this.selectedDivision.label
            this.activity.activityData.category = this.selectedCategory.categoryName
            this.activity.activityData.categoryId = this.selectedCategory.categoryId
            this.activity.activityData.locationCode = this.currentLocation.lifeCode
            this.activity.female = this.femaleCount
            this.activity.male = this.maleCount
          }
          this.activity.activityData.selectedPMUType = this.selectedActivityType
          this.activity.activityData.type = this.selectedType
          for (let b of this.milestonesList) {
            let m = new Milestone()
            if (this.activity.activityData.activityId * 10 < b.milestoneId) {
              m.milestoneId = 0
            } else {
              m.milestoneId = b.milestoneId
            }
            m.milestoneName = b.milestoneName
            m.plannedStartDate = this.datePipe.transform(b.plannedStartDate, 'yyyy-MM-dd')
            m.plannedEndDate = this.datePipe.transform(b.plannedEndDate, 'yyyy-MM-dd')
            m.documentList = b.documentList
            m.actualEndDate = this.datePipe.transform(b.actualEndDate, 'yyyy-MM-dd')
            m.actualStartDate = this.datePipe.transform(b.actualStartDate, 'yyyy-MM-dd')
            this.activity.milestoneList.push(m)
          }
          for (let a of this.selectedBudgetList) {
            let f = new Field()
            if (this.activity.activityData.activityId * 10 < a.fieldId) {
              f.fieldId = 0
            } else {
              f.fieldId = a.fieldId
            }
            f.fieldId = a.fieldId
            f.fieldName = a.fieldName
            f.budgetValue = a.budgetValue
            f.allocatedBudget = parseInt(a.allocatedBudget)
            this.activity.fieldList.push(f)
          }
          this.activity.userInserted = this.userData[0].userId

        }



        this.activity.activityData.plannedStartDate = this.plannedActivityStartDate
        this.activity.activityData.plannedEndDate = this.plannedActivityEndDate

        let options: RequestOptionsArgs = new RequestOptions();
        let header: Headers = new Headers();
        header.set('bisId', this.currentLocation.parent.parent.parent.id);
        options.headers = header;

        this.http.post(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveActivity", this.activity, options).map(res => res.json()).subscribe(res => {



          if (res.responseCode == 1) {
            this.success = true;
            this.msg = "Yo have successfully added an activity"
            this.getAllActivities()
            setTimeout(() => {
              this.success = false;
              this.msg = null
              this.description = null
              this.viewActivity.hide()
            }, 1000);




            this.resetFields()
          } else {
            this.error = true
            this.msg = "Error occurs"
            setTimeout(() => {
              this.error = false;
              this.msg = null
              this.description = null
            }, 1000);

          }

        }), error => {
          console.log("ERROR")
          this.warnning = true
          this.msg = "Internal Server Error"
          setTimeout(() => {
            this.warnning = false;
            this.msg = null
          }, 1000);
        }
      }), error => {
        console.log("ERROR")
      }
    } else {
      this.warnning = true
      this.msg = "Choose a area"
      setTimeout(() => {
        this.warnning = false;
        this.msg = null
      }, 1000);

    }
    // this.setValues(status)

  }

  closeViewActivity() {

    this.viewActivity.hide()

  }
  setValues(status) {
    this.activity.milestoneList = new Array<Milestone>()
    this.activity.fieldList = new Array<Field>()
    this.activityStatus = status
    if (this.activity.activityData.activityId != null) {
      this.activity.activityData.activityId = this.activityId
    }
    this.activity.activityData.status = status
    this.activity.activityData.activityDescription = this.description
    if (this.selectedType == "PMU") {
      this.activity.activityData.learningCenterId = this.selectedLearningCenter.sn
      this.activity.activityData.learningCenter = this.selectedLearningCenter.name
      this.activity.activityData.address = this.selectedLearningCenter.address
      this.activity.activityData.category = this.selectedCategory.categoryName
      this.activity.activityData.categoryId = this.selectedCategory.categoryId
      if (this.param == "new") {
        this.activity.activityData.locationCode = this.currentLocation.lifeCode
      }
    } else if (this.selectedType == "PHDT") {
      this.activity.activityData.learningCenterId = this.selectedDivision.id
      this.activity.activityData.learningCenter = this.selectedDivision.label
      this.activity.activityData.address = this.selectedDivision.label
      this.activity.activityData.category = this.selectedCategory.categoryName
      this.activity.activityData.categoryId = this.selectedCategory.categoryId

      this.activity.female = this.femaleCount
      this.activity.male = this.maleCount
    }
    this.activity.activityData.selectedPMUType = this.selectedActivityType
    this.activity.activityData.type = this.selectedType
    for (let b of this.milestonesList) {
      let m = new Milestone()
      if (this.activity.activityData.activityId * 10 < b.milestoneId) {
        m.milestoneId = 0
      } else {
        m.milestoneId = b.milestoneId
      }
      m.milestoneName = b.milestoneName
      m.plannedStartDate = this.datePipe.transform(b.plannedStartDate, 'yyyy-MM-dd')
      m.plannedEndDate = this.datePipe.transform(b.plannedEndDate, 'yyyy-MM-dd')
      m.documentList = b.documentList
      m.actualEndDate = this.datePipe.transform(b.actualEndDate, 'yyyy-MM-dd')
      m.actualStartDate = this.datePipe.transform(b.actualStartDate, 'yyyy-MM-dd')
      this.activity.milestoneList.push(m)
    }
    for (let a of this.selectedBudgetList) {
      let f = new Field()
      if (this.activity.activityData.activityId * 10 < a.fieldId) {
        f.fieldId = 0
      } else {
        f.fieldId = a.fieldId
      }
      f.fieldId = a.fieldId
      f.fieldName = a.fieldName
      f.budgetValue = a.budgetValue
      f.allocatedBudget = parseInt(a.allocatedBudget)
      this.activity.fieldList.push(f)
    }
    this.activity.userInserted = this.userData[0].userId
  }

  addBudgetExtra() {

    let a = new Field()

    if (this.otherBudget != undefined) {

      a.fieldId = 0
      a.fieldName = this.otherBudget
      a.budgetValue = this.otherBudgetVal
    } else {
      a.fieldId = this.selectedBudget.fieldId
      a.fieldName = this.selectedBudget.fieldName
      a.budgetValue = this.selectedBudget.budgetValue

    }

    console.log("STATATUS", this.statusofaActivity)
    if (this.statusofaActivity == 4) {
      this.selectedBudgetListExtra.push(a)
      this.selectedBudgetList.push(a)
    } else {
      this.selectedBudgetList.push(a)
    }


    // for (let a of this.selectedBudgetList) {

    this.actualBudget = this.actualBudget + a.budgetValue

    // }
    if (this.otherBudget != undefined) {
      this.otherBudget = ""
      this.otherBudgetVal = 0
    }



  }
  getLocation(address: string) {

    address = address.replace(/ /g, "+")


    this.http.get("https://maps.google.com/maps/api/geocode/json?address=" + address + "&key=AIzaSyDNmiNO2lI8PSr_opm5TrNamNTvTeYU2Jc").map(res => res.json()).subscribe(res => {

      if (res.results.length != 0) {
        this.activity.activityData.longitude = res.results[0].geometry.location.lng
        this.activity.activityData.latitude = res.results[0].geometry.location.lat

      } else {

        this.activity.activityData.longitude = 0.0
        this.activity.activityData.latitude = 0.0

      }

    }), error => {
      console.log("ERROR")
    }


  }
  getUnauthorized() {
    this.getAllActivitiesbyStatus(4)
  }
  getAuthorized() {
    this.getAllActivitiesbyStatus(3)
  }



  validation() {


    // this.formGroup = this.formBuilder.group({



    //   description: new FormControl(),
    //   selectedMilestone: new FormControl(),
    //   startDate: new FormControl(),
    //   endDate: new FormControl(),
    //   selectedDocument: new FormControl(),
    //   selectedBudget: new FormControl(),
    //   budgetValue: new FormControl(),
    //   selectedType: new FormControl(),
    //   Description1: new FormControl(),
    //   type: new FormControl(),
    //   learningCenter: new FormControl(),
    //   category: new FormControl(),
    //   addcategory: new FormControl(),
    //   docStatus: new FormControl(),
    //   Comments: new FormControl(),
    //   grantDate: new FormControl(),

    //   allocated: new FormControl(),
    //   allocatedBudget: new FormControl(),
    //   learningCenteredit: new FormControl(),
    //   actualStartDate: new FormControl(),
    //   grants: new FormControl(),

    //   milestoneC: new FormControl()



    // });



    this.claimForm = new FormGroup({
      description: new FormControl(['', Validators.required]),
      amount: new FormControl(['', Validators.required]),
      claimedDate: new FormControl()

    })


  }


  markDocuments(a: UpdateMilestone) {
    this.updateDocModelList = new Array<UpdateDocModel>()
    this.postedComment = new Comment()
    this.comment = ""
    this.dateCommented = new Date()
    this.selectedMilestoneId = a.milestoneId
    console.log("Milestone id", this.selectedMilestoneId)
    // var index = this.selectedUpdatedMilestoneList.indexOf(a);

    // this.selectedUpdatedMilestone = this.selectedUpdatedMilestoneList[index]
    // this.addedDocs = this.selectedUpdatedMilestoneList[index].documentList
    // this.comment = this.addedDocs[0].description
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getMaintainDocument?activityId=" + this.activityId + "&milestoneId=" + a.milestoneId).subscribe(res => {

      if (res.responseCode == 1) {
        this.docList = res.responseData
        if (this.docList.length != 0) {
          this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getComments?activityId=" + this.activityId + "&milestoneId=" + a.milestoneId).subscribe(res => {

            if (res.responseCode == 1) {
              this.commentList = res.responseData


            }
          }), error => {
            console.log("ERROR")
          }
        }

      }
    }), error => {
      console.log("ERROR")
    }

    this.showDocuments.show()
  }

  postComment() {



    this.postedComment = new Comment()
    this.postedComment.activityId = this.activityId
    this.postedComment.comment = this.comment
    this.postedComment.date = this.dateCommented
    this.postedComment.milestoneId = this.selectedMilestoneId
    this.postedComment.userInserted = this.userId

    this.commonService.processPost(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/insertComment?", this.postedComment).subscribe(res => {

      if (res.responseCode == 1) {
        this.postedComment = new Comment()
        this.dateCommented = new Date()
        this.comment = ""
        this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getComments?activityId=" + this.activityId + "&milestoneId=" + this.selectedMilestoneId).subscribe(res => {

          if (res.responseCode == 1) {
            this.commentList = res.responseData


          }
        }), error => {
          console.log("ERROR")
        }
      }
    }), error => {
      console.log("ERROR")
    };

  }




  //////////////////////////////////////////////////////////////////////////////////////////////////////


  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') { // when all files added in queue
      // uncomment this if you want to auto upload files when added
      // const event: UploadInput = {
      //   type: 'uploadAll',
      //   url: '/upload',
      //   method: 'POST',
      //   data: { foo: 'bar' }
      // };
      // this.uploadInput.emit(event);
    } else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') { // add file to array when added

      this.files.push(output.file);
      if (!this.comparePreViousPhotos(this.files[0])) {
        this.files.splice(0, 1)
      }


    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      // update current data in files array for uploading file
      const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'removed') {
      // remove file from array when removed
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
      this.dragOver = false;
    } else if (output.type === 'drop') {
      this.dragOver = false;
    }
  }
  comparePreViousPhotos(a): any {
    if (a.name == "20171006_130025.jpg") {
      return true
    }
  }
  startUpload(): void {
    console.log("Res", this.files)
    const event: UploadInput = {
      type: 'uploadAll',
      url: API_URL + 'ECD-ActivityMaintenance-1.0/service/Activities/saveImages',
      method: 'POST',
      data: { foo: "bar" }
    };

    this.uploadInput.emit(event);

    if (this.addPhotoBtnCount > 4) {
      this.disable = true
    }
  }

  cancelUpload(id: string): void {
    this.uploadInput.emit({ type: 'cancel', id: id });
  }

  removeFile(id: string): void {
    this.uploadInput.emit({ type: 'remove', id: id });
  }

  removeAllFiles(): void {
    this.uploadInput.emit({ type: 'removeAll' });
  }

  onupload(evt) {
    console.log("sele??10", this.selectedUpdatedMilestone)

    this.addPhotos(this.currentlySelectedMilestone)
    this.matched1 = false
    this.matched2 = false

  }

  addPhotos(a) {

    this.currentlySelectedMilestone = a
    // this.initialLong = 0.0
    // this.initialLang = 0.0
    this.matchedPhotoCount = 0
    this.nonMatchedPhotoCount = 0
    this.matched1 = false
    this.matched2 = false
    this.matched3 = false
    this.matched4 = false;
    this.firstPhoto = false
    this.selectedMilestoneforPhoto = new UpdateMilestone()
    this.selectedMilestoneforPhoto.matchedPhotosList = a.matchedPhotosList
    this.selectedMilestoneforPhoto.nonMatchedPhotosList = a.nonMatchedPhotosList
    this.selectedMilestoneforPhoto.milestoneId = a.milestoneId
    this.images = new Array<any>();

    this.milestoneId = a.milestoneId

    // if (this.photoList.length != 0) {

    //   console.log("sss", this.photoList.length)
    //   this.photo1.path = this.photoList[0].path
    //   this.photo2.path = this.photoList[1].path
    //   this.photo3.path = this.photoList[2].path
    //   this.photo4.path = this.photoList[3].path

    // }


    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getMilestoneById?activityId=" + this.activityId + "&milestoneId=" + this.milestoneId).subscribe(res => {

      if (res.responseCode == 1) {

        let imageCount = 0;
        this.photoList = res.responseData
        // let initialLang;
        // let initialLong;

        if (this.photoList.length == 0) {
          this.firstPhoto = true;
        } else {
          for (let i = 0; i < this.photoList.length; i++) {
            if (i == 0) {

              this.initialLang = this.photoList[i].latitude
              this.initialLong = this.photoList[i].longitude



            }

            var R = 6371e3; // metres
            var φ1 = this.toRad(this.initialLang);
            var φ2 = this.toRad(this.photoList[i].latitude);

            var Δφ = this.toRad(this.photoList[i].latitude - this.initialLang)
            var Δλ = this.toRad(this.photoList[i].longitude - this.initialLong);

            var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
              Math.cos(φ1) * Math.cos(φ2) *
              Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

            var d = R * c;


            if (d < 50) {

              if(this.photoList[i].latitude!=0||this.photoList[i].longitude!=0){
                this.matchedPhotoCount++;
              }else{
                this.nonMatchedPhotoCount++
              }
             
              // this.nonMatchedPhotoCount = this.photoList.length - this.matchedPhotoCount

            } else {
              this.nonMatchedPhotoCount++
            }

          }


          for (let image of this.photoList) {

            this.images.push(API_URL + image.path)
            imageCount++
            this.imageCount.push(imageCount)
          }
        }
      }

    }), error => {
      console.log("ERROR")
    }



    this.addPictures.show()

  }

  checkAreaWithin(lat1, lon1, lat2, lon2): any {
    var R = 6371e3; // metres
    var φ1 = this.toRad(lat1);
    var φ2 = this.toRad(lat2);
    let x1 = lat2 - lat1
    let x2 = lon2 - lon1
    var Δφ = this.toRad(lat2 - lat1)
    var Δλ = this.toRad(lon2 - lon1);

    var a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) *
      Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    var d = R * c;

  }
  toRad(val) {
    return val * Math.PI / 180;
  }
  upload(ev) { }


  beforeUpload(evt2) {//THIS is called after selecT the file
    this.imageSavingURL = API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/uploadImages";
    var me = this
    this.evt3 = evt2;
    let evt = this.evt3;


    for (let a of evt.files) {


      let img1 = a
      if (img1 != undefined || img1 != null) {
        let num
        EXIF.getData(img1, function () {

          let number = EXIF.getTag(this, "GPSLongitude");

          if (number != undefined || number != null) {

            num = number[0].numerator + number[1].numerator /
              (60 * number[1].denominator) + number[2].numerator / (3600 * number[2].denominator);


            me.longitude = num




            // if (me.selectedMilestoneforPhoto.matchedPhotosList.length != 0) {

            //   if (me.initialLong == number) {
            //     me.matched1 = true
            //   }else{
            //     me .matched4=true;
            //   }
            // } else if (me.selectedMilestoneforPhoto.nonMatchedPhotosList.length != 0) {

            //   console.log("Non Matched1", me.selectedMilestoneforPhoto.nonMatchedPhotosList[me.selectedMilestoneforPhoto.nonMatchedPhotosList.length - 1].longitude)
            //   console.log("Non Matched2", me.longitude)
            //   if (Math.round(me.selectedMilestoneforPhoto.nonMatchedPhotosList[me.selectedMilestoneforPhoto.nonMatchedPhotosList.length - 1].longitude) == Math.round(me.longitude)) {
            //     me.matched1 = true
            //   }else{
            //     me .matched4=true;
            //   }
            // }

            EXIF.getData(img1, function () {

              let number = EXIF.getTag(this, "GPSLatitude");

              if (number != undefined || number != null) {

                num = number[0].numerator + number[1].numerator /
                  (60 * number[1].denominator) + number[2].numerator / (3600 * number[2].denominator);

                me.latitude = num
                console.log("long", this.longitude, me.longitude)

                me.imageSavingURL += "?activityId=" + me.activityId + "&milestoneId=" + me.milestoneId + "&long=" + me.longitude + "&lat=" + me.latitude







              } else {
                me.matched4 = true
              }
              if (number != undefined) {
                var R = 6371e3; // metres
                var φ1 = me.toRad(me.initialLang);
                var φ2 = me.toRad(me.latitude);

                var Δφ = me.toRad(me.initialLang - me.latitude)
                var Δλ = me.toRad(me.initialLong - me.longitude);


                var ab = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
                  Math.cos(φ1) * Math.cos(φ2) *
                  Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
                var c = 2 * Math.atan2(Math.sqrt(ab), Math.sqrt(1 - ab));

                var d = R * c;



                if (d < 50) {
                  me.matched1 = true



                } else {


                  me.matched2 = true
                }
              }


            });

          } else {
            me.imageSavingURL += "?activityId=" + me.activityId + "&milestoneId=" + me.milestoneId
            me.matched3 = true
            me.matched4 = true
          }

        });


      }

    }



  }
  resetMsg(e) {
    this.matched1 = false
    this.matched2 = false
    this.matched4 = false
    this.matched3 = false
    this.firstPhoto = false
  }
  converttoDecimal(number) {
    return number[0].numerator + number[1].numerator /
      (60 * number[1].denominator) + number[2].numerator / (3600 * number[2].denominator);
  }
  savePhoto() {


    this.addPhotoBtnCount++

    if (this.addPhotoBtnCount == 1 && this.photo1.path == null) {

      this.photo1.path = this.files[-1].response.responseData
      this.matched1 = false
      // this.photo1.longitude= EXIF.getTag(photo1.path,'GPSLongitude')

      var img1 = document.getElementById("img1");

      EXIF.getData(img1, function () {

        var long = EXIF.getTag(this, "GPSLongitude");
        //this.photo1.latitude  = EXIF.getTag(this, "GPSLatitude");

      });

      this.photoList.push(this.photo1)


    } else if (this.addPhotoBtnCount == 2 && this.photo2.path == null) {

      this.photo2.path = this.files[-1].response.responseData
      this.matched2 = false
      EXIF.getData(this.photo2.path, function () {
        this.photo2.longitude = EXIF.getTag(this, "GPSLongitude");
        this.photo2.latitude = EXIF.getTag(this, "GPSLatitude");

      });
      this.photoList.push(this.photo2)

    } else if (this.addPhotoBtnCount == 3 && this.photo3.path == null) {

      this.photo3.path = this.files[-1].response.responseData
      this.matched3 = false

      EXIF.getData(this.photo3.path, function () {
        this.photo3.longitude = EXIF.getTag(this, "GPSLongitude");
        this.photo3.latitude = EXIF.getTag(this, "GPSLatitude");

      });
      this.photoList.push(this.photo3)
    } else if (this.addPhotoBtnCount == 4 && this.photo4.path == null) {

      this.photo4.path = this.files[-1].response.responseData
      this.matched4 = false


      EXIF.getData(this.photo4.path, function () {
        this.photo4.longitude = EXIF.getTag(this, "GPSLongitude");
        this.photo4.latitude = EXIF.getTag(this, "GPSLatitude");

      });
      this.photoList.push(this.photo4)
    }

    if (this.addPhotoBtnCount == 4) {
      this.disable = true
    }
    if (this.addPhotoBtnCount == 4) {
      this.saveAllImages()
    }


  }
  getMetaData(path) {

  }
  saveAllImages() {
    let path: string
    let photostoBeSaved = new Array<Photo>()
    for (let a of this.photoList) {
      path = a.path
      let ar = path.split("/")
      var index = this.photoList.indexOf(a)
      let photo = new Photo()
      photo.description = "Not Matched"
      photo.path = ar[5]
      photo.latitude = a.latitude
      photo.longitude = a.longitude
      photostoBeSaved.push(photo)
    }

    this.commonService.processPost(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveAllImages", photostoBeSaved).subscribe(res => {



      // console.log(this.criteriaList.length)
      if (res.responseCode == 1) {
        this.success = true;
        console.log("Success")
        setTimeout(() => {
          this.success = false;
        }, 5000);

        console.log("now what", this.activity)

      }

    }), error => {
      console.log("ERROR")
    }
  }


  updateMaintainActivity(status) {
    // console.log("Update Maintain", this.comment)
    // console.log("Milestones List", this.selectedUpdatedMilestoneList)
    // console.log("Field List", this.selectedMaintainActivityFieldList)
    // console.log("ALlocated Budget", this.allocatedBudget)


    let tobeUpdatedMIlestoneList = new Array<UpdateMilestone>()




    for (let a of this.selectedUpdatedMilestoneList) {
      let tobeUpdatedMilestone = new UpdateMilestone()

      var index = this.selectedUpdatedMilestoneList.indexOf(a)
      tobeUpdatedMilestone.status = status
      if (a.status == "Cancel") {
        tobeUpdatedMilestone.status = 4
      } else if (a.status == "Pending") {
        tobeUpdatedMilestone.status = 1
      } else if (a.status == "In Progress") {
        tobeUpdatedMilestone.status = 2
      } else if (a.status == "Completed") {
        tobeUpdatedMilestone.status = 3
      } else if (a.status == "Grant Approved") {
        tobeUpdatedMilestone.status = 5
      } else if (a.status == "Grant Released") {
        tobeUpdatedMilestone.status = 6
      }

      tobeUpdatedMilestone.actualEndDate = this.datePipe.transform(a.actualEndDate, 'yyyy-MM-dd')
      tobeUpdatedMilestone.actualStartDate = this.datePipe.transform(a.actualStartDate, 'yyyy-MM-dd');
      tobeUpdatedMilestone.docDescription = this.comment
      tobeUpdatedMilestone.documentList = a.documentList
      tobeUpdatedMilestone.grantDate = this.datePipe.transform(a.grantDate, 'yyyy-MM-dd');
      tobeUpdatedMilestone.grants = a.grants
      tobeUpdatedMilestone.matchedPhotosList = a.matchedPhotosList
      tobeUpdatedMilestone.milestoneId = a.milestoneId
      tobeUpdatedMilestone.milestoneName = a.milestoneName
      tobeUpdatedMilestone.nonMatchedPhotosList = a.nonMatchedPhotosList
      tobeUpdatedMilestone.plannedEndDate = a.plannedEndDate
      tobeUpdatedMilestone.plannedStartDate = a.plannedStartDate
      tobeUpdatedMilestone.progress = a.progress
      tobeUpdatedMIlestoneList.push(tobeUpdatedMilestone)





    }
    let maintainActivtyObj = new MaintainActivity()
    if (maintainActivtyObj.activityData.activityId == null) {
      maintainActivtyObj.activityData.activityId = this.activityId
    }
    maintainActivtyObj.activityData.status = status
    maintainActivtyObj.activityData.status = 4
    maintainActivtyObj.activityData.activityDescription = this.description


    maintainActivtyObj.activityData.learningCenterId = this.selectedLearningCenter.sn
    maintainActivtyObj.activityData.learningCenter = this.selectedLearningCenter.name



    maintainActivtyObj.activityData.category = this.selectedCategory.categoryName
    maintainActivtyObj.activityData.categoryId = this.selectedCategory.categoryId
    maintainActivtyObj.startDate = this.startDate
    maintainActivtyObj.endDate = this.endDate

    maintainActivtyObj.activityData.type = this.selectedType
    maintainActivtyObj.milestoneList = tobeUpdatedMIlestoneList

    maintainActivtyObj.fieldList = this.selectedMaintainActivityFieldList

    maintainActivtyObj.activityData.status = status

    for (let a of this.claimGrid) {
      let claim = new Claim()
      claim.activityId = a.activityId
      claim.amount = a.amount
      claim.claimedDate = a.claimedDate
      claim.description = a.description
      claim.userInserted = this.userId
      maintainActivtyObj.claimList.push(claim)
    }





    this.commonService.processPost(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/updateMaintainActivity", maintainActivtyObj).subscribe(res => {



      // console.log(this.criteriaList.length)
      if (res.responseCode == 1) {
        this.success = true;
        console.log("Success", this.activityStatus)
        setTimeout(() => {
          this.success = false;

        }, 1000);

        console.log("now what", this.activity)
        this.getAllActivitiesbyStatus(4)



      }

    }), error => {
      console.log("ERROR")
    }

  }

  getAllLearningCentersbyId(id1, id2, type) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id1=" + id1 + "&id2=" + id2 + "&type=" + type).subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;
        console.log("learni", this.learningCenterList)
        if (this.learningCenterList.length != 0) {
          // this.selectedLearningCenter = this.learningCenterList[0]
        }

      }


    }), error => {
      console.log("ERROR")
    }
  }
  checked(event, a) {

    this.updateDocModel = new UpdateDocModel()

    // for (let i = 0; i < this.addedDocs.length; i++) {
    let isChecked = event.target.checked



    if (isChecked) {



      this.updateDocModel.availability = 1

    } else {


      this.updateDocModel.availability = 0
    }

    this.updateDocModel.activityId = this.activityId
    this.updateDocModel.milestoneId = this.selectedMilestoneId
    this.updateDocModel.documentId = a.documentId
    this.updateDocModel.documentName = a.documentName
    this.updateDocModel.status = a.status
    this.updateDocModel.description = this.comment
    console.log("UPDATE DOC MODEL", this.updateDocModel)
    var index = this.updateDocModelList.indexOf(this.updateDocModel)
    this.updateDocModelList.splice(index, 1)
    this.updateDocModelList.push(this.updateDocModel)
    // }

  }

  updateDoc() {

    if (this.updateDocModelList.length == 0) {
      this.updateDocModelList = this.addedDocs
      for (let a of this.addedDocs) {
        a.description = this.comment

      }
    } else {
      for (let a of this.updateDocModelList) {
        a.description = this.comment

      }
    }

    console.log("UPDATED DOC MODEL LIST", this.updateDocModelList)
    this.commonService.processPost(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/updateDocumentStatus", this.updateDocModelList).subscribe(res => {

      console.log("Sucess?", res.responseCode)
      if (res.responseCode == 1) {
        this.showDocuments.hide()
      }

    }), error => {
      console.log("ERROR")
    }
  }

  collapse(evt) {
    console.log("COLLAPSE", evt)

  }
  expand(event) {


    this.previouslySelectedNode.id = this.selectedNode.id
    this.previouslySelectedNode.label = this.selectedNode.label
    this.previouslySelectedNode.type = this.selectedNode.type
    this.previouslySelectedNode.code = this.selectedNode.code
    this.previouslySelectedNode.districtId = this.selectedNode.districtId
    this.previouslySelectedNode.dsID = this.selectedNode.dsID
    this.previouslySelectedNode.provinceId = this.selectedNode.provinceId

  }

  nodeSelected(event) {
    this.getAllLocations()
    this.currentLocation = event.node
    console.log("Cyurrent Location",this.currentLocation.parent.parent.parent.id)
    if (this.selectedType == 'PMU') {

      if (this.selectedActivityType == 1) {

        this.selectedLearningCenter.address = event.node.label
        this.getAllLearningCentersbyId(event.node.districtId, event.node.code, event.node.type)
        this.showLearningCenters.show()
        this.collapseAll()
      } else {
        this.selectedAreaPMU = event.node.id
        this.selectedLearningCenter.address = event.node.label
      }



      // this.showLearningCenters.show()
      this.collapseAll()



    }
    if (this.selectedType == 'PHDT') {
      this.selectedDivision = event.node
      // this.selectedLearningCenter.address = event.node.label
      // this.showLearningCenters.show()
      this.collapseAll()
      // this.getAllLocations()
    }


  }

  nodeSelect(event) {
    console.log(this.learningCenterControl.valid)
    if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;




      this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
    }

    // this.getAllLearningCentersbyId(event.node.i)
    let district = "" + event.node.provinceId + event.node.code;




    if ((this.selectedType == 'PMU') || this.selectedType == 'PHDT') {



      if (event.node.type == "province") {

        for (let j = 0; j < event.node.children.length; j++) {

          for (let i = 0; i < this.businessList.length; i++) {


            let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

            if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
              let node = new Node()
              node.id = this.businessList[i].id
              node.label = this.businessList[i].english
              node.type = this.businessList[i].type
              node.code = this.businessList[i].code
              node.districtId = this.businessList[i].districtId
              node.dsID = this.businessList[i].dsID
              node.provinceId = this.businessList[i].provinceId
              node.lifeCode = this.businessList[i].lifeCode

              console.log("life code", this.businessList[i].lifeCode)
              event.node.children[j].children.push(node)



            }
          }
        }
        this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
      }

      if (event.node.type == "region") {

        for (let j = 0; j < event.node.children.length; j++) {

          for (let i = 0; i < this.businessList.length; i++) {


            let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

            if (district == this.businessList[i].districtId && this.businessList[i].type == "estate") {
              let node = new Node()
              node.id = this.businessList[i].id
              node.label = this.businessList[i].english
              node.type = this.businessList[i].type
              node.code = this.businessList[i].code
              node.districtId = this.businessList[i].districtId
              node.dsID = this.businessList[i].dsID
              node.provinceId = this.businessList[i].provinceId
              node.lifeCode = this.businessList[i].lifeCode
              event.node.children[j].children.push(node)




            }

          }
          for (let k = 0; k < event.node.children[j].children.length; k++) {

            for (let l = 0; l < this.businessList.length; l++) {
              if (this.businessList[l].type == "div" && event.node.children[j].children[k].districtId == this.businessList[l].districtId && event.node.children[j].children[k].code == this.businessList[l].dsID) {

                let node = new Node()
                node.id = this.businessList[l].id
                node.label = this.businessList[l].english
                node.type = this.businessList[l].type
                node.code = this.businessList[l].code
                node.districtId = this.businessList[l].districtId
                node.dsID = this.businessList[l].dsID
                node.provinceId = this.businessList[l].provinceId
                node.lifeCode = this.businessList[l].lifeCode
                event.node.children[j].children[k].children.push(node)
              }

            }

          }
        }

      }
    }

  }
  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {



      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node.lifeCode = this.businessList[i].lifeCode
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



        // this.getAllLearningCenters()


      }



      if (this.selectedType == 'PMU') {
        if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node.lifeCode = this.businessList[i].lifeCode

          node1.children.push(node)
        }
      }




      if (this.selectedType == 'PHDT') {

        if (this.businessList[i].type == "region" && this.businessList[i].nodeType == node1.id) {
          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node.lifeCode = this.businessList[i].lifeCode


          node1.children.push(node)

        }
        if (node1.type == "region") {
          if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "rpc") {

            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            node.lifeCode = this.businessList[i].lifeCode
            node1.children.push(node)


          }

        }
      }


      if ((this.selectedType == 'PMU')) {


        if (node1.type == "province") {
          if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            node.lifeCode = this.businessList[i].lifeCode

            node1.children.push(node)

            // this.getAllLearningCenters()
          }
        }


      }



    }


    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])

    }

  }

  getAllLocations() {

    this.selectedNode = new Node()
    let type
    if (this.userId == 1) {
      type = "root"
    } else if (this.userId == 2 || this.userId == 3) {
      type = "node"
    }



    this.businessList = new Array<BusinessLevel>()
    this.file = new Array<Node>()

    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {

      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {


          if (this.businessList[i].type == type && this.businessList[i].id == parseInt(this.userId)) {
            let node = new Node()

            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.lifeCode = this.businessList[i].lifeCode

            this.selectedNode.leaf = false


          }



        }





        this.addChildren(this.selectedNode)



        this.file.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }


  collapseAll() {
    this.file.forEach(node => {
      this.expandRecursive(node, false);
    });
  }

  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }
  onRowSelect(evt) {

  }

  addActualBudget() {


    // this.actualBudget=this.actualBudget+bud
  }
  calculateTotla() {

    this.allocatedBudget = 0

    for (let a of this.selectedBudgetList) {
      this.allocatedBudget = this.allocatedBudget + a.allocatedBudget
    }

  }

  addClaimstoGrid() {
    this.claim = new Claim()
    this.claim.claimedDate = new Date()
    this.addCalims.show()
  }
  addClaimtoGrid() {


    let date = this.datePipe.transform(this.claim.claimedDate, 'yyyy-MM-dd')
    let claim = new Claim()
    claim.activityId = this.activityId
    claim.amount = this.claim.amount
    claim.claimedDate = date
  
    claim.description = this.selectedDescription.milestoneName

    claim.userInserted = this.userId
    this.totalClaim = this.totalClaim + this.claim.amount
    this.claimGrid.push(claim)
  }
  removeClaimfromGrid(claim) {
    var index = this.claimGrid.indexOf(claim)
    this.claimGrid.splice(index, 1)
    this.totalClaim = this.totalClaim - claim.amount

  }

  changeActivityTypePMU() {

    this.selectedLearningCenter = new Data()

    this.collapseAll()
    // this.getAllLocations()
  }

  thousandSeperators(e, a) {

    this.number = 0
    this.number = a
    this.allocatedBudget = 0
    this.numberPipe.transform(a)

    // this.allocatedBudget = this.allocatedBudget+this.number
    for (let a of this.selectedBudgetList) {

      if (a.allocatedBudget == undefined) {
        a.allocatedBudget = 0
      }
      this.allocatedBudget = this.allocatedBudget + a.allocatedBudget



    }

  }

  changeType() {
    this.collapseAll()
    this.loadingTree = true
    this.getAllLocations()
  }

  changeGrant() {
    this.totalGrant = 0
    this.totalClaim = 0
    for (let a of this.selectedUpdatedMilestoneList) {

      this.totalGrant = this.totalGrant + a.grants

    }
    console.log("Sss", this.totalGrant)
    this.totalClaim = this.totalClaim - this.totalGrant


  }
}
