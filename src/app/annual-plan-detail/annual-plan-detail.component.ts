import { Component, OnInit, Input, ViewChild, SimpleChanges } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ModalData } from '../model/ModalData';
import { Router } from '@angular/router';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { AnnualPlan } from '../model/AnnualPlan';

export class Month {
  month
  number
}
export class Year {
  year

}
export class Type {
  type
}


@Component({
  selector: 'app-annual-plan-detail',
  templateUrl: './annual-plan-detail.component.html',
  styleUrls: ['./annual-plan-detail.component.css'],
  providers: [CommonWebService]
})
export class AnnualPlanDetailComponent implements OnInit {
  selectedActType: string;
  userId: any;
  error: boolean;
  userData: any;
  update: boolean;
  success: boolean;

  public annualPlan: FormGroup;
  public annualPlanData: AnnualPlan;

  // public month = new Month()
  // public year = new Year()
  ngOnInit(): void {
    if(this.userId==2){
      this.selectedActType = '1'
    }else if(this.userId==3){
      this.selectedActType = '2'
    }
    if (this.year != null && this.month != null) {
      let year = this.year
      let month = this.month

      // this.loadDetails(year,month);
    }
  }

  ngOnChanges(changes: SimpleChanges) {

    let year = this.year.year
    let month = this.month.number
    console.log("type", this.type.type)
    console.log("year", this.year.year)
    this.loadDetails(year, month)

  }
  @Input('month') month: Month;
  @Input('year') year: Year;
  @Input('type') type: Type;

  @ViewChild("viewDetails") public viewDetails: ModalDirective;

  public model = new ModalData()
  value: number = 20;
  public detailAray = new Array<AnnualPlan>()
  constructor(public route: Router, public commonService: CommonWebService) {
    this.userId = sessionStorage.getItem("bisId");
    this.userId = sessionStorage.getItem("bisId");
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.annualPlanData = new AnnualPlan()
    this.validation();

  }

  validation() {

    this.annualPlan = new FormGroup({
      docId: new FormControl(),
      masterTrainer: new FormControl(['', Validators.required]),
      longTerm: new FormControl(['', Validators.required]),
      shortTerm: new FormControl(['', Validators.required]),
      parentalAwarenes: new FormControl(['', Validators.required]),
      periodPreSchool: new FormControl(['', Validators.required]),
      staffCapacity: new FormControl(['', Validators.required]),
      adminStaff: new FormControl(['', Validators.required]),
      pmu: new FormControl(['', Validators.required]),
      phdt: new FormControl(['', Validators.required]),
      diplomaPhdt: new FormControl(['', Validators.required]),
      refresherTraining: new FormControl(['', Validators.required]),
      parentalPhdt: new FormControl(['', Validators.required]),
      fig: new FormControl(['', Validators.required]),
      ecdCenter: new FormControl(['', Validators.required]),
      ecdResource: new FormControl(['', Validators.required]),
      feeWaiver: new FormControl(['', Validators.required]),
      learningMaterials: new FormControl(['', Validators.required]),
      childAss: new FormControl(['', Validators.required]),
      census: new FormControl(['', Validators.required]),
      socialAudit: new FormControl(['', Validators.required]),
      mis: new FormControl(['', Validators.required]),
      ecdCenterReg: new FormControl(['', Validators.required]),
      newCdc: new FormControl(['', Validators.required]),
      renCdc: new FormControl(['', Validators.required]),
      newPlay: new FormControl(['', Validators.required]),
      renPlay: new FormControl(['', Validators.required]),
      headOrientation : new FormControl(['', Validators.required]),
    })
  }

  click() {
    console.log("CLICKED", this.year.year.id)
    let year = this.year
    let month = this.month.month.number

    this.loadDetails(year, month);
  }
  Open(type) {

    this.model.title = "Master Trainer Program"
    this.viewDetails.show()
  }
  ngAfterViewInit() {
    console.log("LOADONG>>")
  }
  openMap() {
    this.route.navigate(['dashboard_details', { month: this.month.number, year: this.year.year.id }])
  }
  loadDetails(year, month) {

    this.annualPlanData = new AnnualPlan()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getAnnualPlan?year=" + year + "&month=" + month).subscribe(res => {

      this.annualPlanData = res.responseData
      console.log("RES", this.annualPlanData);

    }), error => {
      console.log("ERROR")
    }
  }

  saveDetails() {
    this.annualPlanData.month = this.month.number;
    this.annualPlanData.year = this.year.year;
    this.annualPlanData.userInserted = this.userData[0].userId;

    console.log("save details", this.annualPlanData);
    this.commonService.processPost(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/insertAnnualPlan", this.annualPlanData).subscribe(res => {

      if (res.responseCode == 1) {
        this.success = true;
        setTimeout(() => {
          this.success = false
        }, 2000);

      }else{
        this.error = true;
        setTimeout(() => {
          this.error = false
        }, 2000);
      }
      

    }), error => {
      console.log("ERROR")
      this.error = true;
        setTimeout(() => {
          this.error = false
        }, 2000);
    }
  }

  updateDetails() {
    this.annualPlanData.month = this.month.number;
    this.annualPlanData.year = this.year.year;
    this.annualPlanData.userInserted = "Sandali";

    console.log("save details", this.annualPlanData);
    this.commonService.processPost(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/updateAnnualPlan", this.annualPlanData).subscribe(res => {

      if (res.responseCode == 1) {

        setTimeout(() => {
          this.update = true;
        }, 2000);

      }
      console.log("responseCode:", res.responseCode);

      // console.log("RES", this.detailAray)

    }), error => {
      console.log("ERROR")
    }
  }
}

