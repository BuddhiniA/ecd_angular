import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnnualPlanDetailComponent } from './annual-plan-detail.component';

describe('AnnualPlanDetailComponent', () => {
  let component: AnnualPlanDetailComponent;
  let fixture: ComponentFixture<AnnualPlanDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnnualPlanDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnualPlanDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
