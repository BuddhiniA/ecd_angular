import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'
import { CriteriaManagementComponent } from "./criteria-management/criteria-management.component";
import { CriteriaResultManagementComponent } from "./criteria-result-management/criteria-result-management.component";
import { MaterialStockComponent } from './material-stock/material-stock.component';
import { MaterialPackComponent } from './material-pack/material-pack.component';
import { SendMaterialComponent } from './send-material/send-material.component';
import { ReceiveMaterialComponent } from './receive-material/receive-material.component';
import { DefineDocumentComponent } from './define-document/define-document.component';

import { DefineActivityComponent } from './define-activity/define-activity.component';
import { CertficateManagementComponent } from './certficate-management/certficate-management.component';
import { LongTermTrainingProgramComponent } from './long-term-training-program/long-term-training-program.component';
import { RegisteredParticipantComponent } from './registered-participant/registered-participant.component';
import { StaffCapacityBuildingComponent } from './staff-capacity-building/staff-capacity-building.component';
import { PrimaryShoolTrainingProgramComponent } from './primary-shool-training-program/primary-shool-training-program.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MapComponentComponent } from './map-component/map-component.component';
import { DashBoardActivityDetailsComponent } from './dash-board-activity-details/dash-board-activity-details.component';
import { ListComponent } from './list/list.component';
import { TrainingProgramComponent } from './training-program/training-program.component';
import { DashBoardProgramViewComponent } from './dash-board-program-view/dash-board-program-view.component';
import { GanttComponent } from './gantt/gantt.component';
import { CustomReportsComponent } from './custom-reports/custom-reports.component';
import { FeeWaiverPaymentComponent } from './fee-waiver-payment/fee-waiver-payment.component';
import { SocialAuditsComponent } from './social-audits/social-audits.component';
import { LearningMaterialDefinitionComponent } from './learning-material-definition/learning-material-definition.component';
import { BusinessStructureComponent } from './business-structure/business-structure.component';
import { LmReportsComponent } from './lm-reports/lm-reports.component';
import { DistributedMaterialsComponent } from './distributed-materials/distributed-materials.component';
import { ResourcePoolComponent } from './resource-pool/resource-pool.component';
import { ActivityCategoriesComponent } from './activity-categories/activity-categories.component';
import { MasterTrainerProgramsComponent } from './master-trainer-programs/master-trainer-programs.component';
import { ShorttermTrainerProgramComponent } from './shortterm-trainer-program/shortterm-trainer-program.component';
import { TrainingInstituteComponent } from './training-institute/training-institute.component';
import { ParentalAwarenessComponent } from './parental-awareness/parental-awareness.component';
import { PrimaryschoolHeadorientationProgramComponent } from './primaryschool-headorientation-program/primaryschool-headorientation-program.component';
import { ParentalParticipantComponent } from './parental-participant/parental-participant.component';
import { AdministrativeStaffComponent } from './administrative-staff/administrative-staff.component';
import { OrientationParticipantComponent } from './orientation-participant/orientation-participant.component';
import { FeewaiverRegistrationComponent } from './feewaiver-registration/feewaiver-registration.component';
import { ChildDevelopmentAssmntUpdateComponent } from './child-development-assmnt-update/child-development-assmnt-update.component';
import { UsermanagementComponent } from './usermanagement/usermanagement.component';
import { LoginComponent } from './login/login.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { AnnualPlanDetailComponent } from './annual-plan-detail/annual-plan-detail.component';
import { AnnualPlanComponent } from './annual-plan/annual-plan.component';
import { CensusDataManagementComponent } from './census-data-management/census-data-management.component';
import { MiniCensusDataManagementComponent } from './mini-census-data-management/mini-census-data-management.component';
import { DashBoardManagementComponent } from './dash-board-management/dash-board-management.component';
import { RefresherProgramComponent } from './refresher-program/refresher-program.component';
import { FeeWaiverPaymentUpdateComponent } from './fee-waiver-payment-update/fee-waiver-payment-update.component';


const appRoutes: Routes = [

    {
        path: 'criteria-management',
        component: CriteriaManagementComponent
    },

    {
        path: 'materialstock-management',
        component: MaterialStockComponent
    }
    ,
    {
        path: 'materialpack-management',
        component: MaterialPackComponent
    },
    {
        path: 'criteriaresult-management',
        component: CriteriaResultManagementComponent
    },
    {
        path: 'sendmaterials-management',
        component: SendMaterialComponent
    },
    {
        path: 'receivematerials-management',
        component: ReceiveMaterialComponent
    },
    {
        path: 'define-documents',
        component: DefineDocumentComponent
    },
    {
        path: 'define-activities',
        component: DefineActivityComponent
    },
    {
        path: 'certificate-management',
        component: CertficateManagementComponent
    },
    {
        path: 'longterm-program',
        component: LongTermTrainingProgramComponent
    },
    {
        path: 'registered-participant',
        component: RegisteredParticipantComponent

    },
    {
        path: 'staffcapacitybuilding',
        component: StaffCapacityBuildingComponent

    },
    {
        path: 'primaryschoolteachr-training',
        component: PrimaryShoolTrainingProgramComponent

    },
    {
        path: 'dashboard',
        component:DashBoardManagementComponent

    },
    {
        path: 'dashboard_details',
        component: DashBoardActivityDetailsComponent

    },
    {
        path: 'training_details',
        component: TrainingProgramComponent

    },
    {
        path: 'program_view',
        component: DashBoardProgramViewComponent

    },
    {
        path: 'gantt-view',
        component: GanttComponent

    },
    {
        path: 'custom-report',
        component: CustomReportsComponent

    },
    {
        path: 'feewaiver',
        component: FeeWaiverPaymentComponent

    },
    {
        path: 'social-audits',
        component: SocialAuditsComponent

    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'forgot-password',
        component: ForgotpasswordComponent
    },
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'user-management',
        component: UsermanagementComponent
    },
    {
        path: 'learning-mtr',
        component: LearningMaterialDefinitionComponent
    },
    {
        path: 'bis-str',
        component: BusinessStructureComponent
    },
   
    {
        path: 'learning-reports',
        component: LmReportsComponent
    },
    {
        path: 'distributed-mtr',
        component: DistributedMaterialsComponent
    },
    {
        path: 'resource-pool',
        component: ResourcePoolComponent
    },
    {
        path: 'activity-category',
        component: ActivityCategoriesComponent
    },
    
    {
        path: 'master-trainer',
        component: MasterTrainerProgramsComponent
    },
    {
        path: 'shortterm-trainer',
        component: ShorttermTrainerProgramComponent
    },
    {
        path: 'training-inistitute',
        component: TrainingInstituteComponent
    },
   
    {
        path: 'parental-awareness',
        component: ParentalAwarenessComponent
    },
    
    {
        path: 'primary-headorientation',
        component: PrimaryschoolHeadorientationProgramComponent
    },
    {
        path: 'parental-participant',
        component: ParentalParticipantComponent
    },
    {
        path: 'admin-staff',
        component: AdministrativeStaffComponent
    },
    {
        path: 'orientation-participant',
        component: OrientationParticipantComponent
    },
 
    
    {
        path: 'fee-waiver-reg',
        component: FeewaiverRegistrationComponent

    },
    {
        path: 'child-dev-assmnt-update',
        component: ChildDevelopmentAssmntUpdateComponent

    },
    {
        path: 'annual-plan',
        component:AnnualPlanComponent
        
    },
    {
        path: 'census',
        component:CensusDataManagementComponent        
    },
    {
        path: 'minicensus',
        component:MiniCensusDataManagementComponent        
    },
    {
        path: 'refresher',
        component:RefresherProgramComponent        
    },
    {
        path: 'feewaiver_update',
        component:FeeWaiverPaymentUpdateComponent        
    }
    
    

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);