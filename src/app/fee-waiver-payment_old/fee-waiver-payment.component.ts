import { Component, OnInit, ViewChild } from '@angular/core';

import { Data } from '../model/Data';

import { ModalDirective } from 'ngx-bootstrap';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { Http, RequestOptions, Headers } from '@angular/http';
import { FeeWaiverGrid } from "../model/FeeWaiverGrid";
import { FeeWaiverPayments } from "../model/FeeWaiverPayments";
import { FeeWaiverPaymentData } from '../model/FeewaiverPaymentData';
import { FeeWaiverStudent } from '../model/FeeWaiverStudent';
import { months } from 'moment';
import { DatePipe } from '@angular/common';

export class Criteria {

  id
  criteriaId
  criteriaName
  status
  userEnterd
  dataInserted
  dateModified

}

export class BusinessLevel {
  id
  lifeCode
  districtId
  dsID
  english
  type
  code
  provinceId
  nodeType


}
export class Node {
  id
  label
  type
  code
  lifeCode
  districtId
  dsID
  provinceId
  nodeType
  leaf
  children = new Array<Node>()
}

export class Student {
  studentId
  studentName
  learningCenter
  year
  startMonth
  status
  paymentId1
  attendance1 = 0
  paid1
  paymentId2
  attendance2 = 0
  paid2
  paymentId3
  attendance3 = 0
  paid3
  paymentId4
  attendance4 = 0
  paid4
  paymentId5
  attendance5 = 0
  paid5
  paymentId6
  attendance6 = 0
  paid6
  paymentId7
  attendance7 = 0
  paid7
  paymentId8
  attendance8 = 0
  paid8
  paymentId9
  attendance9 = 0
  paid9
  paymentId10
  attendance10 = 0
  paid10
  paymentId11
  attendance11 = 0
  paid11
  paymentId12
  ttendance12 = 0
  paid12
  paymentId13
  attendance13 = 0
  paid13
  paymentId14
  attendance14 = 0
  paid14
  paymentId15
  attendance15 = 0
  paid15
  paymentId16
  attendance16 = 0
  paid16
  paymentId17
  attendance17 = 0
  paid17
  paymentId18
  attendance18 = 0
  paid18
  paymentId19
  attendance19 = 0
  paid19
  paymentId20
  attendance20 = 0
  paid20
  paymentId21
  attendance21 = 0
  paid21
  paymentId22
  attendance22 = 0
  paid22
  paymentId23
  attendance23 = 0
  paid23
  paymentId24
  attendance24 = 0
  paid24
  attendance25 = 0
  paid25
  attendance26 = 0
  paid26
  attendance27 = 0
  paid27
  attendance28 = 0
  paid28
  attendance29 = 0
  paid29
  attendance30 = 0
  paid30
  attendance31 = 0
  paid31
  attendance32 = 0
  paid32
   attendance33 = 0
  paid33
  attendance34 = 0
  paid34
  attendance35 = 0
  paid35
  attendance36 = 0
  paid36
  attendance37 = 0
  paid37
  attendance38 = 0
  paid38
  attendance39 = 0
  paid39
  attendance40 = 0
  paid40
}

export class Payment {
  learningCenter
  studentId
  year
  month
  status
  paidDate
}
export class PaymentData {

  attendance
  paidDate
  paidMonth
  paidStatus
  precentage
  studentId
  studentName
}

export class FeeWaiverDetail {
  criteriaId
  learningCenter
  paymentData = new Array<PaymentData>()

}

@Component({
  selector: 'app-fee-waiver-payment',
  templateUrl: './fee-waiver-payment.component.html',
  styleUrls: ['./fee-waiver-payment.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class FeeWaiverPaymentComponent implements OnInit {
  loadingMask: boolean;
  month: number;
  year: number;
  feeWaiverId: any;
  globalSt: Student;
  studentList1: Student[];
  @ViewChild("adPaymentView") public adPaymentView: ModalDirective;

  public learningCenterList = new Array<Data>()
  public learningCenterListbyCriteria = new Array<Data>()
  public selectedLearningCenter = new Data();

  public criteriaList = new Array<Criteria>()
  public selectedCriteria = new Criteria();

  public studentList = new Array<Student>()

  public pickDate;
  public feeWaiverGrid = new Array<FeeWaiverGrid>();

  public months = new Array<number>();

  public success;
  public selectedDate
  public yearList = new Array<number>()
  public selectedYear: number;
  public selectedMonth: number;
  public paymentToBeSaved
  public tobeSavedList = new Array<Payment>()
  public headers: Headers = new Headers;
  public value = 3000;

  public checked
  public feeWaiverDetail = new FeeWaiverDetail()

  public paid1
  public paid2
  public paid3
  public paid4
  public paid5
  public paid6
  public paid7
  public paid8
  public paid9
  public paid10
  public paid11
  public paid12
  public paid13
  public paid14
  public paid15
  public paid16
  public paid17
  public paid18
  public paid19
  public paid20
  public paid21
  public paid22
  public paid23
  public paid24

  files = new Array<Node>()
  public businessList = new Array<BusinessLevel>()
  public selectedNode = new Node();
  public previouslySelectedNode = new Node()


  public feeWaiverPaymentList = new Array<FeeWaiverPaymentData>()

  constructor(public commonService: CommonWebService, private http: Http,private datePipe: DatePipe) { }

  ngOnInit() {
    this.month=0
    this.year=0
    this.getAllLearningCenters();
  }


  getAllLearningCenters() {
    // this.loadingMask=true
    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getRegistredFeeWaiverLearningCenter").subscribe(res => {

      if (res.responseCode == 1) {
       
        this.learningCenterList = res.responseData;
        if(this.learningCenterList.length!=0){
          // this.loadingMask=false
        }
        this.selectedLearningCenter = this.learningCenterList[0]

        if (this.selectedLearningCenter != undefined || this.selectedLearningCenter != null) {
          this.getAllStudents()
        }
      }


    }), error => {
      console.log("ERROR")
    }
  }






  getAllStudents() {

    console.log("change")
    this.studentList = new Array<Student>()

    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getRegistredStudents?learningCenter=" + this.selectedLearningCenter.sn).subscribe(res => {

      if (res.responseCode == 1) {

        this.feeWaiverPaymentList = res.responseData
        this.feeWaiverId = this.feeWaiverPaymentList[0].feeWaiver.feeWaiverId


        if (this.feeWaiverPaymentList[0].feeWaiver.startMonth == 0) {
         
          this.selectedMonth = 1
          this.month = 0
        } else {
          this.month = 1
          this.selectedMonth = this.feeWaiverPaymentList[0].feeWaiver.startMonth
         
        }
        if (this.feeWaiverPaymentList[0].feeWaiver.startYear == 0) {
         
          this.year = 0
          this.loadYears(2018)
        } else {
          this.year =1
          this.selectedYear = this.feeWaiverPaymentList[0].feeWaiver.startYear
         
        }

      }

      let distinctStudentList = new Array<Student>()


      for (let k of this.feeWaiverPaymentList) {
        let st = new Student()
        st.studentId = k.student.studentId
        st.studentName = k.student.name
        st.status = k.student.status
        distinctStudentList.push(st)

      }

      var sl = distinctStudentList;
      var out = [];

      for (var i = 0, l = sl.length; i < l; i++) {
        var unique = true;
        for (var j = 0, k = out.length; j < k; j++) {
          if ((sl[i].studentId === out[j].studentId)) {
            unique = false;
          }
        }
        if (unique) {
          out.push(sl[i]);
        }
      }
      console.log(sl.length); // 10
      console.log(out.length); // 5

      this.studentList = out









      for (let i = 0; i < this.studentList.length; i++) {


        for (let j = 0; j < this.feeWaiverPaymentList.length; j++) {

          if (this.feeWaiverPaymentList[j] != undefined) {


            if (this.studentList[i].studentId == this.feeWaiverPaymentList[j].student.studentId) {


              let ar = this.feeWaiverPaymentList[j].payment.paymentMonth



              switch (ar) {
                case 1: {

                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.studentList[i].paid1 = true
                    this.paid1 = true
                    this.studentList[i].attendance1 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId1 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break


                }
                case 2: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid2 = true
                    this.studentList[i].paid2 = true
                    this.studentList[i].attendance2 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId2 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 3: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid3 = true
                    this.studentList[i].paid3 = true
                    this.studentList[i].attendance3 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId3 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 4: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid4 = true
                    this.studentList[i].paid4 = true
                    this.studentList[i].attendance4 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId4 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 5: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid5 = true
                    this.studentList[i].paid5 = true
                    this.studentList[i].attendance5 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId6 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 6: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid6 = true
                    this.studentList[i].paid6 = true
                    this.studentList[i].attendance6 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId6 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 7: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid7 = true
                    this.studentList[i].paid7 = true
                    this.studentList[i].attendance7 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId7 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 8: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid8 = true
                    this.studentList[i].paid8 = true
                    this.studentList[i].attendance8 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId8 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 9: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid9 = true
                    this.studentList[i].paid9 = true
                    this.studentList[i].attendance9 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId9 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 10: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid10 = true
                    this.studentList[i].paid10 = true
                    this.studentList[i].attendance10 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId10 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 11: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid11 = true
                    this.studentList[i].paid11 = true
                    this.studentList[i].attendance11 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId11 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 12: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid12 = true
                    this.studentList[i].ttendance12 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId12 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 13: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid13 = true
                    this.studentList[i].attendance13 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId13 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 14: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid14 = true
                    this.studentList[i].attendance14 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId14 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 15: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid15 = true
                    this.studentList[i].attendance15 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId15 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 16: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid16 = true
                    this.studentList[i].attendance16 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId16 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 17: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid17 = true
                    this.studentList[i].attendance17 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId17 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 18: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid18 = true
                    this.studentList[i].attendance18 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId18 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 19: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid19 = true
                    this.studentList[i].attendance19 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId19 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 20: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid20 = true
                    this.studentList[i].attendance20 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId20 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 21: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid21 = true
                    this.studentList[i].attendance21 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId21 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 22: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid22 = true
                    this.studentList[i].attendance22 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId22 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 23: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid12 = true
                    this.studentList[i].paid23 = true
                    this.studentList[i].attendance23 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId23 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                case 24: {
                  if (this.feeWaiverPaymentList[j].payment.paymantDate != null) {
                    this.paid24 = true
                    this.studentList[i].paid24 = true
                    this.studentList[i].attendance24 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId24 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  break
                }
                default: {
                  console.log("NOTHING")
                }
              }



            }
          }

        }


      }



    }), error => {
      console.log("ERROR")

    }
  }






  loadYears(start) {

    for (let i = 0; i < 10; i++) {
      let starty = start + i
      this.yearList.push(starty)

    }
    this.selectedYear = this.yearList[0]

  }



  
  addPayment() {
    this.selectedDate = this.datePipe.transform(new Date(),'yyyy-MM-dd')
    this.adPaymentView.show()
  }







  savePayment() {


    let localSuccess = true
    for (let a of this.tobeSavedList) {
      a.paidDate = this.selectedDate
      console.log("STYUDENT ID", a.studentId)

      console.log("Paid Dtae", a.paidDate)
      console.log("Learning cen", a.learningCenter)
      console.log("year", a.year)
      console.log("year", this.selectedCriteria.id)
      let paidMonth = a.year + "-" + this.setTwoDigits(a.month) + "-01"
      this.headers.set("Content-Type", "application/json")
      this.headers.set("learningCenter", this.selectedLearningCenter.sn)
      this.headers.set("criteriaId", "1")
      this.headers.set("studentId", a.studentId)
      this.headers.set("paidMonth", paidMonth)

      this.headers.set("paidDate", this.selectedDate)

      let options = new RequestOptions({ headers: this.headers });
      if (localSuccess) {

        this.commonService.processPostWithHeaders(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updatePaymentStatus", "", options).subscribe(res => {


          if (res.responseCode == 1) {

            localSuccess = true
          } else {
            localSuccess = false
            return
          }

        }), error => {
          console.log("ERROR")
        }
        console.log("SUCCESS")
      } else {
        this.success = false
      }


    }
    console.log("TO BE SAVED LIST FINAL", this.tobeSavedList)



  }
  changeStatus(evt, number) {

    // this.months.push(number);

    // console.log("EVENT", evt)
    // if (evt.target.checked) {


    //   for (let a of this.studentList) {
    //     this.paymentToBeSaved = new Payment()
    //     this.paymentToBeSaved.learningCenter = this.selectedLearningCenter.sn
    //     this.paymentToBeSaved.studentId = a.studentId
    //     this.paymentToBeSaved.year = this.selectedYear
    //     this.paymentToBeSaved.month = number
    //     this.paymentToBeSaved.status = 1
    //     this.tobeSavedList.push(this.paymentToBeSaved)
    //   }
    // } else {
    //   let arrayForchecked = new Array<Payment>()
    //   for (let b of this.tobeSavedList) {
    //     if (number == b.month) {
    //       arrayForchecked.push(b)
    //     }
    //   }

    //   console.log("array", arrayForchecked)
    //   for (let v of arrayForchecked) {
    //     var index = this.tobeSavedList.indexOf(v)
    //     this.tobeSavedList.splice(index, 1)
    //   }
    // }


    this.months.push(number)
    console.log("TO BE SAVED LIST", this.months)
  }

  setTwoDigits(num: number): any {
    return (num < 10 ? '0' : '') + num
  }
  changeFIG(evt) {
    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/granted?type=FIG").subscribe(res => {
      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData
      }

    }), error => {
      console.log("ERROR")
    }
  }

  collapse(evt) {
    console.log("COLLAPSE", evt)
  }
  expand(event) {


    this.previouslySelectedNode.id = this.selectedNode.id
    this.previouslySelectedNode.label = this.selectedNode.label
    this.previouslySelectedNode.type = this.selectedNode.type
    this.previouslySelectedNode.code = this.selectedNode.code
    this.previouslySelectedNode.districtId = this.selectedNode.districtId
    this.previouslySelectedNode.dsID = this.selectedNode.dsID
    this.previouslySelectedNode.provinceId = this.selectedNode.provinceId
  }
  nodeSelect(event) {

    let district = "" + event.node.provinceId + event.node.code;


    if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(districtId, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.type)
    }

    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }
  }
  addChildren(node1) {

    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)





      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {
          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
      // if (node1.type == "district") {
      //   let district = "" + node1.provinceId + node1.code;
      //   console.log("distrit", district);
      //   if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
      //     let node = new Node()
      //     node.id = this.businessList[i].id
      //     node.label = this.businessList[i].english
      //     node.type = this.businessList[i].type
      //     node.code = this.businessList[i].code
      //     node.districtId = this.businessList[i].districtId
      //     node.dsID = this.businessList[i].dsID
      //     node.provinceId = this.businessList[i].provinceId
      //    node1.children.push(node)
      //     this.getAllLearningCenters()
      //     console.log("SELECTED NODE", this.selectedNode)
      //   }
      // }
      // if (node1.type == "ds") {
      //   if (node1.districtId == this.businessList[i].districtId && node1.code == this.businessList[i].dsID && this.businessList[i].type == "gnd") {
      //     let node = new Node()
      //     node.id = this.businessList[i].id
      //     node.label = this.businessList[i].english
      //     node.type = this.businessList[i].type
      //     node.code = this.businessList[i].code
      //     node.districtId = this.businessList[i].districtId
      //     node.dsID = this.businessList[i].dsID
      //     node.provinceId = this.businessList[i].provinceId
      //    node1.children.push(node)
      //     console.log("SELECTED NODE", this.selectedNode)
      //     this.getAllLearningCenters()
      //   }
      // }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  getAllLocations() {

    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.businessList = res.responseData
        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }

        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }
  getAllLearningCentersbyId(id, type) {
    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id=" + id + "&type=" + type).subscribe(res => {

      this.learningCenterList = res.responseData;

      this.selectedLearningCenter = this.learningCenterList[0]

    }), error => {
      console.log("ERROR")
    }
  }



  saveStudentPayments() {

    let feePayment = new Array<FeeWaiverPayments>()

    for (let a of this.months) {
      let f: FeeWaiverPayments = new FeeWaiverPayments()
      f.feewaiverId = this.feeWaiverId;
      f.paymentMonth = a
      f.paymantDate = this.selectedDate

      feePayment.push(f)
    }

    let headers = new Headers();
    headers.set("month", this.selectedMonth + "");
    headers.set("year", this.selectedYear + "");

    let options = new RequestOptions({ headers });
    console.log("Payments to be saved", feePayment)
    this.commonService.processPostWithHeaders(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updateFeePayment", feePayment, options).subscribe(res => {


      if (res.responseCode == 1) {

        this.success = true
        setTimeout(() => {
          this.success = false;
          this.adPaymentView.hide()
          this.getAllStudents()
        }, 2000);
      }

    }), error => {
      console.log("ERROR")
    }
  }

  onRowSelect(evt){
    console.log("event",evt)
    this.getAllStudents()
  }
}
