import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { District } from '../model/DIstrict';
import { DSDivision } from '../model/DSDivision';
import { Data } from '../model/Data';
import { Census } from '../model/Census';
import { MiniCensus } from '../model/MiniCensus';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-mini-census-data-management',
  templateUrl: './mini-census-data-management.component.html',
  styleUrls: ['./mini-census-data-management.component.css'],
  providers: [CommonWebService]
})
export class MiniCensusDataManagementComponent implements OnInit {
  dsSelected: boolean;
  dsId: any;
  miniCensusHistory = new Array<any>();
  @ViewChild("historyView") public historyView: ModalDirective;

  miniCensus: MiniCensus;
  districtSelected: boolean;
  proviceSelected: boolean;
  userGroup: string;
  districtId: string;
  location: any;
  bisId: any;
  userData: any;
  census = new Census();
  selectedLearningCenter = new Data();
  learningCenterList = new Array<Data>();
  selectedDS = new DSDivision()
  dsList: any;
  selectedDistrict: District;
  districtList = new Array<District>();
  selectedProvince: number;

  constructor(public commonService: CommonWebService) {
    this.miniCensus = new MiniCensus()
  }

  ngOnInit() {
    this.userGroup = sessionStorage.getItem('userGroup');
    this.getUser()


  }
  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.bisId = this.userData[0].extraParams

    if (this.bisId != undefined) {

      this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getLocation").subscribe(res => {
        if (res.responseCode == 1) {

          if (this.userGroup == "ECDMasterGrp") {


            if (this.bisId < 38) {
              this.location = res.responseData[0]
              this.selectedProvince = this.location.provinceId
              let proviceId: string = this.location.provinceId + ""
              let code: string = this.location.code + ""
              this.districtId = proviceId + code
              this.proviceSelected = true
              this.loadDistricts(this.selectedProvince)
              this.dsId = this.location.code
              console.log("location", this.location)
            } else {
              this.location = res.responseData[0]
              this.selectedProvince = this.location.provinceId
              let proviceId: string = this.location.provinceId + ""
              let code: string = this.location.code + ""
              this.districtId = this.location.districtId
              this.proviceSelected = true
              this.loadDistricts(this.selectedProvince)
              this.dsId= this.location.code
              console.log("location", this.location)

            }

          } else {
            this.selectedProvince = 1
            this.proviceSelected = false
            this.loadDistricts(this.selectedProvince)
          }

        }


      }), error => {
        console.log("ERROR")
      }

    }

  }

  loadDistricts(selectedProvince) {

    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getDistrictList?id=" + selectedProvince).subscribe(res => {
      if (res.responseCode == 1) {

        this.districtList = res.responseData
        if (this.userGroup == "ECDMasterGrp") {
          for (let a of this.districtList) {
            if (a.id == this.districtId) {
              this.selectedDistrict = a
              if (this.selectedDistrict != undefined || this.selectedDistrict != null) {
                this.getDSList(this.selectedDistrict.id)
              }
            }
          }
          this.districtSelected = true
        } else {
          this.selectedDistrict = this.districtList[0]
          this.districtSelected = false
          if (this.selectedDistrict != undefined || this.selectedDistrict != null) {
            this.getDSList(this.selectedDistrict.id)
          }
        }


      }


    }), error => {
      console.log("ERROR")
    }
  }
  getDSList(id) {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getDSList?id=" + id).subscribe(res => {
      if (res.responseCode == 1) {

        this.dsList = res.responseData
        if (this.bisId < 38) {
          this.selectedDS = this.dsList[0]
          if (this.selectedDS != undefined || this.selectedDS != null) {
            this.loadLearningCenters(this.selectedDS.id)
          }
        } else {
         
          for (let ds of this.dsList) {
            if (this.dsId == ds.id) {
              this.dsSelected = true
              this.selectedDS = ds
              if (this.selectedDS != undefined || this.selectedDS != null) {
                this.loadLearningCenters(this.selectedDS.id)
              }
            }
          }
        }


      }


    }), error => {
      console.log("ERROR")
    }
  }

  loadLearningCenters(id) {

    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getLearningCenterforMiniCensus?dsId=" + id + "&districtId=" + this.selectedDistrict.id).subscribe(res => {
      if (res.responseCode == 1) {

        this.learningCenterList = res.responseData
        this.selectedLearningCenter = this.learningCenterList[0]

        if (this.selectedLearningCenter != undefined || this.selectedLearningCenter != null) {
          this.loadAllCensusData(this.selectedLearningCenter)
        }
      }
    })
  }

  loadAllCensusData(selectedLearningCenter) {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getCensusData?ssn=" + selectedLearningCenter.sn).subscribe(res => {
      if (res.responseCode == 1) {

        this.census = res.responseData



      }
    })
  }
  update() {

    this.miniCensus.sn = this.census.SN
    this.miniCensus.a01 = this.census.A01
    this.miniCensus.a02 = this.census.A02
    this.miniCensus.a03 = this.census.A03
    this.miniCensus.a06 = this.census.A06
    this.miniCensus.a12 = this.census.A12
    this.miniCensus.a13 = this.census.A13
    this.miniCensus.a15 = this.census.A15
    this.miniCensus.a16 = this.census.A16
    this.miniCensus.a20a = this.census.A20A
    this.miniCensus.a20b = this.census.A20B
    this.miniCensus.a20c = this.census.A20C
    this.miniCensus.a20d = this.census.A20D
    this.miniCensus.a20e = this.census.A20E
    this.miniCensus.a27 = this.census.A27
    this.miniCensus.a30 = this.census.A30
    this.miniCensus.a32a = this.census.A32A
    this.miniCensus.a32b = this.census.A32B
    this.miniCensus.a33a = this.census.A33A
    this.miniCensus.a33b = this.census.A33B
    this.miniCensus.a34a = this.census.A34A
    this.miniCensus.a34b = this.census.A34B
    this.miniCensus.a35a = this.census.A35A
    this.miniCensus.a35b = this.census.A35B
    this.miniCensus.a35c = this.census.A35C
    this.miniCensus.a35d = this.census.A35D
    this.miniCensus.a35e = this.census.A35E
    this.miniCensus.a35f = this.census.A35F
    this.miniCensus.a35g = this.census.A35G
    this.miniCensus.a35h = this.census.A35H
    this.miniCensus.a36a = this.census.A36A
    this.miniCensus.a36b = this.census.A36B
    this.miniCensus.a36c = this.census.A36C
    this.miniCensus.a36d = this.census.A36D
    this.miniCensus.a36e = this.census.A36E
    this.miniCensus.a36f = this.census.A36F
    this.miniCensus.a36g = this.census.A36G
    this.miniCensus.a37a = this.census.A37A
    this.miniCensus.a37b = this.census.A37B
    this.miniCensus.a37c = this.census.A37C
    this.miniCensus.a37d = this.census.A37D
    this.miniCensus.a37e = this.census.A37E
    this.miniCensus.a37f = this.census.A37F
    this.miniCensus.a37g = this.census.A37G
    this.miniCensus.a37h = this.census.A37H
    this.miniCensus.a40a = this.census.A40A
    this.miniCensus.a40b = this.census.A40B
    this.miniCensus.a40c = this.census.A40C
    this.miniCensus.a40d = this.census.A40D
    this.miniCensus.a40e = this.census.A40E
    this.miniCensus.a40f = this.census.A40F
    this.miniCensus.a40g = this.census.A40G
    this.miniCensus.a41a = this.census.A41A
    this.miniCensus.a41b = this.census.A41B
    this.miniCensus.a41c = this.census.A41C
    this.miniCensus.a43 = this.census.A43
    this.miniCensus.a49a = this.census.A49A
    this.miniCensus.a49b = this.census.A49B
    this.miniCensus.a49c = this.census.A49C
    this.miniCensus.a49d = this.census.A49D
    this.miniCensus.a51a1 = this.census.A51A1
    this.miniCensus.a51a2 = this.census.A51A2
    this.miniCensus.a51a3 = this.census.A51A3
    this.miniCensus.a51a4 = this.census.A51A4
    this.miniCensus.a51a5 = this.census.A51A5
    this.miniCensus.a51a6 = this.census.A51A6
    this.miniCensus.a51b1 = this.census.A51B1
    this.miniCensus.a51b2 = this.census.A51B2
    this.miniCensus.a51b3 = this.census.A51B3
    this.miniCensus.a51b4 = this.census.A51B4
    this.miniCensus.a51b5 = this.census.A51B5
    this.miniCensus.a51b6 = this.census.A51B6
    this.miniCensus.a51c1 = this.census.A51C1
    this.miniCensus.a51c2 = this.census.A51C2
    this.miniCensus.a51c3 = this.census.A51C3
    this.miniCensus.a51c4 = this.census.A51C4
    this.miniCensus.a51c5 = this.census.A51C5
    this.miniCensus.a51c6 = this.census.A51C6
    this.miniCensus.a61a2 = this.census.A61A2
    this.miniCensus.a61a3 = this.census.A61A3
    this.miniCensus.a61a4 = this.census.A61A4
    this.miniCensus.a61a5 = this.census.A61A5
    this.miniCensus.a61a6y = this.census.A61A6Y
    this.miniCensus.a61a6m = this.census.A61A6M
    this.miniCensus.a61a7y = this.census.A61A7Y
    this.miniCensus.a61b2 = this.census.A61B2
    this.miniCensus.a61b3 = this.census.A61B3
    this.miniCensus.a61b4 = this.census.A61B4
    this.miniCensus.a61b5 = this.census.A61B5
    this.miniCensus.a61b6y = this.census.A61B6Y
    this.miniCensus.a61b6m = this.census.A61B6M
    this.miniCensus.a61b7y = this.census.A61B7Y
    this.miniCensus.a61c2 = this.census.A61C2
    this.miniCensus.a61c3 = this.census.A61C3
    this.miniCensus.a61c4 = this.census.A61C4
    this.miniCensus.a61c5 = this.census.A61C5
    this.miniCensus.a61c6y = this.census.A61C6Y
    this.miniCensus.a61c6m = this.census.A61C6M
    this.miniCensus.a61c7y = this.census.A61C7Y
    this.miniCensus.a61d2 = this.census.A61D2
    this.miniCensus.a61d3 = this.census.A61D3
    this.miniCensus.a61d4 = this.census.A61D4
    this.miniCensus.a61d5 = this.census.A61D5
    this.miniCensus.a61d6y = this.census.A61D6Y
    this.miniCensus.a61d6m = this.census.A61D6Y
    this.miniCensus.a61d7y = this.census.A61D7Y
    this.miniCensus.a61e2 = this.census.A61E2
    this.miniCensus.a61e3 = this.census.A61E3
    this.miniCensus.a61e4 = this.census.A61E4
    this.miniCensus.a61e5 = this.census.A61E5
    this.miniCensus.a61e6y = this.census.A61E6Y
    this.miniCensus.a61e6m = this.census.A61E6M
    this.miniCensus.a61e7y = this.census.A61E7Y
    this.miniCensus.a61f2 = this.census.A61F2
    this.miniCensus.a61f3 = this.census.A61F3
    this.miniCensus.a61f4 = this.census.A61F4
    this.miniCensus.a61f5 = this.census.A61F5
    this.miniCensus.a61f6y = this.census.A61F6Y
    this.miniCensus.a61f6m = this.census.A61F6M
    this.miniCensus.a61f7y = this.census.A61F7Y
    this.miniCensus.a61g2 = this.census.A61G2
    this.miniCensus.a61g3 = this.census.A61G3
    this.miniCensus.a61g4 = this.census.A61G4
    this.miniCensus.a61g5 = this.census.A61G5
    this.miniCensus.a61g6y = this.census.A61G6Y
    this.miniCensus.a61g6m = this.census.A61G6M
    this.miniCensus.a61g7y = this.census.A61G7Y
    this.miniCensus.a61h2 = this.census.A61H2
    this.miniCensus.a61h3 = this.census.A61H3
    this.miniCensus.a61h4 = this.census.A61H4
    this.miniCensus.a61h5 = this.census.A61H5
    this.miniCensus.a61h6y = this.census.A61H6Y
    this.miniCensus.a61h6m = this.census.A61H6M
    this.miniCensus.a61h7y = this.census.A61H7Y
    this.miniCensus.a61i2 = this.census.A61I2
    this.miniCensus.a61i3 = this.census.A61I3
    this.miniCensus.a61i4 = this.census.A61I4
    this.miniCensus.a61i5 = this.census.A61I5
    this.miniCensus.a61i6y = this.census.A61I6Y
    this.miniCensus.a61i6m = this.census.A61I6M
    this.miniCensus.a61i7y = this.census.A61I7Y
    this.miniCensus.a61j2 = this.census.A61J2
    this.miniCensus.a61j3 = this.census.A61J3
    this.miniCensus.a61j4 = this.census.A61J4
    this.miniCensus.a61j5 = this.census.A61J5
    this.miniCensus.a61j6y = this.census.A61J6Y
    this.miniCensus.a61j6m = this.census.A61J6M
    this.miniCensus.a61j7y = this.census.A61J7Y
    this.miniCensus.a61k2 = this.census.A61K2
    this.miniCensus.a61k3 = this.census.A61K3
    this.miniCensus.a61k4 = this.census.A61K4
    this.miniCensus.a61k5 = this.census.A61K5
    this.miniCensus.a61k6y = this.census.A61K6Y
    this.miniCensus.a61k6m = this.census.A61K6M
    this.miniCensus.a61k7y = this.census.A61K7Y
    this.miniCensus.a61l2 = this.census.A61L2
    this.miniCensus.a61l3 = this.census.A61L3
    this.miniCensus.a61l4 = this.census.A61L4
    this.miniCensus.a61l5 = this.census.A61L5
    this.miniCensus.a61l6y = this.census.A61L6Y
    this.miniCensus.a61l6m = this.census.A61L6M
    this.miniCensus.a61l7y = this.census.A61L7Y
    this.miniCensus.a61m2 = this.census.A61M2
    this.miniCensus.a61m3 = this.census.A61M3
    this.miniCensus.a61m4 = this.census.A61M4
    this.miniCensus.a61m5 = this.census.A61M5
    this.miniCensus.a61m6y = this.census.A61M6Y
    this.miniCensus.a61m6m = this.census.A61M6M
    this.miniCensus.a61m7y = this.census.A61M7Y
    this.miniCensus.a61n2 = this.census.A61N2
    this.miniCensus.a61n3 = this.census.A61N3
    this.miniCensus.a61n4 = this.census.A61N4
    this.miniCensus.a61n5 = this.census.A61N5
    this.miniCensus.a61n6y = this.census.A61N6Y
    this.miniCensus.a61n6m = this.census.A61N6M
    this.miniCensus.a61n7y = this.census.A61N7Y
    this.miniCensus.a61o2 = this.census.A61O2
    this.miniCensus.a61o3 = this.census.A61O3
    this.miniCensus.a61o4 = this.census.A61O4
    this.miniCensus.a61o5 = this.census.A61O5
    this.miniCensus.a61o6y = this.census.A61O6Y
    this.miniCensus.a61o6m = this.census.A61O6M
    this.miniCensus.a61o7y = this.census.A61O7Y
    this.miniCensus.a61p2 = this.census.A61P2
    this.miniCensus.a61p3 = this.census.A61P3
    this.miniCensus.a61p4 = this.census.A61P4
    this.miniCensus.a61p5 = this.census.A61P5
    this.miniCensus.a61p6y = this.census.A61P6Y
    this.miniCensus.a61p6m = this.census.A61P6M
    this.miniCensus.a61p7y = this.census.A61P7Y
    this.miniCensus.a61q2 = this.census.A61Q2
    this.miniCensus.a61q3 = this.census.A61Q3
    this.miniCensus.a61q4 = this.census.A61Q4
    this.miniCensus.a61q5 = this.census.A61Q5
    this.miniCensus.a61q6y = this.census.A61Q6Y
    this.miniCensus.a61q6m = this.census.A61Q6M
    this.miniCensus.a61q7y = this.census.A61Q7Y
    this.miniCensus.a61r2 = this.census.A61R2
    this.miniCensus.a61r3 = this.census.A61R3
    this.miniCensus.a61r4 = this.census.A61R4
    this.miniCensus.a61r5 = this.census.A61R5
    this.miniCensus.a61r6y = this.census.A61R6Y
    this.miniCensus.a61r6m = this.census.A61R6M
    this.miniCensus.a61r7y = this.census.A61R7Y
    this.miniCensus.a61s2 = this.census.A61S2
    this.miniCensus.a61s3 = this.census.A61S3
    this.miniCensus.a61s4 = this.census.A61S4
    this.miniCensus.a61s5 = this.census.A61S5
    this.miniCensus.a61s6y = this.census.A61S6Y
    this.miniCensus.a61s6m = this.census.A61S6M
    this.miniCensus.a61s7y = this.census.A61S7Y
    this.miniCensus.a61t2 = this.census.A61T2
    this.miniCensus.a61t3 = this.census.A61T3
    this.miniCensus.a61t4 = this.census.A61T4
    this.miniCensus.a61t5 = this.census.A61T5
    this.miniCensus.a61t6y = this.census.A61T6Y
    this.miniCensus.a61t6m = this.census.A61T6M
    this.miniCensus.a61t7y = this.census.A61T7Y
    this.miniCensus.a61u2 = this.census.A61U2
    this.miniCensus.a61u3 = this.census.A61U3
    this.miniCensus.a61u4 = this.census.A61U4
    this.miniCensus.a61u5 = this.census.A61U5
    this.miniCensus.a61u6y = this.census.A61U6Y
    this.miniCensus.a61u6m = this.census.A61U6M
    this.miniCensus.a61u7y = this.census.A61U7Y
    this.miniCensus.a61v2 = this.census.A61V2
    this.miniCensus.a61v3 = this.census.A61V3
    this.miniCensus.a61v4 = this.census.A61V4
    this.miniCensus.a61v5 = this.census.A61V5
    this.miniCensus.a61v6y = this.census.A61V6Y
    this.miniCensus.a61v6m = this.census.A61V6M
    this.miniCensus.a61v7y = this.census.A61V7Y
    this.miniCensus.a61w2 = this.census.A61W2
    this.miniCensus.a61w3 = this.census.A61W3
    this.miniCensus.a61w4 = this.census.A61W4
    this.miniCensus.a61w5 = this.census.A61W5
    this.miniCensus.a61w6m = this.census.A61W6M
    this.miniCensus.a61w6y = this.census.A61W6Y
    this.miniCensus.a61w7y = this.census.A61W7Y
    this.miniCensus.a61x2 = this.census.A61X2
    this.miniCensus.a61x3 = this.census.A61X3
    this.miniCensus.a61x4 = this.census.A61X4
    this.miniCensus.a61x5 = this.census.A61X5
    this.miniCensus.a61x6y = this.census.A61X6Y
    this.miniCensus.a61x6m = this.census.A61X6M
    this.miniCensus.a61x7y = this.census.A61X7Y
    this.miniCensus.a61y2 = this.census.A61Y2
    this.miniCensus.a61y3 = this.census.A61Y3
    this.miniCensus.a61y4 = this.census.A61Y4
    this.miniCensus.a61y5 = this.census.A61Y5
    this.miniCensus.a61y6y = this.census.A61Y6Y
    this.miniCensus.a61y6m = this.census.A61Y6M
    this.miniCensus.a61y7y = this.census.A61Y7Y
    this.miniCensus.a61z2 = this.census.A61Z2
    this.miniCensus.a61z3 = this.census.A61Z3
    this.miniCensus.a61z4 = this.census.A61Z4
    this.miniCensus.a61z5 = this.census.A61Z5
    this.miniCensus.a61z6y = this.census.A61Z6Y
    this.miniCensus.a61z6m = this.census.A61Z6M
    this.miniCensus.a61z7y = this.census.A61Z7Y
    this.miniCensus.a61aa2 = this.census.A61AA2
    this.miniCensus.a61aa3 = this.census.A61AA3
    this.miniCensus.a61aa4 = this.census.A61AA4
    this.miniCensus.a61aa5 = this.census.A61AA5
    this.miniCensus.a61aa6y = this.census.A61AA6Y
    this.miniCensus.a61aa6m = this.census.A61AA6M
    this.miniCensus.a61aa7y = this.census.A61AA7Y
    this.miniCensus.a61ab2 = this.census.A61AB2
    this.miniCensus.a61ab3 = this.census.A61AB3
    this.miniCensus.a61ab4 = this.census.A61AB4
    this.miniCensus.a61ab5 = this.census.A61AB5
    this.miniCensus.a61ab6y = this.census.A61AB6Y
    this.miniCensus.a61ab6m = this.census.A61AB6M
    this.miniCensus.a61ab7y = this.census.A61AB7Y
    this.miniCensus.a61ac2 = this.census.A61AC2
    this.miniCensus.a61ac3 = this.census.A61AC3
    this.miniCensus.a61ac4 = this.census.A61AC4
    this.miniCensus.a61ac5 = this.census.A61AC5
    this.miniCensus.a61ac6y = this.census.A61AC6Y
    this.miniCensus.a61ac6m = this.census.A61AC6M
    this.miniCensus.a61ac7y = this.census.A61AC7Y
    this.miniCensus.a61ad2 = this.census.A61AD2
    this.miniCensus.a61ad3 = this.census.A61AD3
    this.miniCensus.a61ad4 = this.census.A61AD4
    this.miniCensus.a61ad5 = this.census.A61AD5
    this.miniCensus.a61ad6y = this.census.A61AD6Y
    this.miniCensus.a61ad6m = this.census.A61AD6M
    this.miniCensus.a61ad7y = this.census.A61AD7Y
    this.miniCensus.a61ae2 = this.census.A61AE2
    this.miniCensus.a61ae3 = this.census.A61AE3
    this.miniCensus.a61ae4 = this.census.A61AE4
    this.miniCensus.a61ae5 = this.census.A61AE5
    this.miniCensus.a61ae6y = this.census.A61AE6Y
    this.miniCensus.a61ae6m = this.census.A61AE6M
    this.miniCensus.a61ae7y = this.census.A61AE7Y
    this.miniCensus.a61af2 = this.census.A61AF2
    this.miniCensus.a61af3 = this.census.A61AF3
    this.miniCensus.a61af4 = this.census.A61AF4
    this.miniCensus.a61af5 = this.census.A61AF5
    this.miniCensus.a61af6y = this.census.A61AF6Y
    this.miniCensus.a61af6m = this.census.A61AF6M
    this.miniCensus.a61af7y = this.census.A61AF7Y
    this.miniCensus.a61ag2 = this.census.A61AG2
    this.miniCensus.a61ag3 = this.census.A61AG3
    this.miniCensus.a61ag4 = this.census.A61AG4
    this.miniCensus.a61ag5 = this.census.A61AG5
    this.miniCensus.a61ag6y = this.census.A61AG6Y
    this.miniCensus.a61ag6m = this.census.A61AG6M
    this.miniCensus.a61ag7y = this.census.A61AG7Y
    this.miniCensus.a61ah2 = this.census.A61AH2
    this.miniCensus.a61ah3 = this.census.A61AH3
    this.miniCensus.a61ah4 = this.census.A61AH4
    this.miniCensus.a61ah5 = this.census.A61AH5
    this.miniCensus.a61ah6y = this.census.A61AH6Y
    this.miniCensus.a61ah6m = this.census.A61AH6M
    this.miniCensus.a61ah7y = this.census.A61AH7Y
    this.miniCensus.a61ai2 = this.census.A61AI2
    this.miniCensus.a61ai3 = this.census.A61AI3
    this.miniCensus.a61ai4 = this.census.A61AI4
    this.miniCensus.a61ai5 = this.census.A61AI5
    this.miniCensus.a61ai6y = this.census.A61AI6Y
    this.miniCensus.a61ai6m = this.census.A61AI6M
    this.miniCensus.a61ai7y = this.census.A61AI7Y
    this.miniCensus.a61aj2 = this.census.A61AJ2
    this.miniCensus.a61aj3 = this.census.A61AJ3
    this.miniCensus.a61aj4 = this.census.A61AJ4
    this.miniCensus.a61aj5 = this.census.A61AJ5
    this.miniCensus.a61aj6y = this.census.A61AJ6Y
    this.miniCensus.a61aj6m = this.census.A61AJ6M
    this.miniCensus.a61aj7y = this.census.A61AJ7Y
    this.miniCensus.a61ak2 = this.census.A61AK2
    this.miniCensus.a61ak3 = this.census.A61AK3
    this.miniCensus.a61ak4 = this.census.A61AK4
    this.miniCensus.a61ak5 = this.census.A61AK5
    this.miniCensus.a61ak6y = this.census.A61AK6Y
    this.miniCensus.a61ak6m = this.census.A61AK6M
    this.miniCensus.a61ak7y = this.census.A61AK7Y
    this.miniCensus.a61al2 = this.census.A61AL2
    this.miniCensus.a61al3 = this.census.A61AL3
    this.miniCensus.a61al4 = this.census.A61AL4
    this.miniCensus.a61al5 = this.census.A61AL5
    this.miniCensus.a61al6y = this.census.A61AL6Y
    this.miniCensus.a61al6m = this.census.A61AL6M
    this.miniCensus.a61al7y = this.census.A61AL7Y
    this.miniCensus.a61am2 = this.census.A61AM2
    this.miniCensus.a61am3 = this.census.A61AM3
    this.miniCensus.a61am4 = this.census.A61AM4
    this.miniCensus.a61am5 = this.census.A61AM5
    this.miniCensus.a61am6y = this.census.A61AM6M
    this.miniCensus.a61am6m = this.census.A61AM6M
    this.miniCensus.a61am7y = this.census.A61AM7Y
    this.miniCensus.a61an2 = this.census.A61AN2
    this.miniCensus.a61an3 = this.census.A61AN3
    this.miniCensus.a61an4 = this.census.A61AN4
    this.miniCensus.a61an5 = this.census.A61AN5
    this.miniCensus.a61an6y = this.census.A61AN6Y
    this.miniCensus.a61an6m = this.census.A61AN6M
    this.miniCensus.a61an7y = this.census.A61AN7Y
    this.miniCensus.a61ao2 = this.census.A61AO2
    this.miniCensus.a61ao3 = this.census.A61AO3
    this.miniCensus.a61ao4 = this.census.A61AO4
    this.miniCensus.a61ao5 = this.census.A61AO5
    this.miniCensus.a61ao6y = this.census.A61AO6Y
    this.miniCensus.a61ao6m = this.census.A61AO6M
    this.miniCensus.a61ao7y = this.census.A61AP7Y
    this.miniCensus.a61ap2 = this.census.A61AP2
    this.miniCensus.a61ap3 = this.census.A61AP3
    this.miniCensus.a61ap4 = this.census.A61AP4
    this.miniCensus.a61ap5 = this.census.A61AP5
    this.miniCensus.a61ap6y = this.census.A61AP6Y
    this.miniCensus.a61ap6m = this.census.A61AP6M
    this.miniCensus.a61ap7y = this.census.A61AP7Y
    this.miniCensus.a80 = this.census.A80
    this.miniCensus.address = this.census.ADDRESS
    this.miniCensus.tp = this.census.TP
    this.miniCensus.gnName = this.census.GN_division

    this.miniCensus.user_modified = this.userData[0].userId

    console.log("All mini", this.miniCensus)
    console.log("SN", this.miniCensus.sn, this.census.sn)

    this.commonService.processPost(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/updateMiniCensus", this.miniCensus).subscribe(res => {
      if (res.responseCode == 1) {

        console.log("Success")



      }
    })
  }

  history(e) {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getMinicensusAudit?sn=" + this.census.SN).subscribe(res => {
      if (res.responseCode == 1) {

        this.miniCensusHistory = res.responseData



        this.historyView.show()



      }
    })

  }
}
