import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MiniCensusDataManagementComponent } from './mini-census-data-management.component';


describe('CensusDataManagementComponent', () => {
  let component: MiniCensusDataManagementComponent;
  let fixture: ComponentFixture<MiniCensusDataManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiniCensusDataManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MiniCensusDataManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
