import { Component, ElementRef, OnInit, ViewChild, Input, SimpleChanges } from "@angular/core";

import "dhtmlx-gantt";
import { } from "@types/dhtmlxgantt";
import { TaskService } from "./task.service";
import { LinkService } from "./link.service";

export class Activity {
    activityId
}

@Component({
    selector: "gantt",
    styles: [
        `
        :host{
            display: block;
            height: 100%;
            position: relative;
            width: 100%;
        }
        
    `],
    providers: [TaskService, LinkService],
    template: "<div #gantt_here style='width: 100%; height: 100%;'></div>",
})
export class GanttChart implements OnInit {
    @ViewChild("gantt_here") ganttContainer: ElementRef;
    @Input('activity') activity: Activity;

    constructor(private taskService: TaskService, private linkService: LinkService) { }
    ngOnInit() {


        // gantt.config.xml_date = "%Y-%m-%d %H:%i";

        //         gantt.init(this.ganttContainer.nativeElement);
        //         gantt.config.columns = [
        //             {name:"text",       label:"Task name",  width:"*", tree:true },
        //             {name:"start_date", label:"Start time", align: "center" },
        //             {name:"duration",   label:"Duration",   align: "center" }

        //         ];
        //         Promise.all([this.taskService.get(10), this.linkService.get()])
        //             .then(([data, links]) => {
        //                 gantt.parse({data, links});
        //             });
    }


    ngOnChanges(changes: SimpleChanges) {


        console.log("ACtivity Id", this.activity.activityId)
        gantt.config.xml_date = "%Y-%m-%d %H:%i";

        gantt.init(this.ganttContainer.nativeElement);
        gantt.config.columns = [
            { name: "text", label: "Task name", width: "*", tree: true },
            { name: "start_date", label: "Start Date", align: "center" },
            { name: "duration", label: "Duration", align: "center" }

        ];
    
      
        // this.taskService.getDetails(this.activity.activityId)
        // Promise.all([this.taskService.get(this.activity.activityId), this.linkService.get()])
        //     .then(([data, links]) => {
        //         gantt.parse({ data, links });
        //     });


        var data = {
            data: [
                {id: 1, text: 'Task #1', start_date: '15-04-2017', duration: 3, progress: 0.6},
                {id: 2, text: 'Task #2', start_date: '18-04-2017', duration: 3, progress: 0.4}
            ]}
         var links ={ links: [
            {id: 1, source: 1, target: 2, type: '0'}
            ]
         };
        gantt.parse({ data, links }); 
    }

}