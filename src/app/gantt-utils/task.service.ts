import { Injectable } from "@angular/core";
import { Task } from "./task";
import { CommonWebService } from "../common-web.service";
import { ActivatedRoute } from "@angular/router";
import { Activity } from "../model/Activity";
import * as moment from 'moment';
import { API_URL } from "../app_params";



@Injectable()
export class TaskService {
    public act = new Activity()
    public taskList = new Array<Task>()


    ngOnInit() {
        this.activateRoute.params.subscribe(params => {
            let activity = params['activity']

            console.log("Activty ID BUUU", activity)


        })
       
    }
    constructor(public commonService: CommonWebService, public activateRoute: ActivatedRoute) { }
    // loadActivityDetails(id) {

    //     this.commonService.processGet("http://192.0.0.65:8080/ECD-ActivityMaintenance-1.0/service/Activities/getActivityById?activityId=" + id).subscribe(res => {

    //         if (res.responseCode == 1) {
    //             let act = res.responseData


    //         }


    //     }), error => {
    //         console.log("ERROR")
    //     }
    // }

    getDetails(id) {

        this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getMaintainActivityById?activityId=" + id).subscribe(res => {
            // this.commonService.processGet(ActivitiyDe).subscribe(res => {

            if (res.responseCode == 1) {
                this.act = res.responseData
                console.log("ActivitydETAILS>>>28",this.act)
                for (let a of this.act.milestoneList) {
                  
                    let task = gantt.addTask({
                        
                        id: a.milestoneId,
                        text: a.milestoneName,
                        start_date: "2017-01-06",
                        // duration :this.getDuration(a.actualStartDate,a.actualEndDate),
                        progress:0.8,
                        // progress: this.getProgress(a.actualStartDate)
                    },a.milestoneName
                    )
                }
                console.log("Activity", this.taskList)
                this.getProgress("01-01-2018")
            }
        })
    }
    get(id): Promise<any[]> {

        let t: Task[] = this.taskList
        // Array.toString
        console.log("ID THIS", t.toString())

        return Promise.resolve(

            // t
            [{ id: 1, text: "Task #1", start_date: "2017-04-15 00:00", duration: 3, progress: 0.6 },
            { id: 2, text: "Task #2", start_date: "2017-04-18 00:00", duration: 3, progress: 0.4 }]
        );
    }

    getDuration(startDate, endDate): any {
        var m1 = moment(startDate);
        var m2 = moment(endDate);
        console.log("Start Date", startDate, endDate)
        // Format duration according your needs
        var ms = moment(endDate, "YYYY-MM-DD HH:mm:ss").diff(moment(startDate, "YYYY-MM-DD HH:mm:ss"));
        var d = moment.duration(ms).asDays();
        console.log("Duration", d)
        // var s = d.format("hh:mm:ss");
    }
    getProgress(actualS): any {
        console.log("progress",actualS)
        let progress = (moment(actualS, "DD-MM-YYYY HH:mm:ss")).fromNow();
        console.log("progress",progress[0])
    }
}