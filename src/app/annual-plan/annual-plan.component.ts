import { Component, OnInit } from '@angular/core';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-annual-plan',
  templateUrl: './annual-plan.component.html',
  styleUrls: ['./annual-plan.component.css']
})
export class AnnualPlanComponent implements OnInit {
  selectedActType: string;

  years: SelectItem[];
  
  
    selectedYear
    constructor() {
  
    }
  
    ngOnInit() {
      this.selectedYear = "2017"
      this.selectedActType="1"
      // this.loadDates(2010, 2020);
    }
    showContent(month){
      
   }
    loadDates(startYear, endYear) {
      this.years = []
      for (let i = startYear; i < endYear + 1; i++) {
        this.years.push({ label: i, value: { id: i, name:i, code: 'i_' } })
      }
      this.selectedYear = this.years[0].value
      console.log(this.selectedYear)
    }
  }