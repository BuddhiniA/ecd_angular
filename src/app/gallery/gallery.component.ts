import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { Photo } from '../model/Photo';
import { API_URL } from '../app_params';

export class PhotoDetail{

  activityId
  milestoneId
}

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css'],
  providers: [CommonWebService]
})
export class GalleryComponent implements OnInit {

  @Input('photoDetail') photoDetail: PhotoDetail;
 
  public images = new Array<any>();
  public photoList = new Array<Photo>()
  constructor(public commonService: CommonWebService) { }

  ngOnInit() {
    this.images.push({ source: './assets/icon/picture.png', alt: 'Description for Image 1', title: 'Title 1' });
    this.images.push({ source: API_URL+'ECD_images/activity/images2.jpeg', alt: 'Description for Image 1', title: 'Title 1' });
    this.commonService.processGet(API_URL+"ECD-ActivityMaintenance-1.0/service/Activities/getMilestoneById?activityId=" + this.photoDetail.activityId + "&milestoneId=" + this.photoDetail.milestoneId).subscribe(res => {
      console.log("PHOTO LIST EL", res.responseData)

      this.photoList = res.responseData
      for (let image of this.photoList) {

        // this.images.push({ source: "http://192.0.0.65:8080"+image.path, alt: 'Description for Image 1', title: 'Title 1' });
        console.log("images list eka gal", this.images)
      }

    }), error => {
      console.log("ERROR")
    }
  }
  ngOnChanges(changes: SimpleChanges) {
    
       
    // this.commonService.processGet("http://192.0.0.65:8080/ECD-ActivityMaintenance-1.0/service/Activities/getMilestoneById?activityId=" + this.photoDetail.activityId + "&milestoneId=" + this.photoDetail.milestoneId).subscribe(res => {
    //   console.log("PHOTO LIST EL", res.responseData)

    //   this.photoList = res.responseData
    //   for (let image of this.photoList) {

    //     this.images.push({ source: "http://192.0.0.65:8080"+image.path, alt: 'Description for Image 1', title: 'Title 1' });
    //     console.log("images list eka", this.images)
    //   }

    // }), error => {
    //   console.log("ERROR")
    // }
    
    
      }
}
