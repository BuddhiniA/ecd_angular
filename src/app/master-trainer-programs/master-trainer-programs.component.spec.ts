import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterTrainerProgramsComponent } from './master-trainer-programs.component';

describe('MasterTrainerProgramsComponent', () => {
  let component: MasterTrainerProgramsComponent;
  let fixture: ComponentFixture<MasterTrainerProgramsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MasterTrainerProgramsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterTrainerProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
