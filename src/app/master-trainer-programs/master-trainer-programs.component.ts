import { Component, OnInit, ViewChild } from '@angular/core';
import { MasterModel } from "../model/Master";
import { RegParticipantModel } from "../model/RegParticipant";
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { Resource } from '../model/Resource';
import { DatePipe } from '@angular/common';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { TreeNode } from 'primeng/primeng';
import { ModalDirective } from 'ngx-bootstrap';
import { ProgramData } from '../model/ProgramData';
import { ResourcePerson } from '../model/ResourcePerson';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-master-trainer-programs',
  templateUrl: './master-trainer-programs.component.html',
  styleUrls: ['./master-trainer-programs.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class MasterTrainerProgramsComponent implements OnInit {
  type: number;
  userId: any;
  error: boolean;
  success: boolean;
  remark: FormControl;
  name: FormControl;
  programForm: any;
  public programData = new ProgramData()
  programId: any;
  userData: any;
  location: any;
  selectedNode = new Node();
  files: Node[];
  businessList: any[];
  loading: boolean;
  loadingTree: boolean;
  endtDate: any;
  startDate: any;

  selectedResourceList = new Array<ResourcePerson>();
  public selectedResourcePersonList = new Array<ResourcePerson>();
  resourceList: any;
  selectedResource: any;
  rType: number;

  public masterModel = Array<MasterModel>();
  public masterDetail = new MasterModel();
  public regModel = Array<RegParticipantModel>();
  public regDetail = new RegParticipantModel;
  public d1;
  public d2;
  public selectedOtherResource = new ResourcePerson();

  @ViewChild("makeMasterSetting") public makeMasterSetting;
  @ViewChild("updateMasterSetting") public updateMasterSetting;
  @ViewChild("addParticipantView") public addParticipantView: ModalDirective;

  constructor(public commonService: CommonWebService, private datePipe: DatePipe) { }

  ngOnInit() {
 
   
    
   
    this.validation()
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.userId = sessionStorage.getItem("bisId");
    this.getAllLocations()
    this.loadToGrid();
  }

  validation() {

    this.programForm = new FormGroup({
      programName: new FormControl(['', Validators.required]),
      description: new FormControl(['', Validators.required]),
      startDate: new FormControl(['', Validators.required]),
      endDate: new FormControl('', [Validators.required]),
      location: new FormControl(''),
      medium: new FormControl(''),
      budget: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      status: new FormControl(''),
      type: new FormControl(),
      resource: new FormControl(),

    })

    this.name = new FormControl('', Validators.required),
      this.remark = new FormControl()

  }
  loadToGrid() {
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllMasterPrograms";
    this.commonService.processGet(url).subscribe(res => {
      if (res.responseCode == 1) {
        this.masterModel = res.responseData;
      }

    }, error => {
      console.log("error ", error);
    })
  }

  openCreateMaster(type,id) {
    this.type=type
    this.programData = new ProgramData()
    
    if(this.type==1&&id==null){

      this.getAllResources()
     
      this.rType = 1;
      this.programData.program.status = 1
      this.programData.program.startDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
      this.programData.program.endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
      this.makeMasterSetting.show();
    }else if(this.type==2&&id!=null){
      this.getProgrambyId(id)
      
    }
    
  }

  getProgrambyId(id){
    this.programData = new ProgramData()
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getMasterProgramById?programId="+id;
    
    
        this.commonService.processGet(url).subscribe(res => {
          if(res.responseCode==1){
            this.programData=res.responseData[0]
            this.selectedResourcePersonList = this.programData.resourcePool
            this.makeMasterSetting.show();
          }
            
        })
    
  }
  getMasterId(master) {
    console.log("master", master);

    this.masterDetail = master;
    let date1: String = master.startDate;
    let date2: String = master.endDate;
    let dt1 = date1.split(" ");
    let dt2 = date2.split(" ");
    this.d1 = dt1[0];
    this.d2 = dt2[0];
    console.log("d1", this.d1);
    this.updateMasterSetting.show();
  }



  createMasterProgram() {


    this.programData.userInserted = this.userData[0].userId

    console.log("tobesaved object", this.masterDetail)
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveMasterProgram";


    this.commonService.processPost(url, this.programData).subscribe(res => {


     

      if (res.responseCode == 1) {
        this.success = true
        this.loadToGrid()
        setTimeout(() => {
          this.success = false
          this.makeMasterSetting.hide()
        }, 2000)
      } else {

        this.error = true
        setTimeout(() => {
        this.error = false

        }, 2000)
      }

    }, error => {
      this.error = true
        setTimeout(() => {
        this.error = false
        }, 2000)
    })

  }

  saveMasterChanges(evt) {
    console.log("saveMasterChanges");
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/EditMasterProgram";


    this.commonService.processPost(url, this.masterDetail).subscribe(res => {


      let response = res.responseCode
      console.log("response ", response);
      if (response == 1) {
        console.log("success");
        // this.success=true;
        console.log("Success")
        // this.successMessage = "User created successfully!"

        // this.isLoading = false;

        // this.resetFields(this.newUserModel);

        // this.successMessage = "User has been created successfully. Password will be sent to the email address";
        // this.success.show();
      } else {
        console.log("NOT success");
      }

      // this.createUserLoader = false;

      this.loadToGrid();
    }, error => {

      console.log("Error");

    })
  }

  addResource() {

    this.selectedResourceList.push(this.selectedResource)
  }
  getAllResources() {

    this.selectedResource = new Resource()
    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources").subscribe(res => {

      if (res.responseCode == 1) {
        this.resourceList = res.responseData
        console.log("res", this.resourceList)
        this.selectedResource = this.resourceList[0]
      }
    }), error => {
      console.log("ERROR")
    }
  }
  getAllLocations() {
    let type
    if (this.userId == 1) {
      type = "root"
    } else if (this.userId == 2 || this.userId == 3) {
      type = "node"
    }

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == type && this.businessList[i].id == parseInt(this.userId)) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }


  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node1.children.push(node)
      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node1.children.push(node)
      }




      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }


    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;
    if (event.node.type == "province") {

    }


    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }


  nodeSelected(event) {
    this.programData.program.locationId = event.node.id
    this.programData.program.locationName = event.node.label
    this.location = event.node.label
    this.collapseAll()
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  openAddParticipant(a) {
    this.programId = a
    console.log("A", this.programId)

    this.addParticipantView.show()

  }

  addResourcePersons() {


    if (this.rType == 1 && this.selectedResource != null) {
      let person = new ResourcePerson()
      person.personId = this.selectedResource.id
      person.personName = this.selectedResource.resourceName
      person.coordinator = 0
      this.programData.resourcePool.push(person)
      this.selectedResourcePersonList.push(person)
    }
    else if (this.rType == 2 && this.selectedOtherResource != null) {

      let person = new ResourcePerson()

      person.personName = this.selectedOtherResource.personName
      person.remarks = this.selectedOtherResource.remarks
      person.coordinator = 0
      this.programData.guestList.push(person)
      this.selectedResourcePersonList.push(person)

      this.selectedResource.personName = ""
      this.selectedResource.remarks = ""

    }
  }

  removeRows(person) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool.splice(index2, 1)
    }
    if (index3 > -1) {
      this.programData.guestList.splice(index3, 1)
    }



  }

  removeRowswhenEdit(person: ResourcePerson) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool[index2].status = 2
    }
    if (index3 > -1) {
      this.programData.guestList[index3].status = 2
    }



  }

  checked(e, person) {

    console.log("Per", person)
    console.log("sdsdsd", this.programData.resourcePool)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    console.log("index", index2)
    console.log("in", index3)
    if (index2 > -1) {
      this.programData.resourcePool[index2].coordinator = 1
    }
    if (index3 > -1) {

      this.programData.guestList[index3].coordinator = 1
    }
  }

  changeStatus(program){
    this.commonService.processGet(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/updateProgramStatus?programId="+program.programId+"&type=2&status="+program.status).subscribe(res => {
      
            if (res.responseCode == 1) {
              console.log("status")
            }
          }), error => {
            console.log("ERROR")
          }
  }
}
