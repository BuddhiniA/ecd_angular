import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CriteriaResultManagementComponent } from './criteria-result-management.component';

describe('CriteriaResultManagementComponent', () => {
  let component: CriteriaResultManagementComponent;
  let fixture: ComponentFixture<CriteriaResultManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CriteriaResultManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CriteriaResultManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
