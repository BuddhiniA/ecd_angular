import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { ActivatedRoute, Route, Router } from '@angular/router';

import { Http, RequestOptions, Headers } from '@angular/http';
import { CommonWebService } from "../common-web.service";
import { Result } from "../model/Result";
import { ResultDetail } from "../model/ResultDetail";
import { DataValue } from "../model/DataValue";
import { CensusDataField } from "../model/CensusDataField";
import { API_URL } from '../app_params';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Status } from '../model/Status';
import { ProgressSpinnerModule } from 'primeng/progressspinner';



@Component({
  selector: 'app-criteria-result-management',
  templateUrl: './criteria-result-management.component.html',
  styleUrls: ['./criteria-result-management.component.css'],
  providers: [CommonWebService, Result, ResultDetail, DataValue]
})


export class CriteriaResultManagementComponent implements OnInit {
  locationforDs: string;

  public loadingMask: boolean;
  loadingResult: boolean;
  nodeBelongs: any;
  loactionForDistrict: any;
  bisId: any;
  userGroup: string;
  userId: any;
  userData: any;
  @ViewChild("createResult") public createResult: ModalDirective;
  @ViewChild("showCriteriaResult") public showCriteriaResult: ModalDirective;
  @ViewChild("showCensus") public showCensus: ModalDirective;


  public selectedStatus = new Status()
  public statusList1 = new Array<Status>()
  public criteriaId;
  public result = new Result();
  public resultSet = new Array<Result>();
  public recordDetailList = new Array<ResultDetail>();
  public selectedRecords = new Array<ResultDetail>();
  public censusList = new Array<CensusDataField>();
  public dataModel = new DataValue();
  public dataValues: DataValue[] = new Array<DataValue>()
  public resultDetail = new ResultDetail()
  public description;


  public success: boolean
  public error: boolean;
  public warning: boolean

  public dataValues1 = new Array()
  public dataValues2 = new Array()
  public dataValues3 = new Array()
  public dataValues4 = new Array()
  public dataValues5 = new Array()
  public dataValues6 = new Array()
  public dataValues7 = new Array()
  public dataValues8 = new Array()
  public dataValues9 = new Array()
  public dataValues10 = new Array()
  public dataValues11 = new Array()
  public dataValues12 = new Array()
  public dataValues13 = new Array()
  public dataValues14 = new Array()
  public dataValues15 = new Array()
  public dataValues16 = new Array()
  public dataValues17 = new Array()
  public dataValues18 = new Array()
  public dataValues19 = new Array()
  public dataValues20 = new Array()
  public dataValues21 = new Array()
  public dataValues22 = new Array()
  public dataValues23 = new Array()

  public headers: Headers = new Headers;
  public statusList = new Array<any>()

  public criteriaType
  resultForm: FormGroup

  constructor(public route: ActivatedRoute, public commonService: CommonWebService, private http: Http, public router: Router) {
    //   this.statusList = [

    //     {label:'Awarded', value:{id:1}},
    //     {label:'Not Awarded Yet', value:{id:0}}
    // ];
  }

  ngOnInit() {
    this.userGroup = sessionStorage.getItem('userGroup');
    this.getUser()
    this.getStatus()
    // this.route.queryParams.subscribe(params=>{

    //   this.criteriaId=params['criteriaId']
    //   console.log("Param Id",params)
    //   if(this.criteriaId!=null||this.criteriaId!=undefined){
    //     this.loadAllResults();
    //   }
    // })
    this.validation()

    this.route.params.subscribe(params => {
      this.criteriaId = params['criteriaId']
      this.criteriaType = params['criteriaType']
      if (this.criteriaId != null || this.criteriaId != undefined) {
        this.loadAllResults();
      }
      console.log("CID", this.criteriaId)
    })

  }

  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));

    this.userId = this.userData[0].userId

    this.bisId = this.userData[0].extraParams
    if (this.userGroup = "ECDMasterGrp") {
      if (this.bisId != undefined || this.bisId != null) {
        this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getLocation").subscribe(res => {
          if (res.responseCode == 1) {
            this.nodeBelongs = res.responseData
            let proviceId: string = this.nodeBelongs[0].provinceId + ""
            let code: string = this.nodeBelongs[0].code + ""
            let district:string = this.nodeBelongs[0].districtId+""
            if(this.bisId<38){
              this.loactionForDistrict = proviceId + code
            }else{
              this.loactionForDistrict = district+code
            }
            
            
            console.log("NODE BELONGS", this.nodeBelongs)



          }


        }), error => {
          console.log("ERROR")
        }
      }
    }



  }
  getStatus() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getStatus?type=Census&subType=Status").subscribe(res => {
      //  this.commonService.processGet("./assets/criteriaresult.json").subscribe(res=>{
      if (res.responseCode == 1) {
    
        // this.statusList = res.responseData
        for (let a of res.responseData) {
          this.statusList.push({ label: a.message, value: a })
        }
        console.log("aaaaaaaaaaaaaaaaaaaaaaaaaa", this.statusList);
      }
      // this.resultSet = res.responseData;
      // for()




    }), error => {
      console.log("ERROR")
    }
  }

  validation() {

    this.resultForm = new FormGroup({
      description: new FormControl('', Validators.required)
    })
  }


  setOutput(result, evt) {
    console.log("Change", evt)
    var index = this.recordDetailList.indexOf(result)
    let st = parseInt(evt.value.seq)
    console.log("Awar status", st)
    if (st == 1) {
      this.recordDetailList[index].award = this.statusList[1].label
    } else if (st == 0) {
      this.recordDetailList[index].award = this.statusList[0].label
    } else if (st == 2) {
      this.recordDetailList[index].award = this.statusList[2].label
    } else if (st == 3) {
      this.recordDetailList[index].award = this.statusList[3].label
    }


  }

  openCreateCriteria() {
    this.createResult.show();
  }
  showResult(record) {
    this.loadingResult = true;
    this.selectedStatus = this.statusList[0].label
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getCensusResults?recordId=" + record.recordId).subscribe(res => {
      //  this.commonService.processGet("./assets/resultbyid.json").subscribe(res=>{ 
      if (res.responseCode == 1) {
        this.loadingResult = false
        this.recordDetailList = res.responseData;
        console.log("result", this.recordDetailList[0])
        console.log("result_length", this.resultSet.length)
        for (let a of this.recordDetailList) {
          var index = this.recordDetailList.indexOf(a)
          if (a.award == 0) {
            this.recordDetailList[index].award = this.statusList[0].label
          } else if (a.award == 0) {
            this.recordDetailList[index].award = this.statusList[1].label
          } else if (a.award == 2) {
            this.recordDetailList[index].award = this.statusList[2].label
          } else if (a.award == 3) {
            this.recordDetailList[index].award = this.statusList[3].label
          }
        }
      }



    }), error => {
      console.log("ERROR")
    }

    this.showCriteriaResult.show();
  }

  showCensusData(censusNo) {
    this.selectedStatus = this.statusList[0]
    let me = this;
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getCensusData?ssn=" + censusNo.censusNo).subscribe(res => {


      // for(let i=0;i<20;i++){
      //   this.dataValues1[i]=this.dataValues[i]
      //   console.log("datValue",this.dataValues1)
      // }
      //  console.log("log",res.responseData)
      //  for(let i=0;i<res.responseData.length;i++){
      //   Object.keys(res.responseData)
      //  } 
      var result = [];
      Object.keys(res.responseData).forEach(function (name) {
        var data = {
          name: name
        };


        result.push(data);
        let dataModel = new DataValue();
        dataModel.name = data.name



        dataModel.value = res.responseData[name]
        me.dataValues.push(dataModel)
        console.log("hhhh", me.dataValues)

      });
      this.loadValues()
    }), error => {
      console.log("ERROR")
    }

    console.log(this.dataValues.length)
    this.showCensus.show();
  }



  updateStatus() {

    console.log("PayLoad", this.recordDetailList)

    for (let a of this.recordDetailList) {

      let recordToBeSaved = new ResultDetail()
      if (a.award == "No Grants Awarded Yet") {
        recordToBeSaved.award = 0
      } else if (a.award == "Awarded") {
        recordToBeSaved.award = 1
      } else if (a.award == "Not Awarded due to Closure") {
        recordToBeSaved.award = 2
      } else if (a.award == "Pending") {
        recordToBeSaved.award = 3
      }

      recordToBeSaved.censusNo = a.censusNo
      recordToBeSaved.data = a.data
      recordToBeSaved.recordId = a.recordId
      recordToBeSaved.status = a.status





      this.commonService.processPost(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/updateAwardedStatus", recordToBeSaved).subscribe(res => {

        console.log("Data", this.resultDetail)

        // console.log(this.criteriaList.length)
        if (res.responseCode == 1) {
          console.log("Success")
          this.success = true

        }

      }), error => {
        console.log("ERROR")
      }
    }


  }

  loadAllResults() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getResultList?criteriaId=" + this.criteriaId).subscribe(res => {
      //  this.commonService.processGet("./assets/criteriaresult.json").subscribe(res=>{

      this.resultSet = res.responseData;
      // for()




    }), error => {
      console.log("ERROR")
    }
  }
  loadValues() {
    for (let i = 0; i < 20; i++) {
      this.dataValues1[i] = this.dataValues[i]
      console.log("datValue", this.dataValues1)
    }
    for (let i = 20; i < 40; i++) {
      this.dataValues2[i] = this.dataValues[i]
      console.log("datValue2", this.dataValues2)
    }
    for (let i = 40; i < 60; i++) {
      this.dataValues3[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 60; i < 80; i++) {
      this.dataValues4[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 80; i < 100; i++) {
      this.dataValues5[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 100; i < 120; i++) {
      this.dataValues6[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 120; i < 140; i++) {
      this.dataValues7[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 140; i < 160; i++) {
      this.dataValues8[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 160; i < 180; i++) {
      this.dataValues9[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 180; i < 200; i++) {
      this.dataValues10[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 200; i < 220; i++) {
      this.dataValues11[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 220; i < 240; i++) {
      this.dataValues12[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 240; i < 280; i++) {
      this.dataValues13[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 280; i < 300; i++) {
      this.dataValues14[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 300; i < 320; i++) {
      this.dataValues15[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 320; i < 340; i++) {
      this.dataValues16[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }
    for (let i = 340; i < 367; i++) {
      this.dataValues17[i] = this.dataValues[i]
      console.log("datValue", this.dataValues2)
    }

  }

  saveResult() {
    this.headers.set("Content-Type", "application/json")
    this.headers.set("criteriaId", this.criteriaId)
    this.headers.set("criteriaType", "FEE_WAIVER")
    console.log("criteria ", this.criteriaId)
    let options = new RequestOptions({ headers: this.headers });

    this.loadingMask = true;
    this.commonService.processPostWithHeaders(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/generateResults?location=" + this.loactionForDistrict, this.description, options).subscribe(res => {
      if (res.responseCode == 1) {
        console.log("Success")
        this.success = true
        setTimeout(() => {
          this.success = false;
          this.description = ""
          this.loadAllResults()

        }, 2000);

        this.loadingMask = false;

      }




    }), error => {
      console.log("ERROR")
      this.loadingMask = false;
    }
  }

  generateReport(cri) {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/createCensusReport?recordId=" + cri.recordId).subscribe(res => {
      //  this.commonService.processGet("./assets/criteriaresult.json").subscribe(res=>{

      console.log("REPORT", res.responseData)
      if (res.responseData == 1) {
        window.open(API_URL + "CRITERIA_REPORTS/criteriacensusdata.xls")
      }
      // for()




    }), error => {
      console.log("ERROR")
    }
  }

  updateStatusAll(evt) {
    console.log("ALLLLLLL", this.selectedRecords)
  }
}
