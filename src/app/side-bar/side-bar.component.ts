import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { LoggedInUserModel } from "../model/loggedInUserModel";


@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  userId: any;
  userData: any;
  list: any;
  selected: any;

  methodName: any;

  // public user : LoggedInUserModel;


  constructor(private router: Router) {
    // this.list = [
    //    'MajlisDashboard',
    //    'MajlisUser',
    //    'MajlisGroup',
    //    'MajlisCategory',
    //    'MajlisEvent',
    //    'MajlisTemplate',
    //    'MajlisNotification',
    //    'MajlisReport'
    // ];

    // this.methodName = [
    //    'btnClickDash',
    //    'btnClickUser',
    //    'btnGrp',
    //    'btnCat',
    //    'btnEvt',
    //    'btnTmplt',
    //    'btnNotif',
    //    'btnReport'
    // ];

    //     this.list={
    //       "iconName" : 'MajlisDashboard',
    //       "methodName" : 'btnClickDash',
    //       "dashTitle" : 'Dashboard'
    //     };
    // console.log(this.list);

    // let obj = this.list;
    // console.log('###################################',obj.iconName);


    // let userToken = sessionStorage.getItem('id_token');

    // this.user = JSON.parse(atob(userToken));

    // console.log("user model",this.user);

  }

  // getPermission(menuName){

  //   let index = this.user.functions.findIndex(e=>e.menuAction == menuName);

  //   if(index == -1){
  //     return false;
  //   }else{
  //     return true;
  //   }

  // }

  // select(item) {
  //     this.selected = item; 
  // };
  // isActive(item) {
  //     return this.selected === item;
  // };

  ngOnInit() {

 
    this.userId = sessionStorage.getItem("bisId");
  }
  // btnClickDash = function () {
  //   console.log('Dashboard')
  //   // this.router.navigateByUrl('/usermgt');
  //   this.router.navigate(['./dash']);
  // }

  // btnClickUser = function () {
  //   console.log('User Mgt')
  //   // this.router.navigateByUrl('/usermgt');
  //   this.router.navigate(['./usermgt']);
  // }

  // btnGrp = function () {
  //   console.log('Grp Mgt')
  //   this.router.navigate(['./grpmngmnt'])
  // }

  // btnCat = function () {
  //   console.log('Cat Mgt')
  //   this.router.navigate(['./catgrymngmnt'])
  // }

  // btnEvt = function () {
  //   console.log('Evt Mgt')
  //   this.router.navigate(['./evntmngmnt'])
  // }

  // btnTmplt = function () {
  //   console.log('Tmplte Mgt')
  //   this.router.navigate(['./templatemngmnt'])
  // }

  // btnNotif = function () {
  //   console.log('Tmplte Mgt')
  //   this.router.navigate(['./templatemngmnt'])
  // }

  // btnReport = function () {
  //   console.log('Tmplte Mgt')
  //   this.router.navigate(['./templatemngmnt'])
  // }


  validateMenu(menuId) {

    let usr = sessionStorage.getItem("userGroup");

    if (menuId == 'annualPlan') {

      let arr = ['AdminECDGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {

        return true;
      } else {
        
        return false;
      }

    }if (menuId == 'dashboard') {

      let arr = ['AdminECDGrp','ECDPHDTGrp','ECDOfficerGrp','AuthECDGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {

        return true;
      } else {
        
        return false;
      }

    }
     else if (menuId == 'users') {

      let arr = ['AdminECDGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'mat') {

      let arr = ['AdminECDGrp','ECDMasterGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'criteriaMain') {

      let arr = ['AdminECDGrp','ECDMasterGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'business') {

      let arr = ['AdminECDGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'activityCat') {

      let arr = ['AdminECDGrp','ECDPHDTGrp','ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'resource') {

      let arr = ['AdminECDGrp','ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'wave') {

      let arr = ['AdminECDGrp','ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'childAsset') {

      let arr = ['AdminECDGrp','ECDPIOOfficerGrp','ECDMasterGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'criteriaMgt') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'materialStock') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'materialPack') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'sendMat') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'receiveMat') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp','ECDPIOOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'distMat') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'docMgt') {

      let arr = ['AdminECDGrp','ECDOfficerGrp','ECDPHDTGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'defineActi') {

      let arr = ['AdminECDGrp','ECDPHDTGrp','ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'maintActi') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp', 'ECDPHDTGrp','ECDPIOOfficerGrp','ECDMasterGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    }else if (menuId == 'authActi') {
      
            let arr = ['AuthECDGrp'];
      
            let usr = sessionStorage.getItem("userGroup");
      
            if (arr.indexOf(usr) > -1) {
              return true
            } else {
              return false
            }
      
          }
     else if (menuId == 'certMgt') {

      let arr = ['AdminECDGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'longTerm') {

      let arr = ['AdminECDGrp',  'ECDOfficerGrp', 'ECDPHDTGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'staffCap') {

      let arr = ['AdminECDGrp',  'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'primSchTrain') {

      let arr = ['AdminECDGrp',  'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'masterTrain') {

      let arr = ['AdminECDGrp',  'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'shortTermTrain') {

      let arr = ['AdminECDGrp', 'ECDMasterGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'trainInsti') {

      let arr = ['AdminECDGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'parentalAware') {

      let arr = ['AdminECDGrp', 'ECDMasterGrp', 'ECDOfficerGrp', 'ECDPHDTGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'primeSchTeach') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'tranAdminStaff') {

      let arr = ['AdminECDGrp',  'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'cusRepo') {

      let arr = ['AdminECDGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'freeWaveRepo') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'socialAu') {

      let arr = ['AdminECDGrp', 'ECDMasterGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'repo') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp' ];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    } else if (menuId == 'census') {

      let arr = ['AdminECDGrp', 'ECDMasterGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    }else if (menuId == 'refresher') {

      let arr = ['AdminECDGrp', 'ECDOfficerGrp', 'ECDPHDTGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }

    }else if (menuId == 'minicensus') {

      let arr = ['AdminECDGrp', 'ECDMasterGrp', 'ECDOfficerGrp'];

      let usr = sessionStorage.getItem("userGroup");

      if (arr.indexOf(usr) > -1) {
        return true
      } else {
        return false
      }


    

  }

  return true;

  }
}
