import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimaryschoolHeadorientationProgramComponent } from './primaryschool-headorientation-program.component';

describe('PrimaryschoolHeadorientationProgramComponent', () => {
  let component: PrimaryschoolHeadorientationProgramComponent;
  let fixture: ComponentFixture<PrimaryschoolHeadorientationProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimaryschoolHeadorientationProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimaryschoolHeadorientationProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
