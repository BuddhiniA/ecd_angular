import { Component, OnInit, ViewChild } from '@angular/core';
import { PrimarySchoolModel } from '../model/PrimarySchool';

import { ResPoolModel } from '../model/ResPool';
import { PrimaryOrientationModel } from '../model/PrimaryOrientation';
import { ResourcePerson } from '../model/ResourcePerson';
import { Router } from '@angular/router';
import { BusinessStrModel } from '../model/BsStr';
// import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { ProgramData } from '../model/ProgramData';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { Data } from '../model/Data';
import { TreeNode } from 'primeng/primeng';
import { ModalDirective } from 'ngx-bootstrap';

export class PrimaryProgram {
  userInserted: any = " ";
  userModified: any = null;
  dateInserted: any = null;
  dateModified: any = null;
  resourcePool = new Array<ResourcePerson>()
  guestList = new Array<ResourcePerson>()
  program = new PrimarySchoolModel()
}
@Component({
  selector: 'app-primaryschool-headorientation-program',
  templateUrl: './primaryschool-headorientation-program.component.html',
  styleUrls: ['./primaryschool-headorientation-program.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class PrimaryschoolHeadorientationProgramComponent implements OnInit {
  selectedLearningCenter = new Data();
  learningCenterList: any[];
  selectedNode = new Node();
  files: Node[];
  businessList: BusinessLevel[];
  loadingTree: boolean;
  preschool: FormControl;
  remark: FormControl;
  name: FormControl;
  programForm: any;
  userData: any;
  programData = new ProgramData();

  serviceProgress: boolean;
  public imagePath: any;
  public base64Image: string;
  path: any;
  public primaryschoolModel = Array<PrimarySchoolModel>();
  public primaryschoolDetail = new PrimarySchoolModel;

  public programList;
  public selectedprogram = new PrimaryProgram();
  public selectedResourcePersonList = new Array<ResourcePerson>();

  public headOrientationModel = Array<PrimaryOrientationModel>();
  public headOrientationDetail = new PrimaryOrientationModel;
  public PrimaryProgram
  public resModel = Array<ResPoolModel>();
  public resDetail = new Array<ResourcePerson>();

  public selectedResource
  public selectedBisId;
  public selectedOtherResource = new ResourcePerson();
  public selectedOtherResourcesList = new Array<ResourcePerson>()
  public otherResourcesList = new Array<ResourcePerson>()

  public orientationModel = new Array<PrimaryOrientationModel>()
  public orientationDetail = new PrimaryOrientationModel();

  public bisStrList = new Array<BusinessStrModel>();
  public bisStrDetail = new BusinessStrModel();
  public fromPoolList = new Array<ResourcePerson>()
  public success
  public error
  public warn
  public resourceName;
  public rType;
  public programId;
  public ptId;
  public fromPool;
  public bid
  public bname;

  // public uploader: FileUploader = new FileUploader({ url: API_URL+"ECD-TrainingPrograms-1.0/service/trainings/saveOrientationProgramDoc", queueLimit: 1 });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  @ViewChild("makeHeadOrientation") public makeHeadOrientation;
  @ViewChild("updateHeadOrientation") public updateHeadOrientation;
  @ViewChild("showLearningCenters") public showLearningCenters: ModalDirective;
  @ViewChild("addParticipantView") public addParticipantView: ModalDirective;
  constructor(public commonService: CommonWebService, public route: Router, private datePipe: DatePipe) { }

  ngOnInit() {
    this.getUser()
    this.validation()
    this.getAllLocations()
    this.loadToGrid();
    this.loadBisStr();


  }
  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
  }
  validation() {

    this.programForm = new FormGroup({
      programName: new FormControl(['', Validators.required]),
      description: new FormControl(['', Validators.required]),
      startDate: new FormControl(['', Validators.email]),
      endDate: new FormControl('', [Validators.required]),
      location: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      type: new FormControl(),
      resource: new FormControl(),

    })

    this.name = new FormControl('', Validators.required),
      this.remark = new FormControl()

  }

  myFunction() {
    window.open(this.selectedprogram.program.filePath, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=400,height=400");
  }

  openCreateHdOr() {
    this.getResources()
    this.programData = new ProgramData()
    this.rType = 1;
    this.programData.program.status = 1
    this.programData.program.startDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.programData.program.endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.makeHeadOrientation.show();
  }

  getProgramId(param) {

    // this.headOrientationDetail = pr;
    console.log("headOrientationDetail ", param);
    this.updateHeadOrientation.show();
    this.getAllProgramById(param)
    // this.uploader.queue.pop();
  }

  getProgramIdPt(programId) {
    this.programId = programId;
    this.loadParticipant();
    this.route.navigate(['/orientation-participant', { programId: programId }]);
    console.log("parental", programId);
  }

  getAllProgramById(param: PrimarySchoolModel) {
    console.log("paramz", param)
    this.programList = new Array<PrimarySchoolModel>()
    this.selectedprogram = new PrimaryProgram();
    this.selectedResourcePersonList = new Array<ResourcePerson>()

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getOrientationProgramById?programId=" + param;
    this.commonService.processGet(url).subscribe(res => {

      this.selectedprogram = res.responseData
      console.log("selectedprogram.program.programName", this.selectedprogram.program.programName);
      this.resDetail = this.selectedprogram.resourcePool
      for (let a of this.selectedprogram.resourcePool) {
        console.log("A", a)
        this.selectedResourcePersonList.push(a)
      }
      for (let a of this.selectedprogram.guestList) {
        console.log("B", a)
        this.selectedResourcePersonList.push(a)
      }
      console.log("Program", this.selectedResourcePersonList)
    }, error => {

      console.log("error ", error);
      console.log("response bstrz ", this.bisStrList)
    })
  }

  updateHeadProgram() {
    console.log("updateHeadProgram");
    this.selectedprogram.program.filePath = this.path;
    console.log("path", this.selectedprogram.program.filePath)
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/updateOrientationProgram", this.selectedprogram).subscribe(res => {

      console.log("Sucess?", res.responseCode)
      if (res.responseCode == 1) {
        this.updateHeadOrientation.hide();
        this.loadToGrid();
        setTimeout(() => {
          console.log("succes", this.selectedprogram);
          //  this.resetFields()
          this.success = false
        }, 5000)
      }

    }), error => {
      console.log("ERROR")
    }
    // this.uploader.removeFromQueue; 
  }

  loadBisStr() {
    let url = API_URL + "ECD-UserManagement-1.0/service/user/getBusinessStructure ";
    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.responseCode == 1) {

        this.bisStrList = res.responseData;
        console.log("response bstr ", this.bisStrList)
      }

    }, error => {

      console.log("error ", error);
      console.log("response bstrz ", this.bisStrList)
    })
  }


  loadToGrid() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getOrientationProgram";
    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.responseCode == 1) {

        this.primaryschoolModel = res.responseData;
        console.log("response data ", this.primaryschoolModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.primaryschoolModel)
    })
  }

  loadParticipant() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getOrientationParticipant?programId=" + this.ptId;
    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.responseCode == 1) {

        this.orientationModel = res.responseData;
        console.log("response data ", this.orientationModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.orientationModel)
    })
  }

  getResources() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources";
    this.commonService.processGet(url).subscribe(res => {
      if (res.responseCode == 1) {
        this.resModel = res.responseData;
        this.selectedResource = this.resModel[0]
      }

    }, error => {

      console.log("error ", error);

    })
  }
  getBisId(p) {
    console.log("getBisId", p.bisId)
    this.bid = p.bisId
    this.bname = p.bname
    // console.log("bid",p.bid);
  }
  createHeadProgram(e) {

    this.programData.userInserted = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveOrientationProgram", this.programData).subscribe(res => {



      if (res.responseCode == 1) {
        this.success = true;
        this.loadToGrid()

        setTimeout(() => {

          this.success = false
          this.makeHeadOrientation.hide()
        }, 2000)
      } else {
        this.error = true
        setTimeout(() => {

          this.error = false
        }, 2000)
      }


    }), error => {
      console.log("ERROR")
      this.error = true
      setTimeout(() => {

        this.error = false
      }, 2000)
    }

  }



  clearHP() {

    this.primaryschoolDetail.programId = "";
    this.primaryschoolDetail.programName = "";
    this.primaryschoolDetail.description = "";
    this.primaryschoolDetail.startDate = "";
    this.primaryschoolDetail.endDate = "";
    this.primaryschoolDetail.status = "";
    this.selectedOtherResource.personName = "";
    this.selectedOtherResourcesList = null;
    // this.PrimaryProgram.studentName = "";
    // this.PrimaryProgram.userInserted = "";
    // this.PrimaryProgram.userModified = "";
  }



  getAllLocations() {

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData
        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }

  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node1.children.push(node)
      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        node1.children.push(node)
      }

      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }
  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;
    if (event.node.type == "province") {

    }
    else if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.code, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.code, event.node.type)
    }

    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }


  nodeSelected(event) {
    this.programData.program.locationId = event.node.id
    this.programData.program.locationName = event.node.label
    this.collapseAll()
  }



  getAllLearningCentersbyId(id1, id2, type) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id1=" + id1 + "&id2=" + id2 + "&type=" + type).subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;

        this.selectedLearningCenter = this.learningCenterList[0]


      }


    }), error => {
      console.log("ERROR")
    }
  }
  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }
  addResourcePersons() {


    if (this.rType == 1 && this.selectedResource != null) {
      let person = new ResourcePerson()
      person.personId = this.selectedResource.id
      person.personName = this.selectedResource.resourceName
      person.coordinator = 0
      this.programData.resourcePool.push(person)
      this.selectedResourcePersonList.push(person)
    }
    else if (this.rType == 2 && this.selectedOtherResource != null) {

      let person = new ResourcePerson()

      person.personName = this.selectedOtherResource.personName
      person.remarks = this.selectedOtherResource.remarks
      person.coordinator = 0
      this.programData.guestList.push(person)
      this.selectedResourcePersonList.push(person)

      this.selectedResource.personName = ""
      this.selectedResource.remarks = ""

    }
  }

  removeRows(person) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool.splice(index2, 1)
    }
    if (index3 > -1) {
      this.programData.guestList.splice(index3, 1)
    }



  }

  removeRowswhenEdit(person: ResourcePerson) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool[index2].status = 2
    }
    if (index3 > -1) {
      this.programData.guestList[index3].status = 2
    }



  }

  checked(e, person) {

    console.log("Per", person)
    console.log("sdsdsd", this.programData.resourcePool)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    console.log("index", index2)
    console.log("in", index3)
    if (index2 > -1) {
      this.programData.resourcePool[index2].coordinator = 1
    }
    if (index3 > -1) {

      this.programData.guestList[index3].coordinator = 1
    }
  }

  openAddParticipant(a) {
    this.programId = a
    console.log("A", this.programId)
   
    this.addParticipantView.show()

  }
}



