import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentalAwarenessComponent } from './parental-awareness.component';

describe('ParentalAwarenessComponent', () => {
  let component: ParentalAwarenessComponent;
  let fixture: ComponentFixture<ParentalAwarenessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentalAwarenessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentalAwarenessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
