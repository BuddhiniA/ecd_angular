import { Component, OnInit, ViewChild, Inject } from '@angular/core';
// import { TreeModule, ITreeState, TreeNode } from 'angular-tree-component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BusinessStrModel } from "../model/BsStr";
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { TreeNode, TreeDragDropService, MenuItem } from 'primeng/primeng';
import { NodeService } from '../NodeService';
import { ModalDirective } from 'ngx-bootstrap';
import { Http, RequestOptions, Headers, RequestOptionsArgs } from '@angular/http';
import { UserModel } from '../model/UserModel';
import { distinct } from 'rxjs/operator/distinct';
import { MatDialog } from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
declare var google;

export class BusinessLevel {
  id
  lifeCode
  districtId
  dsID
  english
  type
  code
  provinceId
  nodeType



}
export class Node {
  id
  label
  expandedIcon = "fa-folder-open"
  collapsedIcon = "fa-folder"
  type
  code
  lifeCode
  districtId
  dsID
  provinceId
  nodeType
  leaf
  children = new Array<Node>()
}


@Component({
  selector: 'app-business-structure',
  templateUrl: './business-structure.component.html',
  styleUrls: ['./business-structure.component.css'],
  providers: [NodeService, TreeDragDropService]
})
export class BusinessStructureComponent implements OnInit {
  display: boolean;
  censusList: any;
  @ViewChild("viewUser") public viewUser: ModalDirective;
  @ViewChild("addDept") public addDept: ModalDirective;
  @ViewChild("pickListView") public pickListView: ModalDirective;
  // state: IreeState;
  // bsVar: string;


  public startingNode = new Node()
  files = new Array<Node>()
  public businessList = new Array<BusinessLevel>()
  public selectedNode = new Node()
  public previouslySelectedNode = new Node()
  public parent = new Node();
  // files: TreeNode[];

  public loading: boolean
  public headers: Headers = new Headers;
  public userModel = new Array<UserModel>();
  public userModel1 = new Array<UserModel>();
  public BsModel = Array<BusinessStrModel>();
  public list2: any[];
  public success;
  options = {
    allowDrag: true
  }
  private items: MenuItem[];
  constructor(public commonService: CommonWebService, private nodeService: NodeService, public dialog: MatDialog) { }

  ngOnInit() {
    this.loading = true

    this.items = [
      { label: 'Assign User', icon: 'fa fa-plus', command: (event) => this.addUser() },
      { label: 'Assign Department', icon: 'fa fa-plus', command: (event) => this.addDepart() }

    ];

    this.getAllLocations()

    this.list2 = new Array<UserModel>();
    // this.nodeService.getFiles().then(files => this.files = files);

    // this.selectedNode.label = "ECD PROJECT"
    // this.items = [
    //   {
    //       label: 'Add User',

    //   }



    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loading = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            // this.selectedNode.children.push(node)
            // for (let i = 0; i < this.businessList.length; i++) {
            //   if (this.businessList[i].type == "node" && this.businessList[i].id == 2) {
            //     console.log("inside node.....");
            //     let node = new Node()
            //     this.selectedNode.children[0] = new Node()
            //     this.selectedNode.children[0].id = this.businessList[i].id
            //     this.selectedNode.children[0].label = this.businessList[i].english
            //     this.selectedNode.children[0].type = this.businessList[i].type
            //     this.selectedNode.children[0].code = this.businessList[i].code
            //     this.selectedNode.children[0].districtId = this.businessList[i].districtId
            //     this.selectedNode.children[0].dsID = this.businessList[i].dsID
            //     this.selectedNode.children[0].provinceId = this.businessList[i].provinceId
            //     // this.selectedNode.children[0].children.push(node)


            //   }
            //   if (this.businessList[i].type == "node" && this.businessList[i].id == 3) {
            //     console.log("inside node.....");
            //     let node = new Node()
            //     this.selectedNode.children[1] = new Node()
            //     this.selectedNode.children[1].id = this.businessList[i].id
            //     this.selectedNode.children[1].label = this.businessList[i].english
            //     this.selectedNode.children[1].type = this.businessList[i].type
            //     this.selectedNode.children[1].code = this.businessList[i].code
            //     this.selectedNode.children[1].districtId = this.businessList[i].districtId
            //     this.selectedNode.children[1].dsID = this.businessList[i].dsID
            //     this.selectedNode.children[1].provinceId = this.businessList[i].provinceId
            //     // this.selectedNode.children[1].children.push(node)


            //   }


            // }
          }


        }

        console.log("Starting Noede", this.startingNode)






      }


    }), error => {
      console.log("ERROR")
    }

  }


  loadUserToGrid() {

    let url = API_URL + "ECD-UserManagement-1.0/service/user/getAllUsersDBLocation?location=" + this.selectedNode.id;
    let header: Headers = new Headers();
    header.append('Content-Type', 'application/json');
    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", "Sandali");
    header.set("room", "DefaultRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "LK");
    header.set("division", "ECDLK");
    header.set("organization", "ECD");
    header.set("system", "ECDCMS");

    option.headers = header;
    console.log("hhh ", header);
    this.commonService.processGet(url).subscribe(res => {



      // this.userModel = res.users.responseData;
      // this.userModel = res.data[0].user;
      this.userModel = res.responseData;
      console.log("user name", this.userModel);
      // this.isLoading = false;

    }, error => {
      // this.isLoading = false;
      console.log("error ", error);

    })

  }

  addUser() {

    this.loadUserToGrid()
    this.viewUser.show()
  }
  addDepart() {


    this.viewUser.show()
  }

  getAllUsers() {

    this.pickListView.show()
  }
  getAllUsersLocation() {

    this.list2 = []
    let url = API_URL + "ECD-UserManagement-1.0/service/user/getAllUsersDBLocation?location=0";
    let header: Headers = new Headers();
    header.append('Content-Type', 'application/json');
    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", "Sandali");
    header.set("room", "DefaultRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "LK");
    header.set("division", "ECDLK");
    header.set("organization", "ECD");
    header.set("system", "ECDCMS");

    option.headers = header;
    console.log("hhh ", header);
    this.commonService.processGet(url).subscribe(res => {



      // this.userModel = res.users.responseData;
      // this.userModel = res.data[0].user;
      this.userModel1 = res.responseData;
      console.log("user name", this.userModel);
      this.pickListView.show()
      // this.isLoading = false;

    }, error => {
      // this.isLoading = false;
      console.log("error ", error);

    })

  }

  assignUser() {
    console.log("List", this.list2)

    let url = API_URL + "ECD-UserManagement-1.0/service/user/assignUserLocation?locationId=" + this.selectedNode.id;
    let header: Headers = new Headers();
    header.append('Content-Type', 'application/json');
    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", "Sandali");
    header.set("room", "DefaultRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "LK");
    header.set("division", "ECDLK");
    header.set("organization", "ECD");
    header.set("system", "ECDCMS");

    option.headers = header;
    console.log("hhh ", header);
    this.commonService.processPostWithHeaders(url, this.list2, option).subscribe(res => {


      if (res.responseCode == 1) {
        setTimeout(() => {
          this.success = false;


        }, 2000);
      } else {

      }


    }, error => {
      // this.isLoading = false;
      console.log("error ", error);

    })

  }

  collapse(evt) {
    console.log("COLLAPSE", evt)
  }
  expand(event) {

    console.log("EXPAND", event.node.label)
    this.previouslySelectedNode.id = this.selectedNode.id
    this.previouslySelectedNode.label = this.selectedNode.label
    this.previouslySelectedNode.type = this.selectedNode.type
    this.previouslySelectedNode.code = this.selectedNode.code
    this.previouslySelectedNode.districtId = this.selectedNode.districtId
    this.previouslySelectedNode.dsID = this.selectedNode.dsID
    this.previouslySelectedNode.provinceId = this.selectedNode.provinceId
  }
  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;



    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }
    if (event.node.type == "region") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "estate") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
        for (let k = 0; k < event.node.children[j].children.length; k++) {
          for (let l = 0; l < this.businessList.length; l++) {
            if (this.businessList[l].type == "div" && event.node.children[j].children[k].districtId == this.businessList[l].districtId && event.node.children[j].children[k].code == this.businessList[l].dsID) {

              let node = new Node()
              node.id = this.businessList[l].id
              node.label = this.businessList[l].english
              node.type = this.businessList[l].type
              node.code = this.businessList[l].code
              node.districtId = this.businessList[l].districtId
              node.dsID = this.businessList[l].dsID
              node.provinceId = this.businessList[l].provinceId
              event.node.children[j].children[k].children.push(node)
            }

          }

        }
      }

    }
  }


  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()

        node1.children.push(node)

      }
      if (this.businessList[i].type == "region" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()

        node1.children.push(node)

      }


      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }

      }
      if (node1.type == "region") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "rpc") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }

      }


      // if (node1.type == "district") {
      //   let district = "" + node1.provinceId + node1.code;
      //   console.log("distrit", district);
      //   if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
      //     let node = new Node()
      //     node.id = this.businessList[i].id
      //     node.label = this.businessList[i].english
      //     node.type = this.businessList[i].type
      //     node.code = this.businessList[i].code
      //     node.districtId = this.businessList[i].districtId
      //     node.dsID = this.businessList[i].dsID
      //     node.provinceId = this.businessList[i].provinceId
      //    node1.children.push(node)
      //     this.getAllLearningCenters()
      //     console.log("SELECTED NODE", this.selectedNode)
      //   }
      // }
      // if (node1.type == "ds") {
      //   if (node1.districtId == this.businessList[i].districtId && node1.code == this.businessList[i].dsID && this.businessList[i].type == "gnd") {
      //     let node = new Node()
      //     node.id = this.businessList[i].id
      //     node.label = this.businessList[i].english
      //     node.type = this.businessList[i].type
      //     node.code = this.businessList[i].code
      //     node.districtId = this.businessList[i].districtId
      //     node.dsID = this.businessList[i].dsID
      //     node.provinceId = this.businessList[i].provinceId
      //    node1.children.push(node)
      //     console.log("SELECTED NODE", this.selectedNode)
      //     this.getAllLearningCenters()
      //   }
      // }

    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  getAllLocations() {

    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }
  openDialog(): void {
    // this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getLocationofCensus").subscribe(res => {
    //   if (res.responseCode == 1) {
    //     this.censusList = res.responseData
        // for (let b of this.activityList) {
  
        //   this.overlays.push(new google.maps.Marker(
        //     {
        //       obj: b,
        //       position:
        //       { lat: b.latitude, lng: b.longitude },
        //       title: b.activityDescription
        //     }
  
        //   ))
  
        // }
        this.display = true;
    //   }
    // })
   
  }
}

