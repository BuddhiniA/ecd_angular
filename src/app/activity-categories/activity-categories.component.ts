import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivityCategoryModel } from "../model/ActivityCategory";
import { BudgetAllocationModel } from "../model/BudgetAllocation";
import { BudgetFieldsModel } from "../model/BudgetFields";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ActivityAddModel } from '../model/ActivityAdd';
import { MilestoneAll } from '../model/MilestoneALL';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { API_URL } from '../app_params';
import { CommonWebService } from '../common-web.service';
import { Documents } from '../model/document';
import { Budget } from '../model/Budget';

export class ActivityCategorydModel{

  categoryId: any = null;
  categoryName: any = null;
  totalBudget: any = null;

  budgetFields = new Array<BudgetFieldsModel>()

  milestoneList = new Array<MilestoneAll>()
  documentList = new Array<CategoryDoc>()

  status: any = null;
  userInserted: any = null;
  userModified: any = null;
  dateInserted: any = null;
  dateModified: any = null;
}

export class BudgetField {
  fieldId
  fieldName
  status
  budgetValue
  percentage
}

export class Milestone {
  milestoneId
  milestoneName
  status
}

export class CategoryDoc {
  documentId
  milestoneId
  documentName
  status
}

export class MilestoneShow {
  milestoneId
  milestoneName
  documentCatList = new Array<CategoryDoc>()
}

@Component({
  selector: 'app-activity-categories',
  templateUrl: './activity-categories.component.html',
  styleUrls: ['./activity-categories.component.css'],
  providers: [CommonWebService]
})
export class ActivityCategoriesComponent implements OnInit {
  userData: any;
  userId: string;
  activityForm: FormGroup;

  public catId: any;
  public randomArr = new Array<any>()
  public activitycatModel = Array<ActivityCategoryModel>();
  public activitycatList = new ActivityCategoryModel();
  public activitycatEditList = Array<ActivityCategoryModel>();
  public activitycatEditModel = new ActivityCategoryModel();
  public budgetallocationModel = Array<BudgetAllocationModel>();
  public budgetallocationList = new BudgetAllocationModel();
  public budgetFields = Array<BudgetFieldsModel>();
  public activitycataddList = Array<ActivityAddModel>();
  public activitycataddModel = new ActivityAddModel();

  public budgetArrayForGrid = new Array<any>();
  public milestoneArrayForGrid = new Array<any>();
  public budgetFieldLIst = new Array<BudgetField>()

  public selectedBudget: BudgetField
  public milestoneLIst = new Array<Milestone>()
  public selectedMilestone: Milestone
  public selectedBudgetFieldLIst = new Array<BudgetField>()
  public selectedMilestoneFieldLIst = new Array<Milestone>()  //selectedMilestone
  public rowCount = 0;
  public rowCountM = 0;
  public rowArr = new Array<number>()
  public rowArrM = new Array<number>()
  public milestoneModel = Array<MilestoneAll>();
  public milestoneList = new MilestoneAll();
  // public activityForm :FormGroup
  public budgetFieldsTmp;
  public success;

  public selecteddocumentList = new Array<Documents>()
  public selectedDocument = new Documents()
  public document: Documents = new Documents()

  public documentForCat: CategoryDoc = new CategoryDoc()
  public documentList = new Array<Documents>()
  public selecteddocumentListForCat = new Array<CategoryDoc>()
  public milestoneListForShow = new MilestoneShow()
  public milestoneListForShowList = new Array<MilestoneShow>()
  public totalBudgetVal;

  public spining:boolean
  public selectedType
  public milestoneId
  constructor(public commonService: CommonWebService) {
    this.userId = sessionStorage.getItem("bisId");
    this.validation();
  }

  @ViewChild('childModal') public childModal: ModalDirective;
  @ViewChild("addActivity") addActivity;
  @ViewChild("editActivity") editActivity;
  @ViewChild("addBudgetModal") addBudgetModal;
  @ViewChild("addMilestoneModal") addMilestoneModal;
  @ViewChild("createDocument") createDocument;


  ngOnInit() {

    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    
    this.loadActivityCategoryList();
    this.addToGrid();
    this.loadCategoryList();
    this.getAllBudgetFields()
    this.getAllMilstone();
   
    // this.getAllDocuments()
  }

  validation(){

        this.activityForm = new FormGroup({
          categoryName:new FormControl(['',Validators.required]),
          totalBudget:new FormControl(['',Validators.required]),
          type:new FormControl()
        })
      }

  openCreateCategory() {

    // this.budgetFields = JSON.parse(this.budgetFieldsTmp);
    this.totalBudgetVal=null
    this.activitycataddModel = new ActivityAddModel();
    this.selectedBudgetFieldLIst = new Array<BudgetField>()
    this.milestoneListForShowList = new Array<MilestoneShow>()
    this.selectedMilestoneFieldLIst = new Array<Milestone>()
    this.selectedType=1
    this.addActivity.show();
    // this.commonService.processGet("http://192.0.0.59:8080/ECD-ActivityMaintenance-1.0/service/Activities/getAllCategoryData").subscribe(res=>{

    //     this.budgetallocationList=res.responseData
    //     this.budgetFields = this.budgetallocationList.budgetFields;
    //     console.log("This ",this.budgetallocationList)
    //   }),error=>{
    //       console.log("ERROR")
    //   }    
    //  this.addToGrid()
  }

  createCategory() {

    // this.BudgetFieldsModel
    this.activitycataddModel.budgetFields = this.selectedBudgetFieldLIst;
    this.activitycataddModel.milestoneList = this.selectedMilestoneFieldLIst;
    this.activitycataddModel.documentList = this.selecteddocumentListForCat
    this.activitycataddModel.totalBudget = this.totalBudgetVal
    for(let a of this.selecteddocumentListForCat)
    console.log("saveCategoryChanges", this.activitycataddModel.milestoneList);
    console.log("saveCategoryChanges1", this.selectedMilestoneFieldLIst);
    console.log("saveCategoryChanges2", this.activitycataddModel);
    this.activitycataddModel.userInserted = this.userData[0].userId
    this.activitycataddModel.status = 1;
    let url = API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveCategory";


    this.commonService.processPost(url, this.activitycataddModel).subscribe(res => {


      let response = res.responseCode
      console.log("response ", response);
      if (response == 1) {
        this.spining=true
        console.log("success");
        this.success = true;
        setTimeout(() => {
          this.success = false;
          this.spining=false
          this.loadActivityCategoryList()
        }, 1000);

      } else {
        console.log("NOT success");
      }

      // this.createUserLoader = false;

      // this.loadToGrid();
    }, error => {

      console.log("Error");

    })
  }

  getcatId(cat) {
    console.log("activity ", cat);
    this.activitycataddModel.categoryId = cat;
    this.editActivity.show();
    this.getCatData();
    // this.addToGrid()
    // this.addToGrid();
  }
  getCatData() {
    let url = API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategoryData?catId=" + this.activitycataddModel.categoryId;
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.activitycatEditModel = res.responseData;
        this.budgetArrayForGrid = this.activitycatEditModel.budgetFields
        this.milestoneArrayForGrid = this.activitycatEditModel.milestoneList
        console.log("this.budgetArrayForGrid", this.budgetArrayForGrid);
        console.log("loadCategoryList data ", this.activitycatEditModel);
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.activitycatEditModel);
    })
  }

  onChange(value) {
    console.log("ssd ", value);
    let arr = value.split(": ");
    console.log("arr ", arr[1]);
    this.catId = arr[1];
    this.addToGrid();
    // console.log("activity ",act);
    // this.loadPacks();

  }

  loadCategoryList() {

    let url = API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategories";
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.activitycatList = res.responseData;
        console.log("loadCategoryList data ", this.activitycatModel);
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.activitycatModel);
    })
  }

  loadActivityCategoryList() {
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategories").subscribe(res => {

      this.activitycatModel = res.responseData
      console.log("This time", this.activitycatModel.length)
    }), error => {
      console.log("ERROR")
    }
  }



  addToGrid() {
    // this.catId;

    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllCategoryData?catId=1").subscribe(res => {

      this.budgetallocationList = res.responseData
      this.budgetFields = this.budgetallocationList.budgetFields;
      console.log("This ", this.budgetallocationList)

      this.budgetFieldsTmp = JSON.stringify(this.budgetFields);

    }), error => {
      console.log("ERROR")
    }
    // let activityDetailtoAr = this.activitycatList;
    // let arr = new Array<any>();
    // activityDetailtoAr.budgetFields
  }


  getAllBudgetFields() {

    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllBudgetFields").subscribe(res => {
      console.log("Budget")
      if (res.responseCode == 1) {
        this.budgetFieldLIst = res.responseData
        this.budgetArrayForGrid = this.budgetFieldLIst
        this.selectedBudget = this.budgetArrayForGrid[0]
        console.log("Budget", this.selectedBudget)

        // for(let a of this.budgetFieldLIst){
        //   this.totalBudgetVal=+a.budgetValue
        // }
        // console.log("TOTLA",this.totalBudgetVal)
      }
    }), error => {
      console.log("ERROR")
    }
  }
  addBudget() {
    this.addBudgetModal.show();
    // this.rowCount++;
    // this.rowArr.push(this.rowCount)
  }
  addMilestone() {
    console.log("addMilestoneModal");
    this.addMilestoneModal.show();
    // this.rowCountM++;
    // this.rowArrM.push(this.rowCountM)
  }

  removeRow(budget) {
    console.log("id", budget);
    var index = this.selectedBudgetFieldLIst.indexOf(budget)


    if (index != -1) {
      this.selectedBudgetFieldLIst.splice(index, 1);
      this.totalBudgetVal = this.totalBudgetVal-budget.budgetValue

    }
    let milestoneForShow = new MilestoneShow()
    milestoneForShow.milestoneId = this.selectedMilestone.milestoneId
    milestoneForShow.milestoneName = this.selectedMilestone.milestoneName
    this.removeRowM(milestoneForShow)


  }

  removeRowM(mid:MilestoneShow) {
    console.log("b", mid);
    console.log("AAA",this.milestoneListForShowList)
    if(this.userId=="2"){
      for(let a of this.milestoneListForShowList){
        if(a.milestoneId==mid.milestoneId){
          var ind = this.milestoneListForShowList.indexOf(a)
          this.milestoneListForShowList.splice(ind, 1);
          
        }
      }
    }

    // var index = this.selectedMilestoneFieldLIst.indexOf(mid)
    var index1= this.milestoneListForShowList.indexOf(mid)
    let milestoneId = mid.milestoneId


    for(let a of this.selectedMilestoneFieldLIst){
      if(milestoneId==a.milestoneId){
        var index = this.selectedMilestoneFieldLIst.indexOf(a)
        this.selectedMilestoneFieldLIst.splice(index,1)
      }
    }
    
    if (index1 != -1) {
      // this.selectedMilestoneFieldLIst.splice(index, 1);
      this.milestoneListForShowList.splice(index1, 1);

    }
  }

  addBudgettoSelectedList() {

    let bud = new Budget()

    bud.fieldId = this.selectedBudget.fieldId
    bud.fieldName = this.selectedBudget.fieldName
    bud.status = this.selectedBudget.status
    bud.budgetValue = this.selectedBudget.budgetValue
    if(this.userId=="3"||this.selectedType==1){
      this.totalBudgetVal=this.totalBudgetVal+this.selectedBudget.budgetValue
    }
    if(this.userId=="2"||this.selectedType==2){
      console.log("2")
      bud.budgetValue=this.totalBudgetVal*this.selectedBudget.percentage
       for(let a of this.milestoneLIst){
        if(bud.fieldId==a.status){
          this.selectedMilestone=a
          this.addMilestonestoGrid()
        }
    }
    }
   
    this.selectedBudgetFieldLIst.push(bud)



  }


  addDocumentsToGrid() {

    for (let a of this.milestoneListForShowList) {
      if (this.milestoneId == a.milestoneId) {
        var index = this.milestoneListForShowList.indexOf(a)
        let doc = new CategoryDoc()
        doc.documentId = this.selectedDocument.documentId
        doc.status = this.selectedDocument.status
        doc.documentName = this.selectedDocument.documentName
        doc.milestoneId = this.milestoneId

        this.milestoneListForShowList[index].documentCatList.push(doc)
        this.selecteddocumentListForCat.push(doc)
      }

    }
  }
    addMilestonestoGrid() {
      // this.milestoneListForShowList = new Array<MilestoneShow>()
      let milestoneForShow = new MilestoneShow()

      let catDocList = new Array<CategoryDoc>()
      let miles = new Milestone()

      miles.milestoneId = this.selectedMilestone.milestoneId
      miles.milestoneName = this.selectedMilestone.milestoneName
      miles.status = this.selectedMilestone.status

      this.selectedMilestoneFieldLIst.push(miles)

      // if (this.milestoneId != null || this.milestoneId != undefined) {
      //   let doc = new CategoryDoc()
      //   doc.documentId = this.selectedDocument.documentId
      //   doc.status = this.selectedDocument.status
      //   doc.documentName = this.selectedDocument.documentName
      //   doc.milestoneId = this.milestoneId

      //   this.selecteddocumentListForCat.push(doc)

      //   milestoneForShow.documentCatList = this.selecteddocumentListForCat

      // }



      milestoneForShow.milestoneId = this.selectedMilestone.milestoneId
      milestoneForShow.milestoneName = this.selectedMilestone.milestoneName





      
      this.milestoneListForShowList.push(milestoneForShow)
    
      // this.removeFromList()
      // this.selectedDocument = this.documentList[0]

    }

    removeFromList() {
      for (let i = 0; i <= this.selecteddocumentList.length; i++) {
        for (let j = 0; j <= this.documentList.length; j++) {
          if (this.selecteddocumentList[i] == this.documentList[j]) {
            var index = this.documentList.indexOf(this.documentList[j]);
            this.documentList.splice(index, 1);
          }
        }
      }
    }


    getAllMilstone() {
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllMilestones").subscribe(res => {
        console.log("Budget")
        if (res.responseCode == 1) {
          this.milestoneLIst = res.responseData
          this.milestoneArrayForGrid = this.milestoneLIst
          this.selectedMilestone = this.milestoneArrayForGrid[0]
          
        }
      }), error => {
        console.log("ERROR")
      }
    }

    addMilestonetoSelectedList() {

      console.log("Add milestone>>")
      let miles = new Milestone()

      miles.milestoneId = this.selectedMilestone.milestoneId
      miles.milestoneName = this.selectedMilestone.milestoneName
      miles.status = this.selectedMilestone.status

      this.selectedMilestoneFieldLIst.push(miles)

    }
    addDocument(milestoneID){
      console.log("MILESTONE ID", milestoneID)
      this.milestoneId = milestoneID
      this.getAllDocuments()
      this.createDocument.show()
    }

    getAllDocuments() {
      this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getAllDocuments").subscribe(res => {
        this.documentList = res.responseData
        this.selectedDocument = this.documentList[0]

      }), error => {
        console.log("ERROR")
      }
    }
    setStatus(a){
      console.log("Status",a)
      let url = API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/editCategory";
      
      
          this.commonService.processPost(url, a).subscribe(res => {
      
      
            let response = res.responseCode
            console.log("response ", response);
            if (response == 1) {
              console.log("success");
              this.success = true;
              setTimeout(() => {
                this.success = false;
                this.loadActivityCategoryList()
              }, 1000);
      
            } else {
              console.log("NOT success");
            }
      
            // this.createUserLoader = false;
      
            // this.loadToGrid();
          }, error => {
      
            console.log("Error");
      
          })
    }
  }
