import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CensusViewComponent } from './census-view.component';

describe('CensusViewComponent', () => {
  let component: CensusViewComponent;
  let fixture: ComponentFixture<CensusViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CensusViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CensusViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
