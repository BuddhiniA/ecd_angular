import { Component, OnInit, SimpleChanges } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { Data } from '../model/Data';
declare var google;


export class Overlay {
  position
  title
}

@Component({
  selector: 'app-census-view',
  templateUrl: './census-view.component.html',
  styleUrls: ['./census-view.component.css']
})
export class CensusViewComponent implements OnInit {
  options: { center: { lat: number; lng: number; }; zoom: number; };
  overlays: any[];
  censusList: Data[];

  constructor(public commonService: CommonWebService) {
    this.overlays = new Array<Overlay>()
    this.censusList = new Array<Data>()
  }


  ngOnInit() {
    this.overlays = new Array<Overlay>()
    this.options = {
      center: { lat: 7.8774, lng: 80.7003 },
      zoom: 12
    };
    this.censusList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getLocationofCensus").subscribe(res => {
      if (res.responseCode == 1) {
        this.censusList = res.responseData
        for (let b of this.censusList) {

          // console.log("b geolocation",b.lat,b.longt)
          this.overlays.push(new google.maps.Marker(
            {
            
              position:
                { lat: parseFloat(b.lat), lng: parseFloat(b.longt) },
              // { lat:6.1522647 , lng:80.5539935  },
              title: b.name + "-" + b.longt + "," + b.lat

            }

          ))
          console.log("b geolocation", parseFloat(b.longt), parseFloat(b.lat))
        }

      }
    })

  }

  ngOnChanges(changes: SimpleChanges) {
    this.overlays = new Array<Overlay>()
    this.censusList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getLocationofCensus").subscribe(res => {
      if (res.responseCode == 1) {
        this.censusList = res.responseData
        for (let b of this.censusList) {

       
          this.overlays.push(new google.maps.Marker(
            {
              obj: b,
              position:
                { lat: parseFloat(b.lat), lng: parseFloat(b.longt) },
              title: b.longt
            }

          ))
          console.log("b geolocation>>", b.longt, b.lat)
        }

      }
    })
  }
}
