import { Component, OnInit } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { MaterialStock } from '../model/MaterialStock';
import { GRNDetails } from '../model/MaterialGRNDetails';
import { API_URL } from '../app_params';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { CustomValidator } from '../CustomValidator';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-material-stock',
  templateUrl: './material-stock.component.html',
  styleUrls: ['./material-stock.component.css'],
  providers: [CommonWebService, MaterialStock, GRNDetails,DatePipe]
})
export class MaterialStockComponent implements OnInit {
  materialStockForm: FormGroup;


  public materialList = new Array<MaterialStock>()
  public selectedMaterial = new MaterialStock();
  public material = new MaterialStock()
  public grnDetails = new GRNDetails()
  public success: boolean
  public customValidator = new CustomValidator();
  public maxDateVAlue
  public selectedDate

  constructor(public commonService: CommonWebService,private datePipe: DatePipe) {
    this.validation()
  }

  ngOnInit() {
    this.maxDateVAlue = new Date()
    this.grnDetails.grnDate = this.maxDateVAlue
    this.loadMaterials()
  }



  validation() {
    this.materialStockForm = new FormGroup({
      materialName: new FormControl(),
      quantity: new FormControl(['', Validators.required, Validators.pattern("^[0-9]*[1-9][0-9]*$")]),
      date: new FormControl()

    })
  }

  loadMaterials() {
    this.commonService.processGet(API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/getAllMaterials").subscribe(res => {
      //this.commonService.processGet("./assets/criteriadetail.json").subscribe(res=>{
      this.materialList = res.responseData
      console.log("ID", this.materialList.length)
      this.selectedMaterial = this.materialList[0];

    }), error => {
      console.log("ERROR")
    }
  }
  AddStock(f: NgForm) {

    if (f.valid) {

      this.grnDetails.materialId = this.selectedMaterial.materialId;
      this.grnDetails.materialName = this.selectedMaterial.materialName
      this.grnDetails.grnDate=this.datePipe.transform(this.selectedDate, 'yyyy-MM-dd')
      console.log("Payload", this.grnDetails)
      this.grnDetails.userEntered = ""

      this.commonService.processPost(API_URL+"ECD-MaterialMaintanance-1.0/service/Materials/addNewMaterialStock", this.grnDetails).subscribe(res => {

        console.log("Success")
        if (res.responseCode == 1) {
          this.success = true;
          console.log("Success")
          setTimeout(() => {
            this.success = false;
            this.grnDetails.quantity = ""
            this.grnDetails.grnDate = ""
            this.loadMaterials()
          }, 1000);



        }

      }), error => {
        console.log("ERROR")
      }
    }

  }
}
