import { Component, OnInit, ViewChild, Directive } from '@angular/core';
import { NewUserModel } from "../model/NewUserModel";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Headers, RequestOptionsArgs, RequestOptions } from "@angular/http";

import { User } from "../model/User";


// import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { CommonWebService } from '../common-web.service';
import { UserModel } from '../model/UserModel';
import { LoignValidator } from '../login-validator';
import { API_URL } from '../app_params';
import { LoggedInUserModel } from '../model/loggedInUserModel';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['./usermanagement.component.css'],
  providers: [LoggedInUserModel, CommonWebService]
})
@Directive({ selector: '[ng2FileSelect]' })
export class UsermanagementComponent implements OnInit {
  userData: any;
  error: boolean;
  msg: any;
  showImage: any;
  serviceProgress: boolean;
  public imagePath: any;
  public base64Image: string;

  failMessage: string;
  public isSuccess: boolean = false;
  public isLoading: boolean = true;
  public successMessage = "";
  public selectedValueStatus = 1
  status: string;
  public a: boolean;
  public b: boolean;
  public imageSavingURL
  path: any;
  public loading: boolean
  public image


  ////////////////////////////SANDALI ///////////////////////////////////////

  // public uploader: FileUploader = new FileUploader({ url: API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveImages", queueLimit: 1 });

  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
  //////////////////////////////////////////////////////////////////////////

  @ViewChild("success") success;

  @ViewChild("fail") fail;

  @ViewChild('childModal') public childModal: ModalDirective;
  @ViewChild("makeUserSetting") public makeUserSetting: ModalDirective;
  @ViewChild("makeGroupAdminSetting") makeGroupAdminSetting;
  UserModel
  // public userModel = new Array<NewUserModel>();
  public userModel = new Array<UserModel>();
  public newUserModel = new User();
  public user = new User();
  public arrNewUserModel = new Array<User>();

  public userModelDetail = new NewUserModel();
  public userForm: FormGroup
  // public arrNewUserModel = new Array<User>();



  constructor(public commonService: CommonWebService, public validator: LoignValidator) {
    // this.validator.validateUser("user-management");
    // this.newUserModel.userImage;
    this.validation();
  }

  ngOnInit() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.loadUserToGrid();
    // this.imageSavingURL = API_URL + "ECD-UserManagement-1.0/service/user/uploadImages"

    // this.uploader.onAfterAddingFile = (file) => {
    //   file.withCredentials = false;

    // }


    // this.uploader.onCompleteItem = (m, v) => {
    //   //  console.log("zzz",m);
    //   //  console.log("vvv",v);

    //   var responsePath = JSON.parse(v);


    //   console.log("aaa", responsePath);
    //   this.path = responsePath.responseData;
    //   // this.newUserModel.userImage = this.path;

    // }

  }

  validation() {

    this.userForm = new FormGroup({
      uid: new FormControl(['', Validators.required]),
      fname: new FormControl(['', Validators.required]),
      lname: new FormControl(['', Validators.email]),
      designation: new FormControl('', [Validators.required]),
      telephone: new FormControl('', [Validators.required, Validators.maxLength(12)]),
      email: new FormControl('', [Validators.email]),
      utype: new FormControl('', [Validators.required]),
      status: new FormControl('', Validators.required)

    })
  }

  public showChildModal(): void {
    this.childModal.show();

  }

  public hideChildModal(): void {
    this.childModal.hide();
  }

  openCreateUser() {
    this.newUserModel.user.status = 1
    this.newUserModel.userType = 1
    console.log("show modal");
    this.makeUserSetting.show();
    // this.newUserModel = new User();
  }

  openNewGroup() {
    // http://192.0.0.59:8080/ECD-UserManagement-1.0/service/user/saveUserImage
    // this.uploader = new FileUploader({ url: API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/saveImages", queueLimit: 1 });


    // this.uploader.onAfterAddingFile = (file) => {
    //   file.withCredentials = false;

    // }

    // this.uploader.onBeforeUploadItem = (e) => {
    //   this.serviceProgress = true;
    //   console.log("serviceProgress", this.serviceProgress);
    // }

    // this.uploader.onCompleteItem = (m, v) => {

    //   console.log("completed! 1")


    //   this.serviceProgress = false;

    //   //  console.log("zzz",m);
    //   //  console.log("vvv",v);
    //   var responsePath = JSON.parse(v);



    //   console.log("aaa", responsePath);
    //   this.path = responsePath.responseData;
    // }


    // this.uploader.onAfterAddingFile = (file) => {
    //   file.withCredentials = false;

    // }


    // this.uploader.onCompleteItem = (m, v) => {
    //   var responsePath = JSON.parse(v);

    //   this.serviceProgress = false;

    //   console.log("aaa", responsePath);
    //   this.path = responsePath.responseData;
    //   console.log("thsi.path", this.path);
    //   // this.newUserModel.userImage = this.path;
    // }


  }


  uploadImg(evnt) {
    console.log("EVENT", evnt)
    console.log("EVENT", evnt.xhr.response)
    let res = JSON.parse(evnt.xhr.response)
    console.log(res.responseData)
    this.image = res.responseData
    this.showImage = API_URL + "/" + res.responseData
  }

  open(on) {


    console.log(">>>>>>>>>>>>>> ", on)

  }

  loadUserToGrid() {
    this.loading = true
    let url = API_URL + "ECD-UserManagement-1.0/service/user/getAllUsersDB";
    let header: Headers = new Headers();
    header.append('Content-Type', 'application/json');
    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", "Sandali");
    header.set("room", "DefaultRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "LK");
    header.set("division", "ECDLK");
    header.set("organization", "ECD");
    header.set("system", "ECDCMS");

    option.headers = header;
    console.log("hhh ", header);
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {
        this.loading = false
        this.userModel = res.responseData;
        console.log("user name", this.userModel);
      }

      // this.userModel = res.users.responseData;
      // this.userModel = res.data[0].user;

      // this.isLoading = false;

    }, error => {
      // this.isLoading = false;
      console.log("error ", error);

    })

  }

  createUser() {

    let me = this;

    // this.createUserLoader = true;

    let url = API_URL + "ECD-UserManagement-1.0/service/user/addUser";

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", "Danushka");
    header.set("room", "DefaultRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "LK");
    header.set("division", "ECDLK");
    header.set("organization", "ECD");
    header.set("system", "ECDCMS");
    header.set("Content-Type", "application/json");

    option.headers = header

    console.log("user mod", this.newUserModel);

    if (this.newUserModel.userType == 1) {
      this.newUserModel.user.groups = [{
        "groupId": "AdminECDGrp",
        "groupName": "Admin Group"
      }]
    } else if (this.newUserModel.userType == 2) {

      this.newUserModel.user.groups = [{
        "groupId": "ECDOfficerGrp",
        "groupName": "ECD Officer Group"
      }]

    } else if (this.newUserModel.userType == 3) {

      this.newUserModel.user.groups = [{
        "groupId": "ECDPHDTGrp",
        "groupName": "PHDT Group"
      }]

    }
    else {

      this.newUserModel.user.groups = [{
        "groupId": "ECDMasterGrp",
        "groupName": "Trainer Group"
      }]
    }


    this.newUserModel.userId = this.newUserModel.user.userId
    this.newUserModel.username = this.newUserModel.user.userId
    this.newUserModel.user.createdBy = this.userData[0].userId;
    this.newUserModel.user.extraParams = '0';

    // console.log("userImage:",this.newUserModel.userImage);
    // console.log("userType:",this.newUserModel.userType)
    console.log("USER", this.newUserModel)
    this.commonService.processPostWithHeaders(url, this.newUserModel, option).subscribe(res => {


      let response = res
      console.log("response", response);
      if (response.flag == 100) {

        this.success = true;


        // this.isLoading = false;
        this.success = true
        this.msg = "Successfully Created a user!"
        setTimeout(() => {
          this.msg = null
          this.success = false;
          this.makeUserSetting.hide()
          this.loadUserToGrid();

        }, 1000);
        // this.resetFields(this.newUserModel);

        // ?  this.successMessage = "User has been created successfully. Password will be sent to the email address";


      } else if (response.flag == 808) {
        this.failMessage = response.exceptionMessages[0];
        // this.fail.show();
      } else {
        this.failMessage = "Cannot create the user at this time"
        // this.fail.show();
      }

      // this.createUserLoader = false;

      this.loadUserToGrid();
    }, error => {
      // this.createUserLoader = false;
      this.failMessage = "Error in connection. Try again later!"
      // this.fail.show();

    })
    //
  }

  getUserId(u) {
    this.b = true;
    this.a = false;
    this.user = u;
    this.showImage = null
    console.log("userz ", this.user);
    this.imageSavingURL = API_URL + "ECD-UserManagement-1.0/service/user/uploadImages"
    this.getUserImage()
    // this.showImage = null
    this.makeGroupAdminSetting.show()
    console.log("a = ", this.a);
    console.log("b = ", this.b);
  }

  getUserIdView(u) {
    // this.showImage = null
    this.a = true;
    this.b = false;
    this.user = u;
    this.showImage = null
  
    this.getUserImage()
    this.makeGroupAdminSetting.show()
    
  }
  saveUserChanges(evt) {

    // this.updateUserLoade = true;

    console.log("saveUserChanges ", this.userModelDetail);

    let url = API_URL + "ECD-UserManagement-1.0/service/user/modifyCmsUser";

    let header: Headers = new Headers();

    let option: RequestOptionsArgs = new RequestOptions();

    header.set("userId", this.userData[0].userId);
    header.set("room", "DefaultRoom");
    header.set("department", "DefaultDepartment");
    header.set("branch", "HeadOffice");
    header.set("countryCode", "LK");
    header.set("division", "ECDLK");
    header.set("organization", "ECD");
    header.set("system", "ECDCMS");

    option.headers = header;


    option.headers = header;

    if (this.image != undefined || this.image != null) {
      // this.user.userImage = this.image
      this.user.userImage = this.image
    }

    this.user.user.createdBy = ""
    console.log("User to be updated")
    this.commonService.processPostWithHeaders(url, this.user, option).subscribe(res => {

      this.isSuccess = true;

      let response = res;
      console.log("saveUserChanges ", response);



      if (response.flag == 100) {
        // this.makeGroupAdminSetting.show();
        this.success = true;
        this.msg = "Successfully Updated the user!"
        // this.success.show();
        setTimeout(() => {
          this.success = false;


        }, 1000);

      } else {
        this.error = true
        setTimeout(() => {
          this.error = false;
          this.msg = "Error Updating the user!"

        }, 1000);
      }
      // this.updateUserLoade = false;



    }, error => {

      console.log("error >>> ", error);

    })
    this.loadUserToGrid();
  }
  getUserImage() {

    console.log("calling")
    this.commonService.processGet(API_URL + "ECD-UserManagement-1.0/service/user/getUserImage?userId=" + this.user.user.userId).subscribe(res => {
      this.showImage = res.responseData



    }), error => {
      console.log("ERROR")
    }
  }
}

