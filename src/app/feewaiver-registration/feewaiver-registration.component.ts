import { Component, OnInit, ViewChild } from '@angular/core';

import { FeeWaiver } from '../model/FeeWaiver';
import { CriteriaAllModel } from '../model/CriteriaAll';
import { Data } from '../model/Data';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TreeNode } from 'primeng/primeng';
import { Criteria } from '../criteria-management/criteria-management.component';
import { FeeWaiverData } from '../model/FeeWaiverData';
import { FeeWaiverStudent } from '../model/FeeWaiverStudent';
import { DatePipe } from '@angular/common';
import { Preschool } from '../model/Preschool';



export class BusinessLevel {
  id
  lifeCode
  districtId
  dsID
  english
  type
  code
  provinceId
  nodeType



}
export class Node {
  id
  label
  type
  code
  lifeCode
  districtId
  dsID
  provinceId
  nodeType
  leaf
  children = new Array<Node>()
}

@Component({
  selector: 'app-feewaiver-registration',
  templateUrl: './feewaiver-registration.component.html',
  styleUrls: ['./feewaiver-registration.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class FeewaiverRegistrationComponent implements OnInit {
  userId: any;
  newPreSchoolId: any;
  selectedProvince: number=0;
  preSchoolForm: FormGroup;
  selectedDS: any;
  dsList: any;
  districtId: any;
  selectedDistrict: any;
  districtList: any;
  preSchholForm: FormGroup;
  error: boolean;
  msg: any;
  taskType: string;
  userData: any;
  selectForm: FormGroup;
  addForm: FormGroup;

  @ViewChild('addModel') public addModel: ModalDirective
  @ViewChild('preschoolView') public preschoolView: ModalDirective
  @ViewChild("showLearningCenters") public showLearningCenters: ModalDirective;

  public feeWaiverList = Array<FeeWaiverData>();
  public feeWaiverModel = new FeeWaiver()
  public criteriaList = Array<CriteriaAllModel>();
  public criteriaModel = new CriteriaAllModel()
  public LearningList = Array<Data>();
  public LearningModel = new Data()
  public selectedCriteria;
  public selectedPreschool;
  public success: boolean
  public checked: any = 1
  public preSchool = new Preschool()

  public currentFeeWaiverId = 0;

  public loading: boolean
  files = new Array<Node>()
  public businessList = new Array<BusinessLevel>()
  public selectedNode = new Node()
  public previouslySelectedNode = new Node()

  public learningCenterList = new Array<Data>()
  public selectedLearningCenter = new Data()
  loadingMask;


  public feeWaiverData = new FeeWaiverData()

  constructor(public commonService: CommonWebService, private datePipe: DatePipe) { 
    this.loadingMask=false;
  }

  ngOnInit() {

    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.userId = sessionStorage.getItem("bisId");
    this.checked = 1



    //  this.loadLr;
    this.validation()
    this.getAllLocations()


  }

  createPreSchool(){
    this.selectedLearningCenter = new Data()
    this.preSchool.district = this.selectedDistrict.id
    this.preSchool.ds  = this.selectedDS.id
    this.preSchool.userInserted = this.userData[0].userId
   console.log("to be saved",this.preSchool)
    this.commonService.processPost(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/insertNewPreSchhol", this.preSchool).subscribe(res => {

      console.log("Sucess?", res.responseCode)
      if (res.responseCode == 1) {
        this.success = true
        this.selectedLearningCenter.sn= res.responseData.id
        this.selectedLearningCenter.name = res.responseData.name
        this.newPreSchoolId = res.responseData.id
        setTimeout(() => {

          this.resetFields()
          this.success = false
          this.preschoolView.hide()
        }, 1000)
      } else {
        this.error = true;
        this.msg = "Cant connect to the server"
        setTimeout(() => {
          this.error = false;
          this.success = false
        }, 1000)
      }

    }), error => {
      this.error = true;
      this.msg = "Cant connect to the server"
      setTimeout(() => {
        this.error = false;
        this.success = false
      }, 1000)
    }

  }

  openCreateFeeWv() {
   
    this.taskType = 'new';
    this.feeWaiverData = new FeeWaiverData()
    this.feeWaiverData.student.status = 1

    this.feeWaiverData.student.gender = 1
    this.feeWaiverData.student.regDate = new Date()
    this.feeWaiverData.student.leaveDate = new Date()
    this.addModel.show();
  }

  loadAllCriteraList() {
    this.selectedLearningCenter = new Data()
    this.learningCenterList = new Array<Data>()

    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllCriteriabyType?type=1").subscribe(res => {

      if (res.responseCode == 1) {
        this.criteriaList = res.responseData

        this.selectedCriteria = this.criteriaList[0]
        if (this.selectedCriteria != undefined) {
          this.loadPreSchools()
        }

      }


    }), error => {
      console.log("ERROR")
    }
  }

  changingLearningCenter() {
    this.getPreSchool()

  }
  changeAction() {

    this.learningCenterList = new Array<Data>()
    console.log("changing>>>>")
    this.loadPreSchools()

  }

  othrChecked(){
    this.selectedCriteria = null
    this.selectedLearningCenter = new Data()
  }

  loadPreSchools() {



    let url = API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllCriteriaLearningCenters?criteriaId=" + this.selectedCriteria.id;
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.learningCenterList = res.responseData;
        console.log("1")
        if (this.learningCenterList.length != 0) {
          console.log("2")
          this.selectedLearningCenter = this.learningCenterList[0]
          if (this.selectedLearningCenter != undefined) {
            console.log("3")
            this.getPreSchool()
          } else {
            this.selectedLearningCenter != null
          }
        } else {
          this.selectedLearningCenter.name = ""
        }




      }

    }, error => {

      console.log("error ", error);

    })

  }

  addNewPreSchool(){
    this.newPreSchoolId=0
    this.selectedLearningCenter = new Data()
    this.selectedProvince=1
    this.loadDistricts(1)
    this.preSchool = new Preschool()
    
    this.preSchool.status = 1
    this.preschoolView.show()
  }

  getPreSchool() {
    this.clearFW();


    this.feeWaiverList = new Array<FeeWaiverData>();
    if (this.selectedCriteria == undefined||this.selectedCriteria == null) {
      this.selectedCriteria = new Criteria()
      this.selectedCriteria.id = 0
    }
    let url = API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getFeeWaiver?criteriaId=" + this.selectedCriteria.id + "&learningCenter=" + this.selectedLearningCenter.sn;
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.feeWaiverList = res.responseData;

        if (this.feeWaiverList[0] != undefined) {
          this.currentFeeWaiverId = this.feeWaiverList[0].feeWaiver.feeWaiverId
        }


      }

    }, error => {

      console.log("error ", error);

    })
  }

  onChangeLearningCenter(value) {
    this.selectedCriteria =null
    this.loadPreSchools()
    // console.log("learningcenters",value)
    // let url = "http://192.0.0.65:8080/ECD-CriteriaManagement-1.0/service/criteria/getAllCriteriaLearningCenters?criteriaId=C0008";
    // this.commonService.processGet(url).subscribe(res => {

    //   if (res.responseCode == 1) {

    //     this.LearningList = res.responseData;
    //     console.log("response data ", this.LearningList)
    //   }

    // }, error => {

    //   console.log("error ", error);
    //   console.log("response dataz ", this.LearningList)
    // })
    console.log("learningcenters", value)
    this.loadLr;
  }

  validation() {

    this.addForm = new FormGroup({

      name: new FormControl(['', Validators.required]),
      status: new FormControl(),
      gender: new FormControl(),
      jDate: new FormControl(),
      lDate: new FormControl(),



    })
    this.preSchoolForm = new FormGroup({

      name: new FormControl(['', Validators.required]),
      address: new FormControl(),
      district: new FormControl(),
      ds: new FormControl(),
      tp: new FormControl(),
      province: new FormControl(),
      status: new FormControl(),


    })
    this.selectForm = new FormGroup({


      radio1: new FormControl(),
      radio2: new FormControl(),
      learningCenter: new FormControl(),
      criteria: new FormControl()

    })
  }


  loadDistricts(selectedProvince) {

    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getDistrictList?id=" + selectedProvince).subscribe(res => {
      if (res.responseCode == 1) {

        this.districtList = res.responseData
        this.selectedDistrict = this.districtList[0]
          for (let a of this.districtList) {
         
              if (this.selectedDistrict != undefined || this.selectedDistrict != null) {
                this.getDSList(this.selectedDistrict.id)
              }
           
          }
         
        
        }


     


    }), error => {
      console.log("ERROR")
    }
  }
  getDSList(id) {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getDSList?id=" + id).subscribe(res => {
      if (res.responseCode == 1) {

        this.dsList = res.responseData
        this.selectedDS = this.dsList[0]
        if (this.selectedDS != undefined || this.selectedDS != null) {
         
        }

      }


    }), error => {
      console.log("ERROR")
    }
  }

  
  loadLr() {

    console.log("learningcenters", )
    let url = API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllCriteriaLearningCenters?criteriaId=C0008";
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.LearningList = res.responseData;

      }

    }, error => {

      console.log("error ", error);

    })

  }

  createFW() {

console.log("pre",this.newPreSchoolId)
    if (this.selectedCriteria == undefined) {
      this.selectedCriteria = new Criteria()
      this.selectedCriteria.id = 0
      this.feeWaiverData.feeWaiver.criteriaId = this.selectedCriteria.id;

    } else {
      this.feeWaiverData.feeWaiver.criteriaId = this.selectedCriteria.id;
    }

    if(this.newPreSchoolId!=0){
      this.feeWaiverData.feeWaiver.learningCenter =this.newPreSchoolId
      this.feeWaiverData.feeWaiver.type=2
    }else if(this.newPreSchoolId==0){
 
      this.feeWaiverData.feeWaiver.learningCenter = this.selectedLearningCenter.sn;
      this.feeWaiverData.feeWaiver.type=1
    }

   
    this.feeWaiverData.feeWaiver.userInserted = this.userData[0].userId
    this.feeWaiverData.student.userInserted = this.userData[0].userId
    this.feeWaiverData.feeWaiver.feeWaiverId = this.currentFeeWaiverId

    console.log("to be saved",this.feeWaiverData)
    this.commonService.processPost(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/insertFeeWaiver", this.feeWaiverData).subscribe(res => {

     this.loadingMask=true
      
      if (res.responseCode == 1) {
        this.success = true
        this.loadingMask=false
        this.msg = "You have successfully created a user!"
        this.getPreSchool();
        setTimeout(() => {

          this.resetFields()
          this.success = false
        }, 1000)
      } else {
        this.error = true;
        this.loadingMask=false
        this.msg = "Cant connect to the server"
        setTimeout(() => {
          this.error = false;
          this.success = false
        }, 1000)
      }

    }), error => {
      this.error = true;
      this.loadingMask=false
      this.msg = "Cant connect to the server"
      setTimeout(() => {
        this.error = false;
        this.success = false
      }, 1000)
    }
  }

  onChangeCheckBox(student, evt) {

    console.log("event", evt)
    console.log("student", student)

    let type
    if (evt) {
      type = 1
    } else if (!evt) {
      type = 2
    }



    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updateFeeWaiverStatus?status=" + type + "&studentId=" + student.student.studentId + "&userId=" + this.userData[0].userId).subscribe(res => {

      if (res.responseCode == 1) {
        console.log("Updated")

      }
    }), error => {
      this.error = true;
      this.msg = "Can't connect to the server"
      setTimeout(() => {
        this.error = false;
        this.success = false
      }, 1000)
    }

  }

  clearFW() {

    this.feeWaiverData.student.name = ""
    this.feeWaiverData.student.gender = 1
    this.feeWaiverData.student.status = 1
    this.feeWaiverData.student.leaveDate = new Date()
    this.feeWaiverData.student.regDate = new Date()
  }

  resetFields() {
    // this.feeWaiverModel.studentId = ""
    // this.feeWaiverModel.studentName = ""
    // this.feeWaiverModel.gender = 1
    // this.feeWaiverModel.joinedDate = new Date()
    // this.feeWaiverModel.leftDate = new Date()
  }

  collapse(evt) {
    console.log("COLLAPSE", evt)
  }
  expand(event) {

    console.log("EXPAND", event.node.label)
    this.previouslySelectedNode.id = this.selectedNode.id
    this.previouslySelectedNode.label = this.selectedNode.label
    this.previouslySelectedNode.type = this.selectedNode.type
    this.previouslySelectedNode.code = this.selectedNode.code
    this.previouslySelectedNode.districtId = this.selectedNode.districtId
    this.previouslySelectedNode.dsID = this.selectedNode.dsID
    this.previouslySelectedNode.provinceId = this.selectedNode.provinceId
  }
  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;
    if (event.node.type == "province") {

    }
    else if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(event.node.provinceId, event.node.code, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.code, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.code, event.node.type)
    }

    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }


  nodeSelected(event) {
   
    this.getAllLearningCentersbyId(event.node.districtId, event.node.code, event.node.type)
    this.showLearningCenters.show()
   
   
    this.getAllLocations()
    
  
  }



  getAllLearningCenters() {
    this.selectedLearningCenter = new Data()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCenters").subscribe(res => {

      this.learningCenterList = res.responseData;

      if (this.selectedLearningCenter.sn == null) {
        this.selectedLearningCenter = this.learningCenterList[0]
      }

    }), error => {
      console.log("ERROR")
    }
  }

  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()

        node1.children.push(node)






      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  getAllLocations() {
    console.log("userid",this.userId)
    this.selectedNode = new Node()
    let type
  
    if (this.userId == 1) {
      type = "root"
    } else if (this.userId == 2 || this.userId == 3) {
      type = "node"
    }


    this.loading = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loading = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == type && this.businessList[i].id == parseInt(this.userId)) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }
  getAllLearningCentersbyId(id1, id2, type) {

    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id1=" + id1 + "&id2=" + id2 + "&type=" + type).subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;

        this.selectedLearningCenter = this.learningCenterList[0]

        this.getPreSchool()
      }


    }), error => {
      console.log("ERROR")
    }
  }



  changeLearningCenter() {
    this.learningCenterList = new Array<Data>()
    this.selectedLearningCenter = new Data()
  }

  changeFIG(evt) {
    this.selectedCriteria = null
    this.learningCenterList = new Array<Data>()
    this.selectedLearningCenter = new Data()
    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getFIGSelected").subscribe(res => {
      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData

        this.selectedLearningCenter = this.learningCenterList[0]
        this.getPreSchool()
        this.showLearningCenters.show()
      }

    }), error => {
      console.log("ERROR")
    }
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }


  editStudent(studentId) {

    this.taskType = 'edit'
    let studentList = new Array<FeeWaiverStudent>()
    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getStudentbyId?studentId=" + studentId).subscribe(res => {
      if (res.responseCode == 1) {
        studentList = res.responseData
        this.feeWaiverData.student = studentList[0]
        this.feeWaiverData.student.regDate = this.datePipe.transform(studentList[0].regDate, 'yyyy-MM-dd')
        this.feeWaiverData.student.leaveDate = this.datePipe.transform(studentList[0].leaveDate, 'yyyy-MM-dd')

        this.addModel.show()
      }
    })
  }

  editFW() {

    this.feeWaiverData.student.userModified = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updateStudent", this.feeWaiverData.student).subscribe(res => {

      console.log("Sucess?", res.responseCode)
      if (res.responseCode == 1) {
        this.success = true
        this.msg = "You have successfully updated the user!"
        this.getPreSchool();
        setTimeout(() => {

          this.success = false
          this.addModel.hide()
        }, 1000)
      } else {
        this.error = true;
        this.msg = "Cant connect to the server"
        setTimeout(() => {
          this.error = false;

          this.addModel.hide()
        }, 1000)
      }

    }), error => {
      this.error = true;
      this.msg = "Cant connect to the server"
      setTimeout(() => {
        this.error = false;
        this.addModel.hide()
      }, 1000)
    }
  }

}
