import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeewaiverRegistrationComponent } from './feewaiver-registration.component';

describe('FeewaiverRegistrationComponent', () => {
  let component: FeewaiverRegistrationComponent;
  let fixture: ComponentFixture<FeewaiverRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeewaiverRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeewaiverRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
