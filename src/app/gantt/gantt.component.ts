import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import { Activity } from '../map-component/map-component.component';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
declare var google;



@Component({
  selector: 'app-gantt',
  templateUrl: './gantt.component.html',
  styleUrls: ['./gantt.component.css'],
  providers:[CommonWebService]
})
export class GanttComponent implements OnInit {
  data: any;
  activity: any;

  @Input('activityId') activityId: number;
  public gantt_ChartData;
  // public activityId
  constructor(public commonService: CommonWebService,public activateRoute:ActivatedRoute) {
    
   }

  ngOnInit() {
   
    console.log("Df")
    google.charts.load('current', {'packages':['gantt']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Task ID');
      data.addColumn('string', 'Task Name');
      data.addColumn('string', 'Resource');
      data.addColumn('date', 'Start Date');
      data.addColumn('date', 'End Date');
      data.addColumn('number', 'Duration');
      data.addColumn('number', 'Percent Complete');
      data.addColumn('string', 'Dependencies');

      data.addRows([
        ['2014Spring', 'Spring 2014', 'spring',
         new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
        ['2014Summer', 'Summer 2014', 'summer',
         new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
        ['2014Autumn', 'Autumn 2014', 'autumn',
         new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
        ['2014Winter', 'Winter 2014', 'winter',
         new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
        ['2015Spring', 'Spring 2015', 'spring',
         new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
        ['2015Summer', 'Summer 2015', 'summer',
         new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
        ['2015Autumn', 'Autumn 2015', 'autumn',
         new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
        ['2015Winter', 'Winter 2015', 'winter',
         new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
        ['Football', 'Football Season', 'sports',
         new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
        ['Baseball', 'Baseball Season', 'sports',
         new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
        ['Basketball', 'Basketball Season', 'sports',
         new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
        ['Hockey', 'Hockey Season', 'sports',
         new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
      ]);

      var options = {
        height: 400,
        gantt: {
          trackHeight: 30
        }
      };

      var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

      chart.draw(data, options);
    }
      
  }
  ngOnChanges(changes: SimpleChanges) {

    google.charts.load('current', {'packages':['gantt']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('string', 'Task ID');
      data.addColumn('string', 'Task Name');
      data.addColumn('string', 'Resource');
      data.addColumn('date', 'Start Date');
      data.addColumn('date', 'End Date');
      data.addColumn('number', 'Duration');
      data.addColumn('number', 'Percent Complete');
      data.addColumn('string', 'Dependencies');

      data.addRows([
        ['2014Spring', 'Spring 2014', 'spring',
         new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
        ['2014Summer', 'Summer 2014', 'summer',
         new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
        ['2014Autumn', 'Autumn 2014', 'autumn',
         new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
        ['2014Winter', 'Winter 2014', 'winter',
         new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
     
      ]);

      var options = {
        height: 400,
        gantt: {
          trackHeight: 30
        }
      };

      var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

      chart.draw(data, options);
    }


  }

  loadActivityDetails(id){

    this.commonService.processGet(API_URL+"ECD-ActivityMaintenance-1.0/service/Activities/getActivityById?activityId="+id).subscribe(res => {
     
      
        if(res.responseCode==1){
          this.activity = res.responseData
         
          google.charts.setOnLoadCallback(this.drawChart(this.activity));

    
          
        }

      
          }), error => {
            console.log("ERROR")
          }
  }
  


  drawChart(act) {

    this.data = new google.visualization.DataTable();
    this.data.addColumn('string', 'Task ID');
    this.data.addColumn('string', 'Task Name');
    this.data.addColumn('string', 'Resource');
    this.data.addColumn('date', 'Start Date');
    this.data.addColumn('date', 'End Date');
    this.data.addColumn('number', 'Duration');
    this.data.addColumn('number', 'Percent Complete');
    this.data.addColumn('string', 'Dependencies');

    this.data.addRows([
      ['2014Spring', 'Spring 2014', 'spring',
       new Date(2014, 2, 22), new Date(2014, 5, 20), null, 100, null],
      ['2014Summer', 'Summer 2014', 'summer',
       new Date(2014, 5, 21), new Date(2014, 8, 20), null, 100, null],
      ['2014Autumn', 'Autumn 2014', 'autumn',
       new Date(2014, 8, 21), new Date(2014, 11, 20), null, 100, null],
      ['2014Winter', 'Winter 2014', 'winter',
       new Date(2014, 11, 21), new Date(2015, 2, 21), null, 100, null],
      ['2015Spring', 'Spring 2015', 'spring',
       new Date(2015, 2, 22), new Date(2015, 5, 20), null, 50, null],
      ['2015Summer', 'Summer 2015', 'summer',
       new Date(2015, 5, 21), new Date(2015, 8, 20), null, 0, null],
      ['2015Autumn', 'Autumn 2015', 'autumn',
       new Date(2015, 8, 21), new Date(2015, 11, 20), null, 0, null],
      ['2015Winter', 'Winter 2015', 'winter',
       new Date(2015, 11, 21), new Date(2016, 2, 21), null, 0, null],
      ['Football', 'Football Season', 'sports',
       new Date(2014, 8, 4), new Date(2015, 1, 1), null, 100, null],
      ['Baseball', 'Baseball Season', 'sports',
       new Date(2015, 2, 31), new Date(2015, 9, 20), null, 14, null],
      ['Basketball', 'Basketball Season', 'sports',
       new Date(2014, 9, 28), new Date(2015, 5, 20), null, 86, null],
      ['Hockey', 'Hockey Season', 'sports',
       new Date(2014, 9, 8), new Date(2015, 5, 21), null, 89, null]
    ]);

    var options = {
      height: 400,
      gantt: {
        trackHeight: 30
      }
    };

    var chart = new google.visualization.Gantt(document.getElementById('chart_div'));

    chart.draw(this.data, options);
  }

}
