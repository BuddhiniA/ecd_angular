import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs, Headers, RequestOptions } from "@angular/http";

@Injectable()
export class CommonWebService {
  userId: any;


  constructor(private http: Http) {
    this.userId = sessionStorage.getItem("bisId");
  }

  /**
   * 
   * @param url - Destination URL
   */
  public processGet(url: any) {

    let options: RequestOptionsArgs = new RequestOptions();
    if (this.userId != null) {

     
      let header: Headers = new Headers();

      header.set("system", "ECDCMS");
      header.set("organization", "ECD");
      header.set("division", "ECDLK");
      header.set("countryCode", "LK");
      header.set("branch", "HeadOffice");
      header.set("department", "DefaultDepartment");
      header.set("room", "DefaultRoom");
      header.set('bisId', this.userId);
      // header.set("Access-Control-Allow-Origin", "*");
      header.set("content-type", "application/json");
      options.headers = header;
      // options.withCredentials=true

    }



    return this.http.get(url, options).map(this.extractData);

  }
  /**
   * 
   * @param url 
   * @param options - Options with header params. If there is no header put null
   */
  public processGetWithHeaders(url: string, options: RequestOptionsArgs) {

    let head = new Headers()

    head = options.headers;

    if (this.userId != null) {
      head.append('bisId', this.userId);
    }

    options.headers = head;

    return this.http.get(url, options).map(this.extractData);
  }

  /**
   * 
   * @param url - Destination URL
   * @param query - JSON query of request payload
   */
  public processPost(url: string, query: any) {


    let options: RequestOptionsArgs = new RequestOptions();
    if (this.userId != null) {

      let header: Headers = new Headers();

      header.set("system", "ECDCMS");
      header.set("organization", "ECD");
      header.set("division", "ECDLK");
      header.set("countryCode", "LK");
      header.set("branch", "HeadOffice");
      header.set("department", "DefaultDepartment");
      header.set("room", "DefaultRoom");
      header.set('bisId', this.userId);
      options.headers = header;

    }

    return this.http.post(url, query, options).map(this.extractData);


  }

  /**
   * 
   * @param url 
   * @param query 
   * @param options - Option with header params. If there is no header put null
   */
  public processPostWithHeaders(url: string, query: any, options: RequestOptionsArgs) {

    let head = new Headers()

    head = options.headers;

    if (this.userId != null) {
      head.append('bisId', this.userId);
    }

    options.headers = head;

    return this.http.post(url, query, options).map(this.extractData);
  }


  private extractData(res: Response) {

    return res.json() || {};
  }

}
