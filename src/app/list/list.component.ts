import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { ActivityDashBoard } from '../map-component/map-component.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Activity } from '../define-activity/define-activity.component';

export class Location {
  activityId
  type
  activityDescription
  longitude
  latitude
  status
  learningCenter
  address
  allMilestones
  completedMilestones
}
export class Data {
  selectedLoc
}
@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [CommonWebService]
})
export class ListComponent implements OnInit {
  // @Input('year') selectedLocation:string;
  @Input('selectedLocation') selectedLocation: Data;
  data: any
  public locationList = new Array<Location>()
  public activityList = new Array<ActivityDashBoard>()
  public progressVal
  public activity = new Activity()

  public selectedActivity = new ActivityDashBoard()
  display: boolean = false;
  displayMilestoneDetails: boolean = false;
  ngOnInit(): void {
    this.getAllActivities()
  }

  ngOnChanges(changes: SimpleChanges) {

    if (this.selectedLocation != null || this.selectedLocation != undefined) {
      console.log("Sss", this.selectedLocation)
      this.getActivityLocations(this.selectedLocation)
    }


  }

  getActivityLocations(a) {
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getCensusDataForMaps?locCode=" + a.locLevel).subscribe(res => {
      // this.commonService.processGet(GetActivtyList).subscribe(res => {
      if (res.responseCode == 1) {

        this.activityList = res.responseData

        this.progressVal
      }


    }), error => {
      console.log("ERROR")
    }
  }

  constructor(public commonService: CommonWebService, public router: Router, public route: ActivatedRoute) {
    // this.data = {
    //   labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    //   datasets: [
    //     {
    //       label: 'My First dataset',
    //       backgroundColor: '#42A5F5',
    //       borderColor: '#1E88E5',
    //       data: [65, 59, 80, 81, 56, 55, 40]
    //     },
    //     {
    //       label: 'My Second dataset',
    //       backgroundColor: '#9CCC65',
    //       borderColor: '#7CB342',
    //       data: [28, 48, 40, 19, 86, 27, 90]
    //     }
    //   ]
    // }
  }
  getAllActivities() {


    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getCensusDataForMaps?locCode=NCP").subscribe(res => {


      this.locationList = res.responseData
      console.log("Loc List", this.locationList)



    }), error => {
      console.log("ERROR")
    }
  }
  openView(a) {

    this.router.navigate(['gantt-view', { activity: a.activityId }])


  }
  showDialog(a) {
    this.selectedActivity = a
    console.log("avc", a)
    this.display = true;
  }
  openMilestone() {
    this.display = false
    this.commonService.processGet(API_URL + "ECD-ActivityMaintenance-1.0/service/Activities/getActivityById?activityId="+this.selectedActivity.activityId).subscribe(res => {


      this.activity = res.responseData
      console.log("Activty", this.activity.milestoneList)



    }), error => {
      console.log("ERROR")
    }
    this.displayMilestoneDetails = true;
  }
}



