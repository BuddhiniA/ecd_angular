import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShorttermTrainerProgramComponent } from './shortterm-trainer-program.component';

describe('ShorttermTrainerProgramComponent', () => {
  let component: ShorttermTrainerProgramComponent;
  let fixture: ComponentFixture<ShorttermTrainerProgramComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShorttermTrainerProgramComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShorttermTrainerProgramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
