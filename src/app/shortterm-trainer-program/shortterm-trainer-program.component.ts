import { Component, OnInit, ViewChild } from '@angular/core';

import { ShortTermModel } from "../model/ShortTerm";
import { ShortTermParticipantModel } from "../model/ShortTermParticipant";
import { Router } from '@angular/router';
import { ShortAddModel } from '../model/ShortAdd';
import { ResPoolModel } from '../model/ResPool';
import { ShortProDetailsModel } from '../model/ShortProDetails';
import { ResourcePerson } from '../model/ResourcePerson';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { TreeNode } from 'primeng/primeng';
import { ModalDirective } from 'ngx-bootstrap';
import { ProgramData } from '../model/ProgramData';
import { DatePipe } from '@angular/common';

export class Resource {
  id
  resourceName
  institute
  qualification
  status
  userEnterd
  userModified
  dateInserted
  dateModified
}

export class Other {
  personId
  personName
  remarks;
  status
  coordinator
}


@Component({
  selector: 'app-shortterm-trainer-program',
  templateUrl: './shortterm-trainer-program.component.html',
  styleUrls: ['./shortterm-trainer-program.component.css'],
  providers: [CommonWebService, DatePipe]
})

export class ShorttermTrainerProgramComponent implements OnInit {
  viewType: number;
  nodeType: number;
  type: string;
  bisId: any;
  error: boolean;
  remark: FormControl;
  name: FormControl;
  programForm: FormGroup;
  selectedResourcePersonList = new Array<ResourcePerson>();
  location: any;
  programData = new ProgramData();
  selectedNode = new Node();
  files: Node[];
  businessList: any[];
  loadingTree: boolean;
  userData: any;
  public success: boolean;

  public shortModel = Array<ShortTermModel>();
  public shortDetail = new ShortTermModel();

  public participantModel = Array<ShortTermParticipantModel>();
  public participantDetail = new ShortTermParticipantModel();
  public d1;
  public d2;
  public programId: number;
  public rType;
  public shortTermModel = Array<ShortProDetailsModel>();
  public shortTermDetail = new ShortProDetailsModel;
  public shortTerm = new ShortProDetailsModel;
  public shortTermAddModel = Array<ShortAddModel>();
  public shortTermAddDetail = new ShortAddModel;
  public shortTermAdd = new ShortAddModel;
  public resModel = Array<ResPoolModel>();
  public resDetail = new ResPoolModel;
  public selectedResource;
  public selectedOtherResource = new ResourcePerson();
  public selectedOtherResourcesList = new Array<ResourcePerson>()
  public otherResourcesList = new Array<ResourcePerson>()
  public PrimaryProgram;
  public fromPool;
  public fromPoolList = new Array<ResourcePerson>();
  public shortTermForm: FormGroup;
  public shortTermForm2: FormGroup;
  public resourcePool = new Array<Other>();

  @ViewChild("makeShortSetting") public makeShortSetting;
  @ViewChild("updateShortSetting") public updateShortSetting;
  @ViewChild("viewParticipantSetting") public viewParticipantSetting;
  @ViewChild("updateParticipantSetting") public updateParticipantSetting;
  @ViewChild("addParticipantView") public addParticipantView: ModalDirective;

  constructor(public commonService: CommonWebService, public route: Router, private datePipe: DatePipe) {
    this.validation();
  }

  ngOnInit() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.bisId = this.userData[0].extraParams
    if (this.bisId < 38 && this.bisId > 12) {
      this.nodeType = 2
      this.type = 'district';
    } else if (this.bisId > 37) {
      this.nodeType = 2
      this.type = 'ds'
    } else {
      this.nodeType = 0
      this.type = "root"
    }
    this.getAllLocations()
    this.validation()

    this.loadToGrid();
  }

  openCreateShortProgram() {
    this.viewType = 1
    this.getResources();
    this.programData = new ProgramData()
    this.rType = 1;
    this.programData.program.status = 1
    this.programData.program.startDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.programData.program.endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.selectedResourcePersonList = new Array<ResourcePerson>()
    this.makeShortSetting.show();
  }

  getProgramId(sh) {
    
    this.selectedResourcePersonList = new Array<ResourcePerson>();
    this.viewType = 2
    this.programData = new ProgramData
    this.getResources();
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getShortProgramsById?programId=" + sh.programId;
    this.commonService.processGet(url).subscribe(res => {
      if (res.responseCode == 1) {
        this.programData = res.responseData[0];
        this.selectedResourcePersonList = this.programData.resourcePool
        console.log("response data ", this.participantDetail)
        this.makeShortSetting.show()
      }
    }, error => {
      console.log("error ", error);
      console.log("response dataz ", this.participantDetail)
    })
  }

  validation() {

    this.programForm = new FormGroup({
      programName: new FormControl(['', Validators.required]),
      description: new FormControl(['', Validators.required]),
      startDate: new FormControl(['', Validators.required]),
      endDate: new FormControl('', [Validators.required]),
      location: new FormControl(''),
      budget: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      status: new FormControl(''),
      type: new FormControl(),
      resource: new FormControl(),

    })

    this.name = new FormControl('', Validators.required),
      this.remark = new FormControl()

  }


  getProgramIdParticipant(sh: number) {

    this.programId = sh;
    this.route.navigate(['/registered-participants', { programName: 'shortTerm', programId: this.programId }])
    // this.loadParticipants();
    //this.viewParticipantSetting.show();
    console.log("shortparticipant", sh);
  }

  editParticipant(p) {

    this.updateParticipantSetting.show();
  }

  selctedCoordinatorforPool(event, param) {
    console.log("Here")
    let isChecked = event.target.checked
    var index

    if (isChecked && this.resourcePool.length != 0) {
      index = this.resourcePool.indexOf(param);

      this.resourcePool[index].coordinator = 1
      console.log("Ressource Pool", this.resourcePool[index].coordinator)
    } else if (!isChecked && this.resourcePool.length != 0) {

      index = this.resourcePool.indexOf(param);
      this.resourcePool[index].coordinator = 0
    }
  }



  loadToGrid() {
    // console.log("User Id ",this.validator.getUserId()); 
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getShortTermPrograms";
    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.responseCode == 1) {

        this.shortTermModel = res.responseData;
        console.log("response data ", this.shortTermModel)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.shortTermModel)
    })
  }

  createShortProgram() {

    console.log("save createMasterProgram")
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveShortTerm";

    this.programData.userInserted = this.userData[0].userId


    this.commonService.processPost(url, this.programData).subscribe(res => {



      if (res.responseCode == 1) {

        this.success = true;

        this.loadToGrid()
        setTimeout(() => {
          this.success = false
          this.makeShortSetting.hide()
        }, 2000)
      } else {
        this.error = true
        setTimeout(() => {
          this.error = false
          this.makeShortSetting.hide()
        }, 2000)
      }

    }, error => {
      this.error = true
      setTimeout(() => {
        this.error = false
        this.makeShortSetting.hide()
      }, 2000)


    })

  }

  saveMasterChanges() {

    console.log("save createMasterProgram")
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveShortTerm";

    this.shortTermAdd.programDetails = this.shortTerm;
    this.shortTermAdd.masterTrainerList = this.fromPoolList;
    this.shortTermAdd.guestList = this.selectedOtherResourcesList;


    this.commonService.processPost(url, this.shortTermAdd).subscribe(res => {


      let response = res.responseData
      console.log("response ", response);
      if (response.responseCode == 1) {


        this.success = true

      } else if (response.flag == 808) {

      } else {

      }

      this.loadToGrid();
    }, error => {

      // this.failMessage = "Error in connection. Try again later!"
      // this.fail.show();

    })
  }

  loadParticipants() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllShortTermParticipants?programId=" + this.programId;
    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.responseCode == 1) {

        this.participantModel = res.responseData;
        console.log("response data ", this.participantDetail)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.participantDetail)
    })

  }


  getResources() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources";
    this.commonService.processGet(url).subscribe(res => {
      if (res.responseCode == 1) {

        this.resModel = res.responseData;
        this.selectedResource = this.resModel[0]

      }

    }, error => {

      console.log("error ", error);

    })
  }



  getAllLocations() {

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    console.log("type>>", this.type)
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == this.type && this.businessList[i].nodeType == this.nodeType) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false
            console.log("SELECTED NODE", this.selectedNode)
          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }


  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.type == "district") {


        if (this.businessList[i].type == this.type && this.businessList[i].districtId == (node1.provinceId + "") + (node1.code + "")) {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)

        }
      }
      else {
        if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)
          console.log("Second Node", node)
        }

        if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)
        }




        if (node1.type == "province") {
          if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            node1.children.push(node)


          }
        }
      }



    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;
    if (event.node.type == "province") {

    }


    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }


  nodeSelected(event) {
    this.programData.program.locationId = event.node.id
    this.programData.program.locationName = event.node.label
    this.location = event.node.label
    this.collapseAll()
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  openAddParticipant(a) {
    this.programId = a
    console.log("A", this.programId)

    this.addParticipantView.show()

  }

  addResourcePersons() {


    if (this.rType == 1 && this.selectedResource != null) {
      let person = new ResourcePerson()
      person.personId = this.selectedResource.id
      person.personName = this.selectedResource.resourceName
      person.coordinator = 0
      this.programData.resourcePool.push(person)
      this.selectedResourcePersonList.push(person)
    }
    else if (this.rType == 2 && this.selectedOtherResource != null) {

      let person = new ResourcePerson()

      person.personName = this.selectedOtherResource.personName
      person.remarks = this.selectedOtherResource.remarks
      person.coordinator = 0
      this.programData.guestList.push(person)
      this.selectedResourcePersonList.push(person)

      this.selectedResource.personName = ""
      this.selectedResource.remarks = ""

    }
  }

  removeRows(person) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool.splice(index2, 1)
    }
    if (index3 > -1) {
      this.programData.guestList.splice(index3, 1)
    }



  }

  removeRowswhenEdit(person: ResourcePerson) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool[index2].status = 2
    }
    if (index3 > -1) {
      this.programData.guestList[index3].status = 2
    }



  }

  checked(e, person) {

    console.log("Per", person)
    console.log("sdsdsd", this.programData.resourcePool)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    console.log("index", index2)
    console.log("in", index3)
    if (index2 > -1) {
      this.programData.resourcePool[index2].coordinator = 1
    }
    if (index3 > -1) {

      this.programData.guestList[index3].coordinator = 1
    }
  }

}
