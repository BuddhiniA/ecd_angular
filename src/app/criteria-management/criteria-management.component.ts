import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';


import { Router } from '@angular/router';


import { CommonWebService } from "../common-web.service";
import { CensusDataField } from "../model/CensusDataField";

import { API_URL } from '../app_params';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoggedInUserModel } from '../model/loggedInUserModel';


export class CriteriaInfo {
  criteria: Criteria = new Criteria();
  subCriteria = new Array<SubCriteria>()
}
export class SubCriteria {
  seqNo
  censusField
  censusFieldDes
  operator
  value
  points
  function = null
  status
  operatorsList
}
export class Criteria {


  criteriaId
  criteriaName
  criteriaType:number
  dateInserted
  dateModified
  id
  status
  userEnterd
  userModified


}
@Component({
  selector: 'app-criteria-management',
  templateUrl: './criteria-management.component.html',
  styleUrls: ['./criteria-management.component.css'],
  providers: [Criteria, CommonWebService, CriteriaInfo, SubCriteria, CensusDataField,LoggedInUserModel]
})
export class CriteriaManagementComponent implements OnInit {
  loactionForDistrict: string;
  nodeBelongs: any;
  userGroup: string;
  bisId: any;
  userId: any;

  public criteriaList = new Array<Criteria>()
  @ViewChild("createCriteriaView") public createCriteriaView: ModalDirective;
  @ViewChild("createSubCriteria") public createSubCriteria: ModalDirective;
  @ViewChild("showCriteria") public showCriteria: ModalDirective;

  @ViewChild("updateriteria") public updateriteria: ModalDirective;
  @ViewChild("updateSubCriteria") public updateSubCriteria: ModalDirective;
  constructor(public criteria: Criteria,
    public commonService: CommonWebService,
    public criteriaInfo: CriteriaInfo, public subCriteria: SubCriteria, public route: Router, public userData: LoggedInUserModel) {


    this.validation()
    this.statusList = [

      { label: 'Active', value: { id: 1 } },
      { label: 'Inactive', value: { id: 2 } }
    ];
  }

  public criteriaTypeVal
  public statusList = new Array<any>()
  public subCriteriaList = new Array<SubCriteria>();
  public fieldList = new Array<CensusDataField>();
  public censusField = new CensusDataField();
  public operatorList = new Array<String>();
  public selectedField: CensusDataField;
  public selectedSubCriteriaStatus = "1";
  public selectedOperator;
  public selectionFunction;
  public success: boolean;
  public error: boolean
  public warn: boolean
  public selectedStatus
  criteriaForm: FormGroup
  subCriteriaForm: FormGroup
  public subCriteriaAdded = new Array<SubCriteria>();
  public lastIndex

  blocked: boolean;
  public isLoading;


  validation() {

    this.criteriaForm = new FormGroup({
      criteriaName: new FormControl('', [Validators.required]),
      criteriaType: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    })
    this.subCriteriaForm = new FormGroup({
      censusField: new FormControl('', [Validators.required]),
      criteriaType: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    })
  }

  ngOnInit() {
    this.userGroup = sessionStorage.getItem('userGroup');
    this.load_All_CriteriaList_for_Grid();
    this.getUser()


  }

  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));

    this.userId = this.userData[0].userId
    this.bisId =this.userData[0].extraParams
    
    if(this.userGroup="ECDMasterGrp"){
      if(this.bisId!=undefined||this.bisId!=null){
        this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getLocation").subscribe(res => {
         if(res.responseCode==1){
          this.nodeBelongs = res.responseData
          this.loactionForDistrict= this.nodeBelongs[0].provinceId+this.nodeBelongs[0].code
          console.log("NODE BELONGS",this.nodeBelongs)
         
        

        }
        
    
        }), error => {
          console.log("ERROR")
        }
      }
    }


  }

  openCreateCriteria() {
    console.log("show modal");
    this.criteriaInfo = new CriteriaInfo()
    this.selectedStatus = this.statusList[0]
    this.selectedStatus = 1
    this.criteriaTypeVal = 1
    this.createCriteriaView.show();
  }

  openAddSubCriteriaForm() {
    console.log("show modal fxf", this.selectedSubCriteriaStatus);

    this.subCriteria.function = 1
    this.loadFields();
    this.createSubCriteria.show();






  }
  onclick() {
    console.log("hgjgj")
  }
  onChange() {
    console.log("Field", this.selectedField.fieldCode)
    this.loadOperator();
  }

  loadOperator() {
    let a = new Array<String>();
    let ar: String = this.selectedField.operators;
    this.operatorList = ar.split(",");
    this.selectedOperator = this.operatorList[0]
    console.log(this.operatorList[0], ar[1])

  }
  load_All_CriteriaList_for_Grid() {
    this.isLoading =true
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllCriteria").subscribe(res => {
      if (res.responseCode == 1) {
        this.isLoading = false
        this.criteriaList = res.responseData
      } else {
        console.log("Error loading criteria")
      }


    }), error => {
      console.log("ERROR")
    }
  }


  showCriteriaDetails(cri) {
    console.log("ID", cri)
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getCriteriaDetails?criteriaId=" + cri.id).subscribe(res => {
      //this.commonService.processGet("./assets/criteriadetail.json").subscribe(res=>{
      if (res.responseCode == 1) {
        this.criteriaInfo = res.responseData
        this.selectedStatus = this.criteriaInfo.criteria.status
        this.criteriaTypeVal = this.criteriaInfo.criteria.criteriaType
        // for (let a of this.criteriaInfo.subCriteria) {
        //   var index = this.criteriaInfo.subCriteria.indexOf(a)
        //   if (a.status == 1) {
        //     this.criteriaInfo.subCriteria[index].status = this.statusList[0].label
        //   } else if (a.status == 2) {
        //     this.criteriaInfo.subCriteria[index].status = this.statusList[1].label
        //   }
        //   console.log("Data", this.criteriaInfo.criteria.criteriaId)

        // }
        this.showCriteria.show();
      }
      else {
        console.log("ERROR LOADING CRITERIA BY ID")
      }

    }), error => {
      console.log("ERROR")
    }



  }
  loadtoSubGrid() {


    let subCrit = new SubCriteria();

    subCrit.seqNo = this.subCriteria.seqNo
    subCrit.censusFieldDes = this.selectedField.fieldName
    subCrit.censusField = this.selectedField.fieldCode;
    subCrit.function = this.subCriteria.function == 1 ? "AND" : "OR"
    subCrit.operator = this.selectedOperator
    subCrit.points = this.subCriteria.points
    subCrit.value = this.subCriteria.value
    subCrit.status = this.statusList[0].label
    // subCrit.status = this.selectedSubCriteriaStatus

    console.log("Value", subCrit.value)

    this.subCriteriaList.push(subCrit)
    this.loadFields();
    // this.removeFromCombo(this.selectedField)
    console.log("selectedone", this.selectedField)

    this.subCriteria.points = ""
    this.subCriteria.value = ""





  }
  setOutput(subcriteria, evt) {
    var index = this.subCriteriaList.indexOf(subcriteria)
    let st = evt.value.id

    if (st == 1) {
      this.subCriteriaList[index].status = this.statusList[0].label
    } else if (st == 2) {
      this.subCriteriaList[index].status = this.statusList[1].label
    }
  }
  removeRow(a) {
    var index = this.subCriteriaList.indexOf(a);
    this.subCriteriaList.splice(index, 1);

  }
  loadFields() {

    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getSubCriteria").subscribe(res => {
      //this.commonService.processGet("./assets/fielddetails.json").subscribe(res=>{

      this.fieldList = res.responseData

      for (let j = 0; j < this.subCriteriaList.length; j++) {
        console.log("j", this.fieldList.length, j)
        // for (let i = 0; i < this.fieldList.length; i++) {
       
        //   if (this.subCriteriaList[j].censusField == this.fieldList[i].fieldCode) {

        //     this.fieldList.splice(i, 1)

        //   }

        // }

      }

      this.selectedField = this.fieldList[0];
      this.loadOperator();


    }), error => {
      console.log("ERROR")
    }
  }
  loadUpdateFields() {
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getSubCriteria").subscribe(res => {

      this.fieldList = res.responseData

      for (let j = 0; j < this.criteriaInfo.subCriteria.length; j++) {
        console.log("j", this.fieldList.length, j)
        for (let i = 0; i < this.fieldList.length; i++) {
          // console.log("Inside",this.subCriteriaList[j].censusField,this.fieldList[i].fieldCode)
          if (this.criteriaInfo.subCriteria[j].censusField == this.fieldList[i].fieldCode) {

            this.fieldList.splice(i, 1)

          }

        }

      }

      this.selectedField = this.fieldList[0];
      this.loadOperator();


    }), error => {
      console.log("ERROR")
    }
  }
  routeToResult(cri) {
    console.log("criteria typpe", cri.criteriaType)
    this.route.navigate(["/criteriaresult-management", { criteriaId: cri.id, criteriaType: cri.criteriaType }])
    // this.route.navigateByUrl("/criteriaresult-management")
  }

  updateCriteria() {

    console.log("UOPDATING>>")
    this.criteriaInfo.subCriteria = new Array<SubCriteria>()
    this.criteriaInfo.criteria.criteriaType = this.criteriaTypeVal
    this.criteriaInfo.criteria.status=this.selectedStatus
    for (let a of this.subCriteriaList) {

      let updatedSubCriteria = new SubCriteria()
      updatedSubCriteria.censusField = a.censusField
      updatedSubCriteria.censusFieldDes = a.censusFieldDes
      updatedSubCriteria.function = a.function
      updatedSubCriteria.operator = a.operator
      updatedSubCriteria.points = a.points
      updatedSubCriteria.seqNo = a.seqNo
      updatedSubCriteria.value = a.value
      updatedSubCriteria.status = 1
      updatedSubCriteria.seqNo = this.lastIndex
      // var index = this.subCriteriaList.indexOf(a)

      // if (a.status = "Active") {
      //   console.log("DDD",a.status)
      //   updatedSubCriteria.status = 1
      //   this.criteriaInfo.subCriteria.push(updatedSubCriteria)
      // } else if (a.status = "Inactive") {
      //   console.log("DDD",a.status)
      //   updatedSubCriteria.status = 2
      this.criteriaInfo.subCriteria.push(updatedSubCriteria)
      // }

      console.log("Sattus", updatedSubCriteria)


      this.lastIndex++
    }

    console.log("PayLoad", this.criteriaInfo.subCriteria)
    this.commonService.processPost(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/updateCriteria", this.criteriaInfo).subscribe(res => {

      console.log("Data", this.criteriaInfo)

      console.log(this.criteriaList.length)

      if (res.responseCode == 1) {
        this.success = true;

        setTimeout(() => {
          this.success = false;

          for (let a of this.subCriteriaList) {
            a.status = 1
            this.subCriteriaAdded.push(a)
            var index = this.subCriteriaList.indexOf(a)
            this.subCriteriaList.splice(index, 1)
          }
          console.log("CI", this.criteriaInfo.subCriteria)
          this.load_All_CriteriaList_for_Grid()
        }, 2000);

      }

    }), error => {
      console.log("ERROR")
    }

  }
  closeAction() {
    this.success = false
    this.createCriteriaView.hide()
  }
  closeUpdateAction() {
    this.success = false
    this.updateriteria.hide()
  }
  createCriteria() {


    // if(this.criteriaTypeVal==1){
    //   this.criteriaInfo.criteria.criteriaType="FIG"
    // }else if(this.criteriaTypeVal==2){
    //   this.criteriaInfo.criteria.criteriaType="Fee Waiver"
    // }else if(this.criteriaTypeVal==3){
    //   this.criteriaInfo.criteria.criteriaType="Material Distribution"
    // }
    console.log("PREV SUB CRITERIA LIST", this.subCriteriaList)
    // this.subCriteriaList.splice(this.subCriteriaList.length - 1, 1)
    console.log("NOW SUB CRITERIA LIST", this.subCriteriaList)
    this.criteriaInfo.criteria.criteriaType = this.criteriaTypeVal
    let i = 1
    for (let a of this.subCriteriaList) {
      var index = this.subCriteriaList.indexOf(a)
      this.subCriteriaList[index].seqNo = i
      if (a.status = "Active") {
        this.subCriteriaList[index].status = 1
      } else if (a.status = "Inactive") {
        this.subCriteriaList[index].status = 2
      }
      i++
    }
    this.criteriaInfo.subCriteria = this.subCriteriaList
    this.criteriaInfo.criteria.status = this.selectedStatus
    this.criteriaInfo.criteria.userEnterd = this.userId

    this.criteriaInfo.criteria.dateInserted = new Date()
    console.log("PayLoad", this.criteriaInfo)
    if (this.criteriaInfo.subCriteria.length != 0) {


      this.commonService.processPost(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/saveCriteria", this.criteriaInfo).subscribe(res => {

        console.log("Data", this.criteriaInfo)

        console.log(this.criteriaList.length)
        if (res.responseCode == 1) {
          this.success = true;
          this.blocked = true
          setTimeout(() => {
            this.success = false;
            this.blocked=false
            this.resetFields_of_critera()
            this.load_All_CriteriaList_for_Grid()

          }, 2000);


        } else {
          console.log("ERROR IN SAVING CRITERIA...")
        }

      }), error => {
        console.log("ERROR")
      }
    } else {
      console.log("ERROR")
      this.error = true
      setTimeout(() => {
        this.error = false;
        this.resetFields_of_critera()


      }, 2000);
    }


  }




  updateCiteriaDetail(cri) {

    this.criteriaInfo = new CriteriaInfo()
    console.log("ID", cri)
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getCriteriaDetails?criteriaId=" + cri.id).subscribe(res => {

      if (res.responseCode == 1) {
        this.criteriaInfo = res.responseData
        console.log("Data", this.criteriaInfo.criteria.criteriaType)
        this.criteriaTypeVal = this.criteriaInfo.criteria.criteriaType
        this.selectedStatus = this.criteriaInfo.criteria.status
        // for(let a of this.criteriaInfo.subCriteria){
        //   var index = this.criteriaInfo.subCriteria.indexOf(a)
        //   if(a.status==1){
        //     this.criteriaInfo.subCriteria[index].status = this.statusList[0].label


        //   }else if(a.status==2){
        //     this.criteriaInfo.subCriteria[index].status = this.statusList[1].label

        //   }
        // }

        this.lastIndex = this.criteriaInfo.subCriteria.length
        this.subCriteriaAdded = this.criteriaInfo.subCriteria
       
        this.criteriaTypeVal=1
        this.updateriteria.show()
      } else {
        console.log("ERROR LOADING CRITERIA BY ID")
      }


    }), error => {
      console.log("ERROR")
    }



  }

  openUpdateSubCriteria() {
    //console.log("show modal",this.selectedSubCriteriaStatus);
    console.log("subcriterialist length", this.subCriteriaList.length)
    if (this.subCriteriaList.length != 0) {
      this.loadUpdateFields()
      this.updateSubCriteria.show();
    } else {
      this.warn = true
      setTimeout(() => {
        this.warn = false;
      }, 2000);

    }

  }
  loadtoUpdateSubGrid() {
    let subCrit = new SubCriteria();

    subCrit.seqNo = this.subCriteria.seqNo
    subCrit.censusFieldDes = this.selectedField.fieldName
    subCrit.censusField = this.selectedField.fieldCode;
    subCrit.function = this.subCriteria.function
    subCrit.operator = this.selectedOperator
    subCrit.points = this.subCriteria.points
    subCrit.value = this.subCriteria.value
    subCrit.status = this.selectedSubCriteriaStatus



    this.criteriaInfo.subCriteria.push(subCrit)
    this.loadUpdateFields();

    this.subCriteria.points = ""
    this.subCriteria.value = ""
  }
  removeUpdateWindowRow(a) {
    console.log("Removing,,")
    var index = this.criteriaInfo.subCriteria.indexOf(a);
    this.criteriaInfo.subCriteria.splice(index, 1);
  }


  resetFields_of_critera() {

    this.selectedStatus = 1
    this.subCriteriaList = new Array<SubCriteria>();
    this.criteriaInfo.criteria.criteriaName = ""
    this.criteriaInfo.criteria.criteriaType = 1
    this.selectedStatus = this.statusList[0].label
  }

}
