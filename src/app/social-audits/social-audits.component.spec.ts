import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialAuditsComponent } from './social-audits.component';

describe('SocialAuditsComponent', () => {
  let component: SocialAuditsComponent;
  let fixture: ComponentFixture<SocialAuditsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialAuditsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialAuditsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
