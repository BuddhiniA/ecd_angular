import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonWebService } from '../common-web.service';
import { ModalDirective } from 'ngx-bootstrap';
import { API_URL } from '../app_params';

export class Audit {
  auditId
  auditDate
  description
  privince
  provinceName
  facilitator
  status
  maleParticipants
  femaleParticipant
  remarks
  userInserted
  userModified
  dateInserted
  dateModified
}

export class Province {
  bisId
  bisName
  bisLevel
  parentId
  locLevel
  status
  userInserted
  userModified
  dateInserted
  daetModified
}

@Component({
  selector: 'app-social-audits',
  templateUrl: './social-audits.component.html',
  styleUrls: ['./social-audits.component.css'],
  providers: [CommonWebService]
})
export class SocialAuditsComponent implements OnInit {

  @ViewChild("viewAuditModel") public viewAuditModel: ModalDirective;
  public auditList = new Array<Audit>()
  public type;
  public audit = new Audit()
  public provinceList = new Array<Province>()

  public success
  public error
  public warn

  public selectedProvince

  constructor(public commonService: CommonWebService) { }

  ngOnInit() {
    this.loadAllAudits()
    this.loadProvince()
  }
  loadAllAudits() {
    this.commonService.processGet(API_URL+"ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getAllSocialAudits").subscribe(res => {

      this.auditList = res.responseData


    }), error => {
      console.log("ERROR")
    }
  }
  newAudit() {
    this.audit = new Audit()
    this.type = "NEW"
    this.audit.status = 1
    this.viewAuditModel.show()
  }

  viewAudit(a) {
    this.audit = a
    this.type = "VIEW"
    this.viewAuditModel.show()
  }

  loadProvince() {
    this.commonService.processGet(API_URL+"ECD-DashboardMaintainance-1.0/service/dashboard/getBisValues?bisId=2").subscribe(res => {

      this.provinceList = res.responseData
      this.selectedProvince = this.provinceList[0]

    }), error => {
      console.log("ERROR")
    }
  }

  saveAudit() {
    console.log("TO BE SAVED", this.audit)
    this.audit.userInserted="Sandali"
    this.audit.privince = this.selectedProvince.bisId
    this.audit.provinceName = this.selectedProvince.bisName
    this.commonService.processPost(API_URL+"ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/insertSocialAudits", this.audit).subscribe(res => {


      if (res.responseCode == 1) {

        this.success = true
        setTimeout(() => {
          this.success = false;
          this.audit.description = ""
          this.audit.facilitator = ""
          this.audit.provinceName = ""
          this.audit.remarks = ""
          this.audit.femaleParticipant = ""
          this.audit.maleParticipants = ""

          this.loadAllAudits()
        }, 1000);

      }

    }), error => {
      console.log("ERROR")
    }
  }

}
