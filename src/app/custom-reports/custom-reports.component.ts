import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { CommonWebService } from '../common-web.service';
import { ResponseContentType, Http } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { API_URL } from '../app_params';

export class Report{
  reportId
  reportName
  query
  status
}

@Component({
  selector: 'app-custom-reports',
  templateUrl: './custom-reports.component.html',
  styleUrls: ['./custom-reports.component.css'],
  providers:[CommonWebService]
})
export class CustomReportsComponent implements OnInit {
  reportForm: FormGroup;

  public type;
  public report
  public reportList = new Array<Report>()


  public success

  @ViewChild("createReport") public createReport: ModalDirective;
  constructor(public commonService: CommonWebService,public http:Http) { 
    this.validation()
  }

  validation(){
    this.reportForm = new FormGroup({
      reportName: new FormControl('', [Validators.required]),
      query: new FormControl('', [Validators.required]),
      status: new FormControl('', [Validators.required]),
    })
  }

  ngOnInit() {
    this.getAllReports()
  }
  editReport(a){
    this.report = new Report()
    this.report.reportId = a.reportId
    this.report.reportName  =a.reportName
    this.report.query = a.query
    this.report.status = a.status
    this.type = "EDIT"
    this.createReport.show()
  }
  openCreateReport(){
    this.report = new Report()
    this.report.status = 1
    this.type = "NEW"
    this.createReport.show()
  }

  getAllReports(){
    this.commonService.processGet(API_URL+"ECD-CustomReports-1.0/service/reports/getReportList").subscribe(res => {
      
        if(res.responseCode==1){
          this.reportList = res.responseData
        }    
        
      
          }), error => {
            console.log("ERROR")
          }
  }
  saveReport(){
    console.log("REPORT TO BE SAVED",this.report)

    this.commonService.processPost(API_URL+"ECD-CustomReports-1.0/service/reports/createReport", this.report).subscribe(res => {
      
      
              if (res.responseCode == 1) {
                this.success = true
                setTimeout(() => {
                  this.success = false;
                  this.report.reportName = ""
                  this.report.query=""
      
                  
                  this.getAllReports()
                }, 1000);
      
              }
      
            }), error => {
              console.log("ERROR")
            }

  }
  generate(a){
    console.log("Query",a.query)
    let report = new Report()
    report.query = a.query
    report.reportName = a.reportName
    this.commonService.processPost(API_URL+"ECD-CustomReports-1.0/service/reports/getReport",report).subscribe(res => {
       
      
              if (res.responseCode == 1) {
               
                console.log("RESPONSE DATA",API_URL+"ECD_customreports/"+res.responseData)
                console.log("SUCCESS")
                // window.open(API_URL+"ECD_customreports/"+res.responseData,'_blank');
                window.open(res.responseData,'_blank');
             
              }
      
            }), error => {
              console.log("ERROR")
            }


    
  }

updateReport(){
  console.log("REPORT TO BE UPDATED",this.report)
  this.commonService.processPost(API_URL+"ECD-CustomReports-1.0/service/reports/updateReport", this.report).subscribe(res => {
    
    
            if (res.responseCode == 1) {
              this.success = true
              setTimeout(() => {
              
                this.success = false;
                this.getAllReports()
               this.createReport.hide()                
              
              }, 2000);
    
            }
    
          }), error => {
            console.log("ERROR")
          }
  
}
}
