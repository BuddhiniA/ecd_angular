import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeeWaiverPaymentComponent } from './fee-waiver-payment.component';

describe('FeeWaiverPaymentComponent', () => {
  let component: FeeWaiverPaymentComponent;
  let fixture: ComponentFixture<FeeWaiverPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeeWaiverPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeeWaiverPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
