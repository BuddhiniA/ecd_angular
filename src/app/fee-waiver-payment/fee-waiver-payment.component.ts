import { Component, OnInit, ViewChild } from '@angular/core';

import { Data } from '../model/Data';

import { ModalDirective } from 'ngx-bootstrap';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { Http, RequestOptions, Headers } from '@angular/http';
import { FeeWaiverGrid } from "../model/FeeWaiverGrid";
import { FeeWaiverPayments } from "../model/FeeWaiverPayments";
import { FeeWaiverPaymentData } from '../model/FeewaiverPaymentData';
import { FeeWaiverStudent } from '../model/FeeWaiverStudent';
import { months } from 'moment';
import { DatePipe } from '@angular/common';

export class Criteria {

  id
  criteriaId
  criteriaName
  status
  userEnterd
  dataInserted
  dateModified

}

export class BusinessLevel {
  id
  lifeCode
  districtId
  dsID
  english
  type
  code
  provinceId
  nodeType


}
export class Node {
  id
  label
  type
  code
  lifeCode
  districtId
  dsID
  provinceId
  nodeType
  leaf
  children = new Array<Node>()
}

export class Student {
  studentId
  studentName
  learningCenter
  year
  startMonth
  status
  paymentId1=0
  attendance1 = 0
  paid1
  schoolfee1
  payment1
  paymentId2
  attendance2 = 0
  paid2
  schoolfee2
  payment2
  paymentId3
  attendance3 = 0
  paid3
  schoolfee3
  payment3
  paymentId4
  attendance4 = 0
  paid4
  schoolfee4
  payment4
  paymentId5
  attendance5 = 0
  paid5
  schoolfee5
  payment5
  paymentId6
  attendance6 = 0
  paid6
  schoolfee6
  payment6
  paymentId7
  attendance7 = 0
  paid7
  schoolfee7
  payment7
  paymentId8
  attendance8 = 0
  paid8
  schoolfee8
  payment8
  paymentId9
  attendance9 = 0
  paid9
  schoolfee9
  payment9
  paymentId10
  attendance10 = 0
  paid10
  schoolfee10
  payment10
  paymentId11
  attendance11 = 0
  paid11
  schoolfee11
  payment11
  paymentId12
  ttendance12 = 0
  paid12
  schoolfee12
  payment12
  paymentId13
  attendance13 = 0
  paid13
  schoolfee13
  payment13
  paymentId14
  attendance14 = 0
  paid14
  schoolfee14
  payment14
  paymentId15
  attendance15 = 0
  paid15
  schoolfee15
  payment15
  paymentId16
  attendance16 = 0
  paid16
  schoolfee16
  payment16
  paymentId17
  attendance17 = 0
  paid17
  schoolfee17
  payment17
  paymentId18
  attendance18 = 0
  paid18
  schoolfee18
  payment18
  paymentId19
  attendance19 = 0
  paid19
  schoolfee19
  payment19
  paymentId20
  attendance20 = 0
  paid20
  schoolfee20
  payment20
  paymentId21
  attendance21 = 0
  paid21
  schoolfee21
  payment21
  paymentId22
  attendance22 = 0
  paid22
  schoolfee22
  payment22
  paymentId23
  attendance23 = 0
  paid23
  schoolfee23
  payment23
  paymentId24
  attendance24 = 0
  paid24
  schoolfee24
  payment24
}

export class Payment {
  learningCenter
  studentId
  year
  month
  status
  paidDate
}
export class PaymentData {

  attendance
  paidDate
  paidMonth
  paidStatus
  precentage
  studentId
  studentName
}

export class FeeWaiverDetail {
  criteriaId
  learningCenter
  paymentData = new Array<PaymentData>()

}

@Component({
  selector: 'app-fee-waiver-payment',
  templateUrl: './fee-waiver-payment.component.html',
  styleUrls: ['./fee-waiver-payment.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class FeeWaiverPaymentComponent implements OnInit {
  initialPayment: any;
  initialfEE: any;
  loadingMask
  month: number;
  year: number;
  feeWaiverId: any;
  globalSt: Student;
  studentList1: Student[];
  status1
  status2
  status3
  status4
  status5
  status6
  status7
  status8
  status9
  status10
  status11
  status12
  status13
  status14
  status15
  status16
  status17
  status18
  status19
  status20
  status21
  status22
  status23
  status24
  @ViewChild("adPaymentView") public adPaymentView: ModalDirective;
  @ViewChild("showLearningCenters") public showLearningCenters: ModalDirective;

  public learningCenterList = new Array<Data>()
  public learningCenterListbyCriteria = new Array<Data>()
  public selectedLearningCenter = new Data();

  public criteriaList = new Array<Criteria>()
  public selectedCriteria = new Criteria();

  public studentList = new Array<Student>()

  public pickDate;
  public feeWaiverGrid = new Array<FeeWaiverGrid>();

  public months = new Array<number>();

  public success;
  public selectedDate
  public yearList = new Array<number>()
  public selectedYear: number;
  public selectedMonth: number;
  public paymentToBeSaved
  public tobeSavedList = new Array<Payment>()
  public headers: Headers = new Headers;
  public value = 3000;

  public checked
  public feeWaiverDetail = new FeeWaiverDetail()

  public paid1
  public paid2
  public paid3
  public paid4
  public paid5
  public paid6
  public paid7
  public paid8
  public paid9
  public paid10
  public paid11
  public paid12
  public paid13
  public paid14
  public paid15
  public paid16
  public paid17
  public paid18
  public paid19
  public paid20
  public paid21
  public paid22
  public paid23
  public paid24

  files = new Array<Node>()
  public businessList = new Array<BusinessLevel>()
  public selectedNode = new Node();
  public previouslySelectedNode = new Node()


  public feeWaiverPaymentList = new Array<FeeWaiverPaymentData>()

  constructor(public commonService: CommonWebService, private http: Http, private datePipe: DatePipe) { }

  ngOnInit() {
    this.value = 0
    // this.getAllLearningCenters();
  }



  getTotal(id) {
    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getTotal?id=" + id).subscribe(res => {


      this.value = res.responseData

    }), error => {
      console.log("ERROR")
    }
  }
  getAllLearningCenters() {
    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getRegistredFeeWaiverLearningCenter").subscribe(res => {

      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData;
        this.selectedLearningCenter = this.learningCenterList[0]

        if (this.selectedLearningCenter != undefined || this.selectedLearningCenter != null) {
          this.showLearningCenters.show()
          this.getAllStudents()
        }
      }


    }), error => {
      console.log("ERROR")
    }
  }






  getAllStudents() {
    this.loadingMask=true;
    this.value = 0
    console.log("change")
    this.studentList = new Array<Student>()

    this.commonService.processGet(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/getRegistredStudents?learningCenter=" + this.selectedLearningCenter.sn).subscribe(res => {

      if (res.responseCode == 1) {
        this.loadingMask=false;
        this.feeWaiverPaymentList = res.responseData
        this.feeWaiverId = this.feeWaiverPaymentList[0].feeWaiver.feeWaiverId

        this.getTotal(this.feeWaiverId)
        if (this.feeWaiverPaymentList[0].feeWaiver.startMonth == 0) {

          this.selectedMonth = 1
          this.month = 0
        } else {
          this.month = 1
          this.selectedMonth = this.feeWaiverPaymentList[0].feeWaiver.startMonth

        }
        if (this.feeWaiverPaymentList[0].feeWaiver.startYear == 0) {

          this.year = 0
          this.loadYears(2018)
        } else {
          this.year = 1
          this.selectedYear = this.feeWaiverPaymentList[0].feeWaiver.startYear

        }

      }

      let distinctStudentList = new Array<Student>()


      for (let k of this.feeWaiverPaymentList) {
        let st = new Student()
        st.studentId = k.student.studentId
        st.studentName = k.student.name
        st.status = k.student.status
        distinctStudentList.push(st)

      }

      var sl = distinctStudentList;
      var out = [];

      for (var i = 0, l = sl.length; i < l; i++) {
        var unique = true;
        for (var j = 0, k = out.length; j < k; j++) {
          if ((sl[i].studentId === out[j].studentId)) {
            unique = false;
          }
        }
        if (unique) {
          out.push(sl[i]);
        }
      }
      console.log(sl.length); // 10
      console.log(out.length); // 5

      this.studentList = out









      for (let i = 0; i < this.studentList.length; i++) {


        for (let j = 0; j < this.feeWaiverPaymentList.length; j++) {

          if (this.feeWaiverPaymentList[j] != undefined) {


            if (this.studentList[i].studentId == this.feeWaiverPaymentList[j].student.studentId) {


              let ar = this.feeWaiverPaymentList[j].payment.paymentMonth



              switch (ar) {
                case 1: {

                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.studentList[i].paid1 = true
                    this.paid1 = true
                    this.studentList[i].attendance1 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId1 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment1 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee1 = this.feeWaiverPaymentList[j].payment.schoolFee
                  this.initialfEE=this.feeWaiverPaymentList[j].payment.schoolFee
                  this.initialPayment=this.feeWaiverPaymentList[j].payment.payment
                  break


                }
                case 2: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid2 = true
                    this.studentList[i].paid2 = true
                    this.studentList[i].attendance2 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId2 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment2 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee2 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 3: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid3 = true
                    this.studentList[i].paid3 = true
                    this.studentList[i].attendance3 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId3 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment3 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee3 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 4: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid4 = true
                    this.studentList[i].paid4 = true
                    this.studentList[i].attendance4 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId4 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment4 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee4 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 5: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid5 = true
                    this.studentList[i].paid5 = true
                    this.studentList[i].attendance5 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId5 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment5 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee5 = this.feeWaiverPaymentList[j].payment.schoolFee

                  break
                }
                case 6: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid6 = true
                    this.studentList[i].paid6 = true
                    this.studentList[i].attendance6 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId6 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment6 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee6 = this.feeWaiverPaymentList[j].payment.schoolFee

                  break
                }
                case 7: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid7 = true
                    this.studentList[i].paid7 = true
                    this.studentList[i].attendance7 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId7 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment7 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee7 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 8: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid8 = true
                    this.studentList[i].paid8 = true
                    this.studentList[i].attendance8 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId8 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment8 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee8 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 9: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid9 = true
                    this.studentList[i].paid9 = true
                    this.studentList[i].attendance9 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId9 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment9 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee9 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 10: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid10 = true
                    this.studentList[i].paid10 = true
                    this.studentList[i].attendance10 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId10 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment10 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee10 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 11: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid11 = true
                    this.studentList[i].paid11 = true
                    this.studentList[i].attendance11 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId11 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment11 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee11 = this.feeWaiverPaymentList[j].payment.schoolFee

                  break
                }
                case 12: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid12 = true
                    this.studentList[i].ttendance12 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId12 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment12 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee12 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 13: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid13 = true
                    this.studentList[i].attendance13 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId13 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment13 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee13 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 14: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid14 = true
                    this.studentList[i].attendance14 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId14 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment14 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee14 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 15: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid15 = true
                    this.studentList[i].attendance15 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId15 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment15 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee15 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 16: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid16 = true
                    this.studentList[i].attendance16 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId16 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment16 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee16 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 17: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid17 = true
                    this.studentList[i].attendance17 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId17 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment17 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee17 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 18: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid18 = true
                    this.studentList[i].attendance18 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId18 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment18 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee18 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 19: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid19 = true
                    this.studentList[i].attendance19 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId19 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment19 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee19 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 20: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid20 = true
                    this.studentList[i].attendance20 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId20 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment20 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee20 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 21: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid21 = true
                    this.studentList[i].attendance21 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId21 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment21 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee21 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 22: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid22 = true
                    this.studentList[i].attendance22 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId22 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment22 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee22 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 23: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid12 = true
                    this.studentList[i].paid23 = true
                    this.studentList[i].attendance23 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId23 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment23 = this.feeWaiverPaymentList[j].payment.payment
                  this.studentList[i].schoolfee24 = this.feeWaiverPaymentList[j].payment.schoolFee
                  break
                }
                case 24: {
                  if (this.feeWaiverPaymentList[j].payment.paid == 1) {
                    this.paid24 = true
                    this.studentList[i].paid24 = true
                    this.studentList[i].attendance24 = this.feeWaiverPaymentList[j].payment.attendance
                    this.studentList[i].paymentId24 = this.feeWaiverPaymentList[j].payment.paymentId
                  }
                  this.studentList[i].payment24 = this.feeWaiverPaymentList[j].payment.payment

                  break
                }
                default: {
                  console.log("NOTHING")
                }
              }



            }
          }

        }


      }



    }), error => {
      console.log("ERROR")

    }
  }


  changeFee(){
    console.log("INITAIL SCHOOL FEE",this.studentList[0].schoolfee1)
    // if(this.initialfEE==0){
      this.initialfEE=this.studentList[0].schoolfee1
    // }
    
  }

  changePayment(){
   
    // if(this.initialPayment==0){
      this.initialPayment=this.studentList[0].payment1
    // }
    
  }

  loadYears(start) {

    for (let i = 0; i < 10; i++) {
      let starty = start + i
      this.yearList.push(starty)

    }
    this.selectedYear = this.yearList[0]

  }




  addPayment() {
    this.selectedDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    
    
    if(!this.studentList[0].paid1){
      this.studentList[0].payment1=0
      this.studentList[0].schoolfee1=0
    }
    if(!this.studentList[0].paid2){
      this.studentList[0].payment2=0
      this.studentList[0].schoolfee2=0
    }
    if(!this.studentList[0].paid3){
      this.studentList[0].payment3=0
      this.studentList[0].schoolfee3=0
    }
    if(!this.studentList[0].paid4){
      this.studentList[0].payment4=0
      this.studentList[0].schoolfee4=0
     
    }
    if(!this.studentList[0].paid5){
      this.studentList[0].payment5=0
      this.studentList[0].schoolfee5=0
    }
    if(!this.studentList[0].paid6){
      this.studentList[0].payment6=0
      this.studentList[0].schoolfee6=0
    }
    if(!this.studentList[0].paid7){
      this.studentList[0].payment7=0
      this.studentList[0].schoolfee7=0
    }
    if(!this.studentList[0].paid8){
      this.studentList[0].payment8=0
      this.studentList[0].schoolfee8=0
    }
    if(!this.studentList[0].paid9){
      this.studentList[0].payment9=0
      this.studentList[0].schoolfee9=0
    }
    if(!this.studentList[0].paid10){
      this.studentList[0].payment10=0
      this.studentList[0].schoolfee10=0
    }
    if(!this.studentList[0].paid11){
      this.studentList[0].payment11=0
      this.studentList[0].schoolfee11=0
    }
    if(!this.studentList[0].paid12){
      this.studentList[0].payment12=0
      this.studentList[0].schoolfee12=0
    }
    if(!this.studentList[0].paid13){
      this.studentList[0].payment13=0
      this.studentList[0].schoolfee13=0
    }
    if(!this.studentList[0].paid14){
      this.studentList[0].payment14=0
      this.studentList[0].schoolfee14=0
    }
    if(!this.studentList[0].paid15){
      this.studentList[0].payment15=0
      this.studentList[0].schoolfee15=0
    }
    if(!this.studentList[0].paid16){
      this.studentList[0].payment16=0
      this.studentList[0].schoolfee16=0
    } 
    if(!this.studentList[0].paid17){
      this.studentList[0].payment17=0
      this.studentList[0].schoolfee17=0
    }
    if(!this.studentList[0].paid18){
      this.studentList[0].payment18=0
      this.studentList[0].schoolfee18=0
    }
    if(!this.studentList[0].paid19){
      this.studentList[0].payment19=0
      this.studentList[0].schoolfee19=0
    }
    if(!this.studentList[0].paid20){
      this.studentList[0].payment20=0
      this.studentList[0].schoolfee20=0
    }
    if(!this.studentList[0].paid21){
      this.studentList[0].payment21=0
      this.studentList[0].schoolfee21=0
    }
    if(!this.studentList[0].paid22){
      this.studentList[0].payment22=0
      this.studentList[0].schoolfee22=0
    }
    if(!this.studentList[0].paid23){
      this.studentList[0].payment23=0
      this.studentList[0].schoolfee23=0
    }
    if(!this.studentList[0].paid24){
      this.studentList[0].payment24=0
      this.studentList[0].schoolfee24=0
    }
    this.getAllStudents()
    this.adPaymentView.show()
  }







  savePayment() {


    let localSuccess = true
    for (let a of this.tobeSavedList) {
      a.paidDate = this.selectedDate
      console.log("STYUDENT ID", a.studentId)

      console.log("Paid Dtae", a.paidDate)
      console.log("Learning cen", a.learningCenter)
      console.log("year", a.year)
      console.log("year", this.selectedCriteria.id)
      let paidMonth = a.year + "-" + this.setTwoDigits(a.month) + "-01"
      this.headers.set("Content-Type", "application/json")
      this.headers.set("learningCenter", this.selectedLearningCenter.sn)
      this.headers.set("criteriaId", "1")
      this.headers.set("studentId", a.studentId)
      this.headers.set("paidMonth", paidMonth)

      this.headers.set("paidDate", this.selectedDate)

      let options = new RequestOptions({ headers: this.headers });
      if (localSuccess) {

        this.commonService.processPostWithHeaders(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updatePaymentStatus", "", options).subscribe(res => {


          if (res.responseCode == 1) {

            localSuccess = true
          } else {
            localSuccess = false
            
            return
          }

        }), error => {
          console.log("ERROR")
        }
        console.log("SUCCESS")
      } else {
        this.success = false
      }


    }
    console.log("TO BE SAVED LIST FINAL", this.tobeSavedList)



  }
  changeStatus(evt, number) {
    this.months.push(number)
    if (number == 1) {
      this.studentList[0].payment1 = 0
      this.status1 = true
      this.studentList[0].schoolfee1 = 0
    } else if (number == 2) {
      if(evt.target.checked){
        this.studentList[0].payment2 =this.initialPayment
        this.status2 = true
        this.studentList[0].schoolfee2 = this.initialfEE
      }else{
        this.studentList[0].payment2 =0
        this.status2 = false
        this.studentList[0].schoolfee2 = 0
      }
     
    } else if (number == 3) {
      if(evt.target.checked){
        this.studentList[0].payment3 =this.initialPayment
        this.status3 = true
        this.studentList[0].schoolfee3 = this.initialfEE
      }else{
        this.studentList[0].payment3 =0
        this.status3 = false
        this.studentList[0].schoolfee3 = 0
      }
    } else if (number == 4) {
      if(evt.target.checked){
        this.studentList[0].payment4 =this.initialPayment
        this.status4 = true
        this.studentList[0].schoolfee4 = this.initialfEE
      }else{
        this.studentList[0].payment4=0
        this.status4 = false
        this.studentList[0].schoolfee4 = 0
      }
    } else if (number == 5) {
      if(evt.target.checked){
        this.studentList[0].payment5 =this.initialPayment
        this.status5 = true
        this.studentList[0].schoolfee5 = this.initialfEE
      }else{
        this.studentList[0].payment5 =0
        this.status5 = false
        this.studentList[0].schoolfee5 = 0
      }
    } else if (number == 6) {
      if(evt.target.checked){
        this.studentList[0].payment6 =this.initialPayment
        this.status6 = true
        this.studentList[0].schoolfee6 = this.initialfEE
      }else{
        this.studentList[0].payment6 =0
        this.status6 = false
        this.studentList[0].schoolfee6 = 0
      }
    } else if (number == 7) {
      if(evt.target.checked){
        this.studentList[0].payment7 =this.initialPayment
        this.status7 = true
        this.studentList[0].schoolfee7 = this.initialfEE
      }else{
        this.studentList[0].payment7 =0
        this.status7 = false
        this.studentList[0].schoolfee7 = 0
      }
    } else if (number == 8) {
      if(evt.target.checked){
        this.studentList[0].payment8 =this.initialPayment
        this.status8 = true
        this.studentList[0].schoolfee8 = this.initialfEE
      }else{
        this.studentList[0].payment8 =0
        this.status8 = false
        this.studentList[0].schoolfee8 = 0
      }
    } else if (number == 9) {
      if(evt.target.checked){
        this.studentList[0].payment9 =this.initialPayment
        this.status9 = true
        this.studentList[0].schoolfee9 = this.initialfEE
      }else{
        this.studentList[0].payment9 =0
        this.status9 = false
        this.studentList[0].schoolfee9 = 0
      }
    } else if (number == 10) {
      if(evt.target.checked){
        this.studentList[0].payment10 =this.initialPayment
        this.status10 = true
        this.studentList[0].schoolfee10 = this.initialfEE
      }else{
        this.studentList[0].payment10 =0
        this.status10 = false
        this.studentList[0].schoolfee10 = 0
      }
    } else if (number == 11) {
      if(evt.target.checked){
        this.studentList[0].payment11 =this.initialPayment
        this.status11 = true
        this.studentList[0].schoolfee11 = this.initialfEE
      }else{
        this.studentList[0].payment11 =0
        this.status11 = false
        this.studentList[0].schoolfee11 = 0
      }
    } else if (number == 12) {
      if(evt.target.checked){
        this.studentList[0].payment12 =this.initialPayment
        this.status12 = true
        this.studentList[0].schoolfee12 = this.initialfEE
      }else{
        this.studentList[0].payment12 =0
        this.status12 = false
        this.studentList[0].schoolfee12 = 0
      }
    } else if (number == 13) {
      if(evt.target.checked){
        this.studentList[0].payment13 =this.initialPayment
        this.status13 = true
        this.studentList[0].schoolfee13 = this.initialfEE
      }else{
        this.studentList[0].payment13 =0
        this.status13 = false
        this.studentList[0].schoolfee13 = 0
      }
    } else if (number == 14) {
      if(evt.target.checked){
        this.studentList[0].payment14 =this.initialPayment
        this.status14 = true
        this.studentList[0].schoolfee14 = this.initialfEE
      }else{
        this.studentList[0].payment14 =0
        this.status14 = false
        this.studentList[0].schoolfee14 = 0
      }
    } else if (number == 15) {
      if(evt.target.checked){
        this.studentList[0].payment15 =this.initialPayment
        this.status15 = true
        this.studentList[0].schoolfee15 = this.initialfEE
      }else{
        this.studentList[0].payment15 =0
        this.status15 = false
        this.studentList[0].schoolfee15 = 0
      }
    } else if (number == 16) {
      if(evt.target.checked){
        this.studentList[0].payment16 =this.initialPayment
        this.status16 = true
        this.studentList[0].schoolfee16 = this.initialfEE
      }else{
        this.studentList[0].payment16 =0
        this.status16 = false
        this.studentList[0].schoolfee16 = 0
      }
    } else if (number == 17) {
      if(evt.target.checked){
        this.studentList[0].payment17 =this.initialPayment
        this.status17 = true
        this.studentList[0].schoolfee17 = this.initialfEE
      }else{
        this.studentList[0].payment17 =0
        this.status17 = false
        this.studentList[0].schoolfee17 = 0
      }
    } else if (number == 18) {
      if(evt.target.checked){
        this.studentList[0].payment18 =this.initialPayment
        this.status18 = true
        this.studentList[0].schoolfee18 = this.initialfEE
      }else{
        this.studentList[0].payment18 =0
        this.status18 = false
        this.studentList[0].schoolfee18 = 0
      }
    } else if (number == 19) {
      if(evt.target.checked){
        this.studentList[0].payment19 =this.initialPayment
        this.status19 = true
        this.studentList[0].schoolfee19 = this.initialfEE
      }else{
        this.studentList[0].payment19 =0
        this.status19 = false
        this.studentList[0].schoolfee19 = 0
      }
    } else if (number == 20) {
      if(evt.target.checked){
        this.studentList[0].payment20 =this.initialPayment
        this.status20 = true
        this.studentList[0].schoolfee20 = this.initialfEE
      }else{
        this.studentList[0].payment20 =0
        this.status20 = false
        this.studentList[0].schoolfee20 = 0
      }
    } else if (number == 21) {
      if(evt.target.checked){
        this.studentList[0].payment21 =this.initialPayment
        this.status21 = true
        this.studentList[0].schoolfee21 = this.initialfEE
      }else{
        this.studentList[0].payment21 =0
        this.status21 = false
        this.studentList[0].schoolfee21 = 0
      }
    } else if (number == 22) {
      if(evt.target.checked){
        this.studentList[0].payment22 =this.initialPayment
        this.status22 = true
        this.studentList[0].schoolfee22 = this.initialfEE
      }else{
        this.studentList[0].payment22 =0
        this.status22 = false
        this.studentList[0].schoolfee22 = 0
      }
    } else if (number == 23) {
      if(evt.target.checked){
        this.studentList[0].payment23 =this.initialPayment
        this.status23 = true
        this.studentList[0].schoolfee23 = this.initialfEE
      }else{
        this.studentList[0].payment23 =0
        this.status23 = false
        this.studentList[0].schoolfee23 = 0
      }
    } else if (number == 24) {
      if(evt.target.checked){
        this.studentList[0].payment24 =this.initialPayment
        this.status24 = true
        this.studentList[0].schoolfee24 = this.initialfEE
      }else{
        this.studentList[0].payment24 =0
        this.status24 = false
        this.studentList[0].schoolfee24 = 0
      }
    }


  }

  setTwoDigits(num: number): any {
    return (num < 10 ? '0' : '') + num
  }
  changeFIG(evt) {
    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/granted?type=FIG").subscribe(res => {
      if (res.responseCode == 1) {
        this.learningCenterList = res.responseData
      }

    }), error => {
      console.log("ERROR")
    }
  }

  collapse(evt) {
    console.log("COLLAPSE", evt)
  }
  expand(event) {


    this.previouslySelectedNode.id = this.selectedNode.id
    this.previouslySelectedNode.label = this.selectedNode.label
    this.previouslySelectedNode.type = this.selectedNode.type
    this.previouslySelectedNode.code = this.selectedNode.code
    this.previouslySelectedNode.districtId = this.selectedNode.districtId
    this.previouslySelectedNode.dsID = this.selectedNode.dsID
    this.previouslySelectedNode.provinceId = this.selectedNode.provinceId
  }
  nodeSelect(event) {

    let district = "" + event.node.provinceId + event.node.code;


    if (event.node.type == "district") {

      let districtId = "" + event.node.provinceId + event.node.code;

      this.getAllLearningCentersbyId(districtId, event.node.type)
    } else if (event.node.type == "ds") {
      this.getAllLearningCentersbyId(event.node.dsID, event.node.type)
    } else if (event.node.type == "gn") {
      this.getAllLearningCentersbyId(event.node.code, event.node.type)
    }

    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }
  }
  addChildren(node1) {

    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)





      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {
          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
      // if (node1.type == "district") {
      //   let district = "" + node1.provinceId + node1.code;
      //   console.log("distrit", district);
      //   if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
      //     let node = new Node()
      //     node.id = this.businessList[i].id
      //     node.label = this.businessList[i].english
      //     node.type = this.businessList[i].type
      //     node.code = this.businessList[i].code
      //     node.districtId = this.businessList[i].districtId
      //     node.dsID = this.businessList[i].dsID
      //     node.provinceId = this.businessList[i].provinceId
      //    node1.children.push(node)
      //     this.getAllLearningCenters()
      //     console.log("SELECTED NODE", this.selectedNode)
      //   }
      // }
      // if (node1.type == "ds") {
      //   if (node1.districtId == this.businessList[i].districtId && node1.code == this.businessList[i].dsID && this.businessList[i].type == "gnd") {
      //     let node = new Node()
      //     node.id = this.businessList[i].id
      //     node.label = this.businessList[i].english
      //     node.type = this.businessList[i].type
      //     node.code = this.businessList[i].code
      //     node.districtId = this.businessList[i].districtId
      //     node.dsID = this.businessList[i].dsID
      //     node.provinceId = this.businessList[i].provinceId
      //    node1.children.push(node)
      //     console.log("SELECTED NODE", this.selectedNode)
      //     this.getAllLearningCenters()
      //   }
      // }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }

  getAllLocations() {

    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.businessList = res.responseData
        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }

        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)





      }


    }), error => {
      console.log("ERROR")
    }
  }
  getAllLearningCentersbyId(id, type) {
    this.learningCenterList = new Array<Data>()
    this.commonService.processGet(API_URL + "ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCentersbyId?id=" + id + "&type=" + type).subscribe(res => {

      this.learningCenterList = res.responseData;

      this.selectedLearningCenter = this.learningCenterList[0]

    }), error => {
      console.log("ERROR")
    }
  }

  updateStatus(e, number, studentId) {
    if (e.target.checked) {
      this.updatePaidStatus(number, studentId, 1)
    } else {
      this.updatePaidStatus(number, studentId, 0)
    }
  }

  updatePaidStatus(num, stuId, param) {
    console.log("Fee waiver if"+this.feeWaiverId)
    this.commonService.processPost(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updatePaidStatus?month=" + num + "&studentId=" + stuId + "&number=" + param+"&feewaiverId="+this.feeWaiverId, "").subscribe(res => {
      this.loadingMask=true

      if (res.responseCode == 1) {
        this.loadingMask=false
        console.log("updated")
      }

    }), error => {
      this.loadingMask=false
      console.log("ERROR")
    }
  }
  saveStudentPayments() {

    let feePayment = new Array<FeeWaiverPayments>()

    for (let a of this.months) {
      let f: FeeWaiverPayments = new FeeWaiverPayments()
      f.feewaiverId = this.feeWaiverId;
      f.paymentMonth = a
      f.paymantDate = this.selectedDate

      if (a == 1) {
        f.payment = this.studentList[0].payment1
        f.schoolFee=parseFloat(this.studentList[0].schoolfee1)
      } else if (a == 2) {
        f.payment = this.studentList[0].payment2
        f.schoolFee=this.studentList[0].schoolfee2
      } else if (a == 3) {
        f.payment = this.studentList[0].payment3
        f.schoolFee=this.studentList[0].schoolfee3
      } else if (a == 4) {
        f.payment = this.studentList[0].payment4
        f.schoolFee=this.studentList[0].schoolfee4
      } else if (a == 5) {
        f.payment = this.studentList[0].payment5
        f.schoolFee=this.studentList[0].schoolfee5
      } else if (a == 6) {
        f.payment = this.studentList[0].payment6
        f.schoolFee=this.studentList[0].schoolfee6
      } else if (a == 7) {
        f.payment = this.studentList[0].payment7
        f.schoolFee=this.studentList[0].schoolfee7
      } else if (a == 8) {
        f.payment = this.studentList[0].payment8
        f.schoolFee=this.studentList[0].schoolfee8
      } else if (a == 9) {
        f.payment = this.studentList[0].payment9
        f.schoolFee=this.studentList[0].schoolfee9
      } else if (a == 10) {
        f.payment = this.studentList[0].payment10
        f.schoolFee=this.studentList[0].schoolfee10
      } else if (a == 11) {
        f.payment = this.studentList[0].payment11
        f.schoolFee=this.studentList[0].schoolfee11
      } else if (a == 12) {
        f.payment = this.studentList[0].payment12
        f.schoolFee=this.studentList[0].schoolfee12
      } else if (a == 13) {
        f.payment = this.studentList[0].payment13
        f.schoolFee=this.studentList[0].schoolfee13
      } else if (a == 14) {
        f.payment = this.studentList[0].payment14
        f.schoolFee=this.studentList[0].schoolfee14
      } else if (a == 15) {
        f.payment = this.studentList[0].payment15
        f.schoolFee=this.studentList[0].schoolfee15
      } else if (a == 16) {
        f.payment = this.studentList[0].payment16
        f.schoolFee=this.studentList[0].schoolfee16
      } else if (a == 17) {
        f.payment = this.studentList[0].payment17
        f.schoolFee=this.studentList[0].schoolfee17
      } else if (a == 18) {
        f.payment = this.studentList[0].payment18
        f.schoolFee=this.studentList[0].schoolfee18
      } else if (a == 19) {
        f.payment = this.studentList[0].payment19
        f.schoolFee=this.studentList[0].schoolfee19
      } else if (a == 20) {
        f.payment = this.studentList[0].payment20
        f.schoolFee=this.studentList[0].schoolfee20
      } else if (a == 21) {
        f.payment = this.studentList[0].payment21
        f.schoolFee=this.studentList[0].schoolfee21
      } else if (a == 22) {
        f.payment = this.studentList[0].payment22
        f.schoolFee=this.studentList[0].schoolfee22
      } else if (a == 23) {
        f.payment = this.studentList[0].payment23
        f.schoolFee=this.studentList[0].schoolfee23
        
      } else if (a == 24) {
        f.payment = this.studentList[0].payment24
        f.schoolFee=this.studentList[0].schoolfee24
      }
      feePayment.push(f)
    }

    
    let headers = new Headers();
    headers.set("month", this.selectedMonth + "");
    headers.set("year", this.selectedYear + "");

    let options = new RequestOptions({ headers });
    console.log("Payments to be saved", feePayment)
    this.commonService.processPostWithHeaders(API_URL + "ECD-FeeWaiverMaintainance-1.0/service/FeeWaiver/updateFeePayment", feePayment, options).subscribe(res => {


      if (res.responseCode == 1) {

        this.success = true
        setTimeout(() => {
          this.success = false;
          this.adPaymentView.hide()
          this.getAllStudents()
          this.getTotal(this.feeWaiverId)
          this.status1=false
          this.status2=false
          this.status3=false
        }, 2000);
      }

    }), error => {
      console.log("ERROR")
    }
  }

  onRowSelect(evt) {
    console.log("event", evt)
    this.getAllStudents()
  }
}
