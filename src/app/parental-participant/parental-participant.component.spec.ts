import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentalParticipantComponent } from './parental-participant.component';

describe('ParentalParticipantComponent', () => {
  let component: ParentalParticipantComponent;
  let fixture: ComponentFixture<ParentalParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentalParticipantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentalParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
