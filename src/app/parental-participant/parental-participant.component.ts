import { Component, OnInit,ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ParentalParticipantModel } from '../model/ParentalParticipant';
import { Data } from '../model/Data';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';

@Component({
  selector: 'app-parental-participant',
  templateUrl: './parental-participant.component.html',
  styleUrls: ['./parental-participant.component.css']
})
export class ParentalParticipantComponent implements OnInit {

  public parentalPtList = Array<ParentalParticipantModel>();
  public parentalPtModel = new ParentalParticipantModel;
  public learningCenterList = new Array<Data>();
  public selectedLearningCenter = new Data();
  public programId;
  public gender
  public success;
  public parentalptForm :FormGroup

  @ViewChild("addParentPt") public addParentPt;
  constructor(public route: ActivatedRoute,public commonService: CommonWebService) { 
    this.validation();
  }

  ngOnInit() {
    // this.loadToGrid();
    // this.validation();
    this.getAllLeaningCenters();
    this.route.params.subscribe(params=>{
      this.programId=params['programId']
      if(this.programId!=null||this.programId!=undefined){
            this.loadToGrid();
           }
      console.log("CID",this.programId)
    })
  }

  validation(){
    
        this.parentalptForm = new FormGroup({
          nic:new FormControl(['',Validators.required]),
          name:new FormControl(['',Validators.required]),
          address:new FormControl(['',Validators.email]),
          tp:new FormControl('',[Validators.required,Validators.maxLength(3)]),
          status:new FormControl('',[Validators.required]),
          gender:new FormControl('',[Validators.required]),
          school:new FormControl('',[Validators.required])
         
        })
      }
  
  loadToGrid() {
        
        let url = API_URL+"ECD-TrainingPrograms-1.0/service/trainings/getOrientationParticipant?programId="+this.programId;
        this.commonService.processGet(url).subscribe(res => {
    
          // console.log("response", res.getAllMobileUsers.responseData);
    
          // this.userModel = res.users.responseData;
          // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
          if (res.responseCode == 1) {
    
            this.parentalPtList = res.responseData;
            console.log("response data ", this.parentalPtList)
          }
    
        }, error => {
    
          console.log("error ", error);
          console.log("response dataz ", this.parentalPtList)
        })
      }

      openAddPt(){
        // console.log("updateInsSetting ",ins);
        // this.instituteDetail = ins;
        this.addParentPt.show();

      }
      createParentalPt(){
        let programId = parseInt(this.programId)
        this.parentalPtModel.programId =programId
        // this.parentalPtModel.participantId =100;
        this.parentalPtModel.learningCenterName = this.selectedLearningCenter.name
        this.parentalPtModel.learningCenter = this.selectedLearningCenter.sn
        this.parentalPtModel.district = this.parentalPtModel.address
        this.parentalPtModel.noOfChildren = 20
        // this.parentalPtModel.status = 2
        this.parentalPtModel.gender=this.gender==1?this.parentalPtModel.gender="Male":this.parentalPtModel.gender="Female"
        // this.parentalPtModel.designation = "aaa"
        // this.parentalPtModel.designation = "aaa"
        console.log("programId",this.parentalPtModel.programId);
        console.log("learning center ",this.parentalPtModel.learningCenterName);

        console.log("TO BE SAVED",this.parentalPtModel)
        this.commonService.processPost("http://192.0.0.59:8080/ECD-TrainingPrograms-1.0/service/trainings/addNewParentalParticipant",this.parentalPtModel).subscribe(res=>{
          
          
           
            if(res.responseCode==1){
              this.success=true;
              this.loadToGrid()
            
              setTimeout(()=>{
               
                // this.resetFields()
                this.success=false
              },2000)
            }
      
          }),error=>{
              console.log("ERROR")
         }
      }

      getAllLeaningCenters() {
            
            // this.selectedLearningCenter = new Data()
            this.commonService.processGet(API_URL+"ECD-CriteriaManagement-1.0/service/criteria/getAllLearningCenters").subscribe(res => {
        
              this.learningCenterList = res.responseData;
        
              if (this.selectedLearningCenter.sn == null || this.selectedLearningCenter.sn == undefined) {
                this.selectedLearningCenter = this.learningCenterList[0]
              }
              console.log("Leaerning Centers All", this.selectedLearningCenter)
        
            }), error => {
              console.log("ERROR")
            }
          }

      //     
      
}
