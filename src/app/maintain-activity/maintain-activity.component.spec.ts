import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintainActivityComponent } from './maintain-activity.component';

describe('MaintainActivityComponent', () => {
  let component: MaintainActivityComponent;
  let fixture: ComponentFixture<MaintainActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintainActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintainActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
