import { Component, OnInit, ViewChild } from '@angular/core';
// import { TreeModule, ITreeState, TreeNode } from 'angular-tree-component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BusinessStrModel } from "../model/BsStr";
import { CommonWebService } from '../common-web.service';
import { API_URL } from '../app_params';
import { TreeNode, TreeDragDropService } from 'primeng/primeng';
import { NodeService } from '../NodeService';
import { ModalDirective } from 'ngx-bootstrap';

export class BusinessLevel {
  bisId
  bisName
  parentId


}
export class Node {
  label
  children = new Array<Node>()
}

@Component({
  selector: 'app-business-structure',
  templateUrl: './business-structure.component.html',
  styleUrls: ['./business-structure.component.css'],
  providers: [NodeService, TreeDragDropService]
})
export class BusinessStructureComponent implements OnInit {
  @ViewChild("viewUser") public viewUser: ModalDirective;
  @ViewChild("pickListView") public pickListView: ModalDirective;
  // state: IreeState;
  // bsVar: string;

  public businessList = new Array<BusinessLevel>()
  public node: TreeNode
  public selectedNode;
  // files: TreeNode[];
  files = new Array<Node>()
  public BsModel = Array<BusinessStrModel>();
  options = {
    allowDrag: true
  }
  constructor(public commonService: CommonWebService, private nodeService: NodeService) { }

  ngOnInit() {
    // this.nodeService.getFiles().then(files => this.files = files);

    this.commonService.processGet(API_URL + "ECD-UserManagement-1.0/service/user/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        let selectedArray = new Array<any>()
        this.businessList = res.responseData
        console.log("Data", res.responseData)

        let increment = 0;

        while (increment < this.businessList.length) {
          let selectedLevel = this.businessList[increment].bisId

        
          // console.log(this.recursive(selectedLevel,increment))
          this.files.push(this.recursive(selectedLevel,increment))
          increment++
        }


      


      }


    }), error => {
      console.log("ERROR")
    }

  }

  public recursive(selectedLevel,increment):any{
   
          for (let j = increment; j < this.businessList.length; j++) {
            let node = new Node()
            node.label = this.businessList[increment].bisName
            if (selectedLevel == this.businessList[j].parentId) {
              console.log("SELECTED BIS", selectedLevel)
              console.log("CHILDE", this.businessList[j].bisId)
             
              let nod = new Node()
              nod.label = this.businessList[j].bisName
              node.children.push(nod)
              this.recursive(nod,j)
            }
       return node;
          }
        
  }

  //  let url = API_URL+"ECD-UserManagement-1.0/service/user/getBusinessStructure";
  // this.commonService.processGet(url).subscribe(res => {

  //   console.log(">>>>>>>>>>>>>>>>>", res.responseData);




  //     function convert(array) {
  //     var map = {};
  //     for (var i = 0; i < array.length; i++) {
  //       var obj = array[i];
  //       obj.children = [];

  //       map[obj.bisId] = obj;

  //       var parent = obj.parentId || '-';
  //       if (!map[parent]) {
  //         map[parent] = {
  //           children: []
  //         };
  //       }
  //       map[parent].children.push(obj);
  //     }

  //     return map['-'].children;

  //   }

  //   var r = convert(res.responseData);
  //   this.bsVar = r;





  //   }, error => {

  //     console.log("error ", error);
  //     console.log("response dataz ", this.BsModel)
  //   })

  // }


  // addUser(a){
  //    console.log("a ",a)
  // }
  // addSubDept(b){
  //   console.log("b ",b)
  // }

  // collapseAll() {
  //   this.state = {
  //     ...this.state,
  //     expandedNodeIds: {}
  //   };
  // }
  addUser(){
    this.viewUser.show()
  }
  nodeSelect(event) {
   
   this.selectedNode = event.node.label
   console.log("SELECTED NODE",this.selectedNode)
}
getAllUsers(){

  this.pickListView.show()
}


}
