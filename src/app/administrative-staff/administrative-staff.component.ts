import { Component, OnInit, ViewChild } from '@angular/core';

import { AdminModel } from '../model/Admin';
import { ResPoolModel } from '../model/ResPool';
import { ResourcePerson } from '../model/ResourcePerson';
import { AdminPtModel } from '../model/AdminPt';
// import { AdminEditModel } from '../model/AdminEdit';
import { AdminAdModel } from '../model/AdminAd';
// import { FileSelectDirective, FileDropDirective, FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { API_URL } from '../app_params';
import { CommonWebService } from '../common-web.service';
import { ProgramData } from '../model/ProgramData';
import { DatePipe } from '@angular/common';
import { BusinessLevel } from '../model/BusinessLevel';
import { Node } from '../model/Node';
import { TreeNode } from 'primeng/primeng';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap';

export class PrimaryProgram {
  userInserted: any = " ";
  userModified: any = null;
  dateInserted: any = null;
  dateModified: any = null;
  resourcePool = new Array<ResourcePerson>()
  // masterTrainerList = new Array<ResourcePersonModel>()
  guestList = new Array<ResourcePerson>()
  program = new AdminModel()
}

export class PrimaryProgramAd {
  userInserted: any = " ";
  userModified: any = null;
  dateInserted: any = null;
  dateModified: any = null;
  resourcePool = new Array<ResourcePerson>()
  // masterTrainerList = new Array<ResourcePersonModel>()
  guestList = new Array<ResourcePerson>()
  program = new AdminAdModel()
}

@Component({
  selector: 'app-administrative-staff',
  templateUrl: './administrative-staff.component.html',
  styleUrls: ['./administrative-staff.component.css'],
  providers: [CommonWebService, DatePipe]
})
export class AdministrativeStaffComponent implements OnInit {
  programId: any;
  name: FormControl;
  remark: FormControl;
  programForm: any;
  selectedNode = new Node();
  files: Node[];
  businessList: any[];
  loadingTree: boolean;
  programData = new ProgramData();
  userId: any;
  userData: any;

  public adminstaffModel = Array<AdminModel>();
  public adminstaffDetail = new AdminModel;
  public adminstaffPtList = Array<AdminPtModel>();
  public adminstaffPt = new AdminPtModel;
  public resModel: any;
  public resDetail = new ResPoolModel;
  public rType;
  public selectedResource: any
  public selectedOtherResource = new ResourcePerson();
  public selectedOtherResourcesList = new Array<ResourcePerson>()
  public PrimaryProgram
  public success
  public error
  public warn
  public selectedprogram = new PrimaryProgram();
  public resPerson = new Array<ResourcePerson>();
  public selectedResourcePersonList = new Array<ResourcePerson>();
  public fromPoolList = new Array<ResourcePerson>()
  path: any;

  // public uploader: FileUploader = new FileUploader({ url: API_URL+"ECD-TrainingPrograms-1.0/service/trainings/saveAdminStaffDoc", queueLimit: 1 });
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  @ViewChild("makeAdminStaff") public makeAdminStaff;
  @ViewChild("makeAdminStaffPt") public makeAdminStaffPt;
  @ViewChild("editAdminStaff") public editAdminStaff;
  @ViewChild("addParticipantView") public addParticipantView: ModalDirective;
  constructor(public commonService: CommonWebService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.validation()
    this.loadToGrid();
    this.getAllLocations()
    this.getUser()


  }

  validation() {

    this.programForm = new FormGroup({
      programName: new FormControl(['', Validators.required]),
      description: new FormControl(['', Validators.required]),
      startDate: new FormControl(['', Validators.email]),
      endDate: new FormControl('', [Validators.required]),
      location: new FormControl('', Validators.required),
      venue: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      type: new FormControl(),
      resource: new FormControl(),

    })

    this.name = new FormControl('', Validators.required),
      this.remark = new FormControl()

  }

  getUser() {
    let getUser = sessionStorage.getItem('id_token');
    this.userData = JSON.parse(atob(getUser));
    this.userId = this.userData[0].userId
  }
  openCreateAdmin() {
    this.getResources();
    this.rType = 1;
    this.programData = new ProgramData()
    this.programData.program.status = 1
    this.programData.program.startDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.programData.program.endDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    this.selectedResourcePersonList = new Array<ResourcePerson>();
    this.makeAdminStaff.show();
  }

  getProgramId(param) {
    // console.log("getProgramId",p);
    // this.adminstaffDetail = p;
    this.getResources();
    this.selectedResourcePersonList = new Array<ResourcePerson>()
    this.rType = 1
    this.getAllProgramById(param);
    this.editAdminStaff.show();

  }

  getProgramIdP(pid) {
    console.log("pid", pid);
    this.adminstaffPt.programId = pid;
    console.log("this.adminstaffDetail.programId", this.adminstaffDetail.programId);
    this.makeAdminStaffPt.show();
    this.getParticipant(pid);
  }

  getAllProgramById(param) {


    this.programData = new ProgramData()
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAdminStaffById?programId=" + param.programId;
    this.commonService.processGet(url).subscribe(res => {


      if (res.responseCode == 1) {

        // this.selectedprogram = res.responseData
        // this.resPerson = this.selectedprogram.resourcePool
        // for (let a of this.selectedprogram.resourcePool) {
        //   console.log("A", a)
        //   this.selectedResourcePersonList.push(a)
        // }
        // for (let a of this.selectedprogram.guestList) {
        //   console.log("B", a)
        //   this.selectedResourcePersonList.push(a)
        // }
        // console.log("response data ", this.adminstaffDetail)
        this.programData = res.responseData
        for (let a of this.programData.resourcePool) {
          let person = new ResourcePerson()
          person.personId = a.personId
          person.personName = a.personName
          person.remarks = a.remarks
          person.status = a.status
          person.coordinator = a.coordinator

          this.selectedResourcePersonList.push(person)

        }
      }
      for (let a of this.programData.guestList) {
        let person = new ResourcePerson()
        person.personId = a.personId
        person.personName = a.personName
        person.remarks = a.remarks
        person.status = a.status
        person.coordinator = a.coordinator

        this.selectedResourcePersonList.push(person)
      }

    }, error => {

      console.log("error ", error);

    })
  }

  updateAdminProgram() {


    this.selectedprogram.program.filePath = this.path;

    console.log("program to be  updated", this.programData)
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/updateAdminStaff", this.programData).subscribe(res => {


      if (res.responseCode == 1) {
        this.success = true
        this.loadToGrid();
        setTimeout(() => {

          this.success = false
        }, 3000)
      } else {
        this.error = true
        setTimeout(() => {

          this.error = false
        }, 3000)
      }

    }), error => {
      console.log("ERROR")
      this.error = true
      setTimeout(() => {

        this.error = false
      }, 3000)

    }
  }

  getParticipant(pid) {
    this.adminstaffPt.programId = pid;
    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAdminStaffParticipant?programId=" + this.adminstaffPt.programId;
    this.commonService.processGet(url).subscribe(res => {

      // console.log("response", res.getAllMobileUsers.responseData);

      // this.userModel = res.users.responseData;
      // this.mobileUserModel = res.getAllMobileUserByGroupAdmin.responseData;
      if (res.responseCode == 1) {

        this.adminstaffPtList = res.responseData;
        console.log("response data ", this.adminstaffDetail)
      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.adminstaffDetail)
    })
  }

  getPid(pid) {
    console.log("pid", pid);

  }

  loadToGrid() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAdminStaff";
    this.commonService.processGet(url).subscribe(res => {

      if (res.responseCode == 1) {

        this.adminstaffModel = res.responseData;

      }

    }, error => {

      console.log("error ", error);

    })
  }

  getResources() {

    let url = API_URL + "ECD-TrainingPrograms-1.0/service/trainings/getAllResources";
    this.commonService.processGet(url).subscribe(res => {
      if (res.responseCode == 1) {
        this.resModel = res.responseData;

        if (this.resModel.length != 0) {
          this.selectedResource = this.resModel[0]
        }

      }

    }, error => {

      console.log("error ", error);
      console.log("response dataz ", this.resModel)
    })
  }

  createAdminProgram() {

    this.programData.userInserted = this.userData[0].userId
    this.commonService.processPost(API_URL + "ECD-TrainingPrograms-1.0/service/trainings/saveAdminStaff", this.programData).subscribe(res => {
      if (res.responseCode == 1) {
        this.success = true;
        this.loadToGrid()
        setTimeout(() => {
          this.success = false
          this.makeAdminStaff.hide()
        }, 2000)
      } else {
        this.error = true
        setTimeout(() => {
          this.error = false
        }, 2000)
      }
    }), error => {
      console.log("ERROR")
      this.error = true
      setTimeout(() => {
        this.error = false
      }, 2000)
    }
  }

  addResourcePersons() {


    if (this.rType == 1 && this.selectedResource != null) {
      let person = new ResourcePerson()
      person.personId = this.selectedResource.id
      person.personName = this.selectedResource.resourceName
      person.coordinator = 0
      this.programData.resourcePool.push(person)
      this.selectedResourcePersonList.push(person)
    }
    else if (this.rType == 2 && this.selectedOtherResource != null) {

      let person = new ResourcePerson()

      person.personName = this.selectedOtherResource.personName
      person.remarks = this.selectedOtherResource.remarks
      person.coordinator = 0
      this.programData.guestList.push(person)
      this.selectedResourcePersonList.push(person)

      this.selectedResource.personName = ""
      this.selectedResource.remarks = ""

    }
  }


  getAllLocations() {

    this.loadingTree = true
    this.businessList = new Array<BusinessLevel>()
    this.files = new Array<Node>()
    this.commonService.processGet(API_URL + "ECD-DashboardMaintainance-1.0/service/dashboard/getBusinessStructure").subscribe(res => {
      if (res.responseCode == 1) {
        this.loadingTree = false
        this.businessList = res.responseData


        for (let i = 0; i < this.businessList.length; i++) {
          if (this.businessList[i].type == "root" && this.businessList[i].nodeType == 0) {
            let node = new Node()
            this.selectedNode.id = this.businessList[i].id
            this.selectedNode.label = this.businessList[i].english
            this.selectedNode.type = this.businessList[i].type
            this.selectedNode.code = this.businessList[i].code
            this.selectedNode.districtId = this.businessList[i].districtId
            this.selectedNode.dsID = this.businessList[i].dsID
            this.selectedNode.provinceId = this.businessList[i].provinceId
            this.selectedNode.leaf = false

          }


        }


        this.addChildren(this.selectedNode)

        this.files.push(this.selectedNode)
      }


    }), error => {
      console.log("ERROR")
    }
  }


  addChildren(node1) {


    for (let i = 0; i < this.businessList.length; i++) {

      if (this.businessList[i].type == "node" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()

        node1.children.push(node)






      }

      if (this.businessList[i].type == "province" && this.businessList[i].nodeType == node1.id) {
        let node = new Node()
        node.id = this.businessList[i].id
        node.label = this.businessList[i].english
        node.type = this.businessList[i].type
        node.code = this.businessList[i].code
        node.districtId = this.businessList[i].districtId
        node.dsID = this.businessList[i].dsID
        node.provinceId = this.businessList[i].provinceId
        // this.selectedNode.children[0] = new Node()
        node1.children.push(node)



      }


      //   console.log("Starting Noede", this.startingNode)
      //   this.files.push(this.selectedNode)



      if (node1.type == "province") {
        if (node1.code == this.businessList[i].provinceId && this.businessList[i].type == "district") {

          let node = new Node()
          node.id = this.businessList[i].id
          node.label = this.businessList[i].english
          node.type = this.businessList[i].type
          node.code = this.businessList[i].code
          node.districtId = this.businessList[i].districtId
          node.dsID = this.businessList[i].dsID
          node.provinceId = this.businessList[i].provinceId
          node1.children.push(node)


        }
      }
    }

    for (let j = 0; j < node1.children.length; j++) {

      this.addChildren(node1.children[j])
    }
  }


  nodeSelect(event) {//this.method is all

    let district = "" + event.node.provinceId + event.node.code;


    if (event.node.type == "province") {

      for (let j = 0; j < event.node.children.length; j++) {

        for (let i = 0; i < this.businessList.length; i++) {


          let district = "" + event.node.children[j].provinceId + event.node.children[j].code;

          if (district == this.businessList[i].districtId && this.businessList[i].type == "ds") {
            let node = new Node()
            node.id = this.businessList[i].id
            node.label = this.businessList[i].english
            node.type = this.businessList[i].type
            node.code = this.businessList[i].code
            node.districtId = this.businessList[i].districtId
            node.dsID = this.businessList[i].dsID
            node.provinceId = this.businessList[i].provinceId
            event.node.children[j].children.push(node)



          }
        }
      }

    }

  }

  nodeSelected(event) {
    this.programData.program.locationId = event.node.id
    this.programData.program.locationName = event.node.label
    this.collapseAll()
  }

  collapseAll() {
    this.files.forEach(node => {
      this.expandRecursive(node, false);
    });
  }
  private expandRecursive(node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  removeRows(person) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool.splice(index2, 1)
    }
    if (index3 > -1) {
      this.programData.guestList.splice(index3, 1)
    }



  }

  removeRowswhenEdit(person: ResourcePerson) {
    var index1 = this.selectedResourcePersonList.indexOf(person)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    this.selectedResourcePersonList.splice(index1, 1)
    if (index2 > -1) {
      this.programData.resourcePool[index2].status = 2
    }
    if (index3 > -1) {
      this.programData.guestList[index3].status = 2
    }



  }

  checked(e, person) {

    console.log("Per", person)
    console.log("sdsdsd", this.programData.resourcePool)
    var index2 = this.programData.resourcePool.indexOf(person)
    var index3 = this.programData.guestList.indexOf(person)
    console.log("index", index2)
    console.log("in", index3)
    if (index2 > -1) {
      this.programData.resourcePool[index2].coordinator = 1
    }
    if (index3 > -1) {

      this.programData.guestList[index3].coordinator = 1
    }
  }

  openAddParticipant(a) {
    this.programId = a
    console.log("A", this.programId)
   
    this.addParticipantView.show()

  }
}
